ALTER TABLE `reliefs` ADD `hw_name` TEXT NULL AFTER `g_name`;
ALTER TABLE `reliefs` ADD `uni_pre` INT(11) NOT NULL AFTER `upo_pre`, ADD `uni_per` INT(11) NOT NULL AFTER `uni_pre`;
ALTER TABLE `reliefs` CHANGE `uni_pre` `uni_pre` VARCHAR(255) NULL, CHANGE `uni_per` `uni_per` VARCHAR(255) NULL;
ALTER TABLE `reliefs` CHANGE `ssnp` `ssnp` TEXT NOT NULL;
ALTER TABLE `reliefs` CHANGE `card_no` `card_no` INT(11) NULL;


DROP VIEW IF EXISTS `sadar_vw`;
CREATE VIEW sadar_vw AS
SELECT
    `rlf`.`id` AS `id`,
    `rlf`.`card_no` AS `card_no`,
    `rlf`.`card_type` AS `card_type`,
    `rlf`.`name` AS `name`,
    `rlf`.`g_name` AS `g_name`,
    `rlf`.`hw_name` AS `hw_name`,
    `rlf`.`age` AS `age`,
    `rlf`.`dob` AS `dob`,
    `rlf`.`gender` AS `gender`,
    `rlf`.`dist_pre` AS `dist_pre`,
    `rlf`.`house_pre` AS `house_pre`,
    `rlf`.`road_pre` AS `road_pre`,
    `rlf`.`word_pre` AS `word_pre`,
    `rlf`.`upo_pre` AS `upo_pre`,
    `rlf`.`uni_pre` AS `uni_pre`,
    `rlf`.`holding_pre` AS `holding_pre`,
    `rlf`.`profession` AS `profession`,
    `rlf`.`phone` AS `phone`,
    `rlf`.`nid` AS `nid`,
    `rlf`.`ssnp` AS `ssnp`,
    `rlf`.`is_deleted` AS `is_deleted`,
    `rlf`.`total_member` AS `total_member`,
    `rlf`.`tot_f_member` AS `tot_f_member`,
    `rlf`.`tot_child` AS `tot_child`,
    `rlf`.`f_nid` AS `f_nid`,
    `rlf`.`f_ssnp` AS `f_ssnp`,
    `rlf`.`poss_help` AS `poss_help`
FROM
    `reliefs` `rlf`
WHERE
        `rlf`.`dist_pre` = 51
  AND `rlf`.`upo_pre` = 385
  AND `rlf`.`is_deleted` != 1;

DROP VIEW IF EXISTS `kasiyani_vw`;
CREATE VIEW kasiyani_vw AS
SELECT
    `rlf`.`id` AS `id`,
    `rlf`.`card_no` AS `card_no`,
    `rlf`.`card_type` AS `card_type`,
    `rlf`.`name` AS `name`,
    `rlf`.`g_name` AS `g_name`,
    `rlf`.`hw_name` AS `hw_name`,
    `rlf`.`age` AS `age`,
    `rlf`.`dob` AS `dob`,
    `rlf`.`gender` AS `gender`,
    `rlf`.`dist_pre` AS `dist_pre`,
    `rlf`.`house_pre` AS `house_pre`,
    `rlf`.`road_pre` AS `road_pre`,
    `rlf`.`word_pre` AS `word_pre`,
    `rlf`.`upo_pre` AS `upo_pre`,
    `rlf`.`uni_pre` AS `uni_pre`,
    `rlf`.`holding_pre` AS `holding_pre`,
    `rlf`.`profession` AS `profession`,
    `rlf`.`phone` AS `phone`,
    `rlf`.`nid` AS `nid`,
    `rlf`.`ssnp` AS `ssnp`,
    `rlf`.`is_deleted` AS `is_deleted`,
    `rlf`.`total_member` AS `total_member`,
    `rlf`.`tot_f_member` AS `tot_f_member`,
    `rlf`.`tot_child` AS `tot_child`,
    `rlf`.`f_nid` AS `f_nid`,
    `rlf`.`f_ssnp` AS `f_ssnp`,
    `rlf`.`poss_help` AS `poss_help`
FROM
    `reliefs` `rlf`
WHERE
        `rlf`.`dist_pre` = 51
  AND `rlf`.`upo_pre` = 386
  AND `rlf`.`is_deleted` != 1;


DROP VIEW IF EXISTS `tungipara_vw`;
CREATE VIEW tungipara_vw AS
SELECT
    `rlf`.`id` AS `id`,
    `rlf`.`card_no` AS `card_no`,
    `rlf`.`card_type` AS `card_type`,
    `rlf`.`name` AS `name`,
    `rlf`.`g_name` AS `g_name`,
    `rlf`.`hw_name` AS `hw_name`,
    `rlf`.`age` AS `age`,
    `rlf`.`dob` AS `dob`,
    `rlf`.`gender` AS `gender`,
    `rlf`.`dist_pre` AS `dist_pre`,
    `rlf`.`house_pre` AS `house_pre`,
    `rlf`.`road_pre` AS `road_pre`,
    `rlf`.`word_pre` AS `word_pre`,
    `rlf`.`upo_pre` AS `upo_pre`,
    `rlf`.`uni_pre` AS `uni_pre`,
    `rlf`.`holding_pre` AS `holding_pre`,
    `rlf`.`profession` AS `profession`,
    `rlf`.`phone` AS `phone`,
    `rlf`.`nid` AS `nid`,
    `rlf`.`ssnp` AS `ssnp`,
    `rlf`.`is_deleted` AS `is_deleted`,
    `rlf`.`total_member` AS `total_member`,
    `rlf`.`tot_f_member` AS `tot_f_member`,
    `rlf`.`tot_child` AS `tot_child`,
    `rlf`.`f_nid` AS `f_nid`,
    `rlf`.`f_ssnp` AS `f_ssnp`,
    `rlf`.`poss_help` AS `poss_help`
FROM
    `reliefs` `rlf`
WHERE
        `rlf`.`dist_pre` = 51
  AND `rlf`.`upo_pre` = 387
  AND `rlf`.`is_deleted` != 1;

DROP VIEW IF EXISTS `kotalipara_vw`;
CREATE VIEW kotalipara_vw AS
SELECT
    `rlf`.`id` AS `id`,
    `rlf`.`card_no` AS `card_no`,
    `rlf`.`card_type` AS `card_type`,
    `rlf`.`name` AS `name`,
    `rlf`.`g_name` AS `g_name`,
    `rlf`.`hw_name` AS `hw_name`,
    `rlf`.`age` AS `age`,
    `rlf`.`dob` AS `dob`,
    `rlf`.`gender` AS `gender`,
    `rlf`.`dist_pre` AS `dist_pre`,
    `rlf`.`house_pre` AS `house_pre`,
    `rlf`.`road_pre` AS `road_pre`,
    `rlf`.`word_pre` AS `word_pre`,
    `rlf`.`upo_pre` AS `upo_pre`,
    `rlf`.`uni_pre` AS `uni_pre`,
    `rlf`.`holding_pre` AS `holding_pre`,
    `rlf`.`profession` AS `profession`,
    `rlf`.`phone` AS `phone`,
    `rlf`.`nid` AS `nid`,
    `rlf`.`ssnp` AS `ssnp`,
    `rlf`.`is_deleted` AS `is_deleted`,
    `rlf`.`total_member` AS `total_member`,
    `rlf`.`tot_f_member` AS `tot_f_member`,
    `rlf`.`tot_child` AS `tot_child`,
    `rlf`.`f_nid` AS `f_nid`,
    `rlf`.`f_ssnp` AS `f_ssnp`,
    `rlf`.`poss_help` AS `poss_help`
FROM
    `reliefs` `rlf`
WHERE
        `rlf`.`dist_pre` = 51
  AND `rlf`.`upo_pre` = 388
  AND `rlf`.`is_deleted` != 1;

DROP VIEW IF EXISTS `muksudpur_vw`;
CREATE VIEW muksudpur_vw AS
SELECT
    `rlf`.`id` AS `id`,
    `rlf`.`card_no` AS `card_no`,
    `rlf`.`card_type` AS `card_type`,
    `rlf`.`name` AS `name`,
    `rlf`.`g_name` AS `g_name`,
    `rlf`.`hw_name` AS `hw_name`,
    `rlf`.`age` AS `age`,
    `rlf`.`dob` AS `dob`,
    `rlf`.`gender` AS `gender`,
    `rlf`.`dist_pre` AS `dist_pre`,
    `rlf`.`house_pre` AS `house_pre`,
    `rlf`.`road_pre` AS `road_pre`,
    `rlf`.`word_pre` AS `word_pre`,
    `rlf`.`upo_pre` AS `upo_pre`,
    `rlf`.`uni_pre` AS `uni_pre`,
    `rlf`.`holding_pre` AS `holding_pre`,
    `rlf`.`profession` AS `profession`,
    `rlf`.`phone` AS `phone`,
    `rlf`.`nid` AS `nid`,
    `rlf`.`ssnp` AS `ssnp`,
    `rlf`.`is_deleted` AS `is_deleted`,
    `rlf`.`total_member` AS `total_member`,
    `rlf`.`tot_f_member` AS `tot_f_member`,
    `rlf`.`tot_child` AS `tot_child`,
    `rlf`.`f_nid` AS `f_nid`,
    `rlf`.`f_ssnp` AS `f_ssnp`,
    `rlf`.`poss_help` AS `poss_help`
FROM
    `reliefs` `rlf`
WHERE
        `rlf`.`dist_pre` = 51
  AND `rlf`.`upo_pre` = 389
  AND `rlf`.`is_deleted` != 1;