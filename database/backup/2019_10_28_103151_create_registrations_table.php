<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegistrationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registrations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('training_id');
            $table->integer('dept_id');
            $table->string('name_bn');
            $table->string('name_en');
            $table->string('f_name_bn');
            $table->string('f_name_en');
            $table->string('m_name_bn');
            $table->string('m_name_en');

            $table->string('vill_pre');
            $table->string('post_pre');
            $table->string('up_pre');
            $table->string('dist_pre');

            $table->string('vill_per');
            $table->string('post_per');
            $table->string('up_per');
            $table->string('dist_per');

            $table->string('edu_lvl');
            $table->integer('ssc_yr');
            $table->integer('ssc_roll');
            $table->string('ssc_bord');
            $table->double('ssc_gpa',2,2);

            $table->string('phone');
            $table->string('email');
            $table->integer('nid');

            $table->dateTime('dob');
            $table->integer('age');

            $table->integer('relg');
            $table->string('nation');
            $table->integer('gender');
            $table->integer('married');

            $table->string('cur_job')->nullable();
            $table->string('prev_trng_name')->nullable();
            $table->string('prev_exp')->nullable();

            $table->integer('created_by')->default(0);
            $table->integer('updated_by')->default(0);
            $table->integer('is_deleted')->default(0);
            $table->timestamps();
            $table->collation = 'utf8_general_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registrations');
    }
}
