<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->increments('id');
            $table->string('property_name');
            $table->integer('country_id');
            $table->integer('division_id');
            $table->integer('district_id');
            $table->text('address');
            $table->integer('created_by');
            $table->integer('updated_by')->nullable();
            $table->integer('owner');
            $table->tinyInteger('status')->default(1)->comment('1 = Active, 2 = Inactive, 0 = Deleted');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('properties');
    }
}
