<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReliefsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reliefs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('card_no');
            $table->string('name');
            $table->string('g_name');
            $table->integer('age');
            $table->integer('gender');

            $table->string('house_pre')->nullable();
            $table->string('road_pre')->nullable();
            $table->string('word_pre')->nullable();
            $table->integer('upo_pre');
            $table->string('holding_pre')->nullable();
            $table->integer('dist_pre');

            $table->string('vill_per');
            $table->string('post_per');
            $table->string('word_per');
            $table->string('holding_per')->nullable();
            $table->integer('upo_per');
            $table->integer('dist_per');

            $table->integer('home_type')->default(0);
            $table->string('home_type_other')->nullable();

            $table->string('profession');
            $table->string('phone')->nullable();;
            $table->integer('nid')->nullable();;
            $table->string('picture')->nullable();

            $table->integer('ssnp');

            $table->integer('total_income')->default(0);
            $table->integer('total_spend')->default(0);

            $table->integer('created_by')->default(0);
            $table->integer('updated_by')->default(0);
            $table->integer('is_deleted')->default(0);
            $table->timestamps();
            $table->collation = 'utf8_general_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reliefs');
    }
}
