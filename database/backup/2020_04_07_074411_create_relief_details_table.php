<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReliefDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('relief_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('rlf_id');

            $table->string('name');
            $table->integer('age');
            $table->integer('nid');
            $table->string('profession');
            $table->integer('relation');

            $table->integer('ssnp');

            $table->integer('created_by')->default(0);
            $table->integer('updated_by')->default(0);
            $table->integer('is_deleted')->default(0);
            $table->timestamps();
            $table->collation = 'utf8_general_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('relief_details');
    }
}
