<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bills', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('apt_id');
            $table->integer('client_id');
            $table->integer('pending_ledger_id');
            $table->integer('ledger_id')->default(0)->comment("Value will be added if status is Approved and data inserted into the main Ledger Table");
            $table->integer('rent_amount');
            $table->integer('service_charge');
            $table->integer('security_bill')->default(0)->comment('Value will be added only during the initial time. Other than no value.');
            $table->integer('security_bill_month')->default(0)->comment('Value will be added only during the initial time. Other than no value.');
            $table->integer('total_bill');
            $table->integer('billing_month');
            $table->integer('billing_year');
            $table->integer('current_amount')->default(0)->comment('+(ve) value define Advance, -(ve) value define Due');
            $table->tinyInteger('bill_type')->comment('1 = Initial, 2 = Regular');
            $table->text('comments')->nullable();
            $table->integer('created_by')->default(0);
            $table->integer('updated_by')->default(0);
            $table->integer('approved_by')->default(0);
            $table->tinyInteger('status')->default(1)->comment('1 = Pending, 2 = Approved, 3 = Rejected');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bills');
    }
}
