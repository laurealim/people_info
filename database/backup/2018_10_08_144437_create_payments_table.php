<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('apt_id');
            $table->integer('client_id');
            $table->integer('total_bill_payment');
            $table->integer('pending_ledger_id');
            $table->integer('payment_month');
            $table->integer('payment_year');
            $table->integer('ledger_id')->default(0)->comment("Value will be added if status is Approved and data inserted into the main Ledger Table");
            $table->tinyInteger('payment_type')->comment('1 = Initial, 2 = Regular, 3 = Adjustment, 4 = Advance');
            $table->text('comments')->nullable();
            $table->integer('created_by')->default(0);
            $table->integer('updated_by')->default(0);
            $table->integer('approved_by')->default(0);
            $table->tinyInteger('status')->default(1)->comment('1 = Pending, 2 = Approved, 3 = Rejected');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
