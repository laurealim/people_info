<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApartmentRentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apartment_rents', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('apt_id');
            $table->integer('rent_amount');
            $table->integer('service_charge');
            $table->integer('adv_month')->default(0)->nullable(); // Advance Charge for how many month.
            $table->integer('adv_rent')->default(0)->nullable(); // Advance Rent amount per month
            $table->tinyInteger('electricity')->default(0)->comment('0 = Exclude, 1 = Include');
            $table->tinyInteger('gas')->default(0)->comment('0 = Exclude, 1 = Include');
            $table->tinyInteger('water')->default(0)->comment('0 = Exclude, 1 = Include');
            $table->tinyInteger('status')->default(1)->comment('1 = Initial, 2 = Regular, 0 = Deleted');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('apartment_rents');
    }
}
