@extends('layouts.login')

@section('title')
    User Login
@stop
@section('content')
    <div class="col-sm-10 col-sm-offset-1">
        <div class="login-container">
            <div class="center">
                <h1>
{{--                    <i class="ace-icon fa fa-leaf green"></i>--}}
                    <span class="red">Office User</span>
                    <br>
                    <span class="gray" id="id-text2">Login Panel</span>
                </h1>
{{--                <h4 class="blue" id="id-company-text">&copy; Company Name</h4>--}}
            </div>

            <div class="space-6"></div>

            <div class="position-relative">
                <div id="login-box" class="login-box visible widget-box no-border">
                    <div class="widget-body">
                        <div class="widget-main">
                            <h4 class="header blue lighter bigger">
                                <i class="ace-icon fa fa-key blue"></i>
                                Please Enter Your Information
                            </h4>

                            <div class="space-6"></div>

                            <form id="loginForm" class="form-horizontal" method="POST" action="{{ route('user.login') }}">
                                {{ csrf_field() }}
                                <fieldset>
                                    <label class="block clearfix">
                                        <span class="block input-icon input-icon-right{{ $errors->has('email') ? ' has-error' : '' }}">
                                            <input id="email_login" type="email" class="form-control" name="email"
                                                   value="{{ old('email') }}" placeholder="Email" required autofocus>

                                            @if ($errors->has('email'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                            <i class="ace-icon fa fa-envelope"></i>
                                        </span>
                                    </label>

                                    <label class="block clearfix">
                                        <span class="block input-icon input-icon-right{{ $errors->has('password') ? ' has-error' : '' }}">
                                            <input id="password_login" type="password" class="form-control" name="password" placeholder="Password"
                                                   required>
                                            @if ($errors->has('password'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                            @endif

                                            <i class="ace-icon fa fa-lock"></i>
                                        </span>
                                    </label>

                                    {{--<label class="block clearfix">--}}
                                        {{--<span class="block input-icon input-icon-right{{ $errors->has('user_type') ? ' has-error' : '' }}">--}}
                                            {{--<select id="user_type" class="form-control" name="user_type" required>--}}
                                                {{--<option value=>------ Please Select User Type ------</option>--}}
                                                {{--<option value="2">Owner</option>--}}
                                                {{--<option value="3">Tenant</option>--}}
                                            {{--</select>--}}
                                            {{--<input id="password_login" type="password" class="form-control" name="user_type" placeholder="Password" required>--}}
                                            {{--@if ($errors->has('user_type'))--}}
                                                {{--<span class="help-block">--}}
                                                    {{--<strong>{{ $errors->first('user_type') }}</strong>--}}
                                                {{--</span>--}}
                                            {{--@endif--}}

                                            {{--<i class="ace-icon fa fa-user"></i>--}}
                                        {{--</span>--}}
                                    {{--</label>--}}

                                    <div class="space"></div>

                                    <div class="clearfix">
                                        <label class="inline">
                                            <input type="checkbox" class="ace"/>
                                            <span class="lbl"> Remember Me</span>
                                        </label>

                                        <button type="button" onclick="document.getElementById('loginForm').submit()" class="width-35 pull-right btn btn-sm btn-primary">
                                            {{--<button type="submit" id="login_button_id" class="width-35 pull-right btn btn-sm btn-primary">--}}
                                            <i class="ace-icon fa fa-key"></i>
                                            <span class="bigger-110">Login</span>
                                        </button>
                                    </div>

                                    <div class="space-4"></div>
                                </fieldset>
                            </form>

{{--                            <div class="social-or-login center">--}}
{{--                                <span class="bigger-110">Or Login Using</span>--}}
{{--                            </div>--}}

{{--                            <div class="space-6"></div>--}}

{{--                            <div class="social-login center">--}}
{{--                                <a class="btn btn-primary">--}}
{{--                                    <i class="ace-icon fa fa-facebook"></i>--}}
{{--                                </a>--}}

{{--                                <a class="btn btn-info">--}}
{{--                                    <i class="ace-icon fa fa-twitter"></i>--}}
{{--                                </a>--}}

{{--                                <a class="btn btn-danger">--}}
{{--                                    <i class="ace-icon fa fa-google-plus"></i>--}}
{{--                                </a>--}}
{{--                            </div>--}}
                        </div>
                        <!-- /.widget-main -->

                        <div class="toolbar clearfix">
                            <div>
{{--                                <a href="#" data-target="#forgot-box" class="forgot-password-link">--}}
{{--                                    <i class="ace-icon fa fa-arrow-left"></i>--}}
{{--                                    I forgot my password--}}
{{--                                </a>--}}
                            </div>

                            <div>
{{--                                <a href="#" data-target="#signup-box" class="user-signup-link">--}}
{{--                                    I want to register--}}
{{--                                    <i class="ace-icon fa fa-arrow-right"></i>--}}
{{--                                </a>--}}
                            </div>
                        </div>
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.login-box -->

                <div id="forgot-box" class="forgot-box widget-box no-border">
                    <div class="widget-body">
                        <div class="widget-main">
                            <h4 class="header red lighter bigger">
                                <i class="ace-icon fa fa-key"></i>
                                Retrieve Password
                            </h4>

                            <div class="space-6"></div>
                            <p>
                                Enter your email and to receive instructions
                            </p>
                            @if (session('status'))
                                <div class="alert alert-success">
                                    {{ session('status') }}
                                </div>
                            @endif
                            <form class="form-horizontal" method="POST" action="{{ route('user.password.email') }}">
                                {{ csrf_field() }}
                                <fieldset>
                                    <label class="block clearfix">
                                        <span class="block input-icon input-icon-right{{ $errors->has('email') ? ' has-error' : '' }}">
                                            <input id="email_forget" type="email" class="form-control" name="email"
                                                   value="{{ old('email') }}" placeholder="Email" required>

                                            @if ($errors->has('email'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                            <i class="ace-icon fa fa-envelope"></i>
                                        </span>
                                    </label>

                                    <div class="clearfix">
                                        <button type="submit" class="width-35 pull-right btn btn-sm btn-danger">
                                            <i class="ace-icon fa fa-lightbulb-o"></i>
                                            <span class="bigger-110">Send Me!</span>
                                        </button>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                        <!-- /.widget-main -->

                        <div class="toolbar center">
                            <a href="#" data-target="#login-box" class="back-to-login-link">
                                Back to login
                                <i class="ace-icon fa fa-arrow-right"></i>
                            </a>
                        </div>
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.forgot-box -->

                <div id="signup-box" class="signup-box widget-box no-border">
                    <div class="widget-body">
                        <div class="widget-main">
                            <h4 class="header green lighter bigger">
                                <i class="ace-icon fa fa-users blue"></i>
                                New User Registration
                            </h4>

                            <div class="space-6"></div>
                            <p> Enter your details to begin: </p>


                            <form class="form-horizontal" method="POST" action="{{ route('user.submit.register') }}">
                                {{ csrf_field() }}
                                <fieldset>
                                    <label class="block clearfix">
                                        <span class="block input-icon input-icon-right{{ $errors->has('email') ? ' has-error' : '' }}">
                                            <input id="email_registration" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email" required>

                                            @if ($errors->has('email'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                            <i class="ace-icon fa fa-envelope"></i>
                                        </span>
                                    </label>

                                    {{--<label class="block clearfix">--}}
                                        {{--<span class="block input-icon input-icon-right{{ $errors->has('user_type') ? ' has-error' : '' }}">--}}
                                            {{--<select id="user_type" class="form-control" name="user_type" required>--}}
                                                {{--<option value=>------ Please Select User Type ------</option>--}}
                                                {{--<option value="2">Owner</option>--}}
                                                {{--<option value="3">Tenant</option>--}}
                                            {{--</select>--}}
                                            {{--<input id="password_login" type="password" class="form-control" name="user_type" placeholder="Password" required>--}}
                                            {{--@if ($errors->has('user_type'))--}}
                                                {{--<span class="help-block">--}}
                                                    {{--<strong>{{ $errors->first('user_type') }}</strong>--}}
                                                {{--</span>--}}
                                            {{--@endif--}}

                                            {{--<i class="ace-icon fa fa-user"></i>--}}
                                        {{--</span>--}}
                                    {{--</label>--}}

                                    <label class="block clearfix">
                                        <span class="block input-icon input-icon-right{{ $errors->has('first_name') ? ' has-error' : '' }}">
                                            <input id="first_name_registration" type="text" class="form-control" name="first_name" value="{{ old('first_name') }}" placeholder="First Name" required autofocus>

                                            @if ($errors->has('first_name'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('first_name') }}</strong>
                                                </span>
                                            @endif
                                            <i class="ace-icon fa fa-user"></i>
                                        </span>
                                    </label>

                                    <label class="block clearfix">
                                        <span class="block input-icon input-icon-right{{ $errors->has('last_name') ? ' has-error' : '' }}">
                                            <input id="last_name_registration" type="text" class="form-control" name="last_name" value="{{ old('last_name') }}" placeholder="Last Name" >

                                            {{--@if ($errors->has('last_name'))--}}
                                            {{--<span class="help-block">--}}
                                            {{--<strong>{{ $errors->first('last_name') }}</strong>--}}
                                            {{--</span>--}}
                                            {{--@endif--}}
                                            <i class="ace-icon fa fa-user"></i>
                                        </span>
                                    </label>

                                    <label class="block clearfix">
                                        <span class="block input-icon input-icon-right{{ $errors->has('password') ? ' has-error' : '' }}">
                                            <input id="password_registration" type="password" class="form-control" name="password" placeholder="Password" required>

                                            @if ($errors->has('password'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                            @endif
                                            <i class="ace-icon fa fa-lock"></i>
                                        </span>
                                    </label>

                                    <label class="block clearfix">
                                        <span class="block input-icon input-icon-right">
                                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirm password" required>
                                            <i class="ace-icon fa fa-retweet"></i>
                                        </span>
                                    </label>

                                    {{--<label class="block">--}}
                                    {{--<input type="checkbox" class="ace"/>--}}
                                    {{--<span class="lbl">--}}
                                    {{--I accept the--}}
                                    {{--<a href="#">User Agreement</a>--}}
                                    {{--</span>--}}
                                    {{--</label>--}}

                                    <div class="space-24"></div>

                                    <div class="clearfix">
                                        <button type="reset" class="width-30 pull-left btn btn-sm">
                                            <i class="ace-icon fa fa-refresh"></i>
                                            <span class="bigger-110">Reset</span>
                                        </button>

                                        <button type="submit" class="width-65 pull-right btn btn-sm btn-success">
                                            <span class="bigger-110">Register</span>

                                            <i class="ace-icon fa fa-arrow-right icon-on-right"></i>
                                        </button>
                                    </div>
                                </fieldset>
                            </form>
                        </div>

                        <div class="toolbar center">
                            <a href="#" data-target="#login-box" class="back-to-login-link">
                                <i class="ace-icon fa fa-arrow-left"></i>
                                Back to login
                            </a>
                        </div>
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.signup-box -->
            </div>
            <!-- /.position-relative -->

            {{--<div class="navbar-fixed-top align-right">--}}
            {{--<br/>--}}
            {{--&nbsp;--}}
            {{--<a id="btn-login-dark" href="#">Dark</a>--}}
            {{--&nbsp;--}}
            {{--<span class="blue">/</span>--}}
            {{--&nbsp;--}}
            {{--<a id="btn-login-blur" href="#">Blur</a>--}}
            {{--&nbsp;--}}
            {{--<span class="blue">/</span>--}}
            {{--&nbsp;--}}
            {{--<a id="btn-login-light" href="#">Light</a>--}}
            {{--&nbsp; &nbsp; &nbsp;--}}
            {{--</div>--}}
        </div>
    </div><!-- /.col -->
@stop

@section('custom_style')

@stop

@section('custom_script')
    <script type="text/javascript">
        var hasEmail = '<?php echo $errors->has('email') ?>';
        var loginError = '<?php echo $errors->has('login_error') ?>';
        {{--var hasEmail = '<? echo $errors->has('email') ?>';--}}
        jQuery(function ($) {

            $(document).on('click', '.toolbar a[data-target]', function (e) {
                e.preventDefault();
                var target = $(this).data('target');
                $('.widget-box.visible').removeClass('visible');//hide others
                $(target).addClass('visible');//show target
            });
        });


        //you don't need this, just used for changing background
        jQuery(function ($) {
            $('#btn-login-dark').on('click', function (e) {
                $('body').attr('class', 'login-layout');
                $('#id-text2').attr('class', 'white');
                $('#id-company-text').attr('class', 'blue');

                e.preventDefault();
            });
            $('#btn-login-light').on('click', function (e) {
                $('body').attr('class', 'login-layout light-login');
                $('#id-text2').attr('class', 'grey');
                $('#id-company-text').attr('class', 'blue');

                e.preventDefault();
            });
            $('#btn-login-blur').on('click', function (e) {
                $('body').attr('class', 'login-layout blur-login');
                $('#id-text2').attr('class', 'white');
                $('#id-company-text').attr('class', 'light-blue');

                e.preventDefault();
            });

        });

        //        $('#login_button_id').on('click', function(){
        //            alert("click me");
        //            $('#loginForm').submit();
        //        });

        if (loginError) {
//            var target = $('#forgot-box').data('target');
            $('.widget-box.visible').removeClass('visible');//hide others
            $('#forgot-box').find('.help-block').remove();
            $('#login-box').addClass('visible');//show target
        }
        else if (hasEmail) {
//            var target = $('#forgot-box').data('target');
            $('.widget-box.visible').removeClass('visible');//hide others
            $('#login-box').find('.help-block').remove();
            $('#forgot-box').addClass('visible');//show target
        }


    </script>
@stop