@extends('layouts.admin')

@section('title')
    Admin Profile
@stop

@section('breadcrumb')
    <ul class="breadcrumb">
        <li>
            <i class="ace-icon fa fa-home home-icon"></i>
            <a href="{{ route('admin.dashboard') }}">Dashboard</a>
        </li>

        <li>
            <a href="{{ route('admin.profile.view') }}">Profile</a>
        </li>
        {{--<li class="active">User Profile</li>--}}
    </ul>
@stop

@section('page_header')
    <h1>Admin Profile</h1>
@stop

@section('content')
    <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->
        <div class="clearfix">
            {{--<div class="pull-left alert alert-success no-margin alert-dismissable">--}}
            {{--<button type="button" class="close" data-dismiss="alert">--}}
            {{--<i class="ace-icon fa fa-times"></i>--}}
            {{--</button>--}}

            {{--<i class="ace-icon fa fa-umbrella bigger-120 blue"></i>--}}
            {{--Click on the image below or on profile fields to edit them ...--}}
            {{--</div>--}}

            <div class="pull-right">
                {{--<span class="green middle bolder">Choose profile: &nbsp;</span>--}}

                {{--<div class="btn-toolbar inline middle no-margin">--}}
                {{--<div data-toggle="buttons" class="btn-group no-margin">--}}
                {{--<label class="btn btn-sm btn-yellow active">--}}
                {{--<span class="bigger-110">1</span>--}}

                {{--<input type="radio" value="1"/>--}}
                {{--</label>--}}

                {{--<label class="btn btn-sm btn-yellow">--}}
                {{--<span class="bigger-110">2</span>--}}

                {{--<input type="radio" value="2"/>--}}
                {{--</label>--}}

                {{--<label class="btn btn-sm btn-yellow">--}}
                {{--<span class="bigger-110">3</span>--}}

                {{--<input type="radio" value="3"/>--}}
                {{--</label>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
            </div>

            {{--<div class="hr dotted"></div>--}}

            <div>
                <div id="user-profile-3" class="user-profile row">
                    <div class="col-sm-offset-1 col-sm-10">
                        <div class="well well-sm">
                            <!-- -
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        &nbsp; -->
                            <div class="inline middle blue bigger-110"> Your profile is <span class="updatedPercentage">25</span>% complete</div>

                            &nbsp; &nbsp; &nbsp;
                            <div style="width:80%;" data-percent="25%"
                                 class="inline middle no-margin progress progress-striped active pos-rel updatedPercentage">
                                <div class="progress-bar progress-bar-success" style="width: 25px%"></div>
                            </div>
                        </div>
                        <!-- /.well -->

                        <div class="space"></div>

                        <form class="form-horizontal" method="post" action="{{ route('admin.profile.update') }}"
                              enctype="multipart/form-data">
                            {{--                            {!! Form::open(array('route' => 'admin.profile.update','files'=>true)) !!}--}}
                            {{ csrf_field() }}
                            <div class="tabbable">
                                <ul class="nav nav-tabs padding-16">
                                    <li class="active">
                                        <a data-toggle="tab" href="#edit-basic">
                                            <i class="green ace-icon fa fa-pencil-square-o bigger-125"></i>
                                            Basic Info
                                        </a>
                                    </li>

                                    {{--<li>--}}
                                    {{--<a data-toggle="tab" href="#edit-settings">--}}
                                    {{--<i class="purple ace-icon fa fa-cog bigger-125"></i>--}}
                                    {{--Settings--}}
                                    {{--</a>--}}
                                    {{--</li>--}}

                                    <li>
                                        <a data-toggle="tab" href="#edit-password">
                                            <i class="blue ace-icon fa fa-key bigger-125"></i>
                                            Password
                                        </a>
                                    </li>
                                </ul>

                                <div class="tab-content profile-edit-tab-content">
                                    <div id="edit-basic" class="tab-pane in active">
                                        <h4 class="header blue bolder smaller">General</h4>

                                        <div class="row">
                                            <div class="col-xs-12 col-sm-4">
                                                <span class="profile-picture" width="300px" height="200px">
                                                    <input type="file" name="profileImg" class=" progressChange"/>
                                                    @if($adminInfo->image != '')
                                                        <img id="profileAvt" width="300px" height="200px"
                                                             class="editable img-responsive"
                                                             alt="Alex's Avatar"
                                                             src="{{ asset('images/avatars/'.$adminInfo->image) }}"/>
                                                    @else
                                                    @endif
												</span>
                                            </div>

                                            {{--<div class="col-xs-12 col-sm-4">--}}
                                            {{--<div class="form-group">--}}
                                            {{--<input type="file" id="inputFile4" multiple="">--}}
                                            {{--<div class="input-group">--}}
                                            {{--<input type="text" readonly="" class="form-control" placeholder="Placeholder w/file chooser...">--}}
                                            {{--<span class="input-group-btn input-group-sm">--}}
                                            {{--<button type="button" class="btn btn-fab btn-fab-mini">--}}
                                            {{--<i class="material-icons">attach_file</i>--}}
                                            {{--</button>--}}
                                            {{--</span>--}}
                                            {{--</div>--}}
                                            {{--</div>--}}
                                            {{--</div>--}}

                                            <div class="vspace-12-sm"></div>

                                            <div class="col-xs-12 col-sm-8">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label no-padding-right"
                                                           for="form-field-username">First Name *</label>

                                                    <div class="col-sm-8">
                                                        <input class="col-xs-12 col-sm-10 progressChange" type="text"
                                                               name="first_name"
                                                               id="form-field-firstName" aria-required="true"
                                                               placeholder="First Name"
                                                               value="{{ $adminInfo->first_name }}"/>
                                                    </div>
                                                    {{--<div class="col-sm-4">--}}
                                                    {{--</div>--}}
                                                    {{--@if ($errors->has('first_name'))--}}
                                                    {{--<span class="alerts alert-danger">--}}
                                                    {{--<strong>{{ $errors->first('first_name') }}</strong>--}}
                                                    {{--</span>--}}
                                                    {{--@endif--}}
                                                </div>

                                                <div class="space-4"></div>

                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label no-padding-right"
                                                           for="form-field-first">Last Name</label>

                                                    <div class="col-sm-8">
                                                        <input class="col-xs-12 col-sm-10 progressChange" type="text"
                                                               id="form-field-lastName" name="last_name"
                                                               placeholder="Last Name"
                                                               value="{{ $adminInfo->last_name }}"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <hr/>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right"
                                                   for="form-field-date">Birth
                                                Date</label>

                                            <div class="col-sm-9">
                                                <div class="input-medium">
                                                    <div class="input-group">
                                                        <input class="input-medium date-picker progressChange"
                                                               id="form-field-date"
                                                               type="text" data-date-format="yyyy-mm-dd" name="dob"
                                                               placeholder="yyyy-mm-dd" value="{{ $adminInfo->dob }}"/>
																			<span class="input-group-addon">
																				<i class="ace-icon fa fa-calendar"></i>
																			</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="space-4"></div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right">Gender</label>

                                            {{--{{ dd($adminInfo->gender) }}--}}
                                            <div class="col-sm-9">
                                                <label class="inline">
                                                    <input type="radio" class="ace progressChange" id="male"
                                                           name="gender"
                                                           value="1" {{ $adminInfo->gender == 1 ? 'checked' : ''}} />
                                                    <span class="lbl middle"> Male</span>
                                                </label>

                                                &nbsp; &nbsp; &nbsp;
                                                <label class="inline">
                                                    <input type="radio" class="ace progressChange" id="female"
                                                           name="gender"
                                                           value="2" {{ $adminInfo->gender == 2 ? 'checked' : '' }} />
                                                    <span class="lbl middle"> Female</span>
                                                </label>
                                            </div>
                                        </div>

                                        <div class="space"></div>
                                        <h4 class="header blue bolder smaller">Contact</h4>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right"
                                                   for="form-field-email">Email</label>

                                            <div class="col-sm-9">
                                                <span class="input-icon input-icon-right">
                                                    <input type="email" name="email" id="form-field-email"
                                                           class="progressChange"
                                                           value="{{ $adminInfo->email }}" readonly/>
                                                    <i class="ace-icon fa fa-envelope"></i>
                                                </span>
                                            </div>
                                        </div>

                                        <div class="space-4"></div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right"
                                                   for="form-field-website">Website</label>

                                            <div class="col-sm-9">
                                                <span class="input-icon input-icon-right">
                                                    <input type="text" name="website" id="form-field-website"
                                                           class="progressChange"
                                                           value="{{ $adminInfo->website }}"
                                                           placeholder="http:\\www.testsite.com"/>
                                                    <i class="ace-icon fa fa-globe"></i>
                                                </span>
                                            </div>
                                        </div>

                                        <div class="space-4"></div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right"
                                                   for="form-field-phone">Phone</label>

                                            <div class="col-sm-9">
                                                <span class="input-icon input-icon-right">
                                                    <input class="input-medium input-mask-phone progressChange"
                                                           name="phone"
                                                           value="{{ $adminInfo->phone }}" type="text"
                                                           id="form-field-phone"/>
                                                    <i class="ace-icon fa fa-phone fa-flip-horizontal"></i>
                                                </span>
                                            </div>
                                        </div>

                                        <div class="space"></div>
                                        <h4 class="header blue bolder smaller">Social</h4>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right"
                                                   for="form-field-facebook">Facebook</label>

                                            <div class="col-sm-9">
                                                <span class="input-icon">
                                                    <input type="text" value="facebook_alexdoe"
                                                           id="form-field-facebook"/>
                                                    <i class="ace-icon fa fa-facebook blue"></i>
                                                </span>
                                            </div>
                                        </div>

                                        <div class="space-4"></div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right"
                                                   for="form-field-twitter">Twitter</label>

                                            <div class="col-sm-9">
																	<span class="input-icon">
																		<input type="text" value="twitter_alexdoe"
                                                                               id="form-field-twitter"/>
																		<i class="ace-icon fa fa-twitter light-blue"></i>
																	</span>
                                            </div>
                                        </div>

                                        <div class="space-4"></div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right"
                                                   for="form-field-gplus">Google+</label>

                                            <div class="col-sm-9">
																	<span class="input-icon">
																		<input type="text" value="google_alexdoe"
                                                                               id="form-field-gplus"/>
																		<i class="ace-icon fa fa-google-plus red"></i>
																	</span>
                                            </div>
                                        </div>
                                    </div>

                                    {{--<div id="edit-settings" class="tab-pane">--}}
                                    {{--<div class="space-10"></div>--}}

                                    {{--<div>--}}
                                    {{--<label class="inline">--}}
                                    {{--<input type="checkbox" name="form-field-checkbox" class="ace"/>--}}
                                    {{--<span class="lbl"> Make my profile public</span>--}}
                                    {{--</label>--}}
                                    {{--</div>--}}

                                    {{--<div class="space-8"></div>--}}

                                    {{--<div>--}}
                                    {{--<label class="inline">--}}
                                    {{--<input type="checkbox" name="form-field-checkbox" class="ace"/>--}}
                                    {{--<span class="lbl"> Email me new updates</span>--}}
                                    {{--</label>--}}
                                    {{--</div>--}}

                                    {{--<div class="space-8"></div>--}}

                                    {{--<div>--}}
                                    {{--<label>--}}
                                    {{--<input type="checkbox" name="form-field-checkbox" class="ace"/>--}}
                                    {{--<span class="lbl"> Keep a history of my conversations</span>--}}
                                    {{--</label>--}}

                                    {{--<label>--}}
                                    {{--<span class="space-2 block"></span>--}}

                                    {{--for--}}
                                    {{--<input type="text" class="input-mini" maxlength="3"/>--}}
                                    {{--days--}}
                                    {{--</label>--}}
                                    {{--</div>--}}
                                    {{--</div>--}}

                                    <div id="edit-password" class="tab-pane">
                                        <div class="space-10"></div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right"
                                                   for="form-field-pass1">Current Password *</label>

                                            <div class="col-sm-9">
                                                <input type="password" id="form-field-pass1" name="crnt_password"/>
                                            </div>
                                            @if ($errors->has('password'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        <div class="space-4"></div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right"
                                                   for="form-field-pass2">New Password *</label>

                                            <div class="col-sm-9">
                                                <input type="password" id="form-field-pass2" name="password"/>
                                            </div>
                                            @if ($errors->has('password'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        <div class="space-4"></div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right"
                                                   for="form-field-pass3">Confirm Password *</label>

                                            <div class="col-sm-9">
                                                <input type="password" id="form-field-pass3"
                                                       name="password_confirmation"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix form-actions">
                                <div class="col-md-offset-3 col-md-9">
                                    <button class="btn btn-info" type="submit">
                                        <i class="ace-icon fa fa-check bigger-110"></i>
                                        Save
                                    </button>

                                    &nbsp; &nbsp;
                                    <button class="btn" type="reset">
                                        <i class="ace-icon fa fa-undo bigger-110"></i>
                                        Reset
                                    </button>
                                </div>
                            </div>
                            {{--                        {!! Form::close() !!}--}}
                        </form>
                    </div>
                    <!-- /.span -->
                </div>
                <!-- /.user-profile -->
            </div>
        </div>

        <!-- PAGE CONTENT ENDS -->
    </div>
    <!-- /.col -->
@stop

@section('custom_style')

@stop

@section('custom_script')
    <script type="text/javascript">
        jQuery(function ($) {
            progressStatus();
            $('#user-profile-3').find('input[type=file]').ace_file_input({
                style: 'well',
                btn_choose: 'Select avatar',
                btn_change: null,
                no_icon: 'ace-icon fa fa-picture-o',
                thumbnail: 'large',
                droppable: true,
                width: 500,

                allowExt: ['jpg', 'jpeg', 'png', 'gif'],
                allowMime: ['image/jpg', 'image/jpeg', 'image/png', 'image/gif']
            }).end().find('button[type=reset]').on(ace.click_event, function () {
                $('#user-profile-3 input[type=file]').ace_file_input('reset_input');
            }).end().find('.date-picker').datepicker().next().on(ace.click_event, function () {
                $(this).prev().focus();
            })
            $('.input-mask-phone').mask('(999) 9999999999');


            $(document).one('ajaxloadstart.page', function (e) {
                //in ajax mode, remove remaining elements before leaving page
                try {
                    $('.editable').editable('destroy');
                } catch (e) {
                }
                $('[class*=select2]').remove();
            });

            $('.progressChange').change(function(){
                progressStatus();
            });

        });

        function progressStatus(){
            var total = -1;
            var count = 1;
            $('.progressChange').each(function(){
                total++;
                if($(this).is(':text')){
                    var valT = $(this).val();
                    if(valT){
                        count++;
                    }
                }
                else if($(this).is(':radio')){
                    if($(this).is(':checked')){
                        var valR = $(this).val();
                        count++;
                    }
                }
                else if($(this).is(':password')){
                    var valP = $(this).val();
                    if(valP){
                        count++;
                    }
                }
                else if($(this).is(':file')){
                    var image = "<?php echo $adminInfo->image; ?>";
//                    var valP = $(this).val();
                    if(image){
                        count++;
                    }
                }

            });
            var percentage = parseInt((100 * count)/ total);
            $('span.updatedPercentage').html(percentage);
            $('div.progress-bar').css('width', percentage+'%');
            $('div.updatedPercentage').attr('data-percent', percentage+'%');

        }
    </script>
@stop