@extends('layouts.admin')

@section('title')
    Duplicate Delete List
@stop

@section('breadcrumb')
    <ul class="breadcrumb">
        <li>
            <i class="ace-icon fa fa-bars home-icon"></i>
            <a href="{{ route('registration.adminDuplicateCheck') }}">Duplicate Check</a>
        </li>

        {{--<li class="active">User Profile</li>--}}
    </ul>
@stop

@section('page_header')
    <h1>Duplicate Delete List</h1>
@stop

@section('content')
    <div class="col-xs-12">
        {{--<h3 class="header smaller lighter blue">jQuery dataTables</h3>--}}

        <div class="clearfix">
            <div class="pull-right tableTools-container"></div>

        </div>
        <div>
            <div id="dynamic-table_wrapper" class="dataTables_wrapper form-inline no-footer">
                <div class="row">
                    <table id="dynamic-table" class="table table-striped table-bordered table-hover"
                           style="margin-bottom: 0px;">
                        <thead>
                        <tr>
                            <th class="center" width="3%">
                                Sr. No.
                            </th>
                            <th width="6%">পরিবার প্রধানের নাম</th>
                            <th width="6%">পিতা/স্বামীর নাম</th>
                            <th width="5%">বয়স</th>
                            <th width="5%">কার্ডের ধরণ</th>
                            <th width="5%">কার্ড নং</th>
                            <th width="5%">জেলা</th>
                            <th width="5%">উপজেলা</th>
                            <th width="5%">ইউনিয়ন / পৌরসভা</th>
                            <th width="5%">ওয়ার্ড</th>
                            <th width="5%">বর্তমান পেশা</th>
                            <th width="6%">মোবাইল নাম্বার</th>
                            <th width="7%">জন্ম নিবন্ধন / ভোটার আইডি নাম্বার</th>
                            <th width="5%">ভাতার নাম</th>
                            <th width="5%">ত্রাণ সহায়তা</th>
                            <th width="5%">সম্ভাব্য সহায়তা</th>
                            <th width="5%">মাসিক আয়</th>
                            <th width="5%">মাসিক ব্যায়</th>
                            <?php if(auth()->user()->id != 532) { ?>
                            <th class="center" width="10%">Action</th>
                            <?php } ?>
                        </tr>
                        </thead>

                        <tbody>

                        <?php
                        $pageNo = 1;
                        //pr($reliefList); ?>
                        @foreach ($reliefList as $relief)
                            <?php if($relief['is_deleted'] == 1) { ?>
                            <tr style="background-color:#f8b1a2">

                            <?php }else{?>
                            <tr>
                                <?php } ?>
                                <td class="center">
                                    <label class="pos-rel">
                                        {{--<input type="checkbox" class="ace"/>--}}
                                        <span class="lbl">{{ $pageNo++ }}</span>
                                    </label>
                                </td>
                                <td>
                                    {{ $relief->name }}
                                </td>
                                <td>
                                    <?php if(!empty($relief->g_name)) { ?>
                                    {{ $relief->g_name }}
                                    <?php } else{ ?>
                                    {{ $relief->hw_name }}
                                    <?php }
                                    ?>
                                </td>
                                <td>
                                    {{ $relief->age }}
                                </td>
                                <td>
                                {{ config('constants.card_type.arr.'.$relief->card_type) }}
                                <!--                --><?php
                                    //                    if($relief->gender == 1){
                                    //                        echo "পুরুষ";
                                    //                    }elseif($relief->gender == 2){
                                    //                        echo "মহিলা";
                                    //                    }else{
                                    //                        echo "অন্যান্ন";
                                    //                    }
                                    //                ?>
                                </td>
                                <td>
                                    <?php
                                    //                $cardId = config('constants.location_code.dist.' . $relief['dist_pre']);
                                    //                $cardId .= config('constants.location_code.upo.' . $relief['upo_pre']);
                                    //                $uni_pre = strlen(config('constants.location_code.uni.' . $relief['uni_pre']));
                                    //                if ($uni_pre > 1) {
                                    //                    $cardId .= config('constants.location_code.uni.' . $relief['uni_pre']);
                                    //                } else {
                                    //                    $cardId .= '0';
                                    //                    $cardId .= config('constants.location_code.uni.' . $relief['uni_pre']);
                                    //
                                    //                }
                                    //                $cardId .= $relief['word_pre'];
                                    //                $length = strlen($relief['id']);
                                    //                for ($count = 6; $count > $length; $count--) {
                                    //                    $cardId .= '0';
                                    //                }
                                    //                $cardId .= $relief['id'];
                                    //                echo $cardId;

                                    $cardNo = $relief['card_no'];
                                    echo $cardNo;
                                    ?>
                                </td>
                                <td>
                                    {{ $relief->dis_name }}
                                </td>
                                <td>
                                    {{ $relief->upa_name }}
                                </td>
                                <td>
                                    <?php
                                    if ($relief->uni_pre < 1) {
                                        echo 'পৌরসভা';
                                    } else {
                                        echo $relief->uni_name;
                                    }
                                    ?>
                                    {{--                {{ $relief->uni_pre }}--}}
                                </td>
                                <td>
                                    {{ $relief->word_pre }}
                                </td>
                                <td>
                                    {{ $relief->profession  }}
                                </td>
                                <td>
                                    {{ $relief->phone }}
                                </td>
                                <td>
                                    {{ $relief->nid }}
                                </td>
                                <td>
                                    <?php
                                    if (empty($relief->ssnp_name)) {
                                        echo "তথ্য নেই";
                                    } else {
                                        echo $relief->ssnp_name;
                                    }
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    if (!empty($relief->packeges)) {

                                        $packageArr = array_filter(explode(',', $relief->packeges));
                                        $dateArr = array_filter(explode(',', $relief->delv_date));
                                        foreach ($packageArr as $key => $value) {
                                            echo $packageList[$value] . ' (' . $dateArr[$key] . '),';
                                        }
                                    } else {
                                        echo "---";
                                    }
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    echo config('constants.possible_help.' . $relief->poss_help)
                                    ?>

                                </td>
                                <td>
                                    {{ $relief->total_income }}
                                </td>
                                <td class="center">
                                    {{ $relief->total_spend }}
                                </td>
                                {{--            <td class="center">--}}
                                {{--                <a class="blue" style="font-weight: bold" href="{{ route('registration.adminEdit',$relief->id) }}" title="Edit">--}}
                                {{--                    View Details--}}
                                {{--                </a>--}}
                                {{--            </td>--}}
                                {{--            <td class="center">--}}
                                {{--                <img src="{{ asset('storage/trainee/'.$relief->id.'/'.$relief->image) }}" class="msg-photo"--}}
                                {{--                     alt="Kate's Avatar" width="120px" height="80px"/>--}}
                                {{--            </td>--}}
                                {{--            <td>--}}
                                {{--                {{ config('constants.edu_level.arr.'.$relief->edu_lvl) }}--}}
                                {{--            </td>--}}
                                {{--            <td class="center">--}}
                                {{--                {{ $relief->user->first_name}}--}}
                                {{--            </td>--}}
                                    <?php if(auth()->user()->id != 532) { ?>
                                <td class="center">
                                    <div class="hidden-sm hidden-xs action-buttons">
                                        {{--<a class="blue" href="{{ route('') }}">--}}
                                        {{--<i class="ace-icon fa fa-search-plus bigger-200"></i>--}}
                                        {{--</a>--}}

                                        {{--&nbsp;&nbsp;| &nbsp;&nbsp;--}}
                                        <a class="green" href="{{ route('registration.adminEdit',$relief->id) }}"
                                           title="Edit">
                                            <i class="ace-icon fa fa-pencil bigger-150"></i>
                                        </a>

                                        &nbsp;| &nbsp;

                                        <a class="red deletebtn"
                                           href="{{ route('registration.adminDestroyDuplicate',$relief->id) }}"
                                           id="{{ $relief->id }}" title="Delete">
                                            <i class="ace-icon fa fa-trash-o bigger-150"></i>
                                        </a>
                                    </div>
                                </td>
                                <?php } ?>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop

@section('custom_style')

@stop

@section('custom_script')
    <script type="text/javascript">
        $(window).on('hashchange', function () {
            if (window.location.hash) {
                var page = window.location.hash.replace('#', '');
                if (page == Number.NaN || page <= 0) {
                    return false;
                } else {
                    getPosts(page);
                }
            }
        });

        $(document).ready(function () {
            $(document).on('click', '.pagination a', function (e) {
                getPosts($(this).attr('href').split('page=')[1], $(this).attr('href'));
                e.preventDefault();
            });
        });

        function getPosts(page, url) {
            console.log(url);
//            url: '?page=' + page
            var displayValue = $("#displayValue").val();
            var searchData = $("#searchData").val();
            $.ajax({
                url: '?page=' + page + '&displayValue=' + displayValue + '&searchData=' + searchData,
                success: function (data) {
                    $('.posts').html(data);
                },
                error: function (err) {
                    alert('Posts could not be loaded.');
                }
            });
        }
        ;


        //        $(".deletebtn").click(function (ev) {
        $(document).on("click", "a.deletebtn", function (ev) {
            ev.preventDefault();
            let url = $(this).attr("href");
            let id = $(this).attr("id");
            if (confirm("Do you wat to delete?")) {
                $.ajax({
                    type: 'DELETE',
                    url: url,
                    dataType: 'json',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    data: {id: id, "_token": "{{ csrf_token() }}"},

                    success: function (data) {
                        if (data.status == 'success') {
                            window.location.reload();
                        } else if (data.status == 'error') {
                        }
//                    data.request->session()->flash('status', 'Task was successful!');
//                    setInterval(function() {
//                    }, 5900);
                    },
                    error: function (data) {
                    }
                });

            } else {
                return false;
            }
        });


        $("#displayValue").on('change', function () {
            var displayValue = $(this).val();
            var url = window.location.href;
            dynamicDataViewCount(displayValue, '', url);
        });

        $("#searchData").keyup(function (e) {
            var searchData = $(this).val();
            var displayValue = $("#displayValue").val();
            /*
             48-57 - (0-9)Numbers
             65-90 - (A-Z)
             97-122 - (a-z)
             8 - (backspace)
             32 - (space)
             https://www.w3schools.com/charsets/ref_html_ascii.asp
             */

            var keyCode = e.which;
            if (keyCode == 46 || keyCode == 8 || (keyCode < 48 || keyCode > 57) || (keyCode < 65 || keyCode > 90) || (keyCode < 97 || keyCode > 122)) {
                var url = window.location.href;
                dynamicDataViewCount(displayValue, searchData, url);
            }
        });

        function dynamicDataViewCount(displayValue, searchData, url) {
            if (displayValue == '') {
                displayValue = $("#displayValue").val();
            }
            if (searchData == '') {
                searchData = $("#searchData").val();
            }
            $.ajax({
                url: url,
                dataType: 'html',
                type: 'GET',
                data: {displayValue: displayValue, searchData: searchData},
                success: function (data) {
                    $('.posts').html(data);
                },
                error: function (err) {
                    console.log(err);
                    alert('Posts could not be loaded.');
                }
            });
        }

        $(".checkAl").click(function () {
            $('input:checkbox').not(this).prop('checked', this.checked);
        });

        function submitform() {
            document.myform.submit();
        }

        $('.date-picker').datepicker({
            autoclose: true,
            todayHighlight: true
        });

        // $(".assign_client").on('click', function (e) {
        $(document).on("click", "a.assign_client", function (ev) {
            ev.preventDefault();
            var regId = $(this).attr("id");
            console.log(regId);

            $.when(
                $.ajax(
                    {
                        type: 'POST',
                        url: "{{ route('package.adminPackageListByRegId') }}",
                        dataType: 'json',
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        data: {regId: regId, "_token": "{{ csrf_token() }}"},
                        success: function (data) {
                            // console.log(data);
                            // alert(key);
                            $('select[name="package_lists"]').empty();
                            $.each(data, function (key, value) {
                                $('select[name="package_lists"]').append('<option value="' + key + '">' + value + '</option>');
                            });
//                             if (data.status == 'success') {
//                                 window.location.reload();
//                             } else if (data.status == 'error') {
// //                                        $(this).dialog("close");
//                             }
                        },
                        error: function (data) {
//                                    $(this).dialog("close");
                        }
                    }
                )).done(function () {
                var dialog = $("#dialog-message").removeClass('hide').dialog({
                    modal: true,
                    title: "Assign a Package",
                    title_html: true,
                    minWidth: 500,
                    position: {my: "center", at: "center", of: "#main"},
                    buttons: [
                        {
                            text: "OK",
                            "class": "btn btn-primary btn-minier",
                            click: function () {
                                let url = "{{ route('package.adminAssignPackage') }}";
                                let packageId = $("#package_lists").val();
                                let last_give_pac = $("#last_give_pac").val();
                                $.ajax({
                                    type: 'POST',
                                    url: url,
                                    dataType: 'json',
                                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                                    data: {
                                        regId: regId,
                                        packageId: packageId,
                                        last_give_pac: last_give_pac,
                                        "_token": "{{ csrf_token() }}"
                                    },
                                    success: function (data) {
                                        console.log(data);
                                        if (data.status == '1') {
                                            alert(data.message);
                                            window.location.reload();
                                        } else if (data.status == 'error') {
//                                        $(this).dialog("close");
                                        }
                                    },
                                    error: function (data) {
//                                    $(this).dialog("close");
                                    }
                                });
                                $(this).dialog("close");
                            }
                        },
                        {
                            text: "Cancel",
                            "class": "btn btn-minier",
                            click: function () {
                                $(this).dialog("close");
                            }
                        }
                    ]
                });
            });
        });

    </script>

@stop


@yield('popup_script')