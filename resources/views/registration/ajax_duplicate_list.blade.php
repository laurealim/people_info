<?php
if($isDisplay == 2) {
?>
<div class="row">
    <div class="col-sm-12 col-md-12">
        <h3><span><?php echo "Total Number of Data is = " . $count; ?></span></h3>
    </div>
</div>
<table id="dynamic-table" class="table table-striped table-bordered table-hover" style="margin-bottom: 0px;">
    <thead>
    <tr>
        <th class="center" width="10%">
            Sr. No.
        </th>
        <th width="22%">ID</th>
        <th width="23%">Mobile No.</th>
        <th width="23%">NID / Birth Certificate</th>
        <th width="22%">Total Count</th>
    </tr>
    </thead>

    <tbody>

    <?php //pr($registrationList); ?>
    <?php $pageNo = 1;?>
    <!--    --><?php //$pageNo = (($registrationList->currentPage() - 1) * $registrationList->perPage()) + 1;?>
    @foreach ($reliefData as $relief)
        <?php //dd($isDisplay);?>
        <tr>
            <td class="center">
                <label class="pos-rel">
                    {{--<input type="checkbox" class="ace"/>--}}
                    <span class="lbl">{{ $pageNo++ }}</span>
                </label>
            </td>
            <td>
                {{ $relief->id }}
            </td>
            <td>
                <?php
                if($groupBy === 'phone'){
                    if(empty($relief->phone)){
                        if(is_null($relief->phone)){ ?>
                            <a class="green" href="{{ route('registration.adminDuplicateList',array("emp",$groupBy)) }}" title="Edit">
                                Null
                            </a>
                        <?php }
                        else{ ?>
                            <a class="green" href="{{ route('registration.adminDuplicateList',array(0,$groupBy)) }}" title="Edit">
                                0
                            </a>
                        <?php }
                        ?>
                    <?php }
                    else{ ?>
                        <a class="green" href="{{ route('registration.adminDuplicateList',array($relief->phone,$groupBy)) }}" title="Edit">
                            {{ $relief->phone }}
                        </a>
                    <?php }
                }
                else{ ?>
                    {{ $relief->phone }}
                <?php }
                ?>
            </td>
            <td>
                <?php
                if($groupBy === 'nid'){
                    if(empty($relief->nid)){
                        if(is_null($relief->nid)){ ?>
                            <a class="green" href="{{ route('registration.adminDuplicateList',array("emp",$groupBy)) }}" title="Edit">
                                Null
                            </a>
                        <?php } else{ ?>
                            <a class="green" href="{{ route('registration.adminDuplicateList',array(0,$groupBy)) }}" title="Edit">
                                0
                            </a>
                    <?php }
                    ?>
                    <?php }
                    else{ ?>
                        <a class="green" href="{{ route('registration.adminDuplicateList',array($relief->nid,$groupBy)) }}" title="Edit">
                            {{ $relief->nid }}
                        </a>
                    <?php }
                }
                else { ?>
                    {{ $relief->nid }}
                <?php }
                ?>
            </td>
            <td>
                {{ $relief->cnt }}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
<?php } ?>

<style type="text/css">
    /*.table tbody > tr > td {*/
    /*text-align: center;*/
    /*}*/
    .facility {
        font-weight: bold;
    }
</style>