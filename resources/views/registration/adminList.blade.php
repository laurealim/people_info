@extends('layouts.admin')

@section('title')
    Registration List
@stop

@section('breadcrumb')
    <ul class="breadcrumb">
        <li>
            <i class="ace-icon fa fa-bars home-icon"></i>
            <a href="{{ route('registration.adminList') }}">Registration List</a>
        </li>

        {{--<li class="active">User Profile</li>--}}
    </ul>
@stop

@section('page_header')
    <h1>Registration List</h1>
@stop

@section('content')
    <div class="col-xs-12">
        {{--<h3 class="header smaller lighter blue">jQuery dataTables</h3>--}}

        <div class="clearfix">
            <div class="pull-right tableTools-container"></div>
            <div style="padding-bottom: 10px;" class="col-xs-6">
                <a href="{{ route('registration.adminForm') }}">
                    <button class="btn btn-primary">Add New Registration</button>
                </a>
                {{--                <a href="{{ route('registration.adminExportRegister') }}">--}}
                {{--                    <button class="btn btn-purple">--}}
                {{--                        <i class="fa fa-download bigger-120" aria-hidden="true"></i>--}}
                {{--                        Export Registration List--}}
                {{--                    </button>--}}
                {{--                </a>--}}

                {{--                <a href="{{ route('registration.adminPrintCard') }}">--}}
                {{--                <a href="javascript: submitform()">--}}
                {{--                    <button class="btn btn-grey" id="prnt_crd">--}}
                {{--                        <i class="fa fa-print bigger-120" aria-hidden="true"></i>--}}
                {{--                        Print Card--}}
                {{--                    </button>--}}
                {{--                </a>--}}
            </div>
        </div>
        <div>
            <form class="form-horizontal" role="form" action="{{ route('registration.adminPrintCard') }}" method="POST"
                  enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="hidden" name="card_data">

            </form>
            <div id="dynamic-table_wrapper" class="dataTables_wrapper form-inline no-footer">
                <div class="row">
                    <div class="col-xs-6">
                        <div class="dataTables_length" id="dynamic-table_length">
                            <label>Display
                                <select id="displayValue" name="dynamic-table_length" aria-controls="dynamic-table"
                                        class="form-control input-sm">
                                    <option value="15">15</option>
                                    <option value="25">25</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                </select>
                                records</label>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div id="dynamic-table_filter" class="dataTables_filter">
                            <label>Search:
                                <input type="text" class="form-control input-sm" placeholder=""
                                       aria-controls="dynamic-table" name="searchData" id="searchData"
                                       value="">
                            </label>

                            {{--<div>--}}
                            {{--<i class="normal-icon ace-icon fa fa-user pink bigger-150"></i><span>&nbsp;= Admin.</span>&nbsp;&nbsp;--}}
                            {{--<i class="normal-icon ace-icon fa fa-user blue bigger-150"></i><span>&nbsp;= Head.</span>&nbsp;&nbsp;--}}
                            {{--<i class="normal-icon ace-icon fa fa-user green bigger-150"></i><span>&nbsp;= User.</span>--}}
                            {{--</div>--}}
                        </div>
                    </div>
                </div>
                <div class="posts">
                    @include('registration.ajax_list')
                </div>


            </div>
        </div>
    </div>
@stop

@section('custom_style')

@stop

@section('custom_script')
    <script type="text/javascript">
        $(window).on('hashchange', function () {
            if (window.location.hash) {
                var page = window.location.hash.replace('#', '');
                if (page == Number.NaN || page <= 0) {
                    return false;
                } else {
                    getPosts(page);
                }
            }
        });

        $(document).ready(function () {
            $(document).on('click', '.pagination a', function (e) {
                getPosts($(this).attr('href').split('page=')[1], $(this).attr('href'));
                e.preventDefault();
            });
        });

        function getPosts(page, url) {
            console.log(url);
//            url: '?page=' + page
            var displayValue = $("#displayValue").val();
            var searchData = $("#searchData").val();
            $.ajax({
                url: '?page=' + page + '&displayValue=' + displayValue + '&searchData=' + searchData,
                success: function (data) {
                    $('.posts').html(data);
                },
                error: function (err) {
                    alert('Posts could not be loaded.');
                }
            });
        }
        ;


        //        $(".deletebtn").click(function (ev) {
        $(document).on("click", "a.deletebtn", function (ev) {
            ev.preventDefault();
            let url = $(this).attr("href");
            let id = $(this).attr("id");
            if (confirm("Do you wat to delete?")) {
                $.ajax({
                    type: 'DELETE',
                    url: url,
                    dataType: 'json',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    data: {id: id, "_token": "{{ csrf_token() }}"},

                    success: function (data) {
                        if (data.status == 'success') {
                            window.location.reload();
                        } else if (data.status == 'error') {
                        }
//                    data.request->session()->flash('status', 'Task was successful!');
//                    setInterval(function() {
//                    }, 5900);
                    },
                    error: function (data) {
                    }
                });

            } else {
                return false;
            }
        });


        $("#displayValue").on('change', function () {
            var displayValue = $(this).val();
            var url = window.location.href;
            dynamicDataViewCount(displayValue, '', url);
        });

        $("#searchData").keyup(function (e) {
            var searchData = $(this).val();
            var displayValue = $("#displayValue").val();
            /*
             48-57 - (0-9)Numbers
             65-90 - (A-Z)
             97-122 - (a-z)
             8 - (backspace)
             32 - (space)
             https://www.w3schools.com/charsets/ref_html_ascii.asp
             */

            var keyCode = e.which;
            if (keyCode == 46 || keyCode == 8 || (keyCode < 48 || keyCode > 57) || (keyCode < 65 || keyCode > 90) || (keyCode < 97 || keyCode > 122)) {
                var url = window.location.href;
                dynamicDataViewCount(displayValue, searchData, url);
            }
        });

        function dynamicDataViewCount(displayValue, searchData, url) {
            if (displayValue == '') {
                displayValue = $("#displayValue").val();
            }
            if (searchData == '') {
                searchData = $("#searchData").val();
            }
            $.ajax({
                url: url,
                dataType: 'html',
                type: 'GET',
                data: {displayValue: displayValue, searchData: searchData},
                success: function (data) {
                    $('.posts').html(data);
                },
                error: function (err) {
                    console.log(err);
                    alert('Posts could not be loaded.');
                }
            });
        }

        $(document).on("click", "a.change_reg_status", function (ev) {
            ev.preventDefault();
            var metaData = $('#uri').val();
            let url = $(this).attr("href");
            let id = $(this).attr("id");
            let status = $(this).attr("status");
            let confMess = "";
            if (status == 1) {
                confMess = "Do you wat to keep this trainee waiting?";
            }
            if (status == 2) {

                confMess = "Do you wat to Approve this trainee ?";
            }
            if (confirm(confMess)) {
                $.ajax({
                    type: 'DELETE',
                    url: url,
                    dataType: 'json',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    data: {id: id, status: status, "_token": "{{ csrf_token() }}"},

                    success: function (data) {
                        if (data.status == 'success') {
                            // window.location.reload();
                            window.location.href = metaData;
                        } else if (data.status == 'error') {
                        }
//                    data.request->session()->flash('status', 'Task was successful!');
//                    setInterval(function() {
//                    }, 5900);
                    },
                    error: function (data) {
                    }
                });

            } else {
                return false;
            }
        });

        $(".checkAl").click(function () {
            $('input:checkbox').not(this).prop('checked', this.checked);
        });

        function submitform() {
            document.myform.submit();
        }

        $('.date-picker').datepicker({
            autoclose: true,
            todayHighlight: true
        });

        // $(".student_check").on("click", function (ev) {}
        $(document).on("click", "a.student_check", function (ev) {
            ev.preventDefault();
            var f_card_id = $(this).attr("id");
            // var name = $("'#"+regId+"'").val();
            // console.log(regId);
            // console.log(name);
            // var url = "http://highschoolbd.com/finapi.php?apiToken=82dD6bH13c17AC70&famIdNo=3532002004054";
            var url = "http://highschoolbd.com/finapi.php?apiToken=82dD6bH13c17AC70&famIdNo="+f_card_id;
            // alert("Working");
            $.when(
                $.ajax(
                    {
                        type: 'GET',
                        url: url,
                        dataType: 'json',
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        success: function (data) {
                            console.log(data);
                            var newData = data.records;
                            // console.log(newData);

                            var trble_body = "";
                            $.map(newData, function (e) {
                                trble_body = trble_body + '<tr>' +
                                    '<td><span id="s_name">' + e.studentName + '</span></td>' +
                                    '<td><span id="s_school">' + e.schoolName + '</span></td>' +
                                    '<td><span id="s_class">' + e.class + '</span></td>' +
                                    '<td><span id="s_roll">' + e.classRoll + '</span></td>' +
                                    '<td><span id="s_group">' + e.group + '</span></td>' +
                                    '<td><span id="s_gender">' + e.gender + '</span></td>' +
                                    '<td><span id="s_section">' + e.section + '</span></td>' +
                                    '<td><span id="s_shift">' + e.shift + '</span></td>' +
                                    '</tr>';
                                console.log(e);
                                // $("#s_name").html(e.studentName);
                                // $("#s_school").html(e.schoolName);
                                // $("#s_class").html(e.class);
                                // $("#s_roll").html(e.classRoll);
                                // $("#s_group").html(e.group);
                                // $("#s_gender").html(e.gender);
                                // $("#s_section").html(e.section);
                                // $("#s_shift").html(e.shift);
                            });
                            $("#student_info").html(trble_body);
                        },
                        error: function (data) {
                            alert("কোন স্কুলগামী সন্তান পাওয়া যায় নি এই পরিবারে।");
                            console.log(data);
                        }
                    }
                )).done(function () {
                var dialog = $("#dialog-message-education").removeClass('hide').dialog({
                    modal: true,
                    title: "ছাত্র/ছাত্রীর তথ্য",
                    title_html: true,
                    minWidth: 800,
                    position: {my: "center", at: "center", of: "#main"},
                });
            });
        });

        // $(".assign_client").on('click', function (e) {
        $(document).on("click", "a.assign_client", function (ev) {
            ev.preventDefault();
            var regId = $(this).attr("id");
            console.log(regId);

            $.when(
                $.ajax(
                    {
                        type: 'POST',
                        url: "{{ route('package.adminPackageListByRegId') }}",
                        dataType: 'json',
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        data: {regId: regId, "_token": "{{ csrf_token() }}"},
                        success: function (data) {
                            console.log(data);
                            // alert(key);
                            $('select[name="package_lists"]').empty();
                            $.each(data.package_list, function (key, value) {
                                $('select[name="package_lists"]').append('<option value="' + key + '">' + value + '</option>');
                            });

                            var assignData = "";
                            // $.each(data.assign_package, function (a_key, a_value) {
                            // if (a_value != 1)
                            // assignData += "<input type='checkbox' name='" + a_value + "' checked=checked> " + data.full_package_list[a_value] + ' ( ' + data.delv_date[a_key] + ' ) ' + "</br>"
                            // });
                            // $(".assign_list").append("<div>" + assignData + "</div>");
//                             if (data.status == 'success') {
//                                 window.location.reload();
//                             } else if (data.status == 'error') {
// //                                        $(this).dialog("close");
//                             }
                        },
                        error: function (data) {
//                                    $(this).dialog("close");
                        }
                    }
                )).done(function () {
                var dialog = $("#dialog-message").removeClass('hide').dialog({
                    modal: true,
                    title: "Assign a Package",
                    title_html: true,
                    minWidth: 500,
                    position: {my: "center", at: "center", of: "#main"},
                    buttons: [
                        {
                            text: "OK",
                            "class": "btn btn-primary btn-minier",
                            click: function () {
                                let url = "{{ route('package.adminAssignPackage') }}";
                                let packageId = $("#package_lists").val();
                                let last_give_pac = $("#last_give_pac").val();
                                $.ajax({
                                    type: 'POST',
                                    url: url,
                                    dataType: 'json',
                                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                                    data: {
                                        regId: regId,
                                        packageId: packageId,
                                        last_give_pac: last_give_pac,
                                        "_token": "{{ csrf_token() }}"
                                    },
                                    success: function (data) {
                                        console.log(data);
                                        if (data.status == '1') {
                                            alert(data.message);
                                            window.location.reload();
                                        } else if (data.status == 'error') {
//                                        $(this).dialog("close");
                                        }
                                    },
                                    error: function (data) {
//                                    $(this).dialog("close");
                                    }
                                });
                                $(this).dialog("close");
                            }
                        },
                        {
                            text: "Cancel",
                            "class": "btn btn-minier",
                            click: function () {
                                $(this).dialog("close");
                            }
                        }
                    ]
                });
            });
        });

    </script>

@stop


@yield('popup_script')