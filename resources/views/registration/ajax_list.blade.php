{{--<script src="{{ asset('js/template/jquery-1.11.3.min.js') }}"></script>--}}
<table id="dynamic-table" class="table table-striped table-bordered table-hover" style="margin-bottom: 0px;">
    <thead>
    <tr>
        <th class="center" width="3%">
            Sr. No.
        </th>
        <th width="7%">ডাটা এন্ট্রি তারিখ</th>
        <th width="7%">ডাটা আপডেটের তারিখ</th>
        <th width="6%">পরিবার প্রধানের নাম</th>
        <th width="5%">পিতা/স্বামীর নাম</th>
        <th width="4%">বয়স</th>
        <th width="5%">কার্ডের ধরণ</th>
        <th width="5%">কার্ড নং</th>
        <th width="5%">মোট প্রিন্ট হয়েছে</th>
        <th width="5%">জেলা</th>
        <th width="5%">উপজেলা</th>
        <th width="5%">ইউনিয়ন / পৌরসভা</th>
        <th width="4%">ওয়ার্ড</th>
        <th width="5%">বর্তমান পেশা</th>
        <th width="5%">মোবাইল নাম্বার</th>
        <th width="5%">জন্ম নিবন্ধন / ভোটার আইডি নাম্বার</th>
        <th width="4%">মোট সদস্য</th>
        <th width="5%">অন্যান্য ভাতা</th>
        <th width="5%">মাসিক আয়</th>
        <th width="5%">মাসিক ব্যায়</th>
        <th class="center" width="10%">Action</th>
    </tr>
    </thead>

    <tbody>

    <?php //pr($reliefList); ?>
    <input type="hidden" id="uri" value="<?php echo $uri; ?>"/>
    <?php $pageNo = (($reliefList->currentPage() - 1) * $reliefList->perPage()) + 1;?>
    @foreach ($reliefList as $relief)
        <?php if($relief['is_deleted'] == 1) { ?>
        <tr style="background-color:#f8b1a2">

        <?php }else{?>
        <tr>
            <?php } ?>
            <td class="center">
                <label class="pos-rel">
                    {{--<input type="checkbox" class="ace"/>--}}
                    <span class="lbl">{{ $pageNo++ }}</span>
                </label>
            </td>
            <td class="center">
                {{ date('Y-m-d', strtotime($relief->created_at)) }}
            </td>
            <td class="center">
                {{ date('Y-m-d', strtotime($relief->updated_at)) }}
            </td>
            <td>
                {{ $relief->name }}
            </td>
            <td>
                <?php if(!empty($relief->g_name)) { ?>
                {{ $relief->g_name }}
                <?php } else{ ?>
                {{ $relief->hw_name }}
                <?php }
                ?>
            </td>
            <td>
                {{ $relief->age }}
            </td>
            <td>
            {{ config('constants.card_type.arr.'.$relief->card_type) }}
            <!--                --><?php
                //                    if($relief->gender == 1){
                //                        echo "পুরুষ";
                //                    }elseif($relief->gender == 2){
                //                        echo "মহিলা";
                //                    }else{
                //                        echo "অন্যান্ন";
                //                    }
                //                ?>
            </td>
            <td>
                <?php
                //                $cardId = config('constants.location_code.dist.' . $relief['dist_pre']);
                //                $cardId .= config('constants.location_code.upo.' . $relief['upo_pre']);
                //                $uni_pre = strlen(config('constants.location_code.uni.' . $relief['uni_pre']));
                //                if ($uni_pre > 1) {
                //                    $cardId .= config('constants.location_code.uni.' . $relief['uni_pre']);
                //                } else {
                //                    $cardId .= '0';
                //                    $cardId .= config('constants.location_code.uni.' . $relief['uni_pre']);
                //
                //                }
                //                $cardId .= $relief['word_pre'];
                //                $length = strlen($relief['id']);
                //                for ($count = 6; $count > $length; $count--) {
                //                    $cardId .= '0';
                //                }
                //                $cardId .= $relief['id'];
                //                echo $cardId;

                $cardNo = $relief['card_no'];
                echo $cardNo;


                //                $cardNo = '353200';
                //                $cardNo .= $relief['word_pre'];
                //                $length = strlen($relief['id']);
                //                for ($count = 6; $count > $length; $count--) {
                //                    $cardNo .= '0';
                //                }
                ?>
            </td>
            <td>
                {{ $relief->print_hit }}
            </td>
            <td>
                {{ $relief->dis_name }}
            </td>
            <td>
                {{ $relief->upa_name }}
            </td>
            <td>
                <?php
                if ($relief->uni_pre < 1) {
                    echo 'পৌরসভা';
                } else {
                    echo $relief->uni_name;
                }
                ?>
                {{--                {{ $relief->uni_pre }}--}}
            </td>
            <td>
                {{ $relief->word_pre }}
            </td>
            <td>
                {{ $relief->profession  }}
            </td>
            <td>
                {{ $relief->phone }}
            </td>
            <td>
                {{ $relief->nid }}
            </td>
            <td>
                {{ $relief->total_member }}
                <br/><a href="#" class="student_check" id="{{ $relief['card_no'] }}">(স্কুলগামী সদস্য)</a>
            </td>
            <td class="center">
                <?php
                if (empty($relief->ssnp_name)) {
                    echo "তথ্য নেই";
                } else {
                    echo $relief->ssnp_name;
                }
                ?>
            </td>
            <td>
                {{ $relief->total_income }}
            </td>
            <td class="center">
                {{ $relief->total_spend }}
            </td>
            {{--            <td class="center">--}}
            {{--                <a class="blue" style="font-weight: bold" href="{{ route('registration.adminEdit',$relief->id) }}" title="Edit">--}}
            {{--                    View Details--}}
            {{--                </a>--}}
            {{--            </td>--}}
            {{--            <td class="center">--}}
            {{--                <img src="{{ asset('storage/trainee/'.$relief->id.'/'.$relief->image) }}" class="msg-photo"--}}
            {{--                     alt="Kate's Avatar" width="120px" height="80px"/>--}}
            {{--            </td>--}}
            {{--            <td>--}}
            {{--                {{ config('constants.edu_level.arr.'.$relief->edu_lvl) }}--}}
            {{--            </td>--}}
            {{--            <td class="center">--}}
            {{--                {{ $relief->user->first_name}}--}}
            {{--            </td>--}}
            <td class="center">
                <div class="hidden-sm hidden-xs action-buttons">
                    {{--<a class="blue" href="{{ route('') }}">--}}
                    {{--<i class="ace-icon fa fa-search-plus bigger-200"></i>--}}
                    {{--</a>--}}

                    {{--&nbsp;&nbsp;| &nbsp;&nbsp;--}}

                    <?php
                    if($relief['is_deleted'] != 1) { ?>
                    <?php if(auth()->user()->id != 532 && auth()->user()->id != 534) { ?>
                    <a class="green" href="{{ route('registration.adminEdit',$relief->id) }}" title="Edit">
                        <i class="ace-icon fa fa-pencil bigger-150"></i>
                    </a>

                    &nbsp;| &nbsp;

                    <a class="red deletebtn" href="{{ route('registration.adminDestroy',$relief->id) }}"
                       id="{{ $relief->id }}" title="Delete">
                        <i class="ace-icon fa fa-trash-o bigger-150"></i>
                    </a>
                    <?php }?>

                    | &nbsp;

                    {{--                    <a class="green" href="{{ route('registration.adminEdit',$relief->id) }}" title="Edit">--}}
                    {{--                        <i class="ace-icon fa fa-pencil bigger-150"></i>--}}
                    {{--                    </a>--}}

                    <?php if((auth()->user()->user_type == 1 ) || (auth()->user()->can_print == 1)){?>

                    <a class="gray" href="{{ route('registration.adminPrintCard',array('id' =>$relief->id)) }}"
                       title="Print">
                        <i class="ace-icon fa fa-print bigger-150"></i>
                    </a>
                    <?php }?>

                    |

                    <a class="red blue assign_client" href="#" title="Assign Package"
                       id={{ $relief->id }}>
                        <i class="ace-icon fa fa-check-square-o bigger-150" style="color:#892E65;"></i>
                    </a>

                    |

                    <a class="manage_client" href="{{ route('package.adminFamilyWiseList',$relief->id) }}"
                       title="Manage Package"
                       id={{ $relief->id }}>
                        <i class="ace-icon fa fa-cog bigger-150" style="color:#8973be;"></i>
                    </a>
                    <?php } ?>

                    {{--                    <a class="grey print" href="{{ route('registration.adminPrintCard',$relief->id) }}"--}}
                    {{--                       id="{{ $relief->id }}" title="Print">--}}
                    {{--                        <i class="ace-icon fa fa-print bigger-150"></i>--}}
                    {{--                    </a>--}}


                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>


<div class="row">
    <div class="col-xs-6">
        <div class="dataTables_info" id="dynamic-table_info" role="status" aria-live="polite">
            Showing {{ (($reliefList->currentPage() - 1) * $reliefList->perPage()) + 1 }}
            to {{ (($reliefList->currentPage() - 1) * $reliefList->perPage()) + $reliefList->perPage() }}
            of
            {{ $reliefList->total() }} entries
        </div>
    </div>
    <div class="col-xs-6">
        <div class="dataTables_paginate paging_simple_numbers" id="dynamic-table_paginate">
            {{ $reliefList->links("vendor.pagination.custom") }}
        </div>
    </div>
</div>
<div class="row">
    <div id="dialog-message" class="hide">
        <p>
            Select a Package to assign this Family
        </p>

        <div class="hr hr-12 hr-double"></div>
        <div class="form-group">
            <p>
                <label class="col-sm-3 control-label no-padding-right" for="package_lists"> Package
                    List </label>

            <div class="col-sm-9">
                <select name='package_lists' class="col-xs-10 col-sm-10" id="package_lists">
                    <option value="">---- Please Select One----</option>
                    {{--                                @foreach($packages as $id => $datas)--}}
                    {{--                                    <option value="{{ $id }}">{{ $datas }}</option>--}}
                    {{--                                @endforeach--}}
                </select>
            </div>
            </p>
        </div>

        <div class="form-group">
            <div class="row col-sm-12 col-md-12">

                <label class="col-sm-3 control-label no-padding-right" for="last_give_pac">Package Creation
                    Date (dd-mm-yyyy)</label>
                {{--                <label class="col-sm-4 control-label no-padding-right" for="dob"></label>--}}

                <div class="col-sm-3">
                    <input class="date-picker form-control col-xs-6 col-sm-5" id="last_give_pac" type="text"
                           name="last_give_pac"
                           data-date-format="dd-mm-yyyy" required/>
                    <span class="input-group-addon">
								<i class="fa fa-calendar bigger-110"></i>
							</span>
                </div>
            </div>
            <div class="row col-sm-12 col-md-12">
                <h4 style="text-align: center">Assigned Packages</h4>
                <div class="assign_list"></div>
            </div>
        </div>
    </div>
    <!-- #dialog-message -->
</div>

<div class="row">
    <div id="dialog-message-education" class="hide">
        {{--                <p>--}}
        {{--                    Select a Package to assign this Family--}}
        {{--                </p>--}}

        {{--                <div class="hr hr-12 hr-double"></div>--}}
        <div class="col-sm-12">
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th scope="col">নাম</th>
                        <th scope="col">স্কুল</th>
                        <th scope="col">শ্রেনী</th>
                        <th scope="col">রোল</th>
                        <th scope="col">শাখা</th>
                        <th scope="col">লিঙ্গ</th>
                        <th scope="col">সেকশন</th>
                        <th scope="col">শিফট</th>
                    </tr>
                    </thead>
                    <tbody id="student_info">
{{--                    <tr>--}}
{{--                        <td><span id="s_name"></span></td>--}}
{{--                        <td><span id="s_school"></span></td>--}}
{{--                        <td><span id="s_class"></span></td>--}}
{{--                        <td><span id="s_roll"></span></td>--}}
{{--                        <td><span id="s_group"></span></td>--}}
{{--                        <td><span id="s_gender"></span></td>--}}
{{--                        <td><span id="s_section"></span></td>--}}
{{--                        <td><span id="s_shift"></span></td>--}}
{{--                    </tr>--}}

                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- #dialog-message -->
</div>

<style type="text/css">
    /*.table tbody > tr > td {*/
    /*text-align: center;*/
    /*}*/
    .facility {
        font-weight: bold;
    }
</style>