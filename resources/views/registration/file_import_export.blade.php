@extends('layouts.admin')

@section('title')
    Add Registration Form
@stop

@section('breadcrumb')
    <ul class="breadcrumb">
        <li>
            <i class="ace-icon fa fa-bars home-icon"></i>
            <a href="{{ route('registration.adminList') }}">Registration List</a>
        </li>

        <li>
            <a href="{{ route('registration.adminForm') }}">Registration Add</a>
        </li>
    </ul>
@stop

@section('page_header')
    <h1>Registration Form Add</h1>
@stop

@section('content')

    <?php
    $password = bcrypt('abc123');
    ?>
    <div class="col-xs-12">
        <form class="form-horizontal" role="form" action="{{ route('registration.adminManobikSohayotaExcelImport') }}" method="POST"
              enctype="multipart/form-data" id="reg_form">
            {{ csrf_field() }}

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        {!! Form::label('sample_file','Select File to Import:',['class'=>'col-md-3']) !!}
                        <div class="col-md-9">
                            {!! Form::file('sample_file', array('class' => 'form-control')) !!}
                            {!! $errors->first('sample_file', '<p class="alert alert-danger">:message</p>') !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix form-actions">
                <div class="col-md-offset-5 col-md-7">
                    <button id="submit" class="btn btn-info" type="submit">
                        <i class="ace-icon fa fa-check bigger-110"></i>
                        Submit
                    </button>

                    &nbsp; &nbsp; &nbsp;
                    <button class="btn" type="reset">
                        <i class="ace-icon fa fa-undo bigger-110"></i>
                        Reset
                    </button>
                </div>
            </div>
        </form>
    </div>
@stop


@section('custom_style')

@stop

@section('custom_script')
    <script type="text/javascript">

        /****************************/
        /****************************/
        /*  File Upload Section */
        /****************************/
        /****************************/

        $('#image').ace_file_input({
            style: 'well',
            btn_choose: 'Drop files here or click to choose',
            btn_change: null,
            no_icon: 'ace-icon fa fa-cloud-upload',
            droppable: true,
            thumbnail: 'small'//large | fit
            //,icon_remove:null//set null, to hide remove/reset button
            /**,before_change:function(files, dropped) {
						//Check an example below
						//or examples/file-upload.html
						return true;
					}*/
            /**,before_remove : function() {
						return true;
					}*/
            ,
            preview_error: function (filename, error_code) {
                //name of the file that failed
                //error_code values
                //1 = 'FILE_LOAD_FAILED',
                //2 = 'IMAGE_LOAD_FAILED',
                //3 = 'THUMBNAIL_FAILED'
                //alert(error_code);
            }

        }).on('change', function () {
            //console.log($(this).data('ace_input_files'));
            //console.log($(this).data('ace_input_method'));
        });

        var whitelist_ext, whitelist_mime;
        var btn_choose;
        var no_icon;

        var file_input = $('#image');

        file_input
            .ace_file_input('update_settings',
                {
                    'btn_choose': btn_choose,
                    'no_icon': no_icon,
                    'allowExt': whitelist_ext,
                    'allowMime': whitelist_mime
                });

        file_input.ace_file_input('reset_input');

        file_input
            .off('file.error.ace')
            .on('file.error.ace', function (e, info) {
                //console.log(info.file_count);//number of selected files
                //console.log(info.invalid_count);//number of invalid files
                //console.log(info.error_list);//a list of errors in the following format

                //info.error_count['ext']
                //info.error_count['mime']
                //info.error_count['size']

                //info.error_list['ext']  = [list of file names with invalid extension]
                //info.error_list['mime'] = [list of file names with invalid mimetype]
                //info.error_list['size'] = [list of file names with invalid size]


                /**
                 if( !info.dropped ) {
							//perhapse reset file field if files have been selected, and there are invalid files among them
							//when files are dropped, only valid files will be added to our file array
							e.preventDefault();//it will rest input
						}
                 */


                //if files have been selected (not dropped), you can choose to reset input
                //because browser keeps all selected files anyway and this cannot be changed
                //we can only reset file field to become empty again
                //on any case you still should check files with your server side script
                //because any arbitrary file can be uploaded by user and it's not safe to rely on browser-side measures
            });


        /****************************/
        /****************************/
        /*  End */
        /****************************/
        /****************************/

        //datepicker plugin
        $('.date-picker').datepicker({
            autoclose: true,
            todayHighlight: true
        });

        // Age Calculation Function
        function getAge(dateString) {
            var now = new Date();
            var today = new Date(now.getFullYear(), now.getMonth(), now.getDate());

            var yearNow = now.getFullYear();
            var monthNow = now.getMonth();
            var dateNow = now.getDate();

            let curDateFormat = new Date(dateString.substring(6, 10), dateString.substring(3, 5), dateString.substring(0, 2));

            var dobs = new Date(dateString.substring(6, 10), dateString.substring(3, 5), dateString.substring(0, 2) - 1);

            var yearDob = dobs.getFullYear();
            var monthDob = dobs.getMonth();
            var dateDob = dobs.getDate();
            var age = {};
            var ageString = "";
            var yearString = "";
            var monthString = "";
            var dayString = "";


            yearAge = yearNow - yearDob;

            if (monthNow >= monthDob)
                var monthAge = monthNow - monthDob;
            else {
                yearAge--;
                var monthAge = 12 + monthNow - monthDob;
            }

            if (dateNow >= dateDob)
                var dateAge = dateNow - dateDob;
            else {
                monthAge--;
                var dateAge = 31 + dateNow - dateDob;

                if (monthAge < 0) {
                    monthAge = 11;
                    yearAge--;
                }
            }

            age = {
                years: yearAge,
                months: monthAge,
                days: dateAge
            };

            if (age.years > 1) yearString = " years";
            else yearString = " year";
            if (age.months > 1) monthString = " months";
            else monthString = " month";
            if (age.days > 1) dayString = " days";
            else dayString = " day";

            let curYear, curMon, curDay;


            if ((age.years > 0) && (age.months > 0) && (age.days > 0)) {
                ageString = age.years + yearString + ", " + age.months + monthString + ", and " + age.days + dayString + " old.";
                curYear = age.years;
                curMon = age.months + 1;
                curDay = age.days;
            } else if ((age.years == 0) && (age.months == 0) && (age.days > 0)) {
                ageString = "Only " + age.days + dayString + " old!";
                curYear = age.years;
                curMon = age.months + 1;
                curDay = age.days;
            } else if ((age.years > 0) && (age.months == 0) && (age.days == 0)) {
                ageString = age.years + yearString + " old. Happy Birthday!!";
                curYear = age.years;
                curMon = age.months + 1;
                curDay = age.days;
            } else if ((age.years > 0) && (age.months > 0) && (age.days == 0)) {
                ageString = age.years + yearString + " and " + age.months + monthString + " old.";
                curYear = age.years;
                curMon = age.months + 1;
                curDay = age.days;
            } else if ((age.years == 0) && (age.months > 0) && (age.days > 0)) {
                ageString = age.months + monthString + " and " + age.days + dayString + " old.";
                curYear = age.years;
                curMon = age.months + 1;
                curDay = age.days;
            } else if ((age.years > 0) && (age.months == 0) && (age.days > 0)) {
                ageString = age.years + yearString + " and " + age.days + dayString + " old.";
                curYear = age.years;
                curMon = age.months + 1;
                curDay = age.days;
            } else if ((age.years == 0) && (age.months > 0) && (age.days == 0)) {
                ageString = age.months + monthString + " old.";
                curYear = age.years;
                curMon = age.months + 1;
                curDay = age.days;
            } else ageString = "Oops! Could not calculate age!";

            return {curYear, curMon, curDay};
        };

        $("#dob").on('change', function () {
            var date = $(this).val();
            var resData = getAge(date);
            console.log(resData);
            $("#age").val(resData.curYear);
        });

        $("#submit").click(function (e) {
            // e.preventDefault();
            if(isNidDup || isPhoneDup){
                $('#submit').prop('disabled', true); //TO DISABLED
                if(isNidDup){
                    alert("Duplicate NID");
                }else{
                    alert("Duplicate Phone");
                }
                return false;
            }
            else{
                $('#submit').prop('disabled', false); //TO ENABLE
            }

            var logoimg = document.getElementById("image");
            let size = logoimg.files[0].size;
            // if (size > 13334325) {
            if (size > 3000000) {
                alert("Logo Image Size is exceeding 2 Mb");
                e.preventDefault();
            }
        });

        $('#dist_pre').on('change', function () {
            var districtID = $(this).val();
            var token = $("input[name='_token']").val();
            var url = "{{ route('upazila.adminUpazilaSelectAjaxList') }}";
            if (districtID) {
                $('select[name="upo_pre"]').empty();
                $('select[name="post_pre"]').empty();
                upazillaList(districtID, token, url, 'pre');
            } else {
                $('select[name="upo_pre"]').empty();
                $('select[name="post_pre"]').empty();
            }
        });

        $('#upo_pre').on('change', function () {
            var upazilaID = $(this).val();
            var token = $("input[name='_token']").val();
            var url = "{{ route('union.adminUnionSelectAjaxList') }}";
            if (upazilaID) {
                $('select[name="uni_pre"]').empty();
                unionList(upazilaID, token, url, 'pre');
            } else {
                $('select[name="uni_pre"]').empty();
            }
        });

        $('#dist_per').on('change', function () {
            var districtID = $(this).val();
            var token = $("input[name='_token']").val();
            var url = "{{ route('upazila.adminUpazilaSelectAjaxList') }}";
            if (districtID) {
                $('select[name="upo_per"]').empty();
                $('select[name="post_per"]').empty();
                upazillaList(districtID, token, url, 'per');
            } else {
                $('select[name="upo_per"]').empty();
                $('select[name="post_per"]').empty();
            }
        });

        $('#upo_per').on('change', function () {
            var upazilaID = $(this).val();
            var token = $("input[name='_token']").val();
            var url = "{{ route('union.adminUnionSelectAjaxList') }}";
            if (upazilaID) {
                $('select[name="uni_per"]').empty();
                unionList(upazilaID, token, url, 'per');
            } else {
                $('select[name="uni_per"]').empty();
            }
        });

        function upazillaList(districtID, token, url, type) {
            $.ajax({
                url: url,
                method: 'POST',
                data: {districtID: districtID, _token: token},
                dataType: "json",
                success: function (data) {
                    console.log(data);
                    if (type == 'pre') {
                        $('select[name="upo_pre"]').append('<option value="">' + "--- বাছাই করুন ---" + '</option>');
                    } else {
                        $('select[name="upo_per"]').append('<option value="">' + "--- বাছাই করুন ---" + '</option>');
                    }
                    $.each(data, function (key, value) {
                        if (type == 'pre')
                            $('select[name="upo_pre"]').append('<option value="' + key + '">' + value + '</option>');
                        else
                            $('select[name="upo_per"]').append('<option value="' + key + '">' + value + '</option>');
                    });
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert("Status: " + textStatus);
                    alert("Error: " + errorThrown);
                    console.log(XMLHttpRequest);
                    console.log(textStatus);
                    console.log(errorThrown);
                }
            });
        };

        function unionList(upazilaID, token, url, type) {
            $.ajax({
                url: url,
                method: 'POST',
                data: {upazilaID: upazilaID, _token: token},
                dataType: "json",
                success: function (data) {
                    console.log(data);
                    if (type == 'pre') {
                        $('select[name="uni_pre"]').append('<option value="">' + "--- বাছাই করুন ---" + '</option>');
                        $('select[name="uni_pre"]').append('<option value="' + 0 + '">' + "পৌরসভা" + '</option>');
                    } else {
                        $('select[name="uni_per"]').append('<option value="">' + "--- বাছাই করুন ---" + '</option>');
                        $('select[name="uni_per"]').append('<option value="' + 0 + '">' + "পৌরসভা" + '</option>');
                    }
                    $.each(data, function (key, value) {
                        if (type == 'pre') {
                            $('select[name="uni_pre"]').append('<option value="' + key + '">' + value + '</option>');
                        } else {
                            $('select[name="uni_per"]').append('<option value="' + key + '">' + value + '</option>');
                        }
                    });
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert("Status: " + textStatus);
                    alert("Error: " + errorThrown);
                    console.log(XMLHttpRequest);
                    console.log(textStatus);
                    console.log(errorThrown);
                }
            });
        };

        $("#same_add").on("change", function () {
            let dist_pre = $("#dist_pre").val();
            let up_pre = $("#up_pre").val();
            let word_pre = $("#word_pre").val();
            if ($("#word_pre").val()) {
                if ($(this).prop("checked") == true) {
                    $("#vill_per").prop("disabled", true);
                    $("#post_per").prop("disabled", true);
                    $("#word_per").prop("disabled", true);
                    $("#holding_per").prop("disabled", true);
                    $("#dist_per").prop("disabled", true);
                    $("#upo_per").prop("disabled", true);
                    $("#uni_per").prop("disabled", true);
                    console.log("Checkbox is checked.")
                } else if ($(this).prop("checked") == false) {
                    $("#vill_per").prop("disabled", false);
                    $("#post_per").prop("disabled", false);
                    $("#word_per").prop("disabled", false);
                    $("#holding_per").prop("disabled", false);
                    $("#dist_per").prop("disabled", false);
                    $("#upo_per").prop("disabled", false);
                    $("#uni_per").prop("disabled", false);
                    console.log("Checkbox is unchecked.")
                }
            } else {
                alert(" দয়া করে বর্তমান ঠিকানা আগে বাছাই করুন।");
                $(this).prop("checked", false);
            }
        });

        $(".othr").hide();

        $("#home_type").on("change", function () {
            let othrVal = $(this).val();
            if (othrVal == 2)
                $(".othr").show();
            else
                $(".othr").hide();
        });

        var isNidDup = false;
        var isPhoneDup = false;

        $("#nid").on("change", function () {
            let nidVal = $(this).val();
            // alert(nidVal);
            var token = $("input[name='_token']").val();
            var url = "{{ route('registration.adminNidDuplicateCheck') }}";

            $.ajax({
                url: url,
                method: 'POST',
                data: {nidVal: nidVal, _token: token},
                dataType: "json",
                success: function (scatterSeries) {
                    console.log(scatterSeries);
                    if (scatterSeries.status == 404) {
                        var cardNo = scatterSeries.data['card_no'];
                        alert("Duplicate NID No. \nCard No. " + cardNo + " holder has the same NID with his / his family.");
                        $('#submit').prop('disabled', true); //TO DISABLED
                        $("#nid").css("border", "1px solid red");
                        $("#nid").css("color", "red");
                        isNidDup = true;
                    }else{
                        $('#submit').prop('disabled', false); //TO ENABLE
                        $("#nid").css("color", "#858585");
                        $("#nid").css("border", "1px solid #d5d5d5");
                        isNidDup = false;
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    console.log(XMLHttpRequest);
                    console.log(textStatus);
                    console.log(errorThrown);
                    // $("#submit").disable();
                    // console.log("adasd");
                }
            });
        });

        $("#phone").on("change", function () {
            let phoneVal = $(this).val();
            // alert(phoneVal);
            var token = $("input[name='_token']").val();
            var url = "{{ route('registration.adminPhoneDuplicateCheck') }}";

            $.ajax({
                url: url,
                method: 'POST',
                data: {phoneVal: phoneVal, _token: token},
                dataType: "json",
                success: function (scatterSeries) {
                    console.log(scatterSeries);
                    if (scatterSeries.status == 404) {
                        var cardNo = scatterSeries.data['card_no'];
                        alert("Duplicate Phone No. \nCard No. " + cardNo + " holder has the same Phone No with his / his family.");
                        $('#submit').prop('disabled', true); //TO DISABLED
                        $("#phone").css("border", "1px solid red");
                        $("#phone").css("color", "red");
                        isPhoneDup = true;
                    }
                    else{
                        $('#submit').prop('disabled', false); //TO ENABLE
                        $("#phone").css("color", "#858585");
                        $("#phone").css("border", "1px solid #d5d5d5");
                        isPhoneDup = false;
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    console.log(XMLHttpRequest);
                    console.log(textStatus);
                    console.log(errorThrown);
                }
            });
        });

        $('#reg_form').on('keyup keypress', function(e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) {
                e.preventDefault();
                return false;
            }
        });


    </script>
@stop