@extends('layouts.admin')

@section('title')
    Master Roll Report
@stop

@section('breadcrumb')
    {{--    <ul class="breadcrumb">--}}
    {{--        <li>--}}
    {{--            <i class="ace-icon fa fa-bars home-icon"></i>--}}
    {{--            <a href="{{ route('training.adminList') }}">Training List</a>--}}
    {{--        </li>--}}

    {{--        --}}{{--<li class="active">User Profile</li>--}}
    {{--    </ul>--}}
@stop

@section('page_header')
    <h1>Duplicate Check</h1>
@stop

@section('content')
    <div class="col-xs-12">
        <div>
            <div id="dynamic-table_wrapper" class="dataTables_wrapper form-inline no-footer">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="pull-right tableTools-container"></div>
                        <div style="padding-bottom: 10px;" class="clearfix">
                            <form class="form-horizontal" role="form"
                                  action="{{ route('report.adminExportMasterRollReport') }}"
                                  method="POST">
                                {{ csrf_field() }}
                                <div class="col-xs-12 col-sm-12">
                                    <div class="col-xs-3">
                                        <label class="col-sm-3 control-label no-padding-right"
                                               for="duplicate">Search By</label>

                                        <div class="col-sm-9">
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <select name='duplicate' id="duplicate">
                                                        <option value="">--- Pleasse Select ---</option>
                                                        <option value="nid">NID Duplicate Check</option>
                                                        <option value="phone">Mobile Duplicate Check</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-xs-9">
{{--                                        <div class="form-group">--}}
{{--                                            <div class="col-sm-3">--}}
{{--                                                <button type="submit" class="btn btn-purple">--}}
{{--                                                    <i class="fa fa-download bigger-120" aria-hidden="true"></i>--}}
{{--                                                    Export Masterroll Report--}}
{{--                                                </button>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
                                        <div class="form-group">
                                            <div class="col-sm-4">
                                                <a href="{{ route('registration.adminDuplicateCheck') }}"
                                                   name="searchData"
                                                   id="searchData">
                                                    <button class="btn btn-primary search">
                                                        <i class="fa fa-search bigger-120" aria-hidden="true"></i>
                                                        Search
                                                    </button>
                                                    <button class="btn btn-primary loading" type="button" disabled>
                                                        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                                        Loading...
                                                    </button>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="posts">
                    @include('report.ajax_registration_report')
                </div>
            </div>
        </div>
    </div>
@stop

@section('custom_style')

@stop

@section('custom_script')
    <script type="text/javascript">
        $(".loading").hide();
        // $(window).on('hashchange', function () {
        //     if (window.location.hash) {
        //         var page = window.location.hash.replace('#', '');
        //         if (page == Number.NaN || page <= 0) {
        //             return false;
        //         } else {
        //             getPosts(page);
        //         }
        //     }
        // });

        // $(document).ready(function () {
        //     $(document).on('click', '.pagination a', function (e) {
        //         getPosts($(this).attr('href').split('page=')[1], $(this).attr('href'));
        //         e.preventDefault();
        //     });
        // });

        $('#upo').on('change', function () {
            var upazilaID = $(this).val();
            var token = $("input[name='_token']").val();
            var url = "{{ route('union.adminUnionSelectAjaxList') }}";
            if (upazilaID) {
                $('select[name="uni_per"]').empty();
                unionList(upazilaID, token, url, 'per');
            } else {
                $('select[name="uni_per"]').empty();
            }
        });

        function unionList(upazilaID, token, url, type) {
            $.ajax({
                url: url,
                method: 'POST',
                data: {upazilaID: upazilaID, _token: token},
                dataType: "json",
                success: function (data) {
                    console.log(data);
                    $('select[name="uni_per"]').append('<option value="">' + "--- বাছাই করুন ---" + '</option>');
                    $('select[name="uni_per"]').append('<option value="' + 0 + '">' + "পৌরসভা" + '</option>');
                    $.each(data, function (key, value) {
                        $('select[name="uni_per"]').append('<option value="' + key + '">' + value + '</option>');
                    });
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert("Status: " + textStatus);
                    alert("Error: " + errorThrown);
                    console.log(XMLHttpRequest);
                    console.log(textStatus);
                    console.log(errorThrown);
                }
            });
        };

//         function getPosts(page, url) {
//             console.log(url);
// //            url: '?page=' + page
//             var displayValue = $("#displayValue").val();
//             var searchData = $("#searchData").val();
//             $.ajax({
//                 url: '?page=' + page + '&displayValue=' + displayValue + '&searchData=' + searchData,
//                 success: function (data) {
//                     $('.posts').html(data);
//                 },
//                 error: function (err) {
//                     alert('Posts could not be loaded.');
//                 }
//             });
//         };


        $("#displayValue").on('change', function () {
            var displayValue = $(this).val();
            var url = window.location.href;
            dynamicDataViewCount(displayValue, '', url);
        });

        $("#searchData").on("click", function (e) {
            e.preventDefault();
            $(".loading").show();
            $(".search").hide();
            var duplicate = $('#duplicate').val();
            var url = window.location.href;
            $.ajax({
                url: url,
                dataType: 'html',
                type: 'GET',
                data: {
                    duplicate: duplicate,
                },
                success: function (data) {
                    $(".loading").hide();
                    $(".search").show();
                    $('.posts').html(data);
                },
                error: function (err) {
                    console.log(err);
                    alert('Data could not be loaded.');
                }
            })
        });

        $("#exportData").on("click", function (e) {
            e.preventDefault();
            var duplicate = $('#duplicate').val();
            var url = $(this).attr('href');

            ajaxCall(dist, upo, uni_per, word_no, name, phone, nid, card_type, card_no, url);
        });

        function ajaxCall(training_id, trade_name, reg_status, url) {

        }

        function dynamicDataViewCount(displayValue, searchData, url) {
            if (displayValue == '') {
                displayValue = $("#displayValue").val();
            }
            if (searchData == '') {
                searchData = $("#searchData").val();
            }
            $.ajax({
                url: url,
                dataType: 'html',
                type: 'GET',
                data: {displayValue: displayValue, searchData: searchData},
                success: function (data) {
                    $('.posts').html(data);
                },
                error: function (err) {
                    console.log(err);
                    alert('Posts could not be loaded.');
                }
            });
        }
    </script>

@stop