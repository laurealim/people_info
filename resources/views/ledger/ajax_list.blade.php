<table id="dynamic-table" class="table table-striped table-bordered table-hover" style="margin-bottom: 0px;">
    <thead>
    <tr>
        <th class="center" width="2%">
            {{--<label class="pos-rel">--}}
            {{--<input type="checkbox" class="ace"/>--}}
            {{--<span class="lbl"></span>--}}
            {{--</label>--}}
        </th>
        <th width="8%">Apartment Number</th>
        <th width="10%">Owner</th>
        <th width="10%">Tenant</th>
        <th width="10%">Bill Amount</th>
        <th width="10%">Payment Amount</th>
        <th width="10%">Previous Due Amount (BDT)</th>
        <th width="10%">Current total Payable Amount (BDT)</th>
        <th width="8%">Ledger For</th>
        <th width="8%">Ledger Type</th>
        <th width="6%">Status</th>
        <th class="center" width="18%">Action</th>
    </tr>
    </thead>

    <tbody>

    <?php $pageNo = (($pendingLedgerLists->currentPage() - 1) * $pendingLedgerLists->perPage()) + 1; ?>
    @foreach ($pendingLedgerLists as $pendingLedgerList)
        <?php
        //pr($pendingLedgerList);
        ?>
        <tr>
            <td class="center">
                <label class="pos-rel">
                    {{--<input type="checkbox" class="ace"/>--}}
                    <span class="lbl">{{ $pageNo++ }}</span>
                </label>
            </td>
            <td>
                {{ $pendingLedgerList->apt_number }}
            </td>
            <td>
                {{ \App\User::getDataById($pendingLedgerList->owner)->first_name.' '.\App\User::getDataById($pendingLedgerList->owner)->last_name }}
            </td>
            <td>
                {{ \App\User::getDataById($pendingLedgerList->client_id)->first_name.' '.\App\User::getDataById($pendingLedgerList->client_id)->last_name }}
            </td>
            <td>
                @if($pendingLedgerList->ledger_for == config('constants.ledgerFor.Billing'))
                    {{ number_format(($pendingLedgerList->amount + $pendingLedgerList->secure_amount), 2,'.',"") }}
                @else
                    {{ number_format(0, 2,'.',"") }}
                @endif
            </td>
            <td>
                @if($pendingLedgerList->ledger_for == config('constants.ledgerFor.Payment'))
                    {{ number_format($pendingLedgerList->amount, 2,'.',"") }}
                @else
                    {{ number_format(0, 2,'.',"") }}
                @endif
            </td>
            <td>
                {{ number_format($pendingLedgerList->previous_amount, 2,'.',"") }}
            </td>
            <td>
                {{ number_format(($pendingLedgerList->amount + $pendingLedgerList->secure_amount + $pendingLedgerList->previous_amount), 2,'.',"") }}
            </td>
            <td>
                {{ config('constants.ledgerFor.'.$pendingLedgerList->ledger_for) }}
            </td>
            <td>
                {{ config('constants.ledgerType.'.$pendingLedgerList->ledger_type) }}
            </td>
            <td>
                <?php if($pendingLedgerList->status == 1){ ?>
                <span class='label label-warning'>Pending</span>
                <?php } else if($pendingLedgerList->status == 2){ ?>
                <span class='label label-success'>Approved</span>
                <?php }?>
            </td>
            <td class="center">
                <div class="hidden-sm hidden-xs action-buttons">
                    {{--<a class="blue" href="{{ route('') }}">--}}
                    {{--<i class="ace-icon fa fa-search-plus bigger-200"></i>--}}
                    {{--</a>--}}

                    {{--&nbsp;&nbsp;| &nbsp;&nbsp;--}}

                    @if($pendingLedgerList->status == 1)

                        <a class="blue approvedBtn"
                           href="{{ route('ledger.adminApproveLedger',[$pendingLedgerList->id,config('constants.state.Approved')]) }}"
                           title="Accept">
                            <i class="ace-icon fa fa-check bigger-150"></i>
                        </a>

                        &nbsp;| &nbsp;

                        <a class="red rejectBtn" href="{{ route('ledger.adminRejectLedger',[$pendingLedgerList->id,config('constants.state.Rejected')]) }}"
                        title="Reject"
                        id={{ $pendingLedgerList->id }}>
                        <i class="ace-icon fa fa-close bigger-150"></i>
                        </a>
                    @else
                        <i class="ace-icon fa fa-check-square-o bigger-150 green"></i>
                    @endif
                </div>

            </td>
        </tr>
    @endforeach
    </tbody>
</table>


<div class="row">
    <div class="col-xs-6">
        <div class="dataTables_info" id="dynamic-table_info" role="status" aria-live="polite">
            Showing {{ (($pendingLedgerLists->currentPage() - 1) * $pendingLedgerLists->perPage()) + 1 }}
            to {{ (($pendingLedgerLists->currentPage() - 1) * $pendingLedgerLists->perPage()) + $pendingLedgerLists->perPage() }}
            of
            {{ $pendingLedgerLists->total() }} entries
        </div>
    </div>
    <div class="col-xs-6">
        <div class="dataTables_paginate paging_simple_numbers" id="dynamic-table_paginate">
            {{ $pendingLedgerLists->links("vendor.pagination.custom") }}
        </div>
    </div>
</div>

<style type="text/css">
    .facility {
        font-weight: bold;
    }
</style>