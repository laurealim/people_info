@extends('layouts.admin')

@section('title')
    Add Package
@stop

@section('breadcrumb')
    <ul class="breadcrumb">
        <li>
            <i class="ace-icon fa fa-bars home-icon"></i>
            <a href="{{ route('package.adminList') }}">Package List</a>
        </li>

        <li>
            <a href="{{ route('package.adminForm') }}">Package Add</a>
        </li>
        {{--<li class="active">User Profile</li>--}}
    </ul>
@stop

@section('page_header')
    <h1>Add Package</h1>
@stop

@section('content')
    <div class="col-xs-12">
        <form class="form-horizontal" role="form" action="{{ route('package.adminStore') }}" method="POST">
            {{ csrf_field() }}
            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="title"> Package Title *</label>

                <div class="col-sm-9">
                    <input type="text" id="title" placeholder="Package Title" name="title"
                           class="col-xs-10 col-sm-5" required/>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('title'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('title') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="space-4"></div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="p_code"> Package Short Code *</label>

                <div class="col-sm-9">
                    <input type="text" id="p_code" placeholder="Package Short Code" name="p_code"
                           class="col-xs-10 col-sm-5" required/>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('p_code'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('p_code') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="space-4"></div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="desc"> Description *</label>

                <div class="col-sm-9">
                    <textarea type="text" id="desc" placeholder="Package Short Code" name="desc"
                              class="col-xs-10 col-sm-5" required></textarea>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('desc'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('desc') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="space-4"></div>
            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="desc">Package Creation Date</label>
{{--                <label class="col-sm-4 control-label no-padding-right" for="dob"></label>--}}

                <div class="col-sm-3">
                    <input class="date-picker form-control col-xs-6 col-sm-5"  id="dob" type="text" name="dob"
                           data-date-format="dd-mm-yyyy" required/>
                    <span class="input-group-addon">
								<i class="fa fa-calendar bigger-110"></i>
							</span>
                </div>
            </div>

            <div class="space-4"></div>

            <div class="clearfix form-actions">
                <div class="col-md-offset-3 col-md-9">
                    <button class="btn btn-info" type="submit">
                        <i class="ace-icon fa fa-check bigger-110"></i>
                        Submit
                    </button>

                    &nbsp; &nbsp; &nbsp;
                    <button class="btn" type="reset">
                        <i class="ace-icon fa fa-undo bigger-110"></i>
                        Reset
                    </button>
                </div>
            </div>
        </form>
    </div>
@stop


@section('custom_style')

@stop

@section('custom_script')
    <script type="text/javascript">
        //datepicker plugin
        $('.date-picker').datepicker({
            autoclose: true,
            todayHighlight: true
        });
    </script>

@stop