@extends('layouts.admin')

@section('title')
    Package View
@stop

@section('breadcrumb')
    {{--    <ul class="breadcrumb">--}}
    {{--        <li>--}}
    {{--            <i class="ace-icon fa fa-bars home-icon"></i>--}}
    {{--            <a href="{{ route('training.adminList') }}">Training List</a>--}}
    {{--        </li>--}}

    {{--        --}}{{--<li class="active">User Profile</li>--}}
    {{--    </ul>--}}
@stop

@section('page_header')
    <h1>Family Wise Package Management</h1>
@stop

@section('content')
    <div class="col-xs-12">
        <div>
            <div id="dynamic-table_wrapper" class="dataTables_wrapper form-inline no-footer">
                <div class="row">
                    <form class="form-horizontal" role="form" action="{{ route('package.adminSaveFamilyWiseList') }}"
                          method="POST">
                        {{ csrf_field() }}
                        <table id="dynamic-table" class="table table-striped table-bordered table-hover"
                               style="margin-bottom: 0px;">
                            <thead>
                            <tr>
                                <th class="center" width="2%">
                                    ক্রমিক নং
                                </th>
                                <th class="center" width="2%">
                                    পরিবারের বর্ননা
                                </th>
                                @foreach($packageList as $pacId => $pacName)
                                    <th width="5%"> {{ $pacName }}</th>
                                @endforeach
                            </tr>
                            </thead>

                            <tbody>

                            <?php $pageNo = 1;?>
                            <!--    --><?php //$pageNo = (($registrationList->currentPage() - 1) * $registrationList->perPage()) + 1;?>
                            @if(!empty($packageData))
                                @foreach ($packageData as $relief)
                                    <input type="hidden" name="reg_id" id="reg_id" value="{{ $relief->reg_id }}">
                                    <tr>
                                        <td class="center">
                                            <label class="pos-rel">
                                                <span class="lbl">{{ $pageNo++ }}</span>
                                            </label>
                                        </td>
                                        <td class="center">
                                            <label class="pos-rel">
                                                <span class="lbl">{{ $relief->name }}</span>
                                                <br/>
                                                <span class="lbl">(কার্ড নং: {{ $relief->card_no }}, মোবাইল: {{ $relief->phone }}, এন,আই,ডি: {{ $relief->nid }})</span>
                                            </label>
                                        </td>
                                        @foreach($packageList as $pacId => $pacName)
                                            <td class="center">
                                                <?php
                                                $disabled = 'disabled';
                                                $checked = '';
                                                $vals = date('Y-m-d', time());
                                                if (array_key_exists($pacId, $finalPacListArr)) {
                                                    $checked = 'checked';
                                                    $vals = $finalPacListArr[$pacId];
                                                    $disabled = '';
                                                }
                                                ?>
                                                <input type="checkbox" name="pac[{{ $pacId }}]" id="pac[{{ $pacId }}]"
                                                       value="{{ $vals }}" {{ $checked }} {{ $disabled }}>
                                                <span>
                                                @if(!empty($finalPacListArr[$pacId]))
                                                        {{ $finalPacListArr[$pacId] }}
                                                    @endif

                                            </span>
                                            </td>
                                        @endforeach
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td>
                                        <span>No package has been assigned to this family</span>
                                    </td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                        <div class="clearfix form-actions">
                            <div class="col-md-offset-3 col-md-9">
                                <button class="btn btn-info" type="submit">
                                    <i class="ace-icon fa fa-check bigger-110"></i>
                                    Submit
                                </button>

                                &nbsp; &nbsp; &nbsp;
                                <button class="btn" type="reset">
                                    <i class="ace-icon fa fa-undo bigger-110"></i>
                                    Reset
                                </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@stop

@section('custom_style')

@stop

@section('custom_script')
    <script type="text/javascript">
        $(".loading").hide();
        // $(window).on('hashchange', function () {
        //     if (window.location.hash) {
        //         var page = window.location.hash.replace('#', '');
        //         if (page == Number.NaN || page <= 0) {
        //             return false;
        //         } else {
        //             getPosts(page);
        //         }
        //     }
        // });
        //
        // $(document).ready(function () {
        //     $(document).on('click', '.pagination a', function (e) {
        //         getPosts($(this).attr('href').split('page=')[1], $(this).attr('href'));
        //         e.preventDefault();
        //     });
        // });

        //         function getPosts(page, url) {
        //             console.log(url);
        // //            url: '?page=' + page
        //             var displayValue = $("#displayValue").val();
        //             var searchData = $("#searchData").val();
        //             $.ajax({
        //                 url: '?page=' + page + '&displayValue=' + displayValue + '&searchData=' + searchData,
        //                 success: function (data) {
        //                     $('.posts').html(data);
        //                 },
        //                 error: function (err) {
        //                     alert('Posts could not be loaded.');
        //                 }
        //             });
        //         };

    </script>

@stop