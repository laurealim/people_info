@extends('layouts.admin')

@section('title')
    Package View
@stop

@section('breadcrumb')
    {{--    <ul class="breadcrumb">--}}
    {{--        <li>--}}
    {{--            <i class="ace-icon fa fa-bars home-icon"></i>--}}
    {{--            <a href="{{ route('training.adminList') }}">Training List</a>--}}
    {{--        </li>--}}

    {{--        --}}{{--<li class="active">User Profile</li>--}}
    {{--    </ul>--}}
@stop

@section('page_header')
    <h1>Family Wise Package Management</h1>
@stop

@section('content')
    <div class="col-xs-12">
        <div>
            <div id="dynamic-table_wrapper" class="dataTables_wrapper form-inline no-footer">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="pull-right tableTools-container"></div>
                        <div style="padding-bottom: 10px;" class="clearfix">
                            <form class="form-horizontal" role="form"
                                  action="{{ route('report.adminExportBulkRegistrationReport') }}"
                                  method="POST">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12">
                                        <div class="col-xs-6">
                                            <label class="col-sm-3 control-label no-padding-right"
                                                   for="poss_help">Bulk Family Id Search</label>

                                            <div class="col-sm-9">
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <textarea id="search_by_id" name="search_by_id" cols="60" rows="4"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-6">
                                            <div class="form-group">
                                                <div class="col-sm-4">
                                                    <a href="{{ route('report.adminRegistrationReport') }}"
                                                       name="searchData"
                                                       id="searchData">
                                                        <button class="btn btn-primary search">
                                                            <i class="fa fa-search bigger-120" aria-hidden="true"></i>
                                                            Search
                                                        </button>
                                                        <button class="btn btn-primary loading" type="button" disabled>
                                                            <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                                            Loading...
                                                        </button>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="posts">
                    @include('package.ajax_family_wise_list')
                </div>
            </div>
        </div>
    </div>
@stop

@section('custom_style')

@stop

@section('custom_script')
    <script type="text/javascript">
        $(".loading").hide();
        // $(window).on('hashchange', function () {
        //     if (window.location.hash) {
        //         var page = window.location.hash.replace('#', '');
        //         if (page == Number.NaN || page <= 0) {
        //             return false;
        //         } else {
        //             getPosts(page);
        //         }
        //     }
        // });
        //
        // $(document).ready(function () {
        //     $(document).on('click', '.pagination a', function (e) {
        //         getPosts($(this).attr('href').split('page=')[1], $(this).attr('href'));
        //         e.preventDefault();
        //     });
        // });

//         function getPosts(page, url) {
//             console.log(url);
// //            url: '?page=' + page
//             var displayValue = $("#displayValue").val();
//             var searchData = $("#searchData").val();
//             $.ajax({
//                 url: '?page=' + page + '&displayValue=' + displayValue + '&searchData=' + searchData,
//                 success: function (data) {
//                     $('.posts').html(data);
//                 },
//                 error: function (err) {
//                     alert('Posts could not be loaded.');
//                 }
//             });
//         };


        $("#displayValue").on('change', function () {
            var displayValue = $(this).val();
            var url = window.location.href;
            dynamicDataViewCount(displayValue, '', url);
        });

        $("#searchData").on("click", function (e) {
            e.preventDefault();
            $(".loading").show();
            $(".search").hide();
            var search_by_id = $('#search_by_id').val();
            var url = window.location.href;
            $.ajax({
                url: url,
                dataType: 'html',
                type: 'GET',
                data: { search_by_id: search_by_id },
                success: function (data) {
                    $(".loading").hide();
                    $(".search").show();
                    $('.posts').html(data);
                },
                error: function (err) {
                    console.log(err);
                    alert('Posts could not be loaded.');
                }
            })
        });

        $("#exportData").on("click", function (e) {
            e.preventDefault();
            var dist = $('#dist').val();
            var upo = $('#upo').val();
            var uni_per = $('#uni_per').val();
            var word_no = $('#word_no').val();
            var name = $('#name').val();
            var phone = $('#phone').val();
            var nid = $('#nid').val();
            var card_type = $('#card_type').val();
            var card_no = $('#card_no').val();
            var ssnp = $('#ssnp').val();
            var url = $(this).attr('href');

            ajaxCall(dist, upo, uni_per, word_no, name, phone, nid, card_type, card_no, url);
        });

        function ajaxCall(training_id, trade_name, reg_status, url) {

        }

        function dynamicDataViewCount(displayValue, searchData, url) {
            if (displayValue == '') {
                displayValue = $("#displayValue").val();
            }
            if (searchData == '') {
                searchData = $("#searchData").val();
            }
            $.ajax({
                url: url,
                dataType: 'html',
                type: 'GET',
                data: {displayValue: displayValue, searchData: searchData},
                success: function (data) {
                    $('.posts').html(data);
                },
                error: function (err) {
                    console.log(err);
                    alert('Posts could not be loaded.');
                }
            });
        }
    </script>

@stop