<?php
if($isDisplay == 2) {
?>
<table id="dynamic-table" class="table table-striped table-bordered table-hover" style="margin-bottom: 0px;">
    <thead>
    <tr>
        <th class="center" width="2%">
            Sr. No.
        </th>
        <th width="5%">পরিবার প্রধানের নাম</th>
        <th width="5%">পিতা/স্বামীর নাম</th>
        <th width="5%">জন্ম তারিখ</th>
        <th width="5%">বয়স</th>
        <th width="5%">কার্ডের ধরণ</th>
        <th width="5%">কার্ড নং</th>
        <th width="5%">জেলা</th>
        <th width="5%">উপজেলা</th>
        <th width="5%">ইউনিয়ন / পৌরসভা</th>
        <th width="5%">ওয়ার্ড</th>
        <th width="5%">বর্তমান পেশা</th>
        <th width="5%">মোবাইল নাম্বার</th>
        <th width="5%">জন্ম নিবন্ধন / ভোটার আইডি নাম্বার</th>
        <th width="6%">ভাতার নাম</th>
        <th width="6%">ত্রাণ সহায়তা</th>
        <th width="6%">সম্ভাব্য সহায়তা</th>
        <th width="5%">মোট সদস্য</th>
        <th width="5%">মাসিক আয়</th>
        <th width="5%">মাসিক ব্যায়</th>
    </tr>
    </thead>

    <tbody>

    <?php $pageNo = 1;?>
    <!--    --><?php //$pageNo = (($registrationList->currentPage() - 1) * $registrationList->perPage()) + 1;?>
    @foreach ($reliefData as $relief)
        <tr>
            <td class="center">
                <label class="pos-rel">
                    {{--<input type="checkbox" class="ace"/>--}}
                    <span class="lbl">{{ $pageNo++ }}</span>
                </label>
            </td>
            <td>
                {{ $relief->name }}
            </td>
            <td>
                <?php if(!empty($relief->g_name)) { ?>
                {{ $relief->g_name }}
                <?php } else{ ?>
                {{ $relief->hw_name }}
                <?php }
                ?>
            </td>
            <td>
                {{ date("Y/m/d",strtotime($relief->dob)) }}
            </td>
            <td>
                {{ $relief->age }}
            </td>
            <td>
                {{ config('constants.card_type.arr.'.$relief->card_type) }}
            </td>
            <td>
                <?php
                $cardNo = $relief['card_no'];
                echo $cardNo;

                //                $cardNo = '353200';
                //                $cardNo .= $relief['word_pre'];
                //                $length = strlen($relief['id']);
                //                for ($count = 6; $count > $length; $count--) {
                //                    $cardNo .= '0';
                //                }
                //                $cardNo .= $relief['id'];
                //                echo $cardNo;
                ?>
            </td>
            <td>
                {{ $relief->dis_name }}
            </td>
            <td>
                {{ $relief->upa_name }}
            </td>
            <td>
                <?php
                if ($relief->uni_pre < 1) {
                    echo 'পৌরসভা';
                } else {
                    echo $relief->uni_name;
                }
                ?>
                {{--                {{ $relief->uni_pre }}--}}
            </td>
            <td>
                {{ $relief->word_pre }}
            </td>
            <td>
                {{ $relief->profession  }}
            </td>
            <td>
                {{ $relief->phone }}
            </td>
            <td>
                {{ $relief->nid }}
            </td>
            <td>
                <?php
                if (empty($relief->ssnp_name)) {
                    echo "তথ্য নেই";
                } else {
                    echo $relief->ssnp_name;
                }
                ?>
                {{--                {{ $relief->ssnp_name }}--}}
            </td>
            <td>
                <?php
                if (!empty($relief->packeges)) {

                    $packageArr = array_filter(explode(',', $relief->packeges));
                    $dateArr = array_filter(explode(',', $relief->delv_date));
//                    pr($packageArr);
//                    pr($dateArr);
                    foreach ($packageArr as $key => $value) {
                        echo $packageList[$value] . '(' . $dateArr[$key] . '),';
                    }
                } else {
                    echo "---";
                }
                ?>
            </td>
            <td>
                <?php
                echo config('constants.possible_help.' . $relief->poss_help)
                ?>

            </td>
            <td>
                {{ $relief->total_member }}
            </td>
            <td>
                {{ $relief->total_income }}
            </td>
            <td class="center">
                {{ $relief->total_spend }}
            </td>

        </tr>
    @endforeach

    <?php
    echo "Not found data(ID) :";
    pr($array_length); ?>
    </tbody>
</table>
<?php } ?>


<style type="text/css">
    /*.table tbody > tr > td {*/
    /*text-align: center;*/
    /*}*/
    .facility {
        font-weight: bold;
    }
</style>