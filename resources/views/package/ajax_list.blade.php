<div id="dynamic-table_wrapper" class="dataTables_wrapper form-inline no-footer">
    <table id="dynamic-table" class="table table-striped table-bordered table-hover" style="margin-bottom: 0px;">
        <thead>
        <tr>
            <th class="center" width="10%">
                <label class="pos-rel">
                    {{--<input type="checkbox" class="ace"/>--}}
                    {{--<span class="lbl"></span>--}}
                    Sr. No.
                </label>
            </th>
            <th width="15%">Package Name</th>
            <th width="15%">Package Short Name</th>
            <th width="15%">Package Details</th>
            <th width="15%">Package Creation Date</th>
            <th width="10%">Created By</th>
            <th width="10%">Status</th>
            <?php if(auth()->user()->id != 532 && auth()->user()->id != 534) { ?>
            <th class="center" width="10%">Action</th>
            <?php }?>
        </tr>
        </thead>

        <tbody>

        <?php $pageNo = (($packageList->currentPage() - 1) * $packageList->perPage()) + 1 ?>

        @foreach ($packageList as $package)
            <tr>
                <td class="center">
                    <label class="pos-rel">
                        {{--<input type="checkbox" class="ace"/>--}}
                        <span class="lbl">{{ $pageNo++ }}</span>
                    </label>
                </td>
                <td>
                    {{ $package->title}}
                </td>
                <td>
                    <?php echo $package->p_code;?>
                </td>
                <td>
                    <?php echo $package->desc;?>
                </td>
                <td>
                    <?php echo date('Y-m-d', strtotime($package->dob));?>
                </td>
                <td>
<!--                    --><?php //echo config('constants.userType.'.$package->created_by);?>
                    <?php echo $package->first_name; ?>
                </td>
                <td>
                    <?php echo config('constants.package_status.'.$package->status);?>
                </td>
                <?php if(auth()->user()->id != 532 && auth()->user()->id != 534) { ?>
                <td class="center">
                    <div class="hidden-sm hidden-xs action-buttons">
                        {{--<a class="blue" href="{{ route('') }}">--}}
                        {{--<i class="ace-icon fa fa-search-plus bigger-200"></i>--}}
                        {{--</a>--}}

                        {{--&nbsp;&nbsp;| &nbsp;&nbsp;--}}

                        <a class="green" href="{{ route('package.adminEdit',$package->id) }}">
                            <i class="ace-icon fa fa-pencil bigger-200"></i>
                        </a>

                        &nbsp;&nbsp;| &nbsp;&nbsp;

                        <a class="red deletebtn" href="{{ route('package.adminDestroy',$package->id) }}" id={{ $package->id }}>
                        <i class="ace-icon fa fa-trash-o bigger-200"></i>
                        </a>
                    </div>
                </td>
                <?php } ?>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div class="row">
        <div class="col-xs-6">
            <div class="dataTables_info" id="dynamic-table_info" role="status" aria-live="polite">
                Showing {{ (($packageList->currentPage() - 1) * $packageList->perPage()) + 1 }}
                to {{ (($packageList->currentPage() - 1) * $packageList->perPage()) + $packageList->perPage() }} of
                {{ $packageList->total() }} entries
            </div>
        </div>
        <div class="col-xs-6">
            <div class="dataTables_paginate paging_simple_numbers" id="dynamic-table_paginate">
                {{ $packageList->links("vendor.pagination.custom") }}
            </div>
        </div>
    </div>
</div>
