@extends('layouts.admin')

@section('title')
    Edit Payment
@stop

@section('breadcrumb')
    <ul class="breadcrumb">
        <li>
            <i class="ace-icon fa fa-bars home-icon"></i>
            <a href="{{ route('payment.adminList') }}">Payment List</a>
        </li>

        <li>
            <a href="{{ route('payment.adminForm') }}">Payment Add</a>
        </li>
        {{--<li class="active">User Profile</li>--}}
    </ul>
@stop

@section('page_header')
    <h1>Edit Payment</h1>
@stop

@section('content')
    <div class="col-xs-12">
        <h3 class="header smaller lighter blue">
            Basic Information
        </h3>

        <form class="form-horizontal" role="form" action="{{ route('payment.adminUpdate', $id) }}" method="POST"
              enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" id="payment_id" value="<?php echo $id;?>">
            <div class="form-group">
                <div class="col-sm-6">
                    <label class="col-sm-4 control-label no-padding-right" for="client_id"> Tenant List
                        * </label>

                    <div class="col-sm-8">
                        <select name='client_id' class="col-xs-10 col-sm-8" id="client_id" disabled="true">
                            <option value="0">---- Please Select One ----</option>
                            @foreach($tenantList as $id => $name)
                                <option value="{{ $id }}" {{ $id == $paymentData->client_id ? 'selected="selected"' : '' }}>{{ $name }}</option>
                            @endforeach

                        </select>
                        <input type="hidden" name="client_id" value={{$paymentData->client_id}}>
                        @if ($errors->has('client_id'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('client_id') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="col-sm-6">
                    <label class="col-sm-4 control-label no-padding-right" for="apt_id"> Apartment Number *</label>

                    <div class="col-sm-8">
                        <select name='apt_id' class="col-xs-10 col-sm-8" id="apt_id" disabled="true">
                            @foreach($apartmentList as $apt_id => $apt_name)
                                <option value="{{ $apt_id }}" {{ $apt_id == $paymentData->apt_id ? 'selected="selected"' : '' }}>{{ $apt_name }}</option>
                            @endforeach
                        </select>
                        <input type="hidden" name="apt_id" value={{$paymentData->apt_id}}>
                        @if ($errors->has('apt_id'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('apt_id') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
            </div>

            <div class="space-4"></div>

            <h3 class="header smaller lighter blue">
                Payment Details Information
            </h3>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="payment_month"> Payment Month *</label>

                <div class="col-sm-6">
                    <select name='payment_month' class="col-xs-10 col-sm-8" id="payment_month">
                        <option value="0">---- Please Select One ----</option>
                        @foreach($monthList as $month_id => $months)
                            <option value="{{ $month_id }}" {{ $month_id == $paymentData->payment_month ? 'selected="selected"' : '' }}>{{ $months }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('billing_month'))
                        <span class="help-block middle">
                                <strong>{{ $errors->first('billing_month') }}</strong>
                            </span>
                    @endif
                </div>
            </div>

            <div class="space-4"></div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="payment_year"> Payment year *</label>

                <div class="col-sm-6">
                    <select name='payment_year' class="col-xs-10 col-sm-8" id="payment_year">
                        <option value="0">---- Please Select One ----</option>
                        @foreach($yearList as $year_id => $year)
                            <option value="{{ $year_id }}" {{ $year_id == $paymentData->payment_year ? 'selected="selected"' : '' }}>{{ $year }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('billing_year'))
                        <span class="help-block middle">
                                <strong>{{ $errors->first('billing_year') }}</strong>
                            </span>
                    @endif
                </div>
            </div>

            <div class="space-4"></div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="amount">Regular Amount*</label>

                <div class="col-sm-9">
                    <input type="number" id="amount" placeholder="10000, 15000, 20000...." name="amount"
                           class="col-xs-10 col-sm-5" value="{{ $billAmountData->total_amount * (-1) }}" readonly=true/>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('amount'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('amount') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="space-4"></div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="pre_ad_du">Previous Amount <span
                            id="adv_due"></span>
                    *</label>

                <div class="col-sm-9">
                    <input type="number" id="pre_ad_du" placeholder="10000, 15000, 20000...." name="pre_ad_du"
                           class="col-xs-10 col-sm-5" value="{{ $billAmountData->previous_amount * (-1) }}" readonly=true/>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('pre_ad_du'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('pre_ad_du') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="space-4"></div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="payable"> Total Payable Amount *</label>

                <div class="col-sm-9">
                    <input type="number" id="payable" placeholder="3000, 4000, 5000...."
                           name="payable"
                           class="col-xs-10 col-sm-5" value="{{ $billAmountData->current_amount * (-1) }}" readonly=true/>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('payable'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('payable') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="space-4"></div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="total_bill_payment"> Total Payment *</label>

                <div class="col-sm-9">
                    <input type="number" id="total_bill_payment" placeholder="3000, 4000, 5000...."
                           name="total_bill_payment" value="{{ $paymentData->total_bill_payment }}"
                           class="col-xs-10 col-sm-5"/>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('total_bill_payment'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('total_bill_payment') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="space-4"></div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="payment_type"> Payment Type </label>

                <div class="col-sm-9">
                    <select name='payment_type' class="col-xs-10 col-sm-5" id="payment_type">
                        <option value="0">---- Please Select One ----</option>
                        @foreach($ledgerType as $key => $val)
                            <option value="{{ $key }}" {{ $key == $paymentData->payment_type ? 'selected="selected"' : '' }}>{{ $val }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('status'))
                        <span class="help-block middle">
                                <strong>{{ $errors->first('status') }}</strong>
                            </span>
                    @endif
                </div>
            </div>

            <div class="space-4"></div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="comments"> Comments</label>

                <div class="col-sm-9">
                    <textarea id="comments" placeholder="Comments" name="comments"
                              class="col-xs-10 col-sm-5">{{ $paymentData->comments }}</textarea>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('comments'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('comments') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="space-4"></div>

            <div class="clearfix form-actions">
                <div class="col-md-offset-3 col-md-9">
                    <button class="btn btn-info" type="submit">
                        <i class="ace-icon fa fa-check bigger-110"></i>
                        Submit
                    </button>

                    &nbsp; &nbsp; &nbsp;
                    <button class="btn" type="reset">
                        <i class="ace-icon fa fa-undo bigger-110"></i>
                        Reset
                    </button>
                </div>
            </div>
        </form>
    </div>
@stop


@section('custom_style')

@stop

@section('custom_script')
    <script type="text/javascript">
        function clearData() {
            $("#amount").val("");
            $("#pre_ad_du").val("");
            $("#payable").val("");
            $("#total_bill_payment").val("");
            $("#comments").val("");
        }

        $("select[name='client_id']").change(function () {
            clearData();
            var client_id = $(this).val();
            var token = $("input[name='_token']").val();
            var url = "{{ route('payment.selectAjax') }}";
//            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            $.ajax({
                url: url,
                method: 'POST',
                data: {client_id: client_id, _token: token},
                success: function (data) {
                    console.log(data);
                    $("select[name='apt_id'").html('');
                    $("select[name='apt_id'").html(data.options);
                }
            });
        });

        $("select[name='apt_id']").change(function () {
            var is_initial = true;
            clearData();
            var aptId = $(this).val();
            var client_id = $("select[name='client_id']").val();

            var token = $("input[name='_token']").val();
            var url = "{{ route('payment.getAptDataAjax') }}";

            $.ajax({
                url: url,
                method: 'POST',
                data: {aptId: aptId, client_id: client_id, _token: token},
                success: function (data) {
                    console.log(data);
                    console.log(data.aptData.rent_amount);

                    if (data.previousAmount <= -1) {
                        $("#adv_due").html("(Due)");
                    } else {
                        $("#adv_due").html("(Advance)");
                    }

                    if (data.error == 1) {
                        alert("This client has some pending ledger payment for this particular selected apartment. \n Please remove or confirm those payments first from 'Ledger List' section");
                    } else {
                        if (data.size >= 1) {
                            is_initial = false;
                        }

                        var rent_amount = data.aptData.rent_amount;
                        var service_charge = data.aptData.service_charge;
                        var adv_rent = data.aptData.adv_rent;
                        var adv_month = data.aptData.adv_month;


                        var previousAmount = data.previousAmount * (-1);
                        var due_amount = data.dueAmount * (-1);
                        var ledgerType = data.ledgerType;

                        var total_security_bill = adv_rent * adv_month;

                        var regularAmount = 0;
                        if (ledgerType == 1) {
                            regularAmount = rent_amount + service_charge + total_security_bill;
                        } else {
                            regularAmount = rent_amount + service_charge;

                        }

                        $("#amount").val(regularAmount);
                        $("#pre_ad_du").val(previousAmount);
                        $("#payable").val(due_amount);
                    }
                }
            });
        });
    </script>
@stop