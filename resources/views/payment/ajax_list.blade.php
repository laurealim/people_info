<table id="dynamic-table" class="table table-striped table-bordered table-hover" style="margin-bottom: 0px;">
    <thead>
    <tr>
        <th class="center" width="3%">
            {{--<label class="pos-rel">--}}
            {{--<input type="checkbox" class="ace"/>--}}
            {{--<span class="lbl"></span>--}}
            {{--</label>--}}
        </th>
        <th width="7%">Apartment Number</th>
        <th width="10%">Client Name</th>
        <th width="10%">Owner</th>
        <th width="10%">Payment Month</th>
        <th width="10%">Bill Amount (BDT)</th>
        <th width="10%">Due Amount (BDT)</th>
        <th width="10%">Total Payment Amount (BDT)</th>
        <th width="10%">Current Amount (BDT)</th>
        {{--<th width="10%">Created By</th>--}}
        {{--<th width="10%">Status</th>--}}
        <th class="center" width="10%">Action</th>
    </tr>
    </thead>

    <tbody>

    <?php $pageNo = (($paymentLists->currentPage() - 1) * $paymentLists->perPage()) + 1; ?>
    <?php //dd($paymentLists);?>
    @foreach ($paymentLists as $paymentList)
        <tr>
            <td class="center">
                <label class="pos-rel">
                    {{--<input type="checkbox" class="ace"/>--}}
                    <span class="lbl">{{ $pageNo++ }}</span>
                </label>
            </td>
            <td>
                {{ $paymentList->apt_number }}
            </td>
            <td>
                {{ \App\User::getDataById($paymentList->client_id)->first_name.' '.\App\User::getDataById($paymentList->client_id)->last_name }}
            </td>
            <td>
                {{ $paymentList->first_name.' '.$paymentList->last_name }}
            </td>
            <td>
                {{ date_format($paymentList->created_at,"F, Y") }}
            </td>
            <td>
                {{ number_format(($paymentList->total_amount *(-1)),2,'.',"") }}
            </td>
            <td>
                {{ number_format(($paymentList->previous_amount *(-1)),2,'.',"")  }}
            </td>
            <td>
                {{ number_format($paymentList->total_bill_payment,2,'.',"") }}
            </td>
            <td>
                {{ number_format(($paymentList->current_amount + $paymentList->total_bill_payment),2) }}
            </td>
            <td class="center">
                <div class="hidden-sm hidden-xs action-buttons">
                    {{--<a class="blue" href="{{ route('') }}">--}}
                    {{--<i class="ace-icon fa fa-search-plus bigger-200"></i>--}}
                    {{--</a>--}}

                    {{--&nbsp;&nbsp;| &nbsp;&nbsp;--}}

                    <?php
                        if($paymentList->status != 2){?>


                    <a class="green" href="{{ route('payment.adminEdit',$paymentList->id) }}">
                        <i class="ace-icon fa fa-pencil bigger-150"></i>
                    </a>
                    &nbsp;| &nbsp;
                    <a class="red deletebtn" href="{{ route('payment.adminDelete',$paymentList->id) }}"
                       id={{ $paymentList->id }}>
                        <i class="ace-icon fa fa-trash-o bigger-150"></i>
                    </a>
                    <?php }
                    ?>

                    {{--@if($paymentList->is_assigned < 1)--}}
                        {{--&nbsp;| &nbsp;--}}

                        {{--<a class="red blue assign_client" href="#" title="Assign Client"--}}
                           {{--id={{ $paymentList->id }}>--}}
                            {{--<i class="ace-icon fa fa-user-plus bigger-150"></i>--}}
                        {{--</a>--}}
                    {{--@endif--}}

                    {{--&nbsp;&nbsp;| &nbsp;&nbsp;--}}

                    {{--<a class="red deletebtn" href="{{ route('country.adminDelete',$country->id) }}" id={{ $country->id }}>--}}
                    {{--<i class="ace-icon fa fa-trash-o bigger-200"></i>--}}
                    {{--</a>--}}
                </div>

            </td>
        </tr>
    @endforeach
    </tbody>
</table>


<div class="row">
    <div class="col-xs-6">
        <div class="dataTables_info" id="dynamic-table_info" role="status" aria-live="polite">
            Showing {{ (($paymentLists->currentPage() - 1) * $paymentLists->perPage()) + 1 }}
            to {{ (($paymentLists->currentPage() - 1) * $paymentLists->perPage()) + $paymentLists->perPage() }} of
            {{ $paymentLists->total() }} entries
        </div>
    </div>
    <div class="col-xs-6">
        <div class="dataTables_paginate paging_simple_numbers" id="dynamic-table_paginate">
            {{ $paymentLists->links("vendor.pagination.custom") }}
        </div>
    </div>
</div>

<style type="text/css">
    .facility {
        font-weight: bold;
    }
</style>