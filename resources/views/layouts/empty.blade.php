<!DOCTYPE html>
<html lang="en">
<head>
    @include('includes.head')
</head>

<body class="no-skin">

<div class="main-container ace-save-state" id="main-container">


    <div class="main-content">
        <div class="main-content-inner">

            <div class="page-content">
                <div class="row" id="main">
                    @yield('content')
                </div>
                <!-- /.row -->
            </div>
            <!-- /.page-content -->
        </div>
    </div>
    <!-- /.main-content -->

    <div class="footer">
        @include('includes.footer')
    </div>

    <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
        <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
    </a>
</div>
<!-- /.main-container -->

<!-- basic scripts -->

<!--[if !IE]> -->
<script src="{{ asset('js/template/jquery-2.1.4.min.js') }}"></script>

<!-- <![endif]-->

<!--[if IE]>
<script src="assets/js/jquery-1.11.3.min.js"></script>
<![endif]-->
<script type="text/javascript">
    if ('ontouchstart' in document.documentElement) document.write("<script src='{{ asset('js/template/jquery.mobile.custom.min.js') }} '>" + "<" + "/script>");
</script>
<script src="{{ asset('js/template/bootstrap.min.js') }}"></script>

<!-- page specific plugin scripts -->

<!-- ace scripts -->
<script src="{{ asset('js/template/ace-elements.min.js') }}"></script>
<script src="{{ asset('js/template/ace.min.js') }}"></script>

@yield('custom_style')
@yield('custom_script')
<!-- inline scripts related to this page -->
</body>
</html>
