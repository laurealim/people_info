<div id="dynamic-table_wrapper" class="dataTables_wrapper form-inline no-footer">
    <table id="dynamic-table" class="table table-striped table-bordered table-hover" style="margin-bottom: 0px;">
        <thead>
        <tr>
            <th class="center" width="10%">
                <label class="pos-rel">
                    {{--<input type="checkbox" class="ace"/>--}}
                    {{--<span class="lbl"></span>--}}
                    Sr. No.
                </label>
            </th>
            <th width="20%">Trade Name</th>
            <th width="20%">Trade Code</th>
            <th width="20%">Created By</th>
            <th width="20%">Status</th>
            <th class="center" width="10%">Action</th>
        </tr>
        </thead>

        <tbody>

        <?php $pageNo = (($trades->currentPage() - 1) * $trades->perPage()) + 1 ?>

        @foreach ($trades as $trade)
            <tr>
                <td class="center">
                    <label class="pos-rel">
                        {{--<input type="checkbox" class="ace"/>--}}
                        <span class="lbl">{{ $pageNo++ }}</span>
                    </label>
                </td>
                <td>
                    {{ $trade->trade_name}}
                </td>
                <td>
                    <?php echo $trade->trade_code;?>
                </td>
                <td>
                    <?php echo config('constants.userType.'.$trade->created_by);?>
                </td>
                <td>
                    <?php echo config('constants.status.'.$trade->status);?>
                </td>
                <td class="center">
                    <div class="hidden-sm hidden-xs action-buttons">
                        {{--<a class="blue" href="{{ route('') }}">--}}
                        {{--<i class="ace-icon fa fa-search-plus bigger-200"></i>--}}
                        {{--</a>--}}

                        {{--&nbsp;&nbsp;| &nbsp;&nbsp;--}}

                        <a class="green" href="{{ route('trade.adminEdit',$trade->id) }}">
                            <i class="ace-icon fa fa-pencil bigger-200"></i>
                        </a>

                        &nbsp;&nbsp;| &nbsp;&nbsp;

                        <a class="red deletebtn" href="{{ route('trade.adminDestroy',$trade->id) }}" id={{ $trade->id }}>
                        <i class="ace-icon fa fa-trash-o bigger-200"></i>
                        </a>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div class="row">
        <div class="col-xs-6">
            <div class="dataTables_info" id="dynamic-table_info" role="status" aria-live="polite">
                Showing {{ (($trades->currentPage() - 1) * $trades->perPage()) + 1 }}
                to {{ (($trades->currentPage() - 1) * $trades->perPage()) + $trades->perPage() }} of
                {{ $trades->total() }} entries
            </div>
        </div>
        <div class="col-xs-6">
            <div class="dataTables_paginate paging_simple_numbers" id="dynamic-table_paginate">
                {{ $trades->links("vendor.pagination.custom") }}
            </div>
        </div>
    </div>
</div>
