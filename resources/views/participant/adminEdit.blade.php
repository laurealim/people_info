@extends('layouts.admin')

@section('title')
    Edit Registration
@stop

@section('breadcrumb')
    <ul class="breadcrumb">
        <li>
            <i class="ace-icon fa fa-bars home-icon"></i>
            <a href="{{ route('registration.adminList') }}">Registration List</a>
        </li>

        <li>
            <a href="{{ route('registration.adminEdit',$id) }}">Registration Edit</a>
        </li>
    </ul>
@stop

@section('page_header')
    <h1>Registration Form</h1>
@stop

@section('content')
    <div class="col-xs-12">
        <form class="form-horizontal" role="form" action="{{ route('registration.adminUpdate', $id) }}" method="POST"
              enctype="multipart/form-data">
            {{ csrf_field() }}

            <h3 class="header smaller lighter blue">
                প্রশিক্ষণের তালিকা
            </h3>
            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="training_id"> প্রশিক্ষণের নাম *</label>
                <div class="col-sm-9">
                    <select id="training_id" placeholder="" name="training_id" class="col-xs-10 col-sm-5" required>
                        <option>--- বাছাই করুন ---</option>
                        <?php foreach ($trainingList as $id => $name) { ?>
                            <?php echo "<option value='{$id}'" . ($registrationData->training_id == $id ? ('selected="selected"') : '') . ">{$name}</option>"; ?>
                        <?php }?>
                    </select>
                </div>
            </div>
            <div class="space-4"></div>

            <h3 class="header smaller lighter blue">
                সাধারণ তথ্য
            </h3>
            <div class="form-group col-sm-6">
                <label class="col-sm-4 control-label no-padding-right" for="name_bn"> শিক্ষার্থীর নাম(বাংলা) *</label>

                <div class="col-sm-8">
                    <input type="text" id="name_bn" placeholder="বাংলায়" name="name_bn"
                           value="{{ $registrationData->name_bn }}" class="col-xs-10 col-sm-12" required/>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('name_bn'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('name_bn') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="form-group col-sm-6">
                <label class="col-sm-4 control-label no-padding-right" for="name_en"> শিক্ষার্থীর নাম(ইংরেজি)
                    *</label>

                <div class="col-sm-8">
                    <input type="text" id="name_en" placeholder="ইংরেজিতে" name="name_en"
                           value="{{ $registrationData->name_en }}" class="col-xs-10 col-sm-12" required/>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('name_en'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('name_en') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="space-4"></div>

            <div class="form-group col-sm-6">
                <label class="col-sm-4 control-label no-padding-right" for="f_name_bn"> পিতার নাম(বাংলা) *</label>

                <div class="col-sm-8">
                    <input type="text" id="f_name_bn" placeholder="বাংলায়" name="f_name_bn"
                           value="{{ $registrationData->f_name_bn }}" class="col-xs-10 col-sm-12" required/>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('f_name_bn'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('f_name_bn') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="form-group col-sm-6">
                <label class="col-sm-4 control-label no-padding-right" for="f_name_en"> পিতার নাম(ইংরেজি) *</label>

                <div class="col-sm-8">
                    <input type="text" id="f_name_en" placeholder="ইংরেজিতে" name="f_name_en"
                           value="{{ $registrationData->f_name_en }}" class="col-xs-10 col-sm-12" required/>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('f_name_en'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('f_name_en') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="space-4"></div>

            <div class="form-group col-sm-6">
                <label class="col-sm-4 control-label no-padding-right" for="m_name_bn"> মাতার নাম(বাংলা) *</label>

                <div class="col-sm-8">
                    <input type="text" id="m_name_bn" placeholder="বাংলায়" name="m_name_bn"
                           value="{{ $registrationData->m_name_bn }}" class="col-xs-10 col-sm-12" required/>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('m_name_bn'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('m_name_bn') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="form-group col-sm-6">
                <label class="col-sm-4 control-label no-padding-right" for="m_name_en"> মাতার নাম(ইংরেজি) *</label>

                <div class="col-sm-8">
                    <input type="text" id="m_name_en" placeholder="ইংরেজিতে" name="m_name_en"
                           value="{{ $registrationData->m_name_en }}" class="col-xs-10 col-sm-12" required/>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('m_name_en'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('m_name_en') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="space-4"></div>
            <div class="space-4"></div>
            <div class="row">
                <div class="col-sm-12">
                    <h3 class="header smaller lighter blue">
                        ঠিকানা
                    </h3>
                    <div class="card">
                        <div class="card-header">
                            <h4>
                                বর্তমান ঠিকানা
                            </h4>
                        </div>
                        <div class="card-body" style="display: flow-root">
                            <br>
                            <div class="form-group col-sm-6">
                                <label class="col-sm-4 control-label no-padding-right" for="vill_pre"> গ্রাম/মহল্লা
                                    *</label>

                                <div class="col-sm-8">
                                    <input type="text" id="vill_pre" placeholder="গ্রাম/মহল্লা" name="vill_pre"
                                           value="{{ $registrationData->vill_pre }}" class="col-xs-10 col-sm-12"
                                           required/>
                                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('vill_pre'))
                                            <span class="help-block middle">
                                <strong>{{ $errors->first('vill_pre') }}</strong>
                            </span>
                                        @endif
                                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                                </div>
                            </div>
                            <div class="form-group col-sm-6">
                                <label class="col-sm-4 control-label no-padding-right" for="post_pre"> পোষ্ট অফিস
                                    *</label>

                                <div class="col-sm-8">
                                    <input type="text" id="post_pre" placeholder="পোষ্ট অফিস" name="post_pre"
                                           value="{{ $registrationData->post_pre }}" class="col-xs-10 col-sm-12"
                                           required/>
                                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('post_pre'))
                                            <span class="help-block middle">
                                <strong>{{ $errors->first('post_pre') }}</strong>
                            </span>
                                        @endif
                                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                                </div>
                            </div>
                            <div class="form-group col-sm-6">
                                <label class="col-sm-4 control-label no-padding-right" for="up_pre"> থানা/উপোজেলা
                                    *</label>

                                <div class="col-sm-8">
                                    <input type="text" id="up_pre" placeholder="থানা/উপোজেলা" name="up_pre"
                                           value="{{ $registrationData->up_pre }}" class="col-xs-10 col-sm-12"
                                           required/>
                                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('up_pre'))
                                            <span class="help-block middle">
                                <strong>{{ $errors->first('up_pre') }}</strong>
                            </span>
                                        @endif
                                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                                </div>
                            </div>
                            <div class="form-group col-sm-6">
                                <label class="col-sm-4 control-label no-padding-right" for="dist_pre"> জেলা
                                    *</label>

                                <div class="col-sm-8">
                                    <input type="text" id="dist_pre" placeholder="জেলা" name="dist_pre"
                                           value="{{ $registrationData->dist_pre }}" class="col-xs-10 col-sm-12"
                                           required/>
                                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('dist_pre'))
                                            <span class="help-block middle">
                                <strong>{{ $errors->first('dist_pre') }}</strong>
                            </span>
                                        @endif
                                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="space-4"></div>

                    <div class="card">
                        <div class="card-header">
                            <h4>
                                স্থায়ী ঠিকানা
                            </h4>
                        </div>
                        <div class="card-body" style="display: flow-root">
                            <br>
                            <div class="form-group col-sm-6">
                                <label class="col-sm-4 control-label no-padding-right" for="vill_per"> গ্রাম/মহল্লা
                                    *</label>

                                <div class="col-sm-8">
                                    <input type="text" id="vill_per" placeholder="গ্রাম/মহল্লা" name="vill_per"
                                           value="{{ $registrationData->vill_per }}" class="col-xs-10 col-sm-12"
                                           required/>
                                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('vill_per'))
                                            <span class="help-block middle">
                                <strong>{{ $errors->first('vill_per') }}</strong>
                            </span>
                                        @endif
                                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                                </div>
                            </div>
                            <div class="form-group col-sm-6">
                                <label class="col-sm-4 control-label no-padding-right" for="post_per"> পোষ্ট অফিস
                                    *</label>

                                <div class="col-sm-8">
                                    <input type="text" id="post_per" placeholder="পোষ্ট অফিস" name="post_per"
                                           value="{{ $registrationData->post_per }}" class="col-xs-10 col-sm-12"
                                           required/>
                                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('post_per'))
                                            <span class="help-block middle">
                                <strong>{{ $errors->first('post_per') }}</strong>
                            </span>
                                        @endif
                                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                                </div>
                            </div>
                            <div class="form-group col-sm-6">
                                <label class="col-sm-4 control-label no-padding-right" for="up_per"> থানা/উপোজেলা
                                    *</label>

                                <div class="col-sm-8">
                                    <input type="text" id="up_per" placeholder="থানা/উপোজেলা" name="up_per"
                                           value="{{ $registrationData->up_per }}" class="col-xs-10 col-sm-12"
                                           required/>
                                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('up_per'))
                                            <span class="help-block middle">
                                <strong>{{ $errors->first('up_per') }}</strong>
                            </span>
                                        @endif
                                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                                </div>
                            </div>
                            <div class="form-group col-sm-6">
                                <label class="col-sm-4 control-label no-padding-right" for="dist_per"> জেলা
                                    *</label>

                                <div class="col-sm-8">
                                    <input type="text" id="dist_per" placeholder="জেলা" name="dist_per"
                                           value="{{ $registrationData->dist_per }}" class="col-xs-10 col-sm-12"
                                           required/>
                                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('dist_per'))
                                            <span class="help-block middle">
                                <strong>{{ $errors->first('dist_per') }}</strong>
                            </span>
                                        @endif
                                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="space-4"></div>
                </div>
            </div>

            <div class="space-4"></div>
            <h3 class="header smaller lighter blue">
                শিক্ষগত যোগ্যতা
            </h3>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group col-sm-6">
                        <label class="col-sm-4 control-label no-padding-right" for="edu_lvl"> সর্বশেষ অর্জন ডিগ্রী
                            *</label>

                        <div class="col-sm-8">
                            <select name='edu_lvl' class="col-xs-10 col-sm-12" id="edu_lvl" required>
                                <option value="">--- বাছাই করুন ---</option>
                                <?php foreach ($eduLevel_list as $id => $name) { ?>
                                    <?php echo "<option value='{$id}'" . ($registrationData->edu_lvl == $id ? ('selected="selected"') : '') . ">{$name}</option>"; ?>
                                <?php }?>
                            </select>
                            <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('edu_lvl'))
                                    <span class="help-block middle">
                                <strong>{{ $errors->first('edu_lvl') }}</strong>
                            </span>
                                @endif
                                {{--<span class="middle">Inline help text</span>--}}
                    </span>
                        </div>
                    </div>
                    <div class="form-group col-sm-6">
                        <label class="col-sm-4 control-label no-padding-right" for="ssc_yr"> S.S.C পাশের সাল
                            *</label>

                        <div class="col-sm-8">
                            <input type="number" id="ssc_yr" placeholder="i.e. 1985" name="ssc_yr"
                                   value="{{ $registrationData->ssc_yr }}" class="col-xs-10 col-sm-12" required/>
                            <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('ssc_yr'))
                                    <span class="help-block middle">
                                <strong>{{ $errors->first('ssc_yr') }}</strong>
                            </span>
                                @endif
                                {{--<span class="middle">Inline help text</span>--}}
                    </span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group col-sm-6">
                        <label class="col-sm-4 control-label no-padding-right" for="ssc_roll"> S.S.C রোল </label>

                        <div class="col-sm-8">
                            <input type="number" id="ssc_roll" placeholder="i.e. 1123445" name="ssc_roll"
                                   value="{{ $registrationData->ssc_roll }}" class="col-xs-10 col-sm-12"/>
                            <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('ssc_roll'))
                                    <span class="help-block middle">
                                <strong>{{ $errors->first('ssc_roll') }}</strong>
                            </span>
                                @endif
                                {{--<span class="middle">Inline help text</span>--}}
                    </span>
                        </div>
                    </div>
                    <div class="form-group col-sm-6">
                        <label class="col-sm-4 control-label no-padding-right" for="ssc_bord"> S.S.C বোর্ড
                            *</label>

                        <div class="col-sm-8">
                            <select name='ssc_bord' class="col-xs-10 col-sm-12" id="ssc_bord" required>
                                <option value="">--- বাছাই করুন ---</option>
                                <?php foreach ($eduBoard_list as $id => $name) { ?>
                                    <?php echo "<option value='{$id}'" . ($registrationData->ssc_bord == $id ? ('selected="selected"') : '') . ">{$name}</option>"; ?>
                                <?php }?>
                            </select>
                            <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('ssc_bord'))
                                    <span class="help-block middle">
                                <strong>{{ $errors->first('ssc_bord') }}</strong>
                            </span>
                                @endif
                                {{--<span class="middle">Inline help text</span>--}}
                    </span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group col-sm-6">
                        <label class="col-sm-4 control-label no-padding-right" for="ssc_gpa"> S.S.C জিপিএ
                            *</label>

                        <div class="col-sm-8">
                            <input type="number" step='.01' min="2.00" max="5.00" id="ssc_gpa" placeholder="5.00"
                                   name="ssc_gpa"
                                   value="{{ $registrationData->ssc_gpa }}" class="col-xs-10 col-sm-12" required/>
                            <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('ssc_gpa'))
                                    <span class="help-block middle">
                                <strong>{{ $errors->first('ssc_gpa') }}</strong>
                            </span>
                                @endif
                                {{--<span class="middle">Inline help text</span>--}}
                    </span>
                        </div>
                    </div>
                    <div class="form-group col-sm-6"></div>
                </div>
            </div>

            <div class="space-4"></div>
            <h3 class="header smaller lighter blue">
                যোগাযোগ
            </h3>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group col-sm-6">
                        <label class="col-sm-4 control-label no-padding-right" for="phone"> মোবাইল / টেলিফোন
                            *</label>

                        <div class="col-sm-8">
                            <input type="text" id="phone" placeholder="i.e. 017XXXXXXXX"
                                   name="phone"
                                   value="{{ $registrationData->phone }}" class="col-xs-10 col-sm-12" required/>
                            <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('phone'))
                                    <span class="help-block middle">
                                <strong>{{ $errors->first('phone') }}</strong>
                            </span>
                                @endif
                                {{--<span class="middle">Inline help text</span>--}}
                    </span>
                        </div>
                    </div>
                    <div class="form-group col-sm-6">
                        <label class="col-sm-4 control-label no-padding-right" for="email"> ই-মেইল *</label>

                        <div class="col-sm-8">
                            <input type="text" id="email" placeholder="i.e. demo@gmail.com"
                                   name="email"
                                   value="{{ $registrationData->email }}" class="col-xs-10 col-sm-12" required/>
                            <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('email'))
                                    <span class="help-block middle">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                                @endif
                                {{--<span class="middle">Inline help text</span>--}}
                    </span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group col-sm-6">
                        <label class="col-sm-4 control-label no-padding-right" for="nid"> জন্মোনিবন্ধন /
                            ভোটার
                            আইডি নাম্বার *</label>

                        <div class="col-sm-8">
                            <input type="text" id="nid" placeholder="i.e. 1234567890" name="nid"
                                   value="{{ $registrationData->nid }}" class="col-xs-10 col-sm-12" required/>
                            <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('nid'))
                                    <span class="help-block middle">
                                <strong>{{ $errors->first('nid') }}</strong>
                            </span>
                                @endif
                                {{--<span class="middle">Inline help text</span>--}}
                    </span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="space-4"></div>
            <h3 class="header smaller lighter blue">
                বিবিধ তথ্য
            </h3>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group col-sm-6">
                        <label class="col-sm-4 control-label no-padding-right" for="dob"> জন্ম তারিখ
                            *</label>

                        <div class="col-sm-8">
                            <input class="date-picker form-control" id="dob" type="text" name="dob"
                                   value="{{ date('d-m-Y', strtotime($registrationData->dob)) }}"
                                   data-date-format="dd-mm-yyyy" required/>
                            <span class="input-group-addon">
                            <i class="fa fa-calendar bigger-110"></i>
                        </span>
                        </div>
                    </div>
                    <div class="form-group col-sm-6">
                        <label class="col-sm-4 control-label no-padding-right" for="training_place"> বয়স *</label>

                        <div class="col-sm-8">
                            <input type="number" id="age" placeholder="i.e. 25"
                                   name="age"
                                   value="{{ $registrationData->age }}" class="col-xs-10 col-sm-12" required/>
                            <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('age'))
                                    <span class="help-block middle">
                                <strong>{{ $errors->first('age') }}</strong>
                            </span>
                                @endif
                                {{--<span class="middle">Inline help text</span>--}}
                    </span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group col-sm-6">
                        <label class="col-sm-4 control-label no-padding-right" for="relg"> ধর্ম *</label>

                        <div class="col-sm-8">
                            <select name='relg' class="col-xs-10 col-sm-12" id="relg" required>
                                <option value="">--- বাছাই করুন ---</option>
                                <?php foreach ($relrg_list as $id => $name) { ?>
                                    <?php echo "<option value='{$id}'" . ($registrationData->relg == $id ? ('selected="selected"') : '') . ">{$name}</option>"; ?>
                                <?php }?>
                            </select>
                            <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('relg'))
                                    <span class="help-block middle">
                                <strong>{{ $errors->first('relg') }}</strong>
                            </span>
                                @endif
                                {{--<span class="middle">Inline help text</span>--}}
                    </span>
                        </div>
                    </div>
                    <div class="form-group col-sm-6">
                        <label class="col-sm-4 control-label no-padding-right" for="nation"> জাতীয়তা *</label>

                        <div class="col-sm-8">
                            <input type="text" id="nation" placeholder="i.e. Bangladeshi" name="nation"
                                   value="{{ $registrationData->nation }}" class="col-xs-10 col-sm-12" required/>
                            <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('nation'))
                                    <span class="help-block middle">
                                <strong>{{ $errors->first('nation') }}</strong>
                            </span>
                                @endif
                                {{--<span class="middle">Inline help text</span>--}}
                    </span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group col-sm-6">
                        <label class="col-sm-4 control-label no-padding-right" for="gender"> লিঙ্গ *</label>

                        <div class="col-sm-8">
                            <select name='gender' class="col-xs-10 col-sm-12" id="gender" required>
                                <option value="">--- বাছাই করুন ---</option>
                                <?php foreach ($gender_list as $id => $name) { ?>
                                    <?php echo "<option value='{$id}'" . ($registrationData->gender == $id ? ('selected="selected"') : '') . ">{$name}</option>"; ?>
                                <?php }?>
                            </select>
                            <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('gender'))
                                    <span class="help-block middle">
                                <strong>{{ $errors->first('gender') }}</strong>
                            </span>
                                @endif
                                {{--<span class="middle">Inline help text</span>--}}
                    </span>
                        </div>
                    </div>
                    <div class="form-group col-sm-6">
                        <label class="col-sm-4 control-label no-padding-right" for="married"> বৈবাহিক অবস্থা
                            *</label>

                        <div class="col-sm-8">
                            <select name='married' class="col-xs-10 col-sm-12" id="married" required>
                                <option value="">--- বাছাই করুন ---</option>
                                <?php foreach ($marital_list as $id => $name) { ?>
                                    <?php echo "<option value='{$id}'" . ($registrationData->married == $id ? ('selected="selected"') : '') . ">{$name}</option>"; ?>
                                <?php }?>
                            </select>
                            <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('married'))
                                    <span class="help-block middle">
                                <strong>{{ $errors->first('married') }}</strong>
                            </span>
                                @endif
                                {{--<span class="middle">Inline help text</span>--}}
                    </span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="space-4"></div>
            <h3 class="header smaller lighter blue">
                অভিজ্ঞতা
            </h3>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group col-sm-6">
                        <label class="col-sm-4 control-label no-padding-right" for="cur_job"> বর্তমান
                            পেশা </label>

                        <div class="col-sm-8">
                            <input type="text" id="cur_job" placeholder=""
                                   name="cur_job"
                                   value="{{ $registrationData->cur_job }}" class="col-xs-10 col-sm-12"/>
                            <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('cur_job'))
                                    <span class="help-block middle">
                                <strong>{{ $errors->first('cur_job') }}</strong>
                            </span>
                                @endif
                                {{--<span class="middle">Inline help text</span>--}}
                    </span>
                        </div>
                    </div>
                    <div class="form-group col-sm-6">
                        <label class="col-sm-4 control-label no-padding-right" for="prev_trng_name"> সর্বশেষ
                            প্রশিক্ষণ(যদি
                            থাকে) </label>

                        <div class="col-sm-8">
                            <input type="text" id="prev_trng_name" placeholder=""
                                   name="prev_trng_name"
                                   value="{{ $registrationData->prev_trng_name }}" class="col-xs-10 col-sm-12"/>
                            <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('prev_trng_name'))
                                    <span class="help-block middle">
                                <strong>{{ $errors->first('prev_trng_name') }}</strong>
                            </span>
                                @endif
                                {{--<span class="middle">Inline help text</span>--}}
                    </span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group col-sm-12">
                        <label class="col-sm-3 control-label no-padding-right" for="prev_exp"> পূর্ব প্রশিক্ষণের
                            বিবরণ
                            (যদি থাকে) </label>

                        <div class="col-sm-9">
                        <textarea id="prev_exp" placeholder="পূর্ব প্রশিক্ষণের বিবরণ" name="prev_exp"
                                  class="col-xs-10 col-sm-12"><?php echo $registrationData->prev_exp ?> </textarea>
                            <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('prev_exp'))
                                    <span class="help-block middle">
                                <strong>{{ $errors->first('prev_exp') }}</strong>
                            </span>
                                @endif
                                {{--<span class="middle">Inline help text</span>--}}
                    </span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="space-4"></div>
            <div class="space-4"></div>
            <div class="clearfix form-actions">
                <div class="col-md-offset-3 col-md-9">
                    <button class="btn btn-info" type="submit">
                        <i class="ace-icon fa fa-check bigger-110"></i>
                        Submit
                    </button>

                    &nbsp; &nbsp; &nbsp;
                    <button class="btn" type="reset">
                        <i class="ace-icon fa fa-undo bigger-110"></i>
                        Reset
                    </button>
                </div>
            </div>
        </form>
    </div>
@stop


@section('custom_style')

@stop

@section('custom_script')
    <script type="text/javascript">

        /****************************/
        /****************************/
        /*  File Upload Section */
        /****************************/
        /****************************/

        $('#image').ace_file_input({
            style: 'well',
            btn_choose: 'Drop files here or click to choose',
            btn_change: null,
            no_icon: 'ace-icon fa fa-cloud-upload',
            droppable: true,
            thumbnail: 'small'//large | fit
            //,icon_remove:null//set null, to hide remove/reset button
            /**,before_change:function(files, dropped) {
						//Check an example below
						//or examples/file-upload.html
						return true;
					}*/
            /**,before_remove : function() {
						return true;
					}*/
            ,
            preview_error: function (filename, error_code) {
                //name of the file that failed
                //error_code values
                //1 = 'FILE_LOAD_FAILED',
                //2 = 'IMAGE_LOAD_FAILED',
                //3 = 'THUMBNAIL_FAILED'
                //alert(error_code);
            }

        }).on('change', function () {
            //console.log($(this).data('ace_input_files'));
            //console.log($(this).data('ace_input_method'));
        });

        var whitelist_ext, whitelist_mime;
        var btn_choose;
        var no_icon;

        var file_input = $('#image');

        file_input
            .ace_file_input('update_settings',
                {
                    'btn_choose': btn_choose,
                    'no_icon': no_icon,
                    'allowExt': whitelist_ext,
                    'allowMime': whitelist_mime
                });

        file_input.ace_file_input('reset_input');

        file_input
            .off('file.error.ace')
            .on('file.error.ace', function (e, info) {
                //console.log(info.file_count);//number of selected files
                //console.log(info.invalid_count);//number of invalid files
                //console.log(info.error_list);//a list of errors in the following format

                //info.error_count['ext']
                //info.error_count['mime']
                //info.error_count['size']

                //info.error_list['ext']  = [list of file names with invalid extension]
                //info.error_list['mime'] = [list of file names with invalid mimetype]
                //info.error_list['size'] = [list of file names with invalid size]


                /**
                 if( !info.dropped ) {
							//perhapse reset file field if files have been selected, and there are invalid files among them
							//when files are dropped, only valid files will be added to our file array
							e.preventDefault();//it will rest input
						}
                 */


                //if files have been selected (not dropped), you can choose to reset input
                //because browser keeps all selected files anyway and this cannot be changed
                //we can only reset file field to become empty again
                //on any case you still should check files with your server side script
                //because any arbitrary file can be uploaded by user and it's not safe to rely on browser-side measures
            });


        /****************************/
        /****************************/
        /*  End */
        /****************************/
        /****************************/

        //datepicker plugin
        $('.date-picker').datepicker({
            autoclose: true,
            todayHighlight: true
        })
    </script>
@stop