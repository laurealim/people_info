@extends('layouts.admin')

@section('title')
    Add Property
@stop

@section('breadcrumb')
    <ul class="breadcrumb">
        <li>
            <i class="ace-icon fa fa-bars home-icon"></i>
            <a href="{{ route('property.adminList') }}">Property List</a>
        </li>

        <li>
            <a href="{{ route('property.adminForm') }}">Property Add</a>
        </li>
        {{--<li class="active">User Profile</li>--}}
    </ul>
@stop

@section('page_header')
    <h1>Add Property</h1>
@stop

@section('content')
    <div class="col-xs-12">
        <h3 class="header smaller lighter blue">
            Basic Information
        </h3>

        <form class="form-horizontal" role="form" action="{{ route('property.adminStore') }}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Property Name *</label>

                <div class="col-sm-9">
                    <input type="text" id="form-field-1" placeholder="Property Name" name="property_name"
                           class="col-xs-10 col-sm-5" value="{{old('property_name')}}"/>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('property_name'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('property_name') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="space-4"></div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Country Name *</label>

                <div class="col-sm-9">
                    <select name='country_id' class="col-xs-10 col-sm-5" id="country_list">
                        <option value="">-- Please Select One --</option>
                        @foreach($countryData as $countryName)
                            <option value="{{ $countryName->id }}" }>{{ $countryName->name }}</option>
                        @endforeach
                    </select>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('country_id'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('country_id') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="space-4"></div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="division_list"> Division Name *</label>

                <div class="col-sm-9">
                    <select name='division_id' class="col-xs-10 col-sm-5" id="division_list">
                        <option value="">-- Please Select One --</option>
                        @foreach($divisionData as $divisionName)
                            <option value="{{ $divisionName->id }}" }>{{ $divisionName->name }}</option>
                        @endforeach
                    </select>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('division_id'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('division_id') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="space-4"></div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="district_list"> District Name *</label>

                <div class="col-sm-9">
                    <select name='district_id' class="col-xs-10 col-sm-5" id="district_list">
                        <option value="">-- Please Select One --</option>
                        @foreach($districtData as $districtName)
                            <option value="{{ $districtName->id }}" }>{{ $districtName->name }}</option>
                        @endforeach
                    </select>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('district_id'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('district_id') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="space-4"></div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Address *</label>

                <div class="col-sm-9">
                    <textarea id="form-field-1" placeholder="Address" name="address"
                              class="col-xs-10 col-sm-5">{{old('address')}}</textarea>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('address'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('address') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="space-4"></div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="form-field-1-1"> Status </label>

                <div class="col-sm-9">
                    <select name='status' class="col-xs-10 col-sm-5" id="form-field-select-1">
                        <option value="1">Active</option>
                        <option value="2">Inactive</option>
                        <option value="0">Deleted</option>
                    </select>
                </div>
            </div>

            <div class="space-4"></div>

            <h3 class="header smaller lighter blue">
                Property Details Information
            </h3>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="builders"> Property Builders Name *</label>

                <div class="col-sm-9">
                    <input type="text" id="builders" placeholder="Property Builders Name" name="builders"
                           class="col-xs-10 col-sm-5" value="{{old('builders')}}"/>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('builders'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('builders') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="owner"> Property Owner*</label>

                <div class="col-sm-9">
                    <select name='owner' class="col-xs-10 col-sm-5" id="owner">
                        <option value="">-- Please Select One --</option>
                        @foreach($clientList as $clientName)
                            <option value="{{ $clientName->id }}">{{ $clientName->first_name }} {{ $clientName->last_name }}</option>
                        @endforeach
                    </select>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('owner'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('owner') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="total_unit"> Total Units *</label>

                <div class="col-sm-9">
                    <input type="number" id="total_unit" placeholder="1,2,3..." name="total_unit"
                           class="col-xs-10 col-sm-5" value="{{old('total_unit')}}"/>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('total_unit'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('total_unit') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="total_floor"> Total Floor *</label>

                <div class="col-sm-9">
                    <input type="number" id="total_floor" placeholder="10,20,30,40...." name="total_floor"
                           class="col-xs-10 col-sm-5" value="{{old('total_floor')}}"/>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('total_floor'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('total_floor') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="unit_floor"> Unit Per Floor *</label>

                <div class="col-sm-9">
                    <input type="number" id="unit_floor" placeholder="1,2,3..." name="unit_floor"
                           class="col-xs-10 col-sm-5" value="{{old('unit_floor')}}"/>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('unit_floor'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('unit_floor') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right">Facilities </label>

                <div class="checkbox col-sm-2 col-lg-2 col-md-3">
                    <label>
                        <input name="has_gas" id="has_gas" type="checkbox" class="ace ace-checkbox-2" value="1"/>
                        <span class="lbl"> &nbsp; &nbsp;Gas</span>
                    </label>
                </div>

                <div class="checkbox col-sm-3 col-lg-2 col-md-3">
                    <label>
                        <input name="has_lift" id="has_lift" type="checkbox" class="ace ace-checkbox-2" value="1"/>
                        <span class="lbl"> &nbsp; &nbsp;Lift</span>
                    </label>
                </div>

                <div class="checkbox col-sm-3 col-lg-2 col-md-3">
                    <label>
                        <input name="has_generator" id="has_generator" class="ace ace-checkbox-2" type="checkbox"
                               value="1"/>
                        <span class="lbl"> &nbsp; &nbsp;Generator</span>
                    </label>
                </div>
            </div>

            <div class="space-4"></div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Property Description</label>

                <div class="col-sm-9">
                    <textarea id="form-field-1" placeholder="Property Description" name="p_description"
                              class="col-xs-10 col-sm-5">{{old('p_description')}}</textarea>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('p_description'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('p_description') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="space-4"></div>

            <div class="form-group">
                <label class="control-label col-xs-12 col-sm-3 no-padding-right "
                       for="logo">Property Images <span class="required">*</span></label>

                <div class="col-xs-10 col-sm-5">
                    <input type="file" id="id-input-file-3" class="col-xs-10 col-sm-5" name="photo"/>
                </div>
            </div>

            <div class="space-4"></div>

            <div class="clearfix form-actions">
                <div class="col-md-offset-3 col-md-9">
                    <button class="btn btn-info" type="submit">
                        <i class="ace-icon fa fa-check bigger-110"></i>
                        Submit
                    </button>

                    &nbsp; &nbsp; &nbsp;
                    <button class="btn" type="reset">
                        <i class="ace-icon fa fa-undo bigger-110"></i>
                        Reset
                    </button>
                </div>
            </div>
        </form>
    </div>
@stop


@section('custom_style')
    <style type="text/css">
        .help-block{
            color: #a34335;
            font-weight: bold;
        }
    </style>
@stop

@section('custom_script')
    <script type="text/javascript">

        /****************************/
        /****************************/
        /*  File Upload Section */
        /****************************/
        /****************************/

        $('#id-input-file-3').ace_file_input({
            style: 'well',
            btn_choose: 'Drop files here or click to choose',
            btn_change: null,
            no_icon: 'ace-icon fa fa-cloud-upload',
            droppable: true,
            thumbnail: 'small'//large | fit
            //,icon_remove:null//set null, to hide remove/reset button
            /**,before_change:function(files, dropped) {
						//Check an example below
						//or examples/file-upload.html
						return true;
					}*/
            /**,before_remove : function() {
						return true;
					}*/
            ,
            preview_error: function (filename, error_code) {
                //name of the file that failed
                //error_code values
                //1 = 'FILE_LOAD_FAILED',
                //2 = 'IMAGE_LOAD_FAILED',
                //3 = 'THUMBNAIL_FAILED'
                //alert(error_code);
            }

        }).on('change', function () {
            //console.log($(this).data('ace_input_files'));
            //console.log($(this).data('ace_input_method'));
        });

        var whitelist_ext, whitelist_mime;
        var btn_choose;
        var no_icon;

        var file_input = $('#id-input-file-3');

        file_input
                .ace_file_input('update_settings',
                {
                    'btn_choose': btn_choose,
                    'no_icon': no_icon,
                    'allowExt': whitelist_ext,
                    'allowMime': whitelist_mime
                });

        file_input.ace_file_input('reset_input');

        file_input
                .off('file.error.ace')
                .on('file.error.ace', function (e, info) {
                    //console.log(info.file_count);//number of selected files
                    //console.log(info.invalid_count);//number of invalid files
                    //console.log(info.error_list);//a list of errors in the following format

                    //info.error_count['ext']
                    //info.error_count['mime']
                    //info.error_count['size']

                    //info.error_list['ext']  = [list of file names with invalid extension]
                    //info.error_list['mime'] = [list of file names with invalid mimetype]
                    //info.error_list['size'] = [list of file names with invalid size]


                    /**
                     if( !info.dropped ) {
							//perhapse reset file field if files have been selected, and there are invalid files among them
							//when files are dropped, only valid files will be added to our file array
							e.preventDefault();//it will rest input
						}
                     */


                    //if files have been selected (not dropped), you can choose to reset input
                    //because browser keeps all selected files anyway and this cannot be changed
                    //we can only reset file field to become empty again
                    //on any case you still should check files with your server side script
                    //because any arbitrary file can be uploaded by user and it's not safe to rely on browser-side measures
                });


        /****************************/
        /****************************/
        /*  End */
        /****************************/
        /****************************/

        $('#country_list').on('change', function () {
            var countryID = $(this).val();
            var token = $("input[name='_token']").val();
            var url = "{{ route('division.adminDivisionSelectAjaxList') }}";
            if (countryID) {
                $.ajax({
                    url: url,
                    method: 'POST',
                    data:{countryID:countryID, _token:token},
                    dataType: "json",
                    success: function (data) {
                        $('select[name="division_id"]').empty();
                        $.each(data, function (key, value) {
                            $('select[name="division_id"]').append('<option value="' + key + '">' + value + '</option>');
                        });
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown) {
                        alert("Status: " + textStatus); alert("Error: " + errorThrown);
                        console.log(XMLHttpRequest);
                        console.log(textStatus);
                        console.log(errorThrown);
                    }
                });
            } else {
                $('select[name="division_id"]').empty();
            }
        });

        $('#division_list').on('change', function () {
            var countryID = $('#country_list').val();
            var divisionID = $(this).val();
            var token = $("input[name='_token']").val();
            var url = "{{ route('district.adminDistrictSelectAjaxList') }}";
            if (countryID) {
                $.ajax({
                    url: url,
                    method: 'POST',
                    data:{countryID:countryID,divisionID:divisionID, _token:token},
                    dataType: "json",
                    success: function (data) {
                        $('select[name="district_id"]').empty();
                        $.each(data, function (key, value) {
                            $('select[name="district_id"]').append('<option value="' + key + '">' + value + '</option>');
                        });
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown) {
                        alert("Status: " + textStatus); alert("Error: " + errorThrown);
                        console.log(XMLHttpRequest);
                        console.log(textStatus);
                        console.log(errorThrown);
                    }
                });
            } else {
                $('select[name="district_id"]').empty();
            }
        });

    </script>
@stop