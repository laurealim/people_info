@extends('layouts.admin')

@section('title')
    Add SSNP
@stop

@section('breadcrumb')
    <ul class="breadcrumb">
        <li>
            <i class="ace-icon fa fa-bars home-icon"></i>
            <a href="{{ route('ssnp.adminList') }}">SSNP List</a>
        </li>

        <li>
            <a href="{{ route('ssnp.adminForm') }}">SSNP Add</a>
        </li>
        {{--<li class="active">User Profile</li>--}}
    </ul>
@stop

@section('page_header')
    <h1>Add Package</h1>
@stop

@section('content')
    <div class="col-xs-12">
        <form class="form-horizontal" role="form" action="{{ route('ssnp.adminStore') }}" method="POST">
            {{ csrf_field() }}
            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="ssnp_name"> SSNP Name *</label>

                <div class="col-sm-9">
                    <input type="text" id="ssnp_name" placeholder="SSNP Name" name="ssnp_name"
                           class="col-xs-10 col-sm-5" required/>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('ssnp_name'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('ssnp_name') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="space-4"></div>

            <div class="clearfix form-actions">
                <div class="col-md-offset-3 col-md-9">
                    <button class="btn btn-info" type="submit">
                        <i class="ace-icon fa fa-check bigger-110"></i>
                        Submit
                    </button>

                    &nbsp; &nbsp; &nbsp;
                    <button class="btn" type="reset">
                        <i class="ace-icon fa fa-undo bigger-110"></i>
                        Reset
                    </button>
                </div>
            </div>
        </form>
    </div>
@stop


@section('custom_style')

@stop

@section('custom_script')
    <script type="text/javascript">
        //datepicker plugin
        $('.date-picker').datepicker({
            autoclose: true,
            todayHighlight: true
        });
    </script>

@stop