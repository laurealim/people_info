<table id="dynamic-table" class="table table-striped table-bordered table-hover" style="margin-bottom: 0px;">
    <thead>
    <tr>
        <th class="center" width="5%">
            <label class="pos-rel">
                {{--<input type="checkbox" class="ace"/>--}}
                {{--<span class="lbl"></span>--}}
            </label>
        </th>
        <th width="30%">Event Type Name</th>
        <th width="25%">Created By</th>
        <th width="25%">Status</th>
        <th class="center" width="15%">Action</th>
    </tr>
    </thead>

    <tbody>

    <?php $pageNo = (($events->currentPage() - 1) * $events->perPage()) + 1 ?>
    @foreach ($events as $event)
        <tr>
            <td class="center">
                <label class="pos-rel">
                    {{--<input type="checkbox" class="ace"/>--}}
                    <span class="lbl">{{ $pageNo++ }}</span>
                </label>
            </td>
            <td>
                {{ $event->name }}
            </td>
            <td> {{ $event->first_name}} </td>
            <td>
                <?php if($event->status == 1){ ?>
                <span class='label label-success'>Active</span>
                <?php } else{ ?>
                <span class='label label-danger'>Inactive</span>
                <?php } ?>
            </td>
            <td class="center">
                <div class="hidden-sm hidden-xs action-buttons">
                    {{--<a class="blue" href="{{ route('') }}">--}}
                    {{--<i class="ace-icon fa fa-search-plus bigger-200"></i>--}}
                    {{--</a>--}}

                    {{--&nbsp;&nbsp;| &nbsp;&nbsp;--}}

                    <a class="green" href="{{ route('event.adminEdit',$event->id) }}">
                        <i class="ace-icon fa fa-pencil bigger-200"></i>
                    </a>

                    &nbsp;&nbsp;| &nbsp;&nbsp;

                    <a class="red deletebtn" href="{{ route('event.adminDelete',$event->id) }}" id={{ $event->id }}>
                        <i class="ace-icon fa fa-trash-o bigger-200"></i>
                    </a>

                    {{--&nbsp;&nbsp;| &nbsp;&nbsp;--}}

                    {{--<a class="red deletebtn" href="{{ route('country.adminDelete',$country->id) }}" id={{ $country->id }}>--}}
                    {{--<i class="ace-icon fa fa-trash-o bigger-200"></i>--}}
                    {{--</a>--}}
                </div>

            </td>
        </tr>
    @endforeach
    </tbody>
</table>


<div class="row">
    <div class="col-xs-6">
        <div class="dataTables_info" id="dynamic-table_info" role="status" aria-live="polite">
            Showing {{ (($events->currentPage() - 1) * $events->perPage()) + 1 }}
            to {{ (($events->currentPage() - 1) * $events->perPage()) + $events->perPage() }} of
            {{ $events->total() }} entries
        </div>
    </div>
    <div class="col-xs-6">
        <div class="dataTables_paginate paging_simple_numbers" id="dynamic-table_paginate">
            {{ $events->links("vendor.pagination.custom") }}
        </div>
    </div>
</div>
