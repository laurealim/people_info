<div id="dynamic-table_wrapper" class="dataTables_wrapper form-inline no-footer">
    <table id="dynamic-table" class="table table-striped table-bordered table-hover" style="margin-bottom: 0px;">
        <thead>
        <tr>
            <th class="center" width="10%">
                <label class="pos-rel">
                    {{--<input type="checkbox" class="ace"/>--}}
                    {{--<span class="lbl"></span>--}}
                    Sr. No.
                </label>
            </th>
            <th width="20%">Name</th>
            <th width="10%">Guard Name</th>
            <th width="10%">Controller Name</th>
            <th width="10%">Action Name</th>
            <th width="20%">Description</th>
            <th class="center" width="20%">Action</th>
        </tr>
        </thead>

        <tbody>

        <?php $pageNo = (($permissionList->currentPage() - 1) * $permissionList->perPage()) + 1 ?>

        @foreach ($permissionList as $permission)
            <tr>
                <td class="center">
                    <label class="pos-rel">
                        {{--<input type="checkbox" class="ace"/>--}}
                        <span class="lbl">{{ $pageNo++ }}</span>
                    </label>
                </td>
                <td>
                    {{ $permission->name}}
                </td>
                <td>
                    {{ $permission->guard_name}}
                </td>
                <td>
                    {{ $permission->controller}}
                </td>
                <td>
                    {{ $permission->action}}
                </td>
                <td>
                    {{ $permission->desc}}
                </td>
                <td class="center">
                    <div class="hidden-sm hidden-xs action-buttons">
                        {{--<a class="blue" href="{{ route('') }}">--}}
                        {{--<i class="ace-icon fa fa-search-plus bigger-200"></i>--}}
                        {{--</a>--}}

                        {{--&nbsp;&nbsp;| &nbsp;&nbsp;--}}

                        <a class="green" href="{{ route('permission.adminEdit',$permission->id) }}">
                            <i class="ace-icon fa fa-pencil bigger-200"></i>
                        </a>

                        &nbsp;&nbsp;| &nbsp;&nbsp;

                        <a class="red deletebtn" href="{{ route('permission.adminDestroy',$permission->id) }}" id={{ $permission->id }}>
                        <i class="ace-icon fa fa-trash-o bigger-200"></i>
                        </a>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div class="row">
        <div class="col-xs-6">
            <div class="dataTables_info" id="dynamic-table_info" role="status" aria-live="polite">
                Showing {{ (($permissionList->currentPage() - 1) * $permissionList->perPage()) + 1 }}
                to {{ (($permissionList->currentPage() - 1) * $permissionList->perPage()) + $permissionList->perPage() }} of
                {{ $permissionList->total() }} entries
            </div>
        </div>
        <div class="col-xs-6">
            <div class="dataTables_paginate paging_simple_numbers" id="dynamic-table_paginate">
                {{ $permissionList->links("vendor.pagination.custom") }}
            </div>
        </div>
    </div>
</div>
