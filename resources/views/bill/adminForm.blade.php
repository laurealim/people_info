@extends('layouts.admin')

@section('title')
    Add Bill
@stop

@section('breadcrumb')
    <ul class="breadcrumb">
        <li>
            <i class="ace-icon fa fa-bars home-icon"></i>
            <a href="{{ route('bill.adminList') }}">Bill List</a>
        </li>

        <li>
            <a href="{{ route('bill.adminForm') }}">Bill Add</a>
        </li>
        {{--<li class="active">User Profile</li>--}}
    </ul>
@stop

@section('page_header')
    <h1>Add Bill</h1>
@stop

@section('content')
    <div class="col-xs-12">
        <h3 class="header smaller lighter blue">
            Basic Information
        </h3>

        <form class="form-horizontal" role="form" action="{{ route('bill.adminStore') }}" method="POST"
              enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group">
                <div class="col-sm-6">
                    <label class="col-sm-4 control-label no-padding-right" for="form-field-1-1"> Tenant List
                        * </label>

                    <div class="col-sm-8">
                        <select name='client_id' class="col-xs-10 col-sm-8" id="form-field-select-1">
                            <option value="0">---- Please Select One ----</option>
                            @foreach($tenantList as $id => $name)
                                <option value="{{ $id }}">{{ $name }}</option>
                            @endforeach

                        </select>
                        @if ($errors->has('client_id'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('client_id') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="col-sm-6">
                    <label class="col-sm-4 control-label no-padding-right" for="builders"> Apartment Number *</label>

                    <div class="col-sm-8">
                        <select name='apt_id' class="col-xs-10 col-sm-8" id="form-field-select-1">
                        </select>
                        @if ($errors->has('apt_id'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('apt_id') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
            </div>

            <div class="space-4"></div>

            <h3 class="header smaller lighter blue">
                Bill Details Information
            </h3>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="apt_size">Monthly Rent Amount *</label>

                <div class="col-sm-9">
                    <input type="number" id="rent_amount" placeholder="10000, 15000, 20000...." name="rent_amount"
                           class="col-xs-10 col-sm-5" readonly=true/>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('rent_amount'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('rent_amount') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="service_charge"> Service Charge *</label>

                <div class="col-sm-9">
                    <input type="number" id="service_charge" placeholder="3000, 4000, 5000...." name="service_charge"
                           class="col-xs-10 col-sm-5" readonly=true/>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('service_charge'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('service_charge') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="form-group is_initial_bill">
                <label class="col-sm-3 control-label no-padding-right" for="security_bill"> Security Charge *</label>

                <div class="col-sm-9">
                    <input type="number" id="security_bill" placeholder="10000, 15000, 20000...." name="security_bill"
                           class="col-xs-10 col-sm-5" readonly=true/>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('security_bill'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('security_bill') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="form-group is_initial_bill">
                <label class="col-sm-3 control-label no-padding-right" for="security_bill_month"> Security Charge Month
                    *</label>

                <div class="col-sm-9">
                    <input type="number" id="security_bill_month" placeholder="1,2,3..." name="security_bill_month"
                           class="col-xs-10 col-sm-5" readonly=true/>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('security_bill_month'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('security_bill_month') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="due_amount"> Due Amount</label>

                <div class="col-sm-9">
                    <input type="number" id="due_amount" placeholder="1,2,3..." name="due_amount"
                           class="col-xs-10 col-sm-5" readonly=true/>
                    <span class="help-inline col-xs-12 col-sm-7">
                        {{--@if ($errors->has('security_bill_month'))--}}
                        {{--<span class="help-block middle">--}}
                        {{--<strong>{{ $errors->first('security_bill_month') }}</strong>--}}
                        {{--</span>--}}
                        {{--@endif--}}
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="total_bill"> Total Bill *</label>

                <div class="col-sm-9">
                    <input type="text" id="total_bill" placeholder="" name="total_bill"
                           class="col-xs-10 col-sm-5" readonly=true/>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('total_bill'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('total_bill') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="space-4"></div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="billing_month"> Billing Month *</label>

                <div class="col-sm-6">
                    <select name='billing_month' class="col-xs-10 col-sm-8" id="billing_month">
                        <option value="0">---- Please Select One ----</option>
                        @foreach($monthList as $month_id => $months)
                            <option value="{{ $month_id }}">{{ $months }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('billing_month'))
                        <span class="help-block middle">
                                <strong>{{ $errors->first('billing_month') }}</strong>
                            </span>
                    @endif
                </div>
            </div>

            <div class="space-4"></div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="billing_year"> Billing year *</label>

                <div class="col-sm-6">
                    <select name='billing_year' class="col-xs-10 col-sm-8" id="billing_year">
                        <option value="0">---- Please Select One ----</option>
                        @foreach($yearList as $year_id => $year)
                            <option value="{{ $year_id }}">{{ $year }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('billing_year'))
                        <span class="help-block middle">
                                <strong>{{ $errors->first('billing_year') }}</strong>
                            </span>
                    @endif
                </div>
            </div>

            <div class="space-4"></div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Comments</label>

                <div class="col-sm-9">
                    <textarea id="comments" placeholder="Apartment Description" name="comments"
                              class="col-xs-10 col-sm-5"></textarea>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('comments'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('comments') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="space-4"></div>

            <div class="clearfix form-actions">
                <div class="col-md-offset-3 col-md-9">
                    <button class="btn btn-info" type="submit">
                        <i class="ace-icon fa fa-check bigger-110"></i>
                        Submit
                    </button>

                    &nbsp; &nbsp; &nbsp;
                    <button class="btn" type="reset">
                        <i class="ace-icon fa fa-undo bigger-110"></i>
                        Reset
                    </button>
                </div>
            </div>
        </form>
    </div>
@stop


@section('custom_style')

@stop

@section('custom_script')
    <script type="text/javascript">

        function clearData() {
            $("#rent_amount").val("");
            $("#service_charge").val("");
            $("#security_bill").val("");
            $("#security_bill_month").val("");
            $("#total_bill").val("");
            $("#due_amount").val("");
            $("#comments").val("");
        }

        $("select[name='client_id']").change(function () {
            clearData();
            var client_id = $(this).val();
            var token = $("input[name='_token']").val();
            var url = "{{ route('bill.selectAjax') }}";
//            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            $.ajax({
                url: url,
                method: 'POST',
                data: {client_id: client_id, _token: token},
                success: function (data) {
                    console.log(data);
                    $("select[name='apt_id'").html('');
                    $("select[name='apt_id'").html(data.options);
                }
            });
        });

        $("select[name='apt_id']").change(function () {
            var is_initial = true;
            clearData();
            var aptId = $(this).val();
            var client_id = $("select[name='client_id']").val();

            var token = $("input[name='_token']").val();
            var url = "{{ route('bill.getAptDataAjax') }}";

            $.ajax({
                url: url,
                method: 'POST',
                data: {aptId: aptId, client_id: client_id, _token: token},
                success: function (data) {
                    console.log(data);
                    console.log(data.aptData.rent_amount);

                    if (data.error == 1) {
                        alert("This client has some pending ledger bill for this particular selected apartment. \n Please remove or confirm those bills first from 'Ledger List' section");
                    } else {
                        if (data.size >= 1) {
                            is_initial = false;
                        }

                        if (is_initial) {
                            $(".is_initial_bill").show();
                        }
                        else {
                            $(".is_initial_bill").hide();
                        }

//                    if (data.bill_type == 1) {
//                        $(".is_initial_bill").show();
//                    } else {
//                        $(".is_initial_bill").hide();
//                    }

                        var rent_amount = data.aptData.rent_amount;
                        var service_charge = data.aptData.service_charge;
                        var security_bill = data.aptData.security_bill;
                        var security_bill_month = data.aptData.security_bill_month;
                        var comments = data.aptData.comments;
                        var due_amount = data.dueAmount * (-1);

                        var total_bill = (rent_amount + service_charge + due_amount);
                        if (is_initial) {
//                    if (data.bill_type == 1) {
                            total_bill = total_bill + (security_bill * security_bill_month);
                        }

                        $("#rent_amount").val(rent_amount);
                        $("#service_charge").val(service_charge);
                        $("#security_bill").val(security_bill);
                        $("#security_bill_month").val(security_bill_month);
                        $("#total_bill").val(total_bill);
                        $("#due_amount").val(due_amount);
                        $("#comments").val(comments);
                    }
                }
            });
        });
    </script>
@stop