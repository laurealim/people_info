<table id="dynamic-table" class="table table-striped table-bordered table-hover" style="margin-bottom: 0px;">
    <thead>
    <tr>
        <th class="center" width="3%">
            {{--<label class="pos-rel">--}}
            {{--<input type="checkbox" class="ace"/>--}}
            {{--<span class="lbl"></span>--}}
            {{--</label>--}}
        </th>
        <th width="7%">Apartment Number</th>
        <th width="10%">Client Name</th>
        <th width="10%">Owner</th>
        <th width="10%">Billing Month</th>
        <th width="10%">Current Bill (BDT)</th>
        <th width="10%">Previous Due Amount(BDT)</th>
        <th width="10%">Total Payable Bill Amount(BDT)</th>
        <th width="10%">Created By</th>
        <th width="10%">Status</th>
        <th class="center" width="10%">Action</th>
    </tr>
    </thead>

    <tbody>

    <?php $pageNo = (($billLists->currentPage() - 1) * $billLists->perPage()) + 1; ?>
    @foreach ($billLists as $billList)
        <?php //pr($billList);?>
        <tr>
            <td class="center">
                <label class="pos-rel">
                    {{--<input type="checkbox" class="ace"/>--}}
                    <span class="lbl">{{ $pageNo++ }}</span>
                </label>
            </td>
            <td>
                {{ $billList->apt_number }}
            </td>
            <td>
                {{ \App\User::getDataById($billList->client_id)->first_name.' '.\App\User::getDataById($billList->client_id)->last_name }}
            </td>
            <td>
                {{ $billList->first_name.' '.$billList->last_name }}
            </td>
            <td>
                {{ date_format($billList->created_at,"F, Y") }}
            </td>
            <td>
                {{ number_format($billList->rent_amount + $billList->service_charge + ($billList->security_bill * $billList->security_bill_month),2)  }}
            </td>
            <td>
                {{ number_format(($billList->current_amount * (-1) - ($billList->rent_amount + $billList->service_charge + ($billList->security_bill * $billList->security_bill_month))),2) }}
            </td>
            <td>
                {{ number_format($billList->current_amount * (-1),2) }}
            </td>
            <td>
                {{ \App\User::getDataById($billList->created_by)->first_name.' '.\App\User::getDataById($billList->created_by)->last_name }}
            </td>
            <td>
                <?php if($billList->status == 1){ ?>
                <span class='label label-danger'>Pending</span>
                <?php } elseif($billList->status == 2){?>
                <span class='label label-success'>Approved</span>
                <?php }?>
            </td>
            <td class="center">
                <div class="hidden-sm hidden-xs action-buttons">
                    {{--<a class="blue" href="{{ route('') }}">--}}
                    {{--<i class="ace-icon fa fa-search-plus bigger-200"></i>--}}
                    {{--</a>--}}

                    {{--&nbsp;&nbsp;| &nbsp;&nbsp;--}}

                    <?php
                        if($billList->status != 2){?>


                    <a class="green" href="{{ route('bill.adminEdit',$billList->id) }}">
                        <i class="ace-icon fa fa-pencil bigger-150"></i>
                    </a>
                    &nbsp;| &nbsp;
                    <a class="red deletebtn" href="{{ route('bill.adminDelete',$billList->id) }}"
                       id={{ $billList->id }}>
                        <i class="ace-icon fa fa-trash-o bigger-150"></i>
                    </a>
                    <?php }
                    ?>

                    {{--@if($billList->is_assigned < 1)--}}
                        {{--&nbsp;| &nbsp;--}}

                        {{--<a class="red blue assign_client" href="#" title="Assign Client"--}}
                           {{--id={{ $billList->id }}>--}}
                            {{--<i class="ace-icon fa fa-user-plus bigger-150"></i>--}}
                        {{--</a>--}}
                    {{--@endif--}}

                    {{--&nbsp;&nbsp;| &nbsp;&nbsp;--}}

                    {{--<a class="red deletebtn" href="{{ route('country.adminDelete',$country->id) }}" id={{ $country->id }}>--}}
                    {{--<i class="ace-icon fa fa-trash-o bigger-200"></i>--}}
                    {{--</a>--}}
                </div>

            </td>
        </tr>
    @endforeach
    </tbody>
</table>


<div class="row">
    <div class="col-xs-6">
        <div class="dataTables_info" id="dynamic-table_info" role="status" aria-live="polite">
            Showing {{ (($billLists->currentPage() - 1) * $billLists->perPage()) + 1 }}
            to {{ (($billLists->currentPage() - 1) * $billLists->perPage()) + $billLists->perPage() }} of
            {{ $billLists->total() }} entries
        </div>
    </div>
    <div class="col-xs-6">
        <div class="dataTables_paginate paging_simple_numbers" id="dynamic-table_paginate">
            {{ $billLists->links("vendor.pagination.custom") }}
        </div>
    </div>
</div>

<style type="text/css">
    .facility {
        font-weight: bold;
    }
</style>