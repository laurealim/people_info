{{--<div id="dynamic-table_wrapper" class="dataTables_wrapper form-inline no-footer">--}}
    <table id="dynamic-table" class="table table-striped table-bordered table-hover" style="margin-bottom: 0px;">
        <thead>
        <tr>
            <th class="center" width="6%">
                <label class="pos-rel">
                    {{--<input type="checkbox" class="ace"/>--}}
                    {{--<span class="lbl"></span>--}}
                </label>
            </th>
            <th width="21%">District Name</th>
            <th width="21%">District Name (Bangla)</th>
            <th width="21%">Division Name</th>
            <th width="21%">URL</th>
            <th class="center" width="10%">Action</th>
        </tr>
        </thead>

        <tbody>

        <?php $pageNo = (($districts->currentPage() - 1) * $districts->perPage()) + 1 ?>

        @foreach ($districts as $district)
            <tr>
                <td class="center">
                    <label class="pos-rel">
                        {{--<input type="checkbox" class="ace"/>--}}
                        <span class="lbl">{{ $pageNo++ }}</span>
                    </label>
                </td>
                <td>
                    {{ $district->name}}
                </td>
                <td>
                    {{ $district->bn_name}}
                </td>
                <td>
                    {{ $district->division_name }}
                </td>
                <td>
                    {{ $district->url }}
                </td>
                <td class="center">
                    <div class="hidden-sm hidden-xs action-buttons">
                        {{--<a class="blue" href="{{ route('') }}">--}}
                        {{--<i class="ace-icon fa fa-search-plus bigger-200"></i>--}}
                        {{--</a>--}}

                        {{--&nbsp;&nbsp;| &nbsp;&nbsp;--}}

                        <a class="green" href="{{ route('district.adminEdit',$district->id) }}">
                            <i class="ace-icon fa fa-pencil bigger-200"></i>
                        </a>

                        &nbsp;&nbsp;| &nbsp;&nbsp;

                        <a class="red deletebtn" href="{{ route('district.adminDelete',$district->id) }}"
                           id={{ $district->id }}>
                            <i class="ace-icon fa fa-trash-o bigger-200"></i>
                        </a>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <div class="row">
        <div class="col-xs-6">
            <div class="dataTables_info" id="dynamic-table_info" role="status" aria-live="polite">
                Showing {{ (($districts->currentPage() - 1) * $districts->perPage()) + 1 }}
                to {{ (($districts->currentPage() - 1) * $districts->perPage()) + $districts->perPage() }} of
                {{ $districts->total() }} entries
            </div>
        </div>
        <div class="col-xs-6">
            <div class="dataTables_paginate paging_simple_numbers" id="dynamic-table_paginate">
                {{ $districts->links("vendor.pagination.custom") }}
            </div>
        </div>
    </div>
{{--</div>--}}
