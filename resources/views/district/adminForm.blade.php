@extends('layouts.admin')

@section('title')
    Add District
@stop

@section('breadcrumb')
    <ul class="breadcrumb">
        <li>
            <i class="ace-icon fa fa-bars home-icon"></i>
            <a href="{{ route('district.adminList') }}">District List</a>
        </li>

        <li>
            <a href="{{ route('district.adminForm') }}">District Add</a>
        </li>
        {{--<li class="active">User Profile</li>--}}
    </ul>
@stop

@section('page_header')
    <h1>Add District</h1>
@stop

@section('content')
    <div class="col-xs-12">
        <form class="form-horizontal" role="form" action="{{ route('district.adminStore') }}" method="POST">
            {{ csrf_field() }}
            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="division_id"> Division Name *</label>

                <div class="col-sm-9">
                    <select name='division_id' class="col-xs-10 col-sm-5" id="division_id">
                        <option value="">-- Please Select One --</option>
                        @foreach($divisionData as $id => $name)
                            <option value="{{ $id }}" }>{{ $name }}</option>
                        @endforeach
                    </select>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('division_id'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('divisions_id') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="space-4"></div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> District Name *</label>

                <div class="col-sm-9">
                    <input type="text" id="form-field-1" placeholder="District Name" name="name"
                           class="col-xs-10 col-sm-5"/>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('name'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="space-4"></div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> District Name (Bangla)
                    *</label>

                <div class="col-sm-9">
                    <input type="text" id="form-field-1" placeholder="District Name(Bangla)" name="bn_name" class="col-xs-10 col-sm-5"/>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('bn_name'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('bn_name') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="space-4"></div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="lat"> Latitude </label>

                <div class="col-sm-9">
                    <input type="number" id="lat" placeholder="Latitude" name="lat" class="col-xs-10 col-sm-5"/>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('lat'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('lat') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="space-4"></div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="lon"> Longitud </label>

                <div class="col-sm-9">
                    <input type="number" id="lon" placeholder="Longitud" name="lon" class="col-xs-10 col-sm-5"/>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('lon'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('lon') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="space-4"></div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="url"> District URL</label>

                <div class="col-sm-9">
                    <input type="text" id="url" placeholder="District URL" name="url" class="col-xs-10 col-sm-5"/>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('url'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('url') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="space-4"></div>

            {{--<div class="form-group">--}}
            {{--<label class="col-sm-3 control-label no-padding-right" for="form-field-1-1"> Status </label>--}}

            {{--<div class="col-sm-9">--}}
            {{--<select name='status' class="col-xs-10 col-sm-5" id="form-field-select-1">--}}
            {{--<option value="1">Active</option>--}}
            {{--<option value="0">Inactive</option>--}}
            {{--</select>--}}
            {{--</div>--}}
            {{--</div>--}}

            <div class="space-4"></div>

            <div class="clearfix form-actions">
                <div class="col-md-offset-3 col-md-9">
                    <button class="btn btn-info" type="submit">
                        <i class="ace-icon fa fa-check bigger-110"></i>
                        Submit
                    </button>

                    &nbsp; &nbsp; &nbsp;
                    <button class="btn" type="reset">
                        <i class="ace-icon fa fa-undo bigger-110"></i>
                        Reset
                    </button>
                </div>
            </div>
        </form>
    </div>
@stop


@section('custom_style')

@stop

@section('custom_script')
    <script type="text/javascript">
        $('#country_list').on('change', function () {
            var countryID = $(this).val();
            var token = $("input[name='_token']").val();
            var url = "{{ route('division.adminDivisionSelectAjaxList') }}";
            if (countryID) {
                $.ajax({
                    url: url,
                    method: 'POST',
                    data: {countryID: countryID, _token: token},
                    dataType: "json",
                    success: function (data) {
                        $('select[name="division_id"]').empty();
                        $.each(data, function (key, value) {
                            $('select[name="division_id"]').append('<option value="' + key + '">' + value + '</option>');
                        });
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("Status: " + textStatus);
                        alert("Error: " + errorThrown);
                        console.log(XMLHttpRequest);
                        console.log(textStatus);
                        console.log(errorThrown);
                    }
                });
            } else {
                $('select[name="division_id"]').empty();
            }
        });
    </script>
@stop