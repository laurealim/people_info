@extends('layouts.admin')

@section('title')
    ACL Permission
@stop

{{--@section('breadcrumb')--}}
{{--    <ul class="breadcrumb">--}}
{{--        <li>--}}
{{--            <i class="ace-icon fa fa-bars home-icon"></i>--}}
{{--            <a href="{{ route('ssnp.adminList') }}">SNNP List</a>--}}
{{--        </li>--}}

{{--        <li>--}}
{{--            <a href="{{ route('ssnp.adminForm') }}">SNNP Add</a>--}}
{{--        </li>--}}
{{--        --}}{{--<li class="active">User Profile</li>--}}
{{--    </ul>--}}
{{--@stop--}}

@section('page_header')
    <h1>ACL Permission</h1>
@stop

@section('content')
    <div class="col-xs-12">
        <form class="form-horizontal" role="form" action="{{ route('ssnp.adminStore') }}" method="POST">
            {{ csrf_field() }}
            <div class="form-group">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            @foreach($roles as $rol_id => $rol_value)
                                <th scope="col">{{ $rol_value }}</th>
                            @endforeach
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            @foreach($permissionLists as $script => $values)
                                @foreach($values as $controller => $actions)
                                    @foreach($actions as $key => $desc)
                                        <th>{{ $desc }}</th>
                                        <td></td>

                                        {{--                                <th scope="col">{{ $rol_value }}</th>--}}
                                    @endforeach
                                @endforeach
                            @endforeach
                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>

            <div class="space-4"></div>

            <div class="clearfix form-actions">
                <div class="col-md-offset-3 col-md-9">
                    <button class="btn btn-info" type="submit">
                        <i class="ace-icon fa fa-check bigger-110"></i>
                        Submit
                    </button>

                    &nbsp; &nbsp; &nbsp;
                    <button class="btn" type="reset">
                        <i class="ace-icon fa fa-undo bigger-110"></i>
                        Reset
                    </button>
                </div>
            </div>
        </form>
    </div>
@stop


@section('custom_style')

@stop

@section('custom_script')
    <script type="text/javascript">
        //datepicker plugin
        $('.date-picker').datepicker({
            autoclose: true,
            todayHighlight: true
        });
    </script>

@stop