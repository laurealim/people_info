<table id="dynamic-table" class="table table-striped table-bordered table-hover" style="margin-bottom: 0px;">
    <thead>
    <tr>
        <th class="center" width="3%">
            Sr. No.
        </th>
        <th width="8%">পরিবার প্রধানের নাম</th>
        <th width="8%">পিতা/স্বামীর নাম</th>
        <th width="5%">বয়স</th>
        <th width="5%">কার্ডের ধরণ</th>
        <th width="5%">কার্ড নং</th>
        <th width="5%">জেলা</th>
        <th width="5%">উপজেলা</th>
        <th width="5%">ইউনিয়ন / পৌরসভা</th>
        <th width="5%">ওয়ার্ড</th>
        <th width="5%">বর্তমান পেশা</th>
        <th width="10%">মোবাইল নাম্বার</th>
        <th width="10%">জন্ম নিবন্ধন / ভোটার আইডি নাম্বার</th>
        <th width="5%">অন্যান্য ভাতা</th>
        <th width="5%">মাসিক আয়</th>
        <th width="5%">মাসিক ব্যায়</th>
        <th class="center" width="10%">Action</th>
{{--        <th class="center" width="10%">Action</th>--}}
    </tr>
    </thead>

    <tbody>

    <?php //pr($registrationList); ?>
    <input type="hidden" id="uri" value="<?php echo $uri; ?>"/>
    <?php $pageNo = (($reliefList->currentPage() - 1) * $reliefList->perPage()) + 1;?>
    @foreach ($reliefList as $relief)
        <tr>
            <td class="center">
                <label class="pos-rel">
                    {{--<input type="checkbox" class="ace"/>--}}
                    <span class="lbl">{{ $pageNo++ }}</span>
                </label>
            </td>
            <td>
                {{ $relief->name }}
            </td>
            <td>
                <?php if(!empty($relief->g_name)) { ?>
                {{ $relief->g_name }}
                <?php } else{ ?>
                {{ $relief->hw_name }}
                <?php }
                ?>
            </td>
            <td>
                {{ $relief->age }}
            </td>
            <td>
            {{ config('constants.card_type.arr.'.$relief->card_type) }}
            <!--                --><?php
                //                    if($relief->gender == 1){
                //                        echo "পুরুষ";
                //                    }elseif($relief->gender == 2){
                //                        echo "মহিলা";
                //                    }else{
                //                        echo "অন্যান্ন";
                //                    }
                //                ?>
            </td>
            <td>
                <?php
//                $cardId = config('constants.location_code.dist.' . $relief['dist_pre']);
//                $cardId .= config('constants.location_code.upo.' . $relief['upo_pre']);
//                $uni_pre = strlen(config('constants.location_code.uni.' . $relief['uni_pre']));
//                if ($uni_pre > 1) {
//                    $cardId .= config('constants.location_code.uni.' . $relief['uni_pre']);
//                } else {
//                    $cardId .= '0';
//                    $cardId .= config('constants.location_code.uni.' . $relief['uni_pre']);
//
//                }
//                $cardId .= $relief['word_pre'];
//                $length = strlen($relief['id']);
//                for ($count = 6; $count > $length; $count--) {
//                    $cardId .= '0';
//                }
//                $cardId .= $relief['id'];
//                echo $cardId;
                $cardNo = $relief['card_no'];
                echo $cardNo;


                //                $cardNo = '353200';
                //                $cardNo .= $relief['word_pre'];
                //                $length = strlen($relief['id']);
                //                for ($count = 6; $count > $length; $count--) {
                //                    $cardNo .= '0';
                //                }
                ?>
            </td>
            <td>
                {{ $relief->dis_name }}
            </td>
            <td>
                {{ $relief->upa_name }}
            </td>
            <td>
                <?php
                if ($relief->uni_pre < 1) {
                    echo 'পৌরসভা';
                } else {
                    echo $relief->uni_pre;
                }
                ?>
                {{--                {{ $relief->uni_pre }}--}}
            </td>
            <td>
                {{ $relief->word_pre }}
            </td>
            <td>
                {{ $relief->profession  }}
            </td>
            <td>
                {{ $relief->phone }}
            </td>
            <td>
                {{ $relief->nid }}
            </td>
            <td class="center">
                <?php
                if ($relief->ssnp == 1) {
                    echo "আছে";
                } else {
                    echo "নেই";
                }
                ?>
            </td>
            <td>
                {{ $relief->total_income }}
            </td>
            <td class="center">
                {{ $relief->total_spend }}
            </td>

            <td class="center">
            
                <!-- <div class="hidden-sm hidden-xs action-buttons"> -->
                <?php if((auth()->user()->user_type == 1 ) || (auth()->user()->can_print == 1)){?>

                <a class="gray" href="{{ route('registration.userPrintCard',array('id' =>$relief->id)) }}"
                title="Print">
                    <i class="ace-icon fa fa-print bigger-150"></i>
                </a>
                <?php }?>
{{--                    <a class="green" href="{{ route('registration.userEdit',$relief->id) }}" title="Edit">--}}
{{--                        <i class="ace-icon fa fa-pencil bigger-150"></i>--}}
{{--                    </a>--}}

{{--                    &nbsp;| &nbsp;--}}

{{--                    <a class="red deletebtn" href="{{ route('registration.userDestroy',$relief->id) }}"--}}
{{--                       id="{{ $relief->id }}" title="Delete">--}}
{{--                        <i class="ace-icon fa fa-trash-o bigger-150"></i>--}}
{{--                    </a>--}}


                <!-- </div> -->
            </td>
        </tr>
    @endforeach
    </tbody>
</table>


<div class="row">
    <div class="col-xs-6">
        <div class="dataTables_info" id="dynamic-table_info" role="status" aria-live="polite">
            Showing {{ (($reliefList->currentPage() - 1) * $reliefList->perPage()) + 1 }}
            to {{ (($reliefList->currentPage() - 1) * $reliefList->perPage()) + $reliefList->perPage() }}
            of
            {{ $reliefList->total() }} entries
        </div>
    </div>
    <div class="col-xs-6">
        <div class="dataTables_paginate paging_simple_numbers" id="dynamic-table_paginate">
            {{ $reliefList->links("vendor.pagination.custom") }}
        </div>
    </div>
</div>

<style type="text/css">
    /*.table tbody > tr > td {*/
    /*text-align: center;*/
    /*}*/
    .facility {
        font-weight: bold;
    }
</style>