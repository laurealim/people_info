@extends('layouts.user')

@section('title')
    Edit Registration
@stop

@section('breadcrumb')
    <ul class="breadcrumb">
        <li>
            <i class="ace-icon fa fa-bars home-icon"></i>
            <a href="{{ route('registration.userList') }}">Registration List</a>
        </li>

        <li>
            <a href="{{ route('registration.userEdit',$id) }}">Registration Edit</a>
        </li>
    </ul>
@stop

@section('page_header')
    <h1>Registration Form</h1>
@stop

@section('content')
    <?php
    //    dd($reliefData);
    ?>
    <div class="col-xs-12">
        <form class="form-horizontal" role="form" action="{{ route('registration.userUpdate', $id) }}" method="POST"
              enctype="multipart/form-data">
            {{ csrf_field() }}

            <div class="row">
                <div class="col-sm-12">
                    <h3 class="header smaller lighter blue">
                        কার্ড
                    </h3>
                    <div class="form-group col-sm-6">
                        <label class="col-sm-4 control-label no-padding-right" for="card_type">কার্ডের ধরণ</label>

                        <div class="col-sm-8">
                            <select name='card_type' class="col-xs-10 col-sm-12" id="card_type">
                                <option value="">--- বাছাই করুন ---</option>
                                @foreach(config('constants.card_type.arr') as $id => $name)
                                    <option value="{{ $id }}" {{ $id == $reliefData->card_type ? 'selected="selected"' : '' }}>{{ $name }}</option>
                                @endforeach

                            </select>
                            <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('card_type'))
                                    <span class="help-block middle">
                                <strong>{{ $errors->first('card_type') }}</strong>
                            </span>
                                @endif
                                {{--<span class="middle">Inline help text</span>--}}
                    </span>
                        </div>
                    </div>
                </div>
            </div>

            <h3 class="header smaller lighter blue">
                সাধারণ তথ্য
            </h3>
            <div class="form-group col-sm-6">
                <label class="col-sm-4 control-label no-padding-right" for="name">পরিবার প্রধানের নাম *</label>

                <div class="col-sm-8">
                    <input type="text" id="name" placeholder="পরিবার প্রধানের নাম" name="name"
                           value="{{ $reliefData->name }}"
                           class="col-xs-10 col-sm-12"/>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('name'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="form-group col-sm-6">
                <label class="col-sm-4 control-label no-padding-right" for="g_name">পিতার নাম *</label>

                <div class="col-sm-8">
                    <input type="text" id="g_name" placeholder="পিতার নাম" name="g_name"
                           value="{{ $reliefData->g_name }}"
                           class="col-xs-10 col-sm-12"/>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('g_name'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('g_name') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="space-4"></div>
            <div class="form-group col-sm-6">
                <label class="col-sm-4 control-label no-padding-right" for="hw_name">স্বামী/স্ত্রীর নাম *</label>

                <div class="col-sm-8">
                    <input type="text" id="hw_name" placeholder="পিতা/স্বামীর নাম" name="hw_name"
                           value="{{ $reliefData->hw_name }}"
                           class="col-xs-10 col-sm-12"/>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('hw_name'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('hw_name') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="form-group col-sm-6">
                <label class="col-sm-4 control-label no-padding-right" for="gender"> লিঙ্গ *</label>

                <div class="col-sm-8">
                    <select name='gender' class="col-xs-10 col-sm-12" id="gender">
                        <option value="">--- বাছাই করুন ---</option>
                        @foreach($gender_list as $id => $name)
                            <option value="{{ $id }}" {{ $id == $reliefData->gender ? 'selected="selected"' : '' }}>{{ $name }}</option>
                        @endforeach
                    </select>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('gender'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('gender') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="space-4"></div>

            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group col-sm-6">
                        <label class="col-sm-4 control-label no-padding-right" for="dob">জন্ম তারিখ</label>
                        {{--                <label class="col-sm-4 control-label no-padding-right" for="dob"></label>--}}

                        <div class="col-sm-8">
                            <input class="date-picker form-control col-xs-10 col-sm-12" id="dob" type="text" name="dob" value="{{ $reliefData->dob }}"
                                   data-date-format="dd-mm-yyyy"/>
                            <span class="input-group-addon">
								<i class="fa fa-calendar bigger-110"></i>
							</span>
                        </div>
                    </div>

                    <div class="form-group col-sm-6">
                        <label class="col-sm-4 control-label no-padding-right" for="age"> বয়স *</label>

                        <div class="col-sm-8">
                            <input type="number" id="age" placeholder="i.e. 25" name="age" class="col-xs-10 col-sm-12"
                                   value="{{ $reliefData->age }}"
                            />
                            <span class="help-inline col-xs-12 col-sm-7">
                                @if ($errors->has('age'))
                                    <span class="help-block middle">
                                        <strong>{{ $errors->first('age') }}</strong>
                                    </span>
                                @endif
                                {{--<span class="middle">Inline help text</span>--}}
                            </span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="space-4"></div>
            <div class="space-4"></div>
            <div class="row">
                <div class="col-sm-12">
                    <h3 class="header smaller lighter blue">
                        ঠিকানা
                    </h3>
                    <div class="card">
                        <div class="card-header">
                            <h4>
                                বর্তমান ঠিকানা
                            </h4>
                        </div>
                        <div class="card-body" style="display: flow-root">
                            <br>

                            {{--                            জেলা --}}
                            <div class="form-group col-sm-6">
                                <label class="col-sm-4 control-label no-padding-right" for="dist_pre"> জেলা *</label>

                                <div class="col-sm-8">
                                    <select name='dist_pre' class="col-xs-10 col-sm-12" id="dist_pre">
                                        <option value="">--- বাছাই করুন ---</option>
                                        @foreach($listArr[0] as $id => $name)
                                            <option value="{{ $id }}" {{ $id == $reliefData->dist_pre ? 'selected="selected"' : '' }}>{{ $name }}</option>
                                        @endforeach
                                    </select>
                                    <span class="help-inline col-xs-12 col-sm-7">
                                        @if ($errors->has('dist_pre'))
                                            <span class="help-block middle">
                                                <strong>{{ $errors->first('dist_pre') }}</strong>
                                            </span>
                                        @endif
                                    </span>
                                </div>
                            </div>

                            {{--                            উপজেয়া --}}
                            <div class="form-group col-sm-6">
                                <label class="col-sm-4 control-label no-padding-right" for="upo_pre"> উপোজেলা
                                    *</label>

                                <div class="col-sm-8">
                                    <select name='upo_pre' class="col-xs-8 col-sm-12" id="upo_pre">
                                        <option value="">--- বাছাই করুন ---</option>
                                        @foreach($listArr[2] as $id => $name)
                                            <option value="{{ $id }}" {{ $id == $reliefData->upo_pre ? 'selected="selected"' : '' }} >{{ $name }}</option>
                                        @endforeach

                                    </select>
                                    <span class="help-inline col-xs-12 col-sm-7">
                                        @if ($errors->has('upo_pre'))
                                            <span class="help-block middle">
                                                <strong>{{ $errors->first('upo_pre') }}</strong>
                                            </span>
                                        @endif
                                    </span>
                                </div>
                            </div>

                            {{--                            ইউনিয়ন --}}
                            <div class="form-group col-sm-6">
                                <label class="col-sm-4 control-label no-padding-right" for="uni_pre"> ইউনিয়ন/
                                    পৌরসভা</label>

                                <div class="col-sm-8">
                                    <select name='uni_pre' class="col-xs-8 col-sm-12" id="uni_pre">
                                        <option value="">--- বাছাই করুন ---</option>
                                        {{--                                        @foreach($listArr[4] as $id => $name)--}}
                                        {{--                                            <option value="{{ $id }}" {{ $id == $reliefData->uni_pre ? 'selected="selected"' : '' }} >{{ $name }}</option>--}}
                                        {{--                                        @endforeach--}}
                                        <?php if($reliefData->uni_pre < 1){?>
                                        <option value=0 selected="selected">পৌরসভা</option>
                                        <?php }?>
                                        @foreach($listArr[4] as $id => $name)

                                            <option value="{{ $id }}" {{ $id == $reliefData->uni_pre ? 'selected="selected"' : '' }} >{{ $name }}</option>
                                        @endforeach

                                    </select>

                                    {{--                                    <input type="text" id="uni_pre" placeholder="ইউনিয়ন/ পৌরসভা" name="uni_pre" value="{{ $reliefData->uni_pre }}"              class="col-xs-10 col-sm-12"/>--}}
                                    <span class="help-inline col-xs-12 col-sm-7">
                                        @if ($errors->has('uni_pre'))
                                            <span class="help-block middle">
                                                <strong>{{ $errors->first('uni_pre') }}</strong>
                                            </span>
                                        @endif
                                        {{--<span class="middle">Inline help text</span>--}}
                                    </span>
                                </div>
                            </div>

                            {{--                            ওয়ার্ড নং --}}
                            <div class="form-group col-sm-6">
                                <label class="col-sm-4 control-label no-padding-right" for="word_pre">ওয়ার্ড নং</label>

                                <div class="col-sm-8">
                                    <select name='word_pre' class="col-xs-10 col-sm-12" id="word_pre">
                                        <option value="">--- বাছাই করুন ---</option>
                                        <?php
                                        for($word_pre_id = 1; $word_pre_id < 10; $word_pre_id++){ ?>
                                        <option value="{{ $word_pre_id }}" {{ $word_pre_id == $reliefData->word_pre ? 'selected="selected"' : '' }}>
                                            ওয়ার্ড নং {{ $word_pre_id }}</option>
                                        <?php }
                                        ?>
                                    </select>
                                    {{--                                    <input type="text" id="word_pre" placeholder="ওয়ার্ড নং" name="word_pre" value="{{ $reliefData->word_pre }}" class="col-xs-10 col-sm-12"/>--}}
                                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('word_pre'))
                                            <span class="help-block middle">
                                <strong>{{ $errors->first('word_pre') }}</strong>
                            </span>
                                        @endif
                                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                                </div>
                            </div>

                            {{--                            মহল্লা / রোড নং --}}
                            <div class="form-group col-sm-6">
                                <label class="col-sm-4 control-label no-padding-right" for="road_pre">মহল্লা/রোড/পোস্টঅফিস
                                    নং</label>

                                <div class="col-sm-8">
                                    <input type="text" id="road_pre" placeholder="মহল্লা/রোড নং" name="road_pre"
                                           value="{{ $reliefData->road_pre }}"
                                           class="col-xs-10 col-sm-12"/>
                                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('road_pre'))
                                            <span class="help-block middle">
                                <strong>{{ $errors->first('road_pre') }}</strong>
                            </span>
                                        @endif
                                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                                </div>
                            </div>

                            {{--                            হোল্ডিং নং --}}
                            <div class="form-group col-sm-6">
                                <label class="col-sm-4 control-label no-padding-right" for="holding_pre">হোল্ডিং
                                    নং</label>

                                <div class="col-sm-8">
                                    <input type="text" id="holding_pre" placeholder="হোল্ডিং নং" name="holding_pre"
                                           value="{{ $reliefData->holding_pre }}"
                                           class="col-xs-10 col-sm-12"/>
                                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('holding_pre'))
                                            <span class="help-block middle">
                                <strong>{{ $errors->first('holding_pre') }}</strong>
                            </span>
                                        @endif
                                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                                </div>
                            </div>

                            {{--                            বাসা নং--}}
                            <div class="form-group col-sm-6">
                                <label class="col-sm-4 control-label no-padding-right" for="house_pre">
                                    বাসা/গ্রাম</label>

                                <div class="col-sm-8">
                                    <input type="text" id="house_pre" placeholder="বাসা নং" name="house_pre"
                                           value="{{ $reliefData->house_pre }}"
                                           class="col-xs-10 col-sm-12"/>
                                    <span class="help-inline col-xs-12 col-sm-7">
                                        @if ($errors->has('house_pre'))
                                            <span class="help-block middle">
                                                <strong>{{ $errors->first('house_pre') }}</strong>
                                            </span>
                                        @endif
                                        {{--<span class="middle">Inline help text</span>--}}
                                    </span>
                                </div>
                            </div>

                            <div class="form-group col-sm-6">
                                <label class="col-sm-4 control-label no-padding-right" for="home_type">বাসার
                                    ধরণ </label>

                                <div class="col-sm-8">
                                    <select name='home_type' class="col-xs-10 col-sm-12" id="home_type">
                                        <option value="">--- বাছাই করুন ---</option>
                                        <option value=0 {{ $reliefData->home_type == 0 ? 'selected="selected"' : '' }}>
                                            নিজের বাসা
                                        </option>
                                        <option value=1 {{ $reliefData->home_type == 1 ? 'selected="selected"' : '' }}>
                                            ভাড়া বাসা
                                        </option>
                                        <option value=2 {{ $reliefData->home_type == 2 ? 'selected="selected"' : '' }}>
                                            অন্যান্য
                                        </option>
                                    </select>
                                    <span class="help-inline col-xs-12 col-sm-7">
                                        @if ($errors->has('home_type'))
                                            <span class="help-block middle">
                                                <strong>{{ $errors->first('home_type') }}</strong>
                                            </span>
                                        @endif
                                    </span>
                                </div>
                            </div>

                            <div class="form-group col-sm-6 othr">
                                <label class="col-sm-4 control-label no-padding-right" for="home_type_other">অন্যান্য
                                    হলে </label>

                                <div class="col-sm-8">
                                    <input type="text" id="home_type_other" placeholder="অন্যান্য হলে"
                                           name="home_type_other" value="{{ $reliefData->home_type_other }}"
                                           class="col-xs-10 col-sm-12"/>
                                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('home_type_other'))
                                            <span class="help-block middle">
                                <strong>{{ $errors->first('home_type_other') }}</strong>
                            </span>
                                        @endif
                                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="space-4"></div>

                    {{--                    <div><span><input type="checkbox" id="same_add" name="same_add" class="same_add">  স্থায়ী ঠিকানা ও বর্তমান ঠিকানা এক  </span>--}}
                    {{--                    </div>--}}
                    <div class="card">
                        <div class="card-header">
                            <h4>
                                স্থায়ী ঠিকানা
                            </h4>
                        </div>
                        <div class="card-body" style="display: flow-root">
                            <br>

                            {{--                            জেলা --}}
                            <div class="form-group col-sm-6">
                                <label class="col-sm-4 control-label no-padding-right" for="dist_per"> জেলা *</label>

                                <div class="col-sm-8">
                                    <select name='dist_per' class="col-xs-10 col-sm-12" id="dist_per">
                                        <option value="">--- বাছাই করুন ---</option>
                                        @foreach($listArr[1] as $id => $name)
                                            <option value="{{ $id }}" {{ $id == $reliefData->dist_per ? 'selected="selected"' : '' }} >{{ $name }}</option>
                                        @endforeach

                                    </select>
                                    <span class="help-inline col-xs-12 col-sm-7">
                                        @if ($errors->has('dist_per'))
                                            <span class="help-block middle">
                                                <strong>{{ $errors->first('dist_per') }}</strong>
                                            </span>
                                        @endif
                                    </span>
                                </div>
                            </div>

                            {{--                            উপজেলা --}}
                            <div class="form-group col-sm-6">
                                <label class="col-sm-4 control-label no-padding-right" for="upo_per"> উপোজেলা
                                    *</label>

                                <div class="col-sm-8">
                                    <select name='upo_per' class="col-xs-8 col-sm-12" id="upo_per">
                                        <option value="">--- বাছাই করুন ---</option>
                                        @foreach($listArr[3] as $id => $name)
                                            <option value="{{ $id }}" {{ $id == $reliefData->upo_per ? 'selected="selected"' : '' }} >{{ $name }}</option>
                                        @endforeach
                                    </select>
                                    <span class="help-inline col-xs-12 col-sm-7">
                                        @if ($errors->has('upo_per'))
                                            <span class="help-block middle">
                                                <strong>{{ $errors->first('upo_per') }}</strong>
                                            </span>
                                        @endif
                                    </span>
                                </div>
                            </div>

                            {{--                            ইউনিয়ন --}}
                            <div class="form-group col-sm-6">
                                <label class="col-sm-4 control-label no-padding-right" for="uni_per"> ইউনিয়ন/
                                    পৌরসভা</label>

                                <div class="col-sm-8">
                                    <select name='uni_per' class="col-xs-8 col-sm-12" id="uni_per">
                                        <option value="">--- বাছাই করুন ---</option>
                                        <?php if($reliefData->uni_per < 1){?>
                                        <option value=0 selected="selected">পৌরসভা</option>
                                        <?php }?>
                                        @foreach($listArr[5] as $id => $name)
                                            <option value="{{ $id }}" {{ $id == $reliefData->uni_per ? 'selected="selected"' : '' }} >{{ $name }}</option>
                                        @endforeach
                                    </select>

                                    {{--                                    <input type="text" id="uni_per" placeholder="ইউনিয়ন/ পৌরসভা" name="uni_per" value="{{ $reliefData->uni_per }}"--}}
                                    {{--                                           class="col-xs-10 col-sm-12"/>--}}
                                    <span class="help-inline col-xs-12 col-sm-7">
                                        @if ($errors->has('uni_per'))
                                            <span class="help-block middle">
                                                <strong>{{ $errors->first('uni_per') }}</strong>
                                            </span>
                                        @endif
                                        {{--<span class="middle">Inline help text</span>--}}
                                    </span>
                                </div>
                            </div>

                            {{--                            ওয়ার্ড নং --}}
                            <div class="form-group col-sm-6">
                                <label class="col-sm-4 control-label no-padding-right" for="word_per">ওয়ার্ড নং</label>

                                <div class="col-sm-8">
                                    <select name='word_per' class="col-xs-10 col-sm-12" id="word_per">
                                        <option value="">--- বাছাই করুন ---</option>
                                        <?php
                                        for($word_per_id = 1; $word_per_id < 10; $word_per_id++){ ?>
                                        <option value="{{ $word_per_id }}" {{ $word_per_id == $reliefData->word_per ? 'selected="selected"' : '' }}>
                                            ওয়ার্ড নং {{ $word_per_id }}</option>
                                        <?php }
                                        ?>
                                    </select>
                                    {{--                                    <input type="text" id="word_per" placeholder="ওয়ার্ড নং" name="word_per" value="{{ $reliefData->word_per }}" class="col-xs-10 col-sm-12"/>--}}
                                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('word_per'))
                                            <span class="help-block middle">
                                <strong>{{ $errors->first('word_per') }}</strong>
                            </span>
                                        @endif
                                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                                </div>
                            </div>

                            {{--                            পোস্টঅফিস --}}
                            <div class="form-group col-sm-6">
                                <label class="col-sm-4 control-label no-padding-right" for="post_per">মহল্লা/রোড/পোস্টঅফিস</label>

                                <div class="col-sm-8">
                                    <input type="text" id="post_per" placeholder="পোস্টঅফিস" name="post_per"
                                           value="{{ $reliefData->post_per }}"
                                           class="col-xs-10 col-sm-12"/>
                                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('post_per'))
                                            <span class="help-block middle">
                                <strong>{{ $errors->first('post_per') }}</strong>
                            </span>
                                        @endif
                                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                                </div>
                            </div>

                            {{--                            হোল্ডিং নং--}}
                            <div class="form-group col-sm-6">
                                <label class="col-sm-4 control-label no-padding-right" for="holding_per">হোল্ডিং
                                    নং</label>

                                <div class="col-sm-8">
                                    <input type="text" id="holding_per" placeholder="হোল্ডিং নং" name="holding_per"
                                           value="{{ $reliefData->holding_per }}"
                                           class="col-xs-10 col-sm-12"/>
                                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('holding_per'))
                                            <span class="help-block middle">
                                <strong>{{ $errors->first('holding_per') }}</strong>
                            </span>
                                        @endif
                                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                                </div>
                            </div>

                            {{--                            গ্রাম --}}
                            <div class="form-group col-sm-6">
                                <label class="col-sm-4 control-label no-padding-right" for="vill_per"> বাসা/গ্রাম
                                </label>

                                <div class="col-sm-8">
                                    <input type="text" id="vill_per" placeholder="গ্রাম" name="vill_per"
                                           value="{{ $reliefData->vill_per }}"
                                           class="col-xs-10 col-sm-12"/>
                                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('vill_per'))
                                            <span class="help-block middle">
                                <strong>{{ $errors->first('vill_per') }}</strong>
                            </span>
                                        @endif
                                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="space-4"></div>
                </div>
            </div>

            <div class="space-4"></div>
            <h3 class="header smaller lighter blue">
                যোগাযোগ
            </h3>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group col-sm-6">
                        <label class="col-sm-4 control-label no-padding-right" for="phone"> মোবাইল </label>

                        <div class="col-sm-8">
                            <input type="text" id="phone" placeholder="i.e. 017XXXXXXXX"
                                   name="phone" value="{{ $reliefData->phone }}"
                                   class="col-xs-10 col-sm-12"/>
                            <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('phone'))
                                    <span class="help-block middle">
                                <strong>{{ $errors->first('phone') }}</strong>
                            </span>
                                @endif
                                {{--<span class="middle">Inline help text</span>--}}
                    </span>
                        </div>
                    </div>
                    <div class="form-group col-sm-6">
                        <label class="col-sm-4 control-label no-padding-right" for="nid"> জন্মোনিবন্ধন /
                            ভোটার
                            আইডি নাম্বার/ NID *</label>

                        <div class="col-sm-8">
                            <input type="text" id="nid" placeholder="i.e. 1234567890" name="nid"
                                   value="{{ $reliefData->nid }}"
                                   class="col-xs-10 col-sm-12"/>
                            <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('nid'))
                                    <span class="help-block middle">
                                <strong>{{ $errors->first('nid') }}</strong>
                            </span>
                                @endif
                                {{--<span class="middle">Inline help text</span>--}}
                    </span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">

                </div>
            </div>

            <div class="space-4"></div>
            <h3 class="header smaller lighter blue">
                বিবিধ তথ্য
            </h3>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group col-sm-6">
                        <label class="col-sm-4 control-label no-padding-right" for="profession"> বর্তমান
                            পেশা </label>

                        <div class="col-sm-8">
                            <input type="text" id="profession" placeholder=""
                                   name="profession" value="{{ $reliefData->profession }}"
                                   class="col-xs-10 col-sm-12"/>
                            <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('profession'))
                                    <span class="help-block middle">
                                <strong>{{ $errors->first('profession') }}</strong>
                            </span>
                                @endif
                                {{--<span class="middle">Inline help text</span>--}}
                    </span>
                        </div>
                    </div>

                    <div class="form-group col-sm-6">
                        <label class="col-sm-4 control-label no-padding-right" for="ssnp"> কোন ভাতা পায় কি না
                            (SSNP)</label>
                        <div class="col-sm-8">
                            <select name='ssnp' class="col-xs-10 col-sm-12" id="ssnp">
                                <option value="">--- বাছাই করুন ---</option>
                                <?php
                                if($reliefData->ssnp == 0){ ?>
                                <option value=0 selected="selected">তথ্য নেই</option>
                                <?php }else{
                                ?>
                                <option value=0 >তথ্য নেই</option>
                                <?php } ?>
                                @foreach($ssnpList as $id => $name)
                                    <option value="{{ $id }}" {{ $id == $reliefData->ssnp ? 'selected="selected"' : '' }} >{{ $name }}</option>
                                @endforeach
                            </select>
                            <span class="help-inline col-xs-12 col-sm-7">
                                        @if ($errors->has('ssnp'))
                                    <span class="help-block middle">
                                                <strong>{{ $errors->first('ssnp') }}</strong>
                                            </span>
                                @endif
                                    </span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group col-sm-6">
                        <label class="col-sm-4 control-label no-padding-right" for="total_income"> মাসিক আয় *</label>

                        <div class="col-sm-8">
                            <input type="number" id="total_income" placeholder="i.e. 25000" name="total_income"
                                   class="col-xs-10 col-sm-12" value="{{ $reliefData->total_income }}"
                            />
                            <span class="help-inline col-xs-12 col-sm-7">
                                @if ($errors->has('total_income'))
                                    <span class="help-block middle">
                                        <strong>{{ $errors->first('total_income') }}</strong>
                                    </span>
                                @endif
                                {{--<span class="middle">Inline help text</span>--}}
                            </span>
                        </div>
                    </div>
                    <div class="form-group col-sm-6">
                        <label class="col-sm-4 control-label no-padding-right" for="total_spend"> মাসিক ব্যয় *</label>

                        <div class="col-sm-8">
                            <input type="number" id="total_spend" placeholder="i.e. 25000" name="total_spend"
                                   class="col-xs-10 col-sm-12" value="{{ $reliefData->total_spend }}"
                            />
                            <span class="help-inline col-xs-12 col-sm-7">
                                @if ($errors->has('total_spend'))
                                    <span class="help-block middle">
                                        <strong>{{ $errors->first('total_spend') }}</strong>
                                    </span>
                                @endif
                                {{--<span class="middle">Inline help text</span>--}}
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="col-sm-12">
                        <div class="form-group col-sm-6">
                            <label class="col-sm-4 control-label no-padding-right" for="poss_help"> সম্ভাব্য সহায়তা</label>
                            <div class="col-sm-8">
                                <select name='poss_help' class="col-xs-10 col-sm-12" id="poss_help">
                                    <option value="">--- বাছাই করুন ---</option>
                                    @foreach($possi_help_list as $id => $name)
                                        <option value="{{ $id }}" {{ $id == $reliefData->poss_help ? 'selected="selected"' : '' }} >{{ $name }}</option>
                                    @endforeach
                                </select>
                                <span class="help-inline col-xs-12 col-sm-7">
                                        @if ($errors->has('poss_help'))
                                        <span class="help-block middle">
                                                <strong>{{ $errors->first('poss_help') }}</strong>
                                            </span>
                                    @endif
                                    </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group col-sm-12">
                        <label class="control-label col-xs-12 col-sm-3 no-padding-right "
                               for="image">ছবি বাছাই করুন </label>

                        <div class="col-xs-10 col-sm-5">
                            <input type="file" id="image" class="col-xs-10 col-sm-5" name="image"/>
                            <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('image'))
                                    <span class="help-block middle">
                                <strong>{{ $errors->first('image') }}</strong>
                            </span>
                                @endif
                                {{--<span class="middle">Inline help text</span>--}}
                    </span>
                        </div>
                        <div class="col-xs-10 col-sm-5 col-md-4" style="margin-top:10px">
                            <?php if($reliefData->image != ""){ ?>
                            <img src="{{ asset('storage/family/'.$reliefData->id.'/'.$reliefData->image) }}"
                                 class="msg-photo" alt="Kate's Avatar" width="120px" height="80"/>
                            <?php }?>

                        </div>
                    </div>
                </div>
            </div>

            <div class="space-4"></div>
            <h3 class="header smaller lighter blue">
                পরিবারের অন্যান্য সদস্যের তথ্য
            </h3>
            <div class="row">
                <div class="col-sm-12">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">নাম</th>
                                <th scope="col">বয়স</th>
                                <th scope="col">লিঙ্গ</th>
                                <th scope="col">জন্মনিবন্ধন / ভোটার আইডি নাম্বার/ NID</th>
                                <th scope="col">পেশা</th>
                                <th scope="col">সম্পর্ক (মা/বাব/ভাই/বোন/স্বামী/স্ত্রী/পুত্র/কন্যা )</th>
                                <th scope="col">SSNP</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            for($row = 1; $row <= 8; $row++){?>
                            <tr>
                                <th scope="row"><?php echo $row; ?></th>
                                <td><input type="text" name="tblData[name][<?php echo $row?>]"
                                           id="tblData[name][<?php echo $row?>]"
                                           value="<?php echo $tblData['name'][$row]; ?>"/></td>
                                <td><input type="number" name="tblData[age][<?php echo $row?>]"
                                           id="tblData[age][<?php echo $row?>]"
                                           value="<?php echo $tblData['age'][$row]; ?>"/></td>
                                <td>
                                    <select name="tblData[f_gender][<?php echo $row?>]" class="col-xs-10 col-sm-12"
                                            id="tblData[f_gender][<?php echo $row?>]">
                                        <option value="">--- বাছাই করুন ---</option>
                                        <option value=1 {{ $tblData['f_gender'][$row] == 1 ? 'selected="selected"' : '' }}>
                                            পুরুষ
                                        </option>
                                        <option value=2 {{ $tblData['f_gender'][$row] == 2 ? 'selected="selected"' : '' }}>
                                            নারী
                                        </option>
                                        <option value=3 {{ $tblData['f_gender'][$row] == 3 ? 'selected="selected"' : '' }}>
                                            অন্যান্য
                                        </option>

                                    </select>
                                </td>
                                <td><input type="number" name="tblData[nid][<?php echo $row?>]"
                                           id="tblData[nid][<?php echo $row?>]"
                                           value="<?php echo $tblData['nid'][$row]; ?>"/></td>
                                <td><input type="text" name="tblData[profession][<?php echo $row?>]"
                                           id="tblData[profession][<?php echo $row?>]"
                                           value="<?php echo $tblData['profession'][$row]; ?>"/></td>
                                <td><input type="text" name="tblData[relation][<?php echo $row?>]"
                                           id="tblData[relation][<?php echo $row?>]"
                                           value="<?php echo $tblData['relation'][$row]; ?>"/></td>
                                <td>
                                    <select name="tblData[ssnp][<?php echo $row?>]" class="col-xs-10 col-sm-12"
                                            id="tblData[ssnp][<?php echo $row?>]">
                                        <option value="">--- বাছাই করুন ---</option>
                                        <?php
                                        if(!is_null($tblData['ssnp'][$row]) && $tblData['ssnp'][$row] == 0){ ?>
                                        <option value=0 selected="selected">তথ্য নেই</option>
                                        <?php }else {
                                        ?>
                                        <option value=0>তথ্য নেই</option>

                                        <?php } ?>
                                        @foreach($ssnpList as $id => $name)
                                            <option value="{{ $id }}" {{ $tblData['ssnp'][$row] == $id ? 'selected="selected"' : '' }}>{{ $name }}</option>
                                        @endforeach
                                    </select>
                                </td>
                            </tr>

                            <?php }
                            ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="space-4"></div>
            <div class="space-4"></div>
            <div class="clearfix form-actions">
                <div class="col-md-offset-3 col-md-9">
                    <button id="submit" class="btn btn-info" type="submit">
                        <i class="ace-icon fa fa-check bigger-110"></i>
                        Submit
                    </button>

                    &nbsp; &nbsp; &nbsp;
                    <button class="btn" type="reset">
                        <i class="ace-icon fa fa-undo bigger-110"></i>
                        Reset
                    </button>
                </div>
            </div>
        </form>
    </div>
@stop


@section('custom_style')

@stop

@section('custom_script')
    <script type="text/javascript">


        /****************************/
        /****************************/
        /*  File Upload Section */
        /****************************/
        /****************************/

        $('#image').ace_file_input({
            style: 'well',
            btn_choose: 'Drop files here or click to choose',
            btn_change: null,
            no_icon: 'ace-icon fa fa-cloud-upload',
            droppable: true,
            thumbnail: 'small'//large | fit
            //,icon_remove:null//set null, to hide remove/reset button
            /**,before_change:function(files, dropped) {
						//Check an example below
						//or examples/file-upload.html
						return true;
					}*/
            /**,before_remove : function() {
						return true;
					}*/
            ,
            preview_error: function (filename, error_code) {
                //name of the file that failed
                //error_code values
                //1 = 'FILE_LOAD_FAILED',
                //2 = 'IMAGE_LOAD_FAILED',
                //3 = 'THUMBNAIL_FAILED'
                //alert(error_code);
            }

        }).on('change', function () {
            //console.log($(this).data('ace_input_files'));
            //console.log($(this).data('ace_input_method'));
        });

        var whitelist_ext, whitelist_mime;
        var btn_choose;
        var no_icon;

        var file_input = $('#image');

        file_input
            .ace_file_input('update_settings',
                {
                    'btn_choose': btn_choose,
                    'no_icon': no_icon,
                    'allowExt': whitelist_ext,
                    'allowMime': whitelist_mime
                });

        file_input.ace_file_input('reset_input');

        file_input
            .off('file.error.ace')
            .on('file.error.ace', function (e, info) {
                //console.log(info.file_count);//number of selected files
                //console.log(info.invalid_count);//number of invalid files
                //console.log(info.error_list);//a list of errors in the following format

                //info.error_count['ext']
                //info.error_count['mime']
                //info.error_count['size']

                //info.error_list['ext']  = [list of file names with invalid extension]
                //info.error_list['mime'] = [list of file names with invalid mimetype]
                //info.error_list['size'] = [list of file names with invalid size]


                /**
                 if( !info.dropped ) {
							//perhapse reset file field if files have been selected, and there are invalid files among them
							//when files are dropped, only valid files will be added to our file array
							e.preventDefault();//it will rest input
						}
                 */


                //if files have been selected (not dropped), you can choose to reset input
                //because browser keeps all selected files anyway and this cannot be changed
                //we can only reset file field to become empty again
                //on any case you still should check files with your server side script
                //because any arbitrary file can be uploaded by user and it's not safe to rely on browser-side measures
            });


        /****************************/
        /****************************/
        /*  End */
        /****************************/
        /****************************/

        //datepicker plugin
        $('.date-picker').datepicker({
            autoclose: true,
            todayHighlight: true
        });

        // Age Calculation Function
        function getAge(dateString) {
            var now = new Date();
            var today = new Date(now.getFullYear(), now.getMonth(), now.getDate());

            var yearNow = now.getFullYear();
            var monthNow = now.getMonth();
            var dateNow = now.getDate();

            let curDateFormat = new Date(dateString.substring(6, 10), dateString.substring(3, 5), dateString.substring(0, 2));

            var dobs = new Date(dateString.substring(6, 10), dateString.substring(3, 5), dateString.substring(0, 2) - 1);

            var yearDob = dobs.getFullYear();
            var monthDob = dobs.getMonth();
            var dateDob = dobs.getDate();
            var age = {};
            var ageString = "";
            var yearString = "";
            var monthString = "";
            var dayString = "";


            yearAge = yearNow - yearDob;

            if (monthNow >= monthDob)
                var monthAge = monthNow - monthDob;
            else {
                yearAge--;
                var monthAge = 12 + monthNow - monthDob;
            }

            if (dateNow >= dateDob)
                var dateAge = dateNow - dateDob;
            else {
                monthAge--;
                var dateAge = 31 + dateNow - dateDob;

                if (monthAge < 0) {
                    monthAge = 11;
                    yearAge--;
                }
            }

            age = {
                years: yearAge,
                months: monthAge,
                days: dateAge
            };

            if (age.years > 1) yearString = " years";
            else yearString = " year";
            if (age.months > 1) monthString = " months";
            else monthString = " month";
            if (age.days > 1) dayString = " days";
            else dayString = " day";

            let curYear, curMon, curDay;


            if ((age.years > 0) && (age.months > 0) && (age.days > 0)) {
                ageString = age.years + yearString + ", " + age.months + monthString + ", and " + age.days + dayString + " old.";
                curYear = age.years;
                curMon = age.months + 1;
                curDay = age.days;
            } else if ((age.years == 0) && (age.months == 0) && (age.days > 0)) {
                ageString = "Only " + age.days + dayString + " old!";
                curYear = age.years;
                curMon = age.months + 1;
                curDay = age.days;
            } else if ((age.years > 0) && (age.months == 0) && (age.days == 0)) {
                ageString = age.years + yearString + " old. Happy Birthday!!";
                curYear = age.years;
                curMon = age.months + 1;
                curDay = age.days;
            } else if ((age.years > 0) && (age.months > 0) && (age.days == 0)) {
                ageString = age.years + yearString + " and " + age.months + monthString + " old.";
                curYear = age.years;
                curMon = age.months + 1;
                curDay = age.days;
            } else if ((age.years == 0) && (age.months > 0) && (age.days > 0)) {
                ageString = age.months + monthString + " and " + age.days + dayString + " old.";
                curYear = age.years;
                curMon = age.months + 1;
                curDay = age.days;
            } else if ((age.years > 0) && (age.months == 0) && (age.days > 0)) {
                ageString = age.years + yearString + " and " + age.days + dayString + " old.";
                curYear = age.years;
                curMon = age.months + 1;
                curDay = age.days;
            } else if ((age.years == 0) && (age.months > 0) && (age.days == 0)) {
                ageString = age.months + monthString + " old.";
                curYear = age.years;
                curMon = age.months + 1;
                curDay = age.days;
            } else ageString = "Oops! Could not calculate age!";

            return {curYear, curMon, curDay};
        };

        $("#dob").on('change', function () {
            var date = $(this).val();
            var resData = getAge(date);
            console.log(resData);
            $("#age").val(resData.curYear);
        });

        $("#submit").click(function (e) {
            // e.preventDefault();
            var logoimg = document.getElementById("image");
            let size = logoimg.files[0].size;
            // if (size > 13334325) {
            if (size > 3000000) {
                alert("Logo Image Size is exceeding 2 Mb");
                e.preventDefault();
            }
        });

        $('#dist_pre').on('change', function () {
            var districtID = $(this).val();
            var token = $("input[name='_token']").val();
            var url = "{{ route('upazila.userUpazilaSelectAjaxList') }}";
            if (districtID) {
                $('select[name="upo_pre"]').empty();
                $('select[name="post_pre"]').empty();
                upazillaList(districtID, token, url, 'pre');
            } else {
                $('select[name="upo_pre"]').empty();
                $('select[name="post_pre"]').empty();
            }
        });

        $('#upo_pre').on('change', function () {
            var upazilaID = $(this).val();
            var token = $("input[name='_token']").val();
            var url = "{{ route('union.userUnionSelectAjaxList') }}";
            if (upazilaID) {
                $('select[name="uni_pre"]').empty();
                unionList(upazilaID, token, url, 'pre');
            } else {
                $('select[name="uni_pre"]').empty();
            }
        });

        $('#dist_per').on('change', function () {
            var districtID = $(this).val();
            var token = $("input[name='_token']").val();
            var url = "{{ route('upazila.userUpazilaSelectAjaxList') }}";
            if (districtID) {
                $('select[name="upo_per"]').empty();
                $('select[name="post_per"]').empty();
                upazillaList(districtID, token, url, 'per');
            } else {
                $('select[name="upo_per"]').empty();
                $('select[name="post_per"]').empty();
            }
        });

        $('#upo_per').on('change', function () {
            var upazilaID = $(this).val();
            var token = $("input[name='_token']").val();
            var url = "{{ route('union.userUnionSelectAjaxList') }}";
            if (upazilaID) {
                $('select[name="uni_per"]').empty();
                unionList(upazilaID, token, url, 'per');
            } else {
                $('select[name="uni_per"]').empty();
            }
        });

        function upazillaList(districtID, token, url, type) {
            $.ajax({
                url: url,
                method: 'POST',
                data: {districtID: districtID, _token: token},
                dataType: "json",
                success: function (data) {
                    console.log(data);
                    if (type == 'pre') {
                        $('select[name="upo_pre"]').append('<option value="">' + "--- বাছাই করুন ---" + '</option>');
                    } else {
                        $('select[name="upo_per"]').append('<option value="">' + "--- বাছাই করুন ---" + '</option>');
                    }
                    $.each(data, function (key, value) {
                        if (type == 'pre')
                            $('select[name="upo_pre"]').append('<option value="' + key + '">' + value + '</option>');
                        else
                            $('select[name="upo_per"]').append('<option value="' + key + '">' + value + '</option>');
                    });
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert("Status: " + textStatus);
                    alert("Error: " + errorThrown);
                    console.log(XMLHttpRequest);
                    console.log(textStatus);
                    console.log(errorThrown);
                }
            });
        };

        function unionList(upazilaID, token, url, type) {
            $.ajax({
                url: url,
                method: 'POST',
                data: {upazilaID: upazilaID, _token: token},
                dataType: "json",
                success: function (data) {
                    console.log(data);
                    if (type == 'pre') {
                        $('select[name="uni_pre"]').append('<option value="">' + "--- বাছাই করুন ---" + '</option>');
                        $('select[name="uni_pre"]').append('<option value="' + 0 + '">' + "পৌরসভা" + '</option>');
                    } else {
                        $('select[name="uni_per"]').append('<option value="">' + "--- বাছাই করুন ---" + '</option>');
                        $('select[name="uni_per"]').append('<option value="' + 0 + '">' + "পৌরসভা" + '</option>');
                    }
                    $.each(data, function (key, value) {
                        if (type == 'pre') {
                            $('select[name="uni_pre"]').append('<option value="' + key + '">' + value + '</option>');
                        } else {
                            $('select[name="uni_per"]').append('<option value="' + key + '">' + value + '</option>');
                        }
                    });
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert("Status: " + textStatus);
                    alert("Error: " + errorThrown);
                    console.log(XMLHttpRequest);
                    console.log(textStatus);
                    console.log(errorThrown);
                }
            });
        };

        $("#same_add").on("change", function () {
            let dist_pre = $("#dist_pre").val();
            let up_pre = $("#up_pre").val();
            let post_pre = $("#post_pre").val();
            if ($("#post_pre").val()) {
                if ($(this).prop("checked") == true) {
                    $("#dist_per").prop("disabled", true);
                    $("#up_per").prop("disabled", true);
                    $("#post_per").prop("disabled", true);
                    $("#vill_per").prop("disabled", true);
                    console.log("Checkbox is checked.")
                } else if ($(this).prop("checked") == false) {
                    $("#dist_per").prop("disabled", false);
                    $("#up_per").prop("disabled", false);
                    $("#post_per").prop("disabled", false);
                    $("#vill_per").prop("disabled", false);
                    console.log("Checkbox is unchecked.")
                }
            } else {
                alert(" দয়া  করে  বর্তমান  ঠিকানা  আগে  বাছাই করুন।");
                $(this).prop("checked", false);
            }
        });

        let hidden_val = '<?php echo $hideenData ?>';

        $(".othr").hide();
        if (hidden_val == 2) {
            $(".othr").show();
        }

        $("#home_type").on("change", function () {
            let othrVal = $(this).val();
            alert(othrVal);
            if (othrVal == 2)
                $(".othr").show();
            else
                $(".othr").hide();
        });

    </script>
@stop