@extends('layouts.user')

@section('title')
    Print Card
@stop

@section('breadcrumb')
    <ul class="breadcrumb">
        <li>
            <i class="ace-icon fa fa-bars home-icon"></i>
            <a href="{{ route('registration.userList') }}">Registration List</a>
        </li>

        {{--        <li>--}}
        {{--            <a href="{{ route('registration.userForm') }}">Registration Add</a>--}}
        {{--        </li>--}}
    </ul>
@stop

@section('page_header')
    <h1>Card</h1>
@stop

@section('content')
    <div class="col-xs-12">
        <div class="row">
            <div class="col-sm-12">
                {{--                <h3 class="header smaller lighter blue">--}}
                {{--                    কার্ড নং--}}
                {{--                </h3>--}}
                <div class="col-sm-12">
                    <button class="btn btn-primary" onclick="printDiv('printMe')">Print Card</button>
                    <span><b>Total number of print of this card is: <?php echo $reliefData['print_hit']; ?></b></span>
                </div>
                <div id='printMe' class="col-sm-12">
                    <table class="tbl_stl">

                        <tr>
                            <td>
                                <?php
                                $idArr = array();

                                //                                pr($length);
                                //                                pr($arr1);
                                //                                dd($reliefData);

                                for($count = 1; $count < 2; $count++){
                                ?>
                                <div class="card" width="100%">
                                    <table class="tbl_stl" width="100%">
                                        <tr>
                                            <td rowspan="2">
                                                <table align="center">
                                                    <tr>
                                                        <td style="padding: 5px">
                                                            {{--                                                            <img src="{{ asset('qr_code/{{ $reliefData }}/22.pngs') }}" class="msg-photos" width="80px" height="120px" style="border: 1px solid silver"/>--}}
                                                            <?php
                                                            //                                                            if($reliefData['id'] == 3519){
                                                            if(empty($reliefData['id'])){?>
                                                            <img src="{{ asset('storage/trainee/1/11.pngs') }}"
                                                                 class="msg-photos" width="140px"
                                                                 height="140"
                                                                 style="border: 1px solid silver"/>
                                                            <?php } else {
                                                            ?>
                                                            <img src="{{ asset('storage/family/'.$reliefData->id.'/'.$reliefData->id.'.jpg') }}"
                                                                 class="msg-photo" alt="Kate's Avatar" width="140px"
                                                                 height="140px" style="border: 1px solid silver"/>

                                                            <?php } ?>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td>
                                                <table width="100%" height="120px">
                                                    <tr>
                                                        <td valign="top">
                                                            <h3 class="id_card" style="text-align: center">পরিবার
                                                                পরিচিতি কার্ড </h3>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table width="100%" border="1px" align="center" hight="auto"
                                                                   class="tbl_stls">
                                                                <tr>
                                                                    <td align="left" width='10%'>কার্ড নং:</td>
                                                                    <?php
                                                                    //                                                                    pr($reliefData['card_no']);
                                                                    $arrList = str_split($reliefData['card_no']);
                                                                    foreach ($arrList as $key => $value){ ?>

                                                                    <td width="7%"
                                                                        align="center"><?php echo $value; ?></td>
                                                                    <?php }
                                                                    ?>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td rowspan="2">
                                                <table align="center">
                                                    <tr>
                                                        <td style="padding: 5px">
                                                            <img src="{{ asset('images/qr_code/'.$reliefData->id.'/'.$reliefData->id.'.png') }}"
                                                                 class="msg-photo" alt="Kate's Avatar" width="140px"
                                                                 height="140"/>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>


                                        {{--                                        <tr>--}}
                                        {{--                                            <td colspan="3" align="center">--}}
                                        {{--                                                <h3 class="id_card" style="">পরিবার পরিচিতি কার্ড </h3>--}}
                                        {{--                                            </td>--}}
                                        {{--                                        </tr>--}}
                                        {{--                                        <tr>--}}
                                        {{--                                            <td width="20%">--}}
                                        {{--                                                <table align="center">--}}
                                        {{--                                                    <tr>--}}
                                        {{--                                                        <td style="padding: 10px">--}}
                                        {{--                                                            <img src="{{ asset('storage/trainee/1/22.png') }}"--}}
                                        {{--                                                                 class="msg-photo" alt="Kate's Avatar" width="100px"--}}
                                        {{--                                                                 height="60px"/>--}}
                                        {{--                                                        </td>--}}
                                        {{--                                                    </tr>--}}
                                        {{--                                                </table>--}}
                                        {{--                                            </td>--}}
                                        {{--                                            <td width="60%">--}}
                                        {{--                                                --}}{{--                                                <table align="center">--}}
                                        {{--                                                --}}{{--                                                    <tr>--}}
                                        {{--                                                --}}{{--                                                        <td><h3 class="id_card" style="">পরিবার পরিচিতি কার্ড </h3></td>--}}
                                        {{--                                                --}}{{--                                                    </tr>--}}
                                        {{--                                                --}}{{--                                                    <tr></tr>--}}
                                        {{--                                                --}}{{--                                                </table>--}}
                                        {{--                                                <table width="100%" border="1px" align="center" hight="auto"--}}
                                        {{--                                                       class="tbl_stl">--}}
                                        {{--                                                    <tr>--}}
                                        {{--                                                        <td align="left" width="10%">কার্ড নং:</td>--}}
                                        {{--                                                        <td></td>--}}
                                        {{--                                                        <td></td>--}}
                                        {{--                                                        <td></td>--}}
                                        {{--                                                        <td></td>--}}
                                        {{--                                                        <td></td>--}}
                                        {{--                                                        <td></td>--}}
                                        {{--                                                        <td></td>--}}
                                        {{--                                                        <td></td>--}}
                                        {{--                                                        <td></td>--}}
                                        {{--                                                        <td></td>--}}
                                        {{--                                                        <td></td>--}}
                                        {{--                                                        <td></td>--}}
                                        {{--                                                    </tr>--}}
                                        {{--                                                </table>--}}
                                        {{--                                            </td>--}}
                                        {{--                                            <td width="20%">--}}
                                        {{--                                                <table align="center">--}}
                                        {{--                                                    <tr>--}}
                                        {{--                                                        <td style="padding: 10px">--}}
                                        {{--                                                            <img src="{{ asset('storage/trainee/1/11.png') }}"--}}
                                        {{--                                                                 class="msg-photo" alt="Kate's Avatar" width="100px"--}}
                                        {{--                                                                 height="60"/>--}}
                                        {{--                                                        </td>--}}
                                        {{--                                                    </tr>--}}
                                        {{--                                                    <tr>--}}
                                        {{--                                                        <td>&nbsp;</td>--}}
                                        {{--                                                    </tr>--}}
                                        {{--                                                </table>--}}
                                        {{--                                            </td>--}}
                                        {{--                                        </tr>--}}
                                    </table>

                                    <table class="tbl_stl" width="100%" hight=""
                                           style="font-size: 14px;">
                                        <tr width="100%">
                                            <td width="50%" valign="top">
                                                <table width="100%">
                                                    <tr>
                                                        <td width="25%" valign="top">নাম</td>
                                                        <td>:</td>
                                                        <td><?php echo $reliefData['name']; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="25%" valign="top">পেশা:</td>
                                                        <td>:</td>
                                                        <td><?php echo $reliefData['profession']; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="25%" valign="top">মোবাইল</td>
                                                        <td>:</td>
                                                        <td><?php echo $reliefData['phone']; ?></td>
                                                    </tr>

                                                    <tr>
                                                        <td width="25%" valign="top">পরিচয় পত্র নং</td>
                                                        <td>:</td>
                                                        <td><?php echo $reliefData['nid']; ?></td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td width="50%">
                                                <table width="100%">
                                                    <tr>
                                                        <?php if(!empty($reliefData['g_name'])){ ?>
                                                        <td width="25%" valign="top">পিতা</td>
                                                        <td>:</td>
                                                        <td><?php echo $reliefData['g_name']; ?></td>
                                                        <?php } else{ ?>

                                                        <td width="25%" valign="top">স্বামী/স্ত্রী</td>
                                                        <td>:</td>
                                                        <td><?php echo $reliefData['hw_name']; ?></td>
                                                        <?php }?>
                                                    </tr>
                                                    <tr>
                                                        <td width="25%" valign="top">ঠিকানা:</td>
                                                        <td>:</td>
                                                        <td>
                                                            {{--                                                {{ $relief->house_pre }}, {{ $relief->road_pre }}, {{ $relief->word_pre }} , {{ $relief->upa_name }}, {{ $relief->dis_name }}--}}
                                                            <?php
                                                            $uniName = 'পৌরসভা';
                                                            if ($reliefData['uni_pre'] != 0)
                                                                $uniName = $reliefData['uni_name'];

                                                            echo 'বাড়ি নংঃ ' . $reliefData['house_pre'] . ', হোল্ডিং নংঃ ' . $reliefData['road_pre'] . ', রোড নংঃ ' . $reliefData['road_pre'] . ', ওয়ার্ড নংঃ ' . $reliefData['word_pre'] . ', ইউনিয়নঃ ' . $uniName . ', উপজেলাঃ ' . $reliefData['upa_name'] . ', জেলাঃ ' . $reliefData['dis_name']; ?>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <br/>

                                    <table border="1px" style="font-size:12px;" width="100%" class="tbl_stl">
                                        <tr align="center" style="font-weight: bold; font-size: 14px">
                                            <td Width="15%" height="25px">সেবা প্রদানের তারিখ</td>
                                            <td Width="45%">সেবার বিবরণ</td>
                                            <td Width="25%">পরিমাণ</td>
                                            <td Width="15%">মন্তব্য</td>
                                        </tr>
                                        <tr>
                                            <td class="blank_row" height="40px"></td>
                                            <td height="15px" style="text-align: center"></td>
                                            <td height="15px"></td>
                                            <td height="15px"></td>
                                        </tr>
                                        <?php
                                        for($rows = 1; $rows < 9; $rows++){ ?>
                                        <tr>
                                            <td class="blank_row" height="40px"></td>
                                            <td height="15px"></td>
                                            <td height="15px"></td>
                                            <td height="15px"></td>
                                        </tr>

                                        <?php }
                                        ?>
                                    </table>

                                    <br/>
                                    <table class="center" width="100%">
                                        <tr>
                                            <td>
                                                <span style="font-weight: bold; text-align: center">জেলা প্রশাসন, গোপালগঞ্জ</span>
                                            </td>
                                        </tr>
                                    </table>

                                    {{--                                    <div style="margin: auto; text-align: center">--}}
                                    {{--                                        <span style="font-weight: bold">জেলা প্রশাসন, গোপালগঞ্জ</span>--}}
                                    {{--                                    </div>--}}

                                </div>

                                <?php } ?>
                            </td>

                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>

@stop

@section('custom_style')
    <style type="text/css">
        .tbl_stl {
            width: 80%;
        }

        .msg-photo {
            padding: 10px;
        }

        @media print {
            @page {
                margin: 0;
            }

            body {
                margin: 0;
            }
        }

        table {
            /*border: 1px solid silver;*/
        }

    </style>
@stop

@section('custom_script')
    <script>
        function printDiv(divName) {
            let hitVal = 1;
            var h_id = "<?php echo $id;?>";
            var token = $("input[name='_token']").val();
            var url = "{{ route('registration.userCountPrintHit') }}";

            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;

            $.ajax({
                url: url,
                method: 'POST',
                data: {h_id:h_id,hitVal:hitVal, _token: token},
                dataType: "json",
                success: function (scatterSeries) {
                    console.log(scatterSeries);
                    document.body.innerHTML = printContents;
                    window.print();
                    document.body.innerHTML = originalContents;
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    console.log(XMLHttpRequest);
                    console.log(textStatus);
                    console.log(errorThrown);
                    // $("#submit").disable();
                    // console.log("adasd");
                }
            });
        }
    </script>
@stop