@extends('layouts.user')

@section('title')
    Add Trade
@stop

@section('breadcrumb')
    <ul class="breadcrumb">
        <li>
            <i class="ace-icon fa fa-bars home-icon"></i>
            <a href="{{ route('trade.clientList') }}">Trade List</a>
        </li>

        <li>
            <a href="{{ route('trade.clientForm') }}">Trade Add</a>
        </li>
        {{--<li class="active">User Profile</li>--}}
    </ul>
@stop

@section('page_header')
    <h1>Add Trade</h1>
@stop

@section('content')
    <div class="col-xs-12">
        <form class="form-horizontal" role="form" action="{{ route('trade.clientStore') }}" method="POST">
            {{ csrf_field() }}
            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="trade_name"> Trade Name *</label>

                <div class="col-sm-9">
                    <input type="text" id="trade_name" placeholder="Trade Name" name="trade_name"
                           class="col-xs-10 col-sm-5"/>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('trade_name'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('trade_name') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="space-4"></div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="trade_code"> Trade Code</label>

                <div class="col-sm-9">
                    <input type="text" id="trade_code" placeholder="Trade Code" name="trade_code"
                           class="col-xs-10 col-sm-5"/>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('trade_code'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('trade_code') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="space-4"></div>

            {{--<div class="form-group">--}}
                {{--<label class="col-sm-3 control-label no-padding-right" for="form-field-1-1"> Status </label>--}}

                {{--<div class="col-sm-9">--}}
                    {{--<select name='status' class="col-xs-10 col-sm-5" id="form-field-select-1">--}}
                        {{--<option value="1">Active</option>--}}
                        {{--<option value="0">Inactive</option>--}}
                    {{--</select>--}}
                {{--</div>--}}
            {{--</div>--}}

            <div class="space-4"></div>

            <div class="clearfix form-actions">
                <div class="col-md-offset-3 col-md-9">
                    <button class="btn btn-info" type="submit">
                        <i class="ace-icon fa fa-check bigger-110"></i>
                        Submit
                    </button>

                    &nbsp; &nbsp; &nbsp;
                    <button class="btn" type="reset">
                        <i class="ace-icon fa fa-undo bigger-110"></i>
                        Reset
                    </button>
                </div>
            </div>
        </form>
    </div>
@stop


@section('custom_style')

@stop

@section('custom_script')

@stop