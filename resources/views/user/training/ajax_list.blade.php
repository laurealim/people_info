<table id="dynamic-table" class="table table-striped table-bordered table-hover" style="margin-bottom: 0px;">
    <thead>
    <tr>
        <th class="center" width="4%">
            Sr. No.
        </th>
        <th width="8%">প্রশিক্ষণের নাম</th>
        <th width="8%">প্রশিক্ষণের স্থান</th>
        <th width="5%">প্রশিক্ষণের সময়কাল (দিন)</th>
        <th width="6%">প্রশিক্ষণ প্রদানকারী সংস্থা</th>
        <th width="8%">ট্রেডের নাম</th>
        <th width="5%">প্রতিষ্ঠানের কোড নং</th>
        <th width="5%">প্রশিক্ষণ অনুমোদনকারী সংস্থা</th>
        <th width="5%">প্রশিক্ষণ অর্থায়নকারী সংস্থা</th>
        <th width="6%">অর্থায়ন মূল্য</th>
        <th width="5%">ট্রেনিং এর শুরুর দিন</th>
        {{--        <th width="5%">ট্রেনিং এর শেষের দিন</th>--}}
        <th width="5%">পরীক্ষার দিন</th>
        <th width="5%">প্রশিক্ষণের স্ট্যাটাস</th>
        <th class="center" width="15%">Action</th>
    </tr>
    </thead>

    <tbody>

    <!--    --><?php //dd($trainingList); ?>
    <?php $pageNo = (($trainingList->currentPage() - 1) * $trainingList->perPage()) + 1;?>
    @foreach ($trainingList as $training)
        <!--        --><?php //pr($training);?>
        <tr>
            <td class="center">
                <label class="pos-rel">
                    {{--<input type="checkbox" class="ace"/>--}}
                    <span class="lbl">{{ $pageNo++ }}</span>
                </label>
            </td>
            <td>
                {{ $training->name }}
            </td>
            <td>
                {{ $training->training_place}}
            </td>
            <td>
                {{ $training->total_day }}
            </td>
            <td>
                {{ $departmentList[$training->dept_id] }}
            </td>
            <td>
                {{ $tradeList[$training->trade_name] }}
            </td>
            <td>
                {{ $training->code }}
            </td>
            <td class="center">
                {{ $training->proposal_org }}
            </td>
            <td>
                {{ $training->financial_org }}
            </td>
            <td>
                {{ $training->budget }}
            </td>
            <td>
                {{ date('Y-m-d', strtotime($training->start_date))  }}
            </td>
            <td>
                {{ date('Y-m-d', strtotime($training->exam_date)) }}
            </td>
            <td class="center">
                <?php if ($training->status == config('constants.training_status.Pending')) {?>
                <span class='label label-warning'><?php echo config('constants.training_status.1'); ?></span>
                <?php } else if ($training->status == config('constants.training_status.Complete')) {?>
                <span class='label label-success'><?php echo config('constants.training_status.2'); ?></span>
                <?php } else {?>
                <span class='label label-danger'><?php echo config('constants.training_status.0'); ?></span>
                <?php }?>
            </td>
            <td class="center">
                <div class="hidden-sm hidden-xs action-buttons">
                    {{--<a class="blue" href="{{ route('') }}">--}}
                    {{--<i class="ace-icon fa fa-search-plus bigger-200"></i>--}}
                    {{--</a>--}}

                    {{--&nbsp;&nbsp;| &nbsp;&nbsp;--}}

                    <?php if($training->status == config('constants.training_status.Pending')){?>
                    <a class="green" href="{{ route('training.clientEdit',$training->id) }}" title="Edit">
                        <i class="ace-icon fa fa-pencil bigger-150"></i>
                    </a>

                    &nbsp;|

                    <a class="red deletebtn" href="{{ route('training.clientDestroy',$training->id) }}"
                       id="{{ $training->id }}" title="Delete">
                        <i class="ace-icon fa fa-trash-o bigger-150"></i>
                    </a>

                    &nbsp;|

                    <a class="blue" href="{{ route('registration.clientListId',$training->id) }}">
                        <i class="ace-icon fa fa-users bigger-150" title="Registered Trainee List"></i>
                    </a>
                    &nbsp; |

                    <?php }?>


                    <a class="grey" href="{{ route('participant.clientListId',$training->id) }}">
                        <i class="ace-icon fa fa-check-square bigger-150" title="Approved Trainee List"></i>
                    </a>
                </div>

            </td>
        </tr>
    @endforeach
    </tbody>
</table>


<div class="row">
    <div class="col-xs-6">
        <div class="dataTables_info" id="dynamic-table_info" role="status" aria-live="polite">
            Showing {{ (($trainingList->currentPage() - 1) * $trainingList->perPage()) + 1 }}
            to {{ (($trainingList->currentPage() - 1) * $trainingList->perPage()) + $trainingList->perPage() }} of
            {{ $trainingList->total() }} entries
        </div>
    </div>
    <div class="col-xs-6">
        <div class="dataTables_paginate paging_simple_numbers" id="dynamic-table_paginate">
            {{ $trainingList->links("vendor.pagination.custom") }}
        </div>
    </div>
</div>

<style type="text/css">
    /*.table tbody > tr > td {*/
    /*text-align: center;*/
    /*}*/
    .facility {
        font-weight: bold;
    }
</style>