@extends('layouts.user')

@section('title')
    User Dashboard
@stop

@section('breadcrumb')
    <ul class="breadcrumb">
        <li>
            <i class="ace-icon fa fa-home home-icon"></i>
            @if($user === 'user')
                <a href="{{ route('user.dashboard') }}">Dashboard</a>
            @else
                <a href="{{ route('client.dashboard') }}">Dashboard</a>
            @endif
        </li>
    </ul>
@stop

@section('page_header')
    <h1>
        @if($user === 'user')
            <?php echo $dept_id; ?> Dashboard
        @else
            <?php echo $dept_id; ?> Dashboard
        @endif
    </h1>
@stop

@section('content')
    @if($user === 'user')
        <div class="col-xs-12">
            <div class="col-sm-6">
                <div class="table-header">
                    Training Registration and Participant List
                </div>
                <div class="col-sm-12" id="tran_reg">
                    <div class="col-sm-4">&nbsp;</div>
                    <div class="col-sm-4 pull-rights center spinner-preview" id="spinner-preview_reg"> Loading</div>
                    <div class="col-sm-4">&nbsp;</div>
                </div>
                <div class="tran_reg_tbl"></div>
            </div>
            <div class="col-sm-6">
                &nbsp;&nbsp;
            </div>

            <div class="clearfix"></div>
            <div class="hr hr8 hr-double">
            </div>
        </div>
    @else
        <div class="col-xs-12">
            <div class="col-sm-6">
                <div class="table-header">
                    Training Registration and Participant List
                </div>
                <div class="col-sm-12" id="tran_reg">
                    <div class="col-sm-4">&nbsp;</div>
                    <div class="col-sm-4 pull-rights center spinner-preview" id="spinner-preview_reg"> Loading</div>
                    <div class="col-sm-4">&nbsp;</div>
                </div>
                <div class="tran_reg_tbl"></div>
            </div>
            <div class="col-sm-6">
                <div class="table-header">
                    Last Upcoming 5 Trainings
                </div>
                <div class="col-sm-12" id="tran_last">
                    <div class="col-sm-4">&nbsp;</div>
                    <div class="col-sm-4 pull-rights center spinner-preview" id="spinner-preview_last"> Loading</div>
                    <div class="col-sm-4">&nbsp;</div>
                </div>
                <div class="tran_latest_tbl"></div>
            </div>

            <div class="clearfix"></div>
            <div class="hr hr8 hr-double">
            </div>
        </div>
    @endif
@stop

@section('custom_style')
    <style type="text/css">
        .spinner-preview {
            /*width: 100px;*/
            height: 100px;
            text-align: center;
        !important;
        }
    </style>
@stop

@section('custom_script')
    <script type="text/javascript">
        $.fn.spin = function (opts) {
            this.each(function () {
                var $this = $(this),
                    data = $this.data();

                if (data.spinner) {
                    data.spinner.stop();
                    delete data.spinner;
                }
                if (opts !== false) {
                    data.spinner = new Spinner($.extend({color: $this.css('color')}, opts)).spin(this);
                }
            });
            return this;
        };

        var opts = {
            corners: 1,
            left: "auto",
            length: 5,
            lines: 11,
            radius: 15,
            rotate: 11,
            speed: 1.2,
            trail: 43,
            width: 5
        }
        // $('#spinner-preview_sts').spin(opts);
        $('#spinner-preview_reg').spin(opts);
        $('#spinner-preview_last').spin(opts);

        function loadRegistrationData(url) {
            $.ajax({
                url: url,
                dataType: 'html',
                type: 'GET',
                data: {status: 1},
                success: function (data) {
                    $("#tran_reg").hide();
                    var table = '<table class="table table-bordered" id="tran_reg_tbl">\n' +
                        '                <thead>\n' +
                        '                <tr>\n' +
                        '                    <th scope="col">Training Name</th>\n' +
                        '                    <th scope="col">Total Registration</th>\n' +
                        '                    <th scope="col">Total Participant</th>\n' +
                        '                </tr>\n' +
                        '                </thead>\n' +
                        '                <tbody>';

                    $.each(JSON.parse(data), function (key, value) {
                        table = table + '<tr><td>' + value.training_name + '</td><td>' + value.total_reg + '</td><td>' + value.reg_confirm + '</td></tr>';
                    });

                    table = table + '</tbody>\n' +
                        '            </table>';
                    $('.tran_reg_tbl').html(table);
                },
                error: function (err) {
                    alert('Data could not be loaded.');
                }
            });
        }

        function loadLatestData(url) {
            $.ajax({
                url: url,
                dataType: 'html',
                type: 'GET',
                data: {status: 1},
                success: function (data) {
                    console.log(JSON.parse(data));
                    $("#tran_last").hide();
                    var table = '<table class="table table-bordered" id="tran_latest_tbl">\n' +
                        '                <thead>\n' +
                        '                <tr>\n' +
                        '                    <th scope="col">Department Name</th>\n' +
                        '                    <th scope="col">Training Name</th>\n' +
                        '                    <th scope="col">Total Day</th>\n' +
                        '                    <th scope="col">Start Date</th>\n' +
                        '                    <th scope="col">Exam Date</th>\n' +
                        '                    <th scope="col">Budget(BDT)</th>\n' +
                        '                </tr>\n' +
                        '                </thead>\n' +
                        '                <tbody>';

                    $.each(JSON.parse(data), function (key, value) {
                        table = table + '<tr><td>' + value.name + '</td><td>' + value.training_name + '</td><td>' + value.total_day + '</td><td>' + value.start_date + '</td><td>' + value.exam_date + '</td><td>' + value.budget + '</td></tr>';
                    });

                    table = table + '</tbody>\n' +
                        '            </table>';
                    $('.tran_latest_tbl').html(table);
                },
                error: function (err) {
                    alert('Data could not be loaded.');
                }
            });
        }

        $(window).on('load', function () {
            // loadStatusData();
            var userType = '<?php echo $user; ?>';
            if (userType === 'user') {
                var urlReg = "<?php echo route('dashboard.userGetRegistration'); ?>";
                loadRegistrationData(urlReg);
            } else if (userType === 'client') {
                var urlReg = "<?php echo route('dashboard.clientGetRegistration'); ?>";
                var urlLast = "<?php echo route('dashboard.clientGetLastTraining'); ?>";
                loadRegistrationData(urlReg);
                loadLatestData(urlLast);
            }
        });

    </script>
@stop