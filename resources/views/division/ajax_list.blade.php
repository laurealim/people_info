<div id="dynamic-table_wrapper" class="dataTables_wrapper form-inline no-footer">
    <table id="dynamic-table" class="table table-striped table-bordered table-hover" style="margin-bottom: 0px;">
        <thead>
        <tr>
            <th class="center" width="6%">
                <label class="pos-rel">
                    {{--<input type="checkbox" class="ace"/>--}}
                    {{--<span class="lbl"></span>--}}
                </label>
            </th>
            <th width="28%">Division Name</th>
            <th width="28%">Division Name (Bangla)</th>
            <th width="28%">Division URL</th>
            <th class="center" width="10%">Action</th>
        </tr>
        </thead>

        <tbody>

        <?php $pageNo = (($divisions->currentPage() - 1) * $divisions->perPage()) + 1 ?>

        @foreach ($divisions as $division)
            <tr>
                <td class="center">
                    <label class="pos-rel">
                        {{--<input type="checkbox" class="ace"/>--}}
                        <span class="lbl">{{ $pageNo++ }}</span>
                    </label>
                </td>
                <td>
                    {{ $division->name}}
                </td>
                <td>
                    {{ $division->bn_name}}
                </td>
                <td>
                    {{ $division->url }}
                </td>
                <td class="center">
                    <div class="hidden-sm hidden-xs action-buttons">
                        {{--<a class="blue" href="{{ route('') }}">--}}
                        {{--<i class="ace-icon fa fa-search-plus bigger-200"></i>--}}
                        {{--</a>--}}

                        {{--&nbsp;&nbsp;| &nbsp;&nbsp;--}}

                        <a class="green" href="{{ route('division.adminEdit',$division->id) }}">
                            <i class="ace-icon fa fa-pencil bigger-200"></i>
                        </a>

                        &nbsp;&nbsp;| &nbsp;&nbsp;

                        <a class="red deletebtn" href="{{ route('division.adminDelete',$division->id) }}" id={{ $division->id }}>
                        <i class="ace-icon fa fa-trash-o bigger-200"></i>
                        </a>
                    </div>

                    {{--<div class="hidden-md hidden-lg">--}}
                    {{--<div class="inline pos-rel">--}}
                    {{--<button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown"--}}
                    {{--data-position="auto">--}}
                    {{--<i class="ace-icon fa fa-caret-down icon-only bigger-120"></i>--}}
                    {{--</button>--}}
                    {{--<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">--}}
                    {{--<li>--}}
                    {{--<a href="#" class="tooltip-info" data-rel="tooltip" title="View">--}}
                    {{--<span class="blue">--}}
                    {{--<i class="ace-icon fa fa-search-plus bigger-120"></i>--}}
                    {{--</span>--}}
                    {{--</a>--}}
                    {{--</li>--}}

                    {{--<li>--}}
                    {{--<a href="#" class="tooltip-success" data-rel="tooltip" title="Edit">--}}
                    {{--<span class="green">--}}
                    {{--<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>--}}
                    {{--</span>--}}
                    {{--</a>--}}
                    {{--</li>--}}

                    {{--<li>--}}
                    {{--<a href="#" class="tooltip-error" data-rel="tooltip" title="Delete">--}}
                    {{--<span class="red">--}}
                    {{--<i class="ace-icon fa fa-trash-o bigger-120"></i>--}}
                    {{--</span>--}}
                    {{--</a>--}}
                    {{--</li>--}}
                    {{--</ul>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div class="row">
        <div class="col-xs-6">
            <div class="dataTables_info" id="dynamic-table_info" role="status" aria-live="polite">
                Showing {{ (($divisions->currentPage() - 1) * $divisions->perPage()) + 1 }}
                to {{ (($divisions->currentPage() - 1) * $divisions->perPage()) + $divisions->perPage() }} of
                {{ $divisions->total() }} entries
            </div>
        </div>
        <div class="col-xs-6">
            <div class="dataTables_paginate paging_simple_numbers" id="dynamic-table_paginate">
                {{ $divisions->links("vendor.pagination.custom") }}
            </div>
        </div>
    </div>
    {{--<div style="width: 100%">--}}
    {{--</div>--}}
    {{--{{ $cities->count() }}--}}
    {{--<br>--}}
    {{--{{ $cities->currentPage() }}--}}
    {{--<br>--}}
    {{--{{ $cities->perPage() }}--}}
    {{--<br>--}}
    {{--<br>--}}
    {{--{{ $cities->total() }}--}}
    {{--<br>--}}
</div>
