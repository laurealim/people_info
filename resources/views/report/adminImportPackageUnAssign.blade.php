@extends('layouts.admin')

@section('title')
    Data Un-Assign File Import
@stop

@section('breadcrumb')
    {{--    <ul class="breadcrumb">--}}
    {{--        <li>--}}
    {{--            <i class="ace-icon fa fa-bars home-icon"></i>--}}
    {{--            <a href="{{ route('training.adminList') }}">Training List</a>--}}
    {{--        </li>--}}

    {{--        --}}{{--<li class="active">User Profile</li>--}}
    {{--    </ul>--}}
@stop

@section('page_header')
    <h1>Data Un-Assign File Import</h1>
@stop

@section('content')
    <div class="col-xs-12">
        <div>
            <div id="dynamic-table_wrapper" class="dataTables_wrapper form-inline no-footer">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="pull-right tableTools-container"></div>
                        <div style="padding-bottom: 10px;" class="clearfix">
                            <form class="form-horizontal" role="form" enctype="multipart/form-data"
                                  action="{{ route('report.adminSaveImportPackageUnAssign') }}"
                                  method="POST">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12">
                                        <div class="col-xs-4">
                                            <label class="col-sm-3 control-label no-padding-right"
                                                   for="package">Running Package</label>

                                            <div class="col-sm-9">
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <select id="package" placeholder="" name="package">
                                                            <option value="">---Please Select ---</option>
                                                            <?php foreach ($packageList as $id => $name) { ?>
                                                            <?php echo "<option value='{$id}'>{$name}</option>"; ?>
                                                            <?php }?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <label class="col-sm-4 control-label no-padding-right"
                                                   for="select_file">Select <span class="text-muted">.xls, .xslx</span>
                                                File only</label>
                                            <div class="col-sm-8">
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <input type="file" id="select_file" name="select_file">
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <button type="submit" class="btn btn-purple">
                                                        <i class="fa fa-cloud-upload bigger-120" aria-hidden="true"></i>
                                                        Import Excel File
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <a href={{ asset('sample_data.xlsx') }}>
                                                        <span class="btn btn-primary download">
                                                            <i class="fa fa-cloud-download bigger-120"
                                                               aria-hidden="true"></i>
                                                            Download Sample Data
                                                        </span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                @if(isset($msg))
                    @if(!empty($msg))
                        <ul>
                            @foreach($msg as $key => $vals)
                                <li> {{ ($key + 1) }} : {{ $vals }}</li>
                            @endforeach
                        </ul>
                    @endif
                @endif
            </div>
        </div>
    </div>
@stop

@section('custom_style')

@stop

@section('custom_script')
    <script type="text/javascript">
        $(".loading").hide();
        $('.date-picker').datepicker({
            autoclose: true,
            todayHighlight: true
        });
        // $(window).on('hashchange', function () {
        //     if (window.location.hash) {
        //         var page = window.location.hash.replace('#', '');
        //         if (page == Number.NaN || page <= 0) {
        //             return false;
        //         } else {
        //             getPosts(page);
        //         }
        //     }
        // });
        //
        // $(document).ready(function () {
        //     $("#ssnp_for").prop("disabled", true);
        // });

        // $("#ssnp").on('change',function() {
        //     if($(this).val() == ''){ // or this.value == 'volvo'
        //         $("#ssnp_for").prop("disabled", true);
        //         $("#package").prop("disabled", false);
        //         $("#firm_land").prop("disabled", false);
        //     }else{
        //         $("#ssnp_for").prop("disabled", false);
        //         $("#package").prop("disabled", true);
        //         $("#firm_land").prop("disabled", true);
        //     }
        // });
        // $("#package").on('change',function() {
        //     if($(this).val() == ''){ // or this.value == 'volvo'
        //         $("#ssnp").prop("disabled", false);
        //         $("#ssnp_for").prop("disabled", true);
        //         $("#firm_land").prop("disabled", false);
        //     }else{
        //         $("#ssnp").prop("disabled", true);
        //         $("#firm_land").prop("disabled", true);
        //     }
        // });
        // $("#firm_land").on('change',function() {
        //     if($(this).val() == ''){ // or this.value == 'volvo'
        //         $("#ssnp").prop("disabled", false);
        //         $("#ssnp_for").prop("disabled", true);
        //         $("#package").prop("disabled", false);
        //     }else{
        //         $("#ssnp").prop("disabled", true);
        //         $("#package").prop("disabled", true);
        //     }
        // });

        {{--$('#upo').on('change', function () {--}}
        {{--    var upazilaID = $(this).val();--}}
        {{--    var token = $("input[name='_token']").val();--}}
        {{--    var url = "{{ route('union.adminUnionSelectAjaxList') }}";--}}
        {{--    if (upazilaID) {--}}
        {{--        $('select[name="uni_per"]').empty();--}}
        {{--        unionList(upazilaID, token, url, 'per');--}}
        {{--    } else {--}}
        {{--        $('select[name="uni_per"]').empty();--}}
        {{--    }--}}
        {{--});--}}

        // function unionList(upazilaID, token, url, type) {
        //     $.ajax({
        //         url: url,
        //         method: 'POST',
        //         data: {upazilaID: upazilaID, _token: token},
        //         dataType: "json",
        //         success: function (data) {
        //             console.log(data);
        //             $('select[name="uni_per"]').append('<option value="">' + "--- বাছাই করুন ---" + '</option>');
        //             $('select[name="uni_per"]').append('<option value="' + 0 + '">' + "পৌরসভা" + '</option>');
        //             $.each(data, function (key, value) {
        //                 $('select[name="uni_per"]').append('<option value="' + key + '">' + value + '</option>');
        //             });
        //         },
        //         error: function (XMLHttpRequest, textStatus, errorThrown) {
        //             alert("Status: " + textStatus);
        //             alert("Error: " + errorThrown);
        //             console.log(XMLHttpRequest);
        //             console.log(textStatus);
        //             console.log(errorThrown);
        //         }
        //     });
        // };

        //         function getPosts(page, url) {
        //             console.log(url);
        // //            url: '?page=' + page
        //             var displayValue = $("#displayValue").val();
        //             var searchData = $("#searchData").val();
        //             $.ajax({
        //                 url: '?page=' + page + '&displayValue=' + displayValue + '&searchData=' + searchData,
        //                 success: function (data) {
        //                     $('.posts').html(data);
        //                 },
        //                 error: function (err) {
        //                     alert('Posts could not be loaded.');
        //                 }
        //             });
        //         };


        // $("#displayValue").on('change', function () {
        //     var displayValue = $(this).val();
        //     var url = window.location.href;
        //     dynamicDataViewCount(displayValue, '', url);
        // });

        // $("#searchData").on("click", function (e) {
        //     e.preventDefault();
        //     $(".loading").show();
        //     $(".search").hide();
        //     var dist = $('#dist').val();
        //     var upo = $('#upo').val();
        //     var uni_per = $('#uni_per').val();
        //     var word_no = $('#word_no').val();
        //     var name = $('#name').val();
        //     var phone = $('#phone').val();
        //     var nid = $('#nid').val();
        //     var card_type = $('#card_type').val();
        //     var card_no = $('#card_no').val();
        //     var ssnp = $('#ssnp').val();
        //     var packages = $('#packages').val();
        //     var poss_help = $('#poss_help').val();
        //     var search_by_id = $('#search_by_id').val();
        //     var firm_land = $('#firm_land').val();
        //     var baby_food = $('#baby_food').val();
        //     var url = window.location.href;
        //     $.ajax({
        //         url: url,
        //         dataType: 'html',
        //         type: 'GET',
        //         data: {dist: dist, upo: upo, uni_per: uni_per,word_no: word_no, name: name, phone: phone,nid: nid, card_type: card_type, card_no: card_no, ssnp: ssnp, packages: packages, poss_help: poss_help, search_by_id: search_by_id, firm_land: firm_land, baby_food: baby_food},
        //         success: function (data) {
        //             $(".loading").hide();
        //             $(".search").show();
        //             $('.posts').html(data);
        //         },
        //         error: function (err) {
        //             console.log(err);
        //             alert('Posts could not be loaded.');
        //         }
        //     })
        // });

        // $("#exportData").on("click", function (e) {
        //     e.preventDefault();
        //     var dist = $('#dist').val();
        //     var upo = $('#upo').val();
        //     var uni_per = $('#uni_per').val();
        //     var word_no = $('#word_no').val();
        //     var name = $('#name').val();
        //     var phone = $('#phone').val();
        //     var nid = $('#nid').val();
        //     var card_type = $('#card_type').val();
        //     var card_no = $('#card_no').val();
        //     var ssnp = $('#ssnp').val();
        //     var url = $(this).attr('href');
        //
        //     ajaxCall(dist, upo, uni_per, word_no, name, phone, nid, card_type, card_no, url);
        // });

        // function ajaxCall(training_id, trade_name, reg_status, url) {
        //
        // }

        // function dynamicDataViewCount(displayValue, searchData, url) {
        //     if (displayValue == '') {
        //         displayValue = $("#displayValue").val();
        //     }
        //     if (searchData == '') {
        //         searchData = $("#searchData").val();
        //     }
        //     $.ajax({
        //         url: url,
        //         dataType: 'html',
        //         type: 'GET',
        //         data: {displayValue: displayValue, searchData: searchData},
        //         success: function (data) {
        //             $('.posts').html(data);
        //         },
        //         error: function (err) {
        //             console.log(err);
        //             alert('Posts could not be loaded.');
        //         }
        //     });
        // }
    </script>

@stop