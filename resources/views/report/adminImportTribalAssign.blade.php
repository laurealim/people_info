@extends('layouts.admin')

@section('title')
    Tribal Assign File Import
@stop

@section('breadcrumb')
    {{--    <ul class="breadcrumb">--}}
    {{--        <li>--}}
    {{--            <i class="ace-icon fa fa-bars home-icon"></i>--}}
    {{--            <a href="{{ route('training.adminList') }}">Training List</a>--}}
    {{--        </li>--}}

    {{--        --}}{{--<li class="active">User Profile</li>--}}
    {{--    </ul>--}}
@stop

@section('page_header')
    <h1>Tribal Assign File Import</h1>
@stop

@section('content')
    <div class="col-xs-12">
        <div>
            <div id="dynamic-table_wrapper" class="dataTables_wrapper form-inline no-footer">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="pull-right tableTools-container"></div>
                        <div style="padding-bottom: 10px;" class="clearfix">
                            <form class="form-horizontal" role="form" enctype="multipart/form-data"
                                  action="{{ route('report.adminSaveImportTribalAssign') }}"
                                  method="POST">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12">

                                        <div class="col-xs-4">
                                            <label class="col-sm-4 control-label no-padding-right"
                                                   for="package">Tribal List</label>

                                            <div class="col-sm-8">
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <select id="tribal" placeholder="" name="tribal">
                                                            <option value="">---Please Select ---</option>
                                                            <?php foreach ($tribalList as $id => $name) { ?>
                                                                <?php echo "<option value='{$id}'>{$name}</option>"; ?>
                                                            <?php }?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <label class="col-sm-4 control-label no-padding-right"
                                                   for="select_file">Select <span class="text-muted">.xls, .xslx</span>
                                                File only</label>
                                            <div class="col-sm-8">
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <input type="file" id="select_file" name="select_file">
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <button type="submit" class="btn btn-purple">
                                                        <i class="fa fa-cloud-upload bigger-120" aria-hidden="true"></i>
                                                        Import Excel File
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <a href={{ asset('tribal_sample_upload_file.xlsx') }}>
                                                        <span class="btn btn-primary download">
                                                            <i class="fa fa-cloud-download bigger-120"
                                                               aria-hidden="true"></i>
                                                            Download Sample Data
                                                        </span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                @if(isset($msg))
                    @if(!empty($msg))
                        <ul>
                            @foreach($msg as $key => $vals)
                                <li> {{ ($key + 1) }} : {{ $vals }}</li>
                            @endforeach
                        </ul>
                    @endif
                @endif
            </div>
        </div>
    </div>
@stop

@section('custom_style')

@stop

@section('custom_script')
    <script type="text/javascript">
        // $(window).on('hashchange', function () {
        //     if (window.location.hash) {
        //         var page = window.location.hash.replace('#', '');
        //         if (page == Number.NaN || page <= 0) {
        //             return false;
        //         } else {
        //             getPosts(page);
        //         }
        //     }
        // });
        //
        $(document).ready(function () {
            $("#ssnp_for").prop("disabled", true);
        });

    </script>

@stop