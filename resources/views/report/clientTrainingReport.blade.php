@extends('layouts.user')

@section('title')
    Training Report
@stop

@section('breadcrumb')
    {{--    <ul class="breadcrumb">--}}
    {{--        <li>--}}
    {{--            <i class="ace-icon fa fa-bars home-icon"></i>--}}
    {{--            <a href="{{ route('training.adminList') }}">Training List</a>--}}
    {{--        </li>--}}

    {{--        --}}{{--<li class="active">User Profile</li>--}}
    {{--    </ul>--}}
@stop

@section('page_header')
    <h1>Training Report</h1>
@stop

@section('content')
    <div class="col-xs-12">
        {{--<h3 class="header smaller lighter blue">jQuery dataTables</h3>--}}

        <div>
            <div id="dynamic-table_wrapper" class="dataTables_wrapper form-inline no-footer">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="pull-right tableTools-container"></div>
                        <div style="padding-bottom: 10px;" class="clearfix">
                            <form class="form-horizontal" role="form"
                                  action="{{ route('report.clientTrainingReportExport') }}"
                                  method="POST">
                                {{ csrf_field() }}
                                <div class="col-xs-12 col-sm-12">
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right" for="dept_id">Institute</label>

                                            <div class="col-sm-9">
                                                <select id="dept_id" placeholder="" name="dept_id"
                                                        class="col-xs-10 col-sm-12">
                                                    <option value="">--- বাছাই করুন ---</option>
                                                    <?php foreach ($departmentList as $id => $name) { ?>
                                                <?php echo "<option value='{$id}'>{$name}</option>"; ?>
                                            <?php }?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label no-padding-right" for="start_date">Start
                                                Date</label>

                                            <div class="col-sm-8">
                                                <div class="input-group col-xs-10 col-sm-12 ">
                                                    <input class="date-picker form-control" id="start_date" type="text"
                                                           name="start_date"
                                                           data-date-format="dd-mm-yyyy"/>
                                                    <span class="input-group-addon">
                            <i class="fa fa-calendar bigger-110"></i>
                        </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label no-padding-right" for="end_date">End
                                                Date</label>

                                            <div class="col-sm-8">
                                                <div class="input-group col-xs-10 col-sm-12 ">
                                                    <input class="date-picker form-control" id="end_date" type="text"
                                                           name="end_date"
                                                           data-date-format="dd-mm-yyyy"/>
                                                    <span class="input-group-addon">
                            <i class="fa fa-calendar bigger-110"></i>
                        </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row col-xs-12 col-sm-12">
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right"
                                                   for="status">Status</label>

                                            <div class="col-sm-9">
                                                <select id="status" placeholder="" name="status"
                                                        class="col-xs-10 col-sm-12">
                                                    <option value="">--- বাছাই করুন ---</option>
                                                    <option value="1">অনিষ্পাদিত</option>
                                                    <option value="2">সম্পন্ন</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right" for="trade_name">Trade</label>
                                            <div class="col-sm-9">
                                                <select id="trade_name" placeholder="" name="trade_name">
                                                    <option value="">---Trade Lists ---</option>
                                                    <?php foreach ($tradeList as $tid => $tname) { ?>
                                                <?php echo "<option value='{$tid}'>{$tname}</option>"; ?>
                                            <?php }?>

                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <a href="{{ route('report.clientTrainingWiseRegistration') }}"
                                               name="searchData"
                                               id="searchData">
                                                <button class="btn btn-primary">
                                                    <i class="fa fa-search bigger-120" aria-hidden="true"></i>
                                                    Search
                                                </button>
                                            </a>
                                            {{--                                        </div>--}}
                                            {{--                                    </div>--}}

                                            {{--                                    <div class="col-sm-2">--}}
                                            {{--                                        <div class="form-group">--}}
                                            <button type="submit" class="btn btn-purple">
                                                <i class="fa fa-download bigger-120" aria-hidden="true"></i>
                                                Export Training List
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="posts">
                    @include('report.ajax_training_report')
                </div>
            </div>
        </div>
    </div>
@stop

@section('custom_style')

@stop

@section('custom_script')
    <script type="text/javascript">
        $(window).on('hashchange', function () {
            if (window.location.hash) {
                var page = window.location.hash.replace('#', '');
                if (page == Number.NaN || page <= 0) {
                    return false;
                } else {
                    getPosts(page);
                }
            }
        });

        $(document).ready(function () {
            $(document).on('click', '.pagination a', function (e) {
                getPosts($(this).attr('href').split('page=')[1], $(this).attr('href'));
                e.preventDefault();
            });
        });

        function getPosts(page, url) {
            console.log(url);
//            url: '?page=' + page
            var displayValue = $("#displayValue").val();
            var searchData = $("#searchData").val();
            $.ajax({
                url: '?page=' + page + '&displayValue=' + displayValue + '&searchData=' + searchData,
                success: function (data) {
                    $('.posts').html(data);
                },
                error: function (err) {
                    alert('Posts could not be loaded.');
                }
            });
        };


        $("#displayValue").on('change', function () {
            var displayValue = $(this).val();
            var url = window.location.href;
            dynamicDataViewCount(displayValue, '', url);
        });

        $("#searchData").on("click", function (e) {
            e.preventDefault();
            var dept_id = $('#dept_id').val();
            var start_date = $('#start_date').val();
            var end_date = $('#end_date').val();
            var status = $('#status').val();
            var trade_name = $('#trade_name').val();
            var url = window.location.href;

            if (end_date.length > 0 && start_date.length < 1) {
                alert("Please Select Start Date.");
                return false;
            } else {
                $.ajax({
                    url: url,
                    dataType: 'html',
                    type: 'GET',
                    data: {dept_id: dept_id, start_date: start_date, end_date: end_date, status: status, trade_name:trade_name},
                    success: function (data) {
                        $('.posts').html(data);
                    },
                    error: function (err) {
                        console.log(err);
                        alert('Posts could not be loaded.');
                    }
                })
            }
        });

        function dynamicDataViewCount(displayValue, searchData, url) {
            if (displayValue == '') {
                displayValue = $("#displayValue").val();
            }
            if (searchData == '') {
                searchData = $("#searchData").val();
            }
            $.ajax({
                url: url,
                dataType: 'html',
                type: 'GET',
                data: {displayValue: displayValue, searchData: searchData},
                success: function (data) {
                    $('.posts').html(data);
                },
                error: function (err) {
                    console.log(err);
                    alert('Posts could not be loaded.');
                }
            });
        }

        //datepicker plugin
        $('.date-picker').datepicker({
            autoclose: true,
            todayHighlight: true
        })
    </script>

@stop