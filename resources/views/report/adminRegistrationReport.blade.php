@extends('layouts.admin')

@section('title')
    Registration Report
@stop

@section('breadcrumb')
    {{--    <ul class="breadcrumb">--}}
    {{--        <li>--}}
    {{--            <i class="ace-icon fa fa-bars home-icon"></i>--}}
    {{--            <a href="{{ route('training.adminList') }}">Training List</a>--}}
    {{--        </li>--}}

    {{--        --}}{{--<li class="active">User Profile</li>--}}
    {{--    </ul>--}}
@stop

@section('page_header')
    <h1>Registration Report</h1>
@stop

@section('content')
    <div class="col-xs-12">
        <div>
            <div id="dynamic-table_wrapper" class="dataTables_wrapper form-inline no-footer">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="pull-right tableTools-container"></div>
                        <div style="padding-bottom: 10px;" class="clearfix">
                            <form class="form-horizontal" role="form"
                                  action="{{ route('report.adminExportRegistrationReport') }}"
                                  method="POST">
                                {{ csrf_field() }}
                                <div class="row">

                                    <div class="col-xs-12 col-sm-12">
                                        <div class="col-xs-3">
                                            <label class="col-sm-4 control-label no-padding-right"
                                                   for="dist">District Name</label>

                                            <div class="col-sm-8">
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <select id="dist" placeholder="" name="dist">
                                                            {{--                                                        <option value="">---Plese Select ---</option>--}}
                                                            <?php foreach ($districtList as $id => $name) { ?>
                                                <?php echo "<option value='{$id}'>{$name}</option>"; ?>
                                            <?php }?>


                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-3">
                                            <label class="col-sm-3 control-label no-padding-right"
                                                   for="upo">Upazilla Name</label>

                                            <div class="col-sm-9">
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <select id="upo" placeholder="" name="upo">
                                                            <option value="">---Please Select ---</option>
                                                            <?php foreach ($upazilaList as $tid => $tname) { ?>
                                                <?php echo "<option value='{$tid}'>{$tname}</option>"; ?>
                                            <?php }?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-3">
                                            <label class="col-sm-3 control-label no-padding-right"
                                                   for="uni_per">Union/Pouroshova</label>

                                            <div class="col-sm-9">
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <select name='uni_per' id="uni_per">
                                                            <option value="">---Please Select---</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-3">
                                            <label class="col-sm-3 control-label no-padding-right"
                                                   for="word_no">Word No.</label>

                                            <div class="col-sm-9">
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <select name='word_no' id="word_no">
                                                            <option value="">---Please Select---</option>
                                                            <?php
                                                            for($count = 1; $count < 16; $count++){ ?>
                                                            <option value="<?php echo $count?>">Word No.
                                                                - <?php echo $count; ?></option>
                                                            <?php }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">

                                    <div class="col-xs-12 col-sm-12">
                                        <div class="col-xs-3">
                                            <label class="col-sm-3 control-label no-padding-right"
                                                   for="name">Name</label>
                                            <div class="col-sm-9">
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <input type="text" id="name" name="name">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-3">
                                            <label class="col-sm-3 control-label no-padding-right"
                                                   for="phome">Phone</label>
                                            <div class="col-sm-9">
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <input type="text" id="phone" name="phone">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-3">
                                            <label class="col-sm-3 control-label no-padding-right"
                                                   for="nid">NID/ Birth ID</label>
                                            <div class="col-sm-9">
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <input type="text" id="nid" name="nid">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-3">
                                            <label class="col-sm-3 control-label no-padding-right"
                                                   for="card_type">Card Type</label>
                                            <div class="col-sm-9">
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <select name='card_type' id="card_type">
                                                            <option value="">---Please Select---</option>
                                                            <?php
                                                            foreach (config('constants.card_type.arr') as $key => $vals) {?>
                                                            <option value="<?php echo $key?>"><?php echo $vals; ?></option>
                                                            <?php }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12">
                                        <div class="col-xs-3">
                                            <label class="col-sm-3 control-label no-padding-right"
                                                   for="card_no">Card No.</label>
                                            <div class="col-sm-9">
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <input type="text" id="card_no" name="card_no">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-3">
                                            <label class="col-sm-3 control-label no-padding-right"
                                                   for="ssnp">SSNP</label>

                                            <div class="col-sm-9">
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <select id="ssnp" placeholder="" name="ssnp">
                                                            <option value="">---Please Select ---</option>
                                                            <option value=-1>তথ্য নেই</option>
                                                            <?php foreach ($ssnpList as $id => $name) { ?>
                                                <?php echo "<option value='{$id}'>{$name}</option>"; ?>
                                            <?php }?>


                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-3">
                                            <label class="col-sm-3 control-label no-padding-right"
                                                   for="packages">Packages</label>

                                            <div class="col-sm-9">
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <select id="packages" placeholder="" name="packages">
                                                            <option value="">---Please Select ---</option>
                                                            <option value=-1>তথ্য নেই</option>
                                                            <?php foreach ($packageList as $id => $name) { ?>
                                                <?php echo "<option value='{$id}'>{$name}</option>"; ?>
                                            <?php }?>


                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-3">
                                            <label class="col-sm-3 control-label no-padding-right"
                                                   for="poss_help">Help Plan</label>

                                            <div class="col-sm-9">
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <?php
                                                        $helpList = config('constants.possible_help');
                                                        ?>
                                                        <select id="poss_help" placeholder="" name="poss_help">
                                                            <option value="">---Please Select ---</option>
                                                            <?php foreach ($helpList as $id => $name) { ?>
                                                            <?php echo "<option value='{$id}'>{$name}</option>"; ?>
                                                            <?php }?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12">
                                        <div class="col-xs-3">
                                            <label class="col-sm-3 control-label no-padding-right"
                                                   for="baby_food">Baby Food</label>

                                            <div class="col-sm-9">
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <?php
                                                        $helpList = config('constants.possible_help');
                                                        ?>
                                                        <select id="baby_food" placeholder="" name="baby_food">
                                                            <option value="">---Please Select ---</option>
                                                            <option value=1>Only Baby Food</option>
                                                            <option value=2>Without Baby Food</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-3">
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <button type="submit" class="btn btn-purple">
                                                        <i class="fa fa-download bigger-120" aria-hidden="true"></i>
                                                        Export Registration Report
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-4">
                                                    <a href="{{ route('report.adminRegistrationReport') }}"
                                                       name="searchData"
                                                       id="searchData">
                                                        <button class="btn btn-primary search">
                                                            <i class="fa fa-search bigger-120" aria-hidden="true"></i>
                                                            Search
                                                        </button>
                                                        <button class="btn btn-primary loading" type="button" disabled>
                                                            <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                                            Loading...
                                                        </button>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="posts">
                    @include('report.ajax_registration_report')
                </div>
            </div>
        </div>
    </div>
@stop

@section('custom_style')

@stop

@section('custom_script')
    <script type="text/javascript">
        $(".loading").hide();
        // $(window).on('hashchange', function () {
        //     if (window.location.hash) {
        //         var page = window.location.hash.replace('#', '');
        //         if (page == Number.NaN || page <= 0) {
        //             return false;
        //         } else {
        //             getPosts(page);
        //         }
        //     }
        // });
        //
        // $(document).ready(function () {
        //     $(document).on('click', '.pagination a', function (e) {
        //         getPosts($(this).attr('href').split('page=')[1], $(this).attr('href'));
        //         e.preventDefault();
        //     });
        // });

        $('#upo').on('change', function () {
            var upazilaID = $(this).val();
            var token = $("input[name='_token']").val();
            var url = "{{ route('union.adminUnionSelectAjaxList') }}";
            if (upazilaID) {
                $('select[name="uni_per"]').empty();
                unionList(upazilaID, token, url, 'per');
            } else {
                $('select[name="uni_per"]').empty();
            }
        });

        function unionList(upazilaID, token, url, type) {
            $.ajax({
                url: url,
                method: 'POST',
                data: {upazilaID: upazilaID, _token: token},
                dataType: "json",
                success: function (data) {
                    console.log(data);
                    $('select[name="uni_per"]').append('<option value="">' + "--- বাছাই করুন ---" + '</option>');
                    $('select[name="uni_per"]').append('<option value="' + 0 + '">' + "পৌরসভা" + '</option>');
                    $.each(data, function (key, value) {
                        $('select[name="uni_per"]').append('<option value="' + key + '">' + value + '</option>');
                    });
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert("Status: " + textStatus);
                    alert("Error: " + errorThrown);
                    console.log(XMLHttpRequest);
                    console.log(textStatus);
                    console.log(errorThrown);
                }
            });
        };

//         function getPosts(page, url) {
//             console.log(url);
// //            url: '?page=' + page
//             var displayValue = $("#displayValue").val();
//             var searchData = $("#searchData").val();
//             $.ajax({
//                 url: '?page=' + page + '&displayValue=' + displayValue + '&searchData=' + searchData,
//                 success: function (data) {
//                     $('.posts').html(data);
//                 },
//                 error: function (err) {
//                     alert('Posts could not be loaded.');
//                 }
//             });
//         };


        $("#displayValue").on('change', function () {
            var displayValue = $(this).val();
            var url = window.location.href;
            dynamicDataViewCount(displayValue, '', url);
        });

        $("#searchData").on("click", function (e) {
            e.preventDefault();
            $(".loading").show();
            $(".search").hide();
            var dist = $('#dist').val();
            var upo = $('#upo').val();
            var uni_per = $('#uni_per').val();
            var word_no = $('#word_no').val();
            var name = $('#name').val();
            var phone = $('#phone').val();
            var nid = $('#nid').val();
            var card_type = $('#card_type').val();
            var card_no = $('#card_no').val();
            var ssnp = $('#ssnp').val();
            var packages = $('#packages').val();
            var poss_help = $('#poss_help').val();
            var baby_food = $('#baby_food').val();
            var url = window.location.href;
            $.ajax({
                url: url,
                dataType: 'html',
                type: 'GET',
                data: {dist: dist, upo: upo, uni_per: uni_per,word_no: word_no, name: name, phone: phone,nid: nid, card_type: card_type, card_no: card_no, ssnp: ssnp, packages: packages, poss_help: poss_help, baby_food: baby_food},
                success: function (data) {
                    $(".loading").hide();
                    $(".search").show();
                    $('.posts').html(data);
                },
                error: function (err) {
                    console.log(err);
                    alert('Posts could not be loaded.');
                }
            })
        });

        $("#exportData").on("click", function (e) {
            e.preventDefault();
            var dist = $('#dist').val();
            var upo = $('#upo').val();
            var uni_per = $('#uni_per').val();
            var word_no = $('#word_no').val();
            var name = $('#name').val();
            var phone = $('#phone').val();
            var nid = $('#nid').val();
            var card_type = $('#card_type').val();
            var card_no = $('#card_no').val();
            var ssnp = $('#ssnp').val();
            var url = $(this).attr('href');

            ajaxCall(dist, upo, uni_per, word_no, name, phone, nid, card_type, card_no, url);
        });

        function ajaxCall(training_id, trade_name, reg_status, url) {

        }

        function dynamicDataViewCount(displayValue, searchData, url) {
            if (displayValue == '') {
                displayValue = $("#displayValue").val();
            }
            if (searchData == '') {
                searchData = $("#searchData").val();
            }
            $.ajax({
                url: url,
                dataType: 'html',
                type: 'GET',
                data: {displayValue: displayValue, searchData: searchData},
                success: function (data) {
                    $('.posts').html(data);
                },
                error: function (err) {
                    console.log(err);
                    alert('Posts could not be loaded.');
                }
            });
        }
    </script>

@stop