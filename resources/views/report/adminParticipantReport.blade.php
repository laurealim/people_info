@extends('layouts.admin')

@section('title')
    <?php echo $titleName; ?>
@stop

@section('breadcrumb')
    {{--    <ul class="breadcrumb">--}}
    {{--        <li>--}}
    {{--            <i class="ace-icon fa fa-bars home-icon"></i>--}}
    {{--            <a href="{{ route('training.adminList') }}">Training List</a>--}}
    {{--        </li>--}}

    {{--        --}}{{--<li class="active">User Profile</li>--}}
    {{--    </ul>--}}
@stop

@section('page_header')
    <h1><?php echo $titleName; ?></h1>
@stop

@section('content')
    <div class="col-xs-12">
        <div>
            <div id="dynamic-table_wrapper" class="dataTables_wrapper form-inline no-footer">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="pull-right tableTools-container"></div>
                        <div style="padding-bottom: 10px;" class="clearfix">
                            <form class="form-horizontal" role="form"
                                  action="{{ route('report.adminParticipantReportExport') }}"
                                  method="POST">
                                {{ csrf_field() }}
                                <div class="col-xs-12 col-sm-12">
                                    <div class="col-xs-3">
                                        <label class="col-sm-4 control-label no-padding-right"
                                               for="training_id">Training Name</label>
                                        <div class="form-group">
                                            <div class="col-sm-3">
                                                <select id="training_id" placeholder="" name="training_id">
                                                    <option value="">---Training Name ---</option>
                                                    <?php foreach ($trainingList as $id => $name) { ?>
                                                <?php echo "<option value='{$id}'>{$name}</option>"; ?>
                                            <?php }?>


                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <label class="col-sm-4 control-label no-padding-right"
                                               for="trade_name">Trade Name</label>
                                        <div class="form-group">
                                            <div class="col-sm-3">
                                                <select id="trade_name" placeholder="" name="trade_name">
                                                    <option value="">---Trade Name ---</option>
                                                    <?php foreach ($tradeList as $tid => $tname) { ?>
                                                <?php echo "<option value='{$tid}'>{$tname}</option>"; ?>
                                            <?php }?>


                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <label class="col-sm-4 control-label no-padding-right"
                                               for="reg_status">Status</label>
                                        <div class="form-group">
                                            <div class="col-sm-3">
                                                <select id="reg_status" placeholder="" name="reg_status">
                                                    <option value="">---Registration Status ---</option>
                                                    <?php foreach ($registrationStatusArr as $id => $name) { ?>
                                                <?php echo "<option value='{$id}'>{$name}</option>"; ?>
                                            <?php }?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <div class="form-group">
                                            <div class="col-sm-3">
                                                <button type="submit" class="btn btn-purple">
                                                    <i class="fa fa-download bigger-120" aria-hidden="true"></i>
                                                    Export Training List
                                                </button>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-4">
                                                <a href="{{ route('report.adminParticipantReport') }}" name="searchData"
                                                   id="searchData">
                                                    <button class="btn btn-primary">
                                                        <i class="fa fa-search bigger-120" aria-hidden="true"></i>
                                                        Search
                                                    </button>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="posts">
                    @include('report.ajax_participant_report')
                </div>
            </div>
        </div>
    </div>
@stop

@section('custom_style')

@stop

@section('custom_script')
    <script type="text/javascript">
        $(window).on('hashchange', function () {
            if (window.location.hash) {
                var page = window.location.hash.replace('#', '');
                if (page == Number.NaN || page <= 0) {
                    return false;
                } else {
                    getPosts(page);
                }
            }
        });

        $(document).ready(function () {
            $(document).on('click', '.pagination a', function (e) {
                getPosts($(this).attr('href').split('page=')[1], $(this).attr('href'));
                e.preventDefault();
            });
        });

        function getPosts(page, url) {
            console.log(url);
//            url: '?page=' + page
            var displayValue = $("#displayValue").val();
            var searchData = $("#searchData").val();
            $.ajax({
                url: '?page=' + page + '&displayValue=' + displayValue + '&searchData=' + searchData,
                success: function (data) {
                    $('.posts').html(data);
                },
                error: function (err) {
                    alert('Posts could not be loaded.');
                }
            });
        };


        $("#displayValue").on('change', function () {
            var displayValue = $(this).val();
            var url = window.location.href;
            dynamicDataViewCount(displayValue, '', url);
        });

        $("#searchData").on("click", function (e) {
            e.preventDefault();
            var training_id = $('#training_id').val();
            var trade_name = $('#trade_name').val();
            var reg_status = $('#reg_status').val();
            var url = window.location.href;
            $.ajax({
                url: url,
                dataType: 'html',
                type: 'GET',
                data: {training_id: training_id, trade_name: trade_name, reg_status: reg_status},
                success: function (data) {
                    $('.posts').html(data);
                },
                error: function (err) {
                    console.log(err);
                    alert('Posts could not be loaded.');
                }
            })
        });

        $("#exportData").on("click", function (e) {
            e.preventDefault();
            var training_id = $('#training_id').val();
            var trade_name = $('#trade_name').val();
            var reg_status = $('#reg_status').val();
            var url = $(this).attr('href');

            ajaxCall(training_id, trade_name, reg_status, url);
        });

        function ajaxCall(training_id, trade_name, reg_status, url) {

        }

        function dynamicDataViewCount(displayValue, searchData, url) {
            if (displayValue == '') {
                displayValue = $("#displayValue").val();
            }
            if (searchData == '') {
                searchData = $("#searchData").val();
            }
            $.ajax({
                url: url,
                dataType: 'html',
                type: 'GET',
                data: {displayValue: displayValue, searchData: searchData},
                success: function (data) {
                    $('.posts').html(data);
                },
                error: function (err) {
                    console.log(err);
                    alert('Posts could not be loaded.');
                }
            });
        }
    </script>

@stop