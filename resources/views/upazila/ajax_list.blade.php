{{--<div id="dynamic-table_wrapper" class="dataTables_wrapper form-inline no-footer">--}}
    <table id="dynamic-table" class="table table-striped table-bordered table-hover" style="margin-bottom: 0px;">
        <thead>
        <tr>
            <th class="center" width="6%">
                <label class="pos-rel">
                    {{--<input type="checkbox" class="ace"/>--}}
                    {{--<span class="lbl"></span>--}}
                </label>
            </th>
            <th width="20%">Upazila Name</th>
            <th width="20%">Upazila Name (Bangla)</th>
            <th width="15%">District Name</th>
            <th width="15%">Division Name</th>
            <th width="14%">URL</th>
            <th class="center" width="10%">Action</th>
        </tr>
        </thead>

        <tbody>

        <?php $pageNo = (($upazilaLists->currentPage() - 1) * $upazilaLists->perPage()) + 1 ?>

        @foreach ($upazilaLists as $upazilaList)
            <tr>
                <td class="center">
                    <label class="pos-rel">
                        {{--<input type="checkbox" class="ace"/>--}}
                        <span class="lbl">{{ $pageNo++ }}</span>
                    </label>
                </td>
                <td>
                    {{ $upazilaList->name}}
                </td>
                <td>
                    {{ $upazilaList->bn_name}}
                </td>
                <td>
                    {{ $upazilaList->district_name}}
                </td>
                <td>
                    {{ $upazilaList->division_name }}
                </td>
                <td>
                    {{ $upazilaList->url }}
                </td>
                <td class="center">
                    <div class="hidden-sm hidden-xs action-buttons">
                        {{--<a class="blue" href="{{ route('') }}">--}}
                        {{--<i class="ace-icon fa fa-search-plus bigger-200"></i>--}}
                        {{--</a>--}}

                        {{--&nbsp;&nbsp;| &nbsp;&nbsp;--}}

                        <a class="green" href="{{ route('upazila.adminEdit',$upazilaList->id) }}">
                            <i class="ace-icon fa fa-pencil bigger-200"></i>
                        </a>

                        &nbsp;&nbsp;| &nbsp;&nbsp;

                        <a class="red deletebtn" href="{{ route('upazila.adminDelete',$upazilaList->id) }}"
                           id={{ $upazilaList->id }}>
                            <i class="ace-icon fa fa-trash-o bigger-200"></i>
                        </a>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <div class="row">
        <div class="col-xs-6">
            <div class="dataTables_info" id="dynamic-table_info" role="status" aria-live="polite">
                Showing {{ (($upazilaLists->currentPage() - 1) * $upazilaLists->perPage()) + 1 }}
                to {{ (($upazilaLists->currentPage() - 1) * $upazilaLists->perPage()) + $upazilaLists->perPage() }} of
                {{ $upazilaLists->total() }} entries
            </div>
        </div>
        <div class="col-xs-6">
            <div class="dataTables_paginate paging_simple_numbers" id="dynamic-table_paginate">
                {{ $upazilaLists->links("vendor.pagination.custom") }}
            </div>
        </div>
    </div>
{{--</div>--}}
