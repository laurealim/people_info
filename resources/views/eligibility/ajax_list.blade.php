<table id="dynamic-table" class="table table-striped table-bordered table-hover" style="margin-bottom: 0px;">
    <thead>
    <tr>
        <th class="center" width="5%">
            <label class="pos-rel">
                <input type="checkbox" class="ace"/>
                <span class="lbl"></span>
            </label>
        </th>
        <th width="30%">Eligibility Name</th>
        <th width="25%">Created By</th>
        <th width="25%">Status</th>
        <th class="center" width="15%">Action</th>
    </tr>
    </thead>

    <tbody>

    <?php $pageNo = (($eligibilities->currentPage() - 1) * $eligibilities->perPage()) + 1 ?>
    @foreach ($eligibilities as $eligibility)
        <tr>
            <td class="center">
                <label class="pos-rel">
                    {{--<input type="checkbox" class="ace"/>--}}
                    <span class="lbl">{{ $pageNo++ }}</span>
                </label>
            </td>
            <td>
                {{ $eligibility->name }}
            </td>
            <td> {{ $eligibility->first_name}} </td>
            <td>
                <?php if($eligibility->status == 1){ ?>
                <span class='label label-success'>Active</span>
                <?php } else{ ?>
                <span class='label label-danger'>Inactive</span>
                <?php } ?>
            </td>
            <td class="center">
                <div class="hidden-sm hidden-xs action-buttons">
                    {{--<a class="blue" href="{{ route('') }}">--}}
                    {{--<i class="ace-icon fa fa-search-plus bigger-200"></i>--}}
                    {{--</a>--}}

                    {{--&nbsp;&nbsp;| &nbsp;&nbsp;--}}

                    <a class="green" href="{{ route('eligibility.adminEdit',$eligibility->id) }}">
                        <i class="ace-icon fa fa-pencil bigger-200"></i>
                    </a>

                    &nbsp;&nbsp;| &nbsp;&nbsp;

                    <a class="red deletebtn" href="{{ route('eligibility.adminDelete',$eligibility->id) }}" id={{ $eligibility->id }}>
                        <i class="ace-icon fa fa-trash-o bigger-200"></i>
                    </a>

                    {{--&nbsp;&nbsp;| &nbsp;&nbsp;--}}

                    {{--<a class="red deletebtn" href="{{ route('country.adminDelete',$country->id) }}" id={{ $country->id }}>--}}
                    {{--<i class="ace-icon fa fa-trash-o bigger-200"></i>--}}
                    {{--</a>--}}
                </div>

            </td>
        </tr>
    @endforeach
    </tbody>
</table>


<div class="row">
    <div class="col-xs-6">
        <div class="dataTables_info" id="dynamic-table_info" role="status" aria-live="polite">
            Showing {{ (($eligibilities->currentPage() - 1) * $eligibilities->perPage()) + 1 }}
            to {{ (($eligibilities->currentPage() - 1) * $eligibilities->perPage()) + $eligibilities->perPage() }} of
            {{ $eligibilities->total() }} entries
        </div>
    </div>
    <div class="col-xs-6">
        <div class="dataTables_paginate paging_simple_numbers" id="dynamic-table_paginate">
            {{ $eligibilities->links("vendor.pagination.custom") }}
        </div>
    </div>
</div>
