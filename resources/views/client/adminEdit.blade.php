@extends('layouts.admin')

@section('title')
    Edit User
@stop

@section('breadcrumb')
    <ul class="breadcrumb">
        <li>
            <i class="ace-icon fa fa-bars home-icon"></i>
            <a href="{{ route('client.adminList') }}">User List</a>
        </li>

        <li>
            <a href="{{ route('client.adminEdit',$id) }}">User Edit</a>
        </li>
        {{--<li class="active">User Profile</li>--}}
    </ul>
@stop

@section('page_header')
    <h1>Edit User</h1>
@stop

@section('content')
    <div class="col-xs-12">
        <h3 class="header smaller lighter blue">
            Basic Information
        </h3>

        <form class="form-horizontal" role="form" action="{{ route('client.adminUpdate', $id) }}" method="POST"
              enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> First Name *</label>

                <div class="col-sm-9">
                    <input type="text" id="form-field-1" placeholder="First Name" name="first_name"
                           value="{{ $clientData['first_name'] }}"
                           class="col-xs-10 col-sm-5"/>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('first_name'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('first_name') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Last Name </label>

                <div class="col-sm-9">
                    <input type="text" id="form-field-1" placeholder="Last Name" name="last_name"
                           value="{{ $clientData['last_name'] }}"
                           class="col-xs-10 col-sm-5"/>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('last_name'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('last_name') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="space-4"></div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="builders"> Email *</label>

                <div class="col-sm-9">
                    <input type="text" id="builders" placeholder="i.e. userdomail.com" name="email"
                           value="{{ $clientData['email'] }}"
                           class="col-xs-10 col-sm-5"/>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('email'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="space-4"></div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="phone"> Mobile No. *</label>

                <div class="col-sm-9">
                    <input type="text" id="phone" placeholder="i.e. +8801XXXXXXXXX" name="phone"
                           value="{{ $clientData['phone'] }}"
                           class="col-xs-10 col-sm-5"/>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('phone'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('phone') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="space-4"></div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="gender"> Gender </label>

                <div class="col-sm-9">
                    <select name='gender' class="col-xs-10 col-sm-5" id="gender">
                        <option value="male" {{ $clientData['gender'] == 'male' ? 'selected="selected"' : '' }} >Male
                        </option>
                        <option value="female" {{ $clientData['gender'] == 'female' ? 'selected="selected"' : '' }} >Female
                        </option>
                    </select>
                </div>
            </div>

            <div class="space-4"></div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="user_type"> User Type *</label>

                <div class="col-sm-9">
                    <select name='user_type' class="col-xs-10 col-sm-5" id="user_type">

                        <option value="1" {{ $clientData['user_type'] == 1 ? 'selected="selected"' : '' }} >Admin
                        </option>
                        <option value="2" {{ $clientData['user_type'] == 2 ? 'selected="selected"' : '' }} >Office Head
                        </option>
                        <option value="3" {{ $clientData['user_type'] == 3 ? 'selected="selected"' : '' }} >User
                        </option>
                    </select>
                </div>
            </div>

            <div class="space-4"></div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="dept_id"> Department *</label>

                <div class="col-sm-9">
                    <select name='dept_id' class="col-xs-10 col-sm-5" id="dept_id">
                        <option value="">---- Select One ----</option>
                        <?php foreach($departmentList as $key => $value){ ?>
                        <option value="<?php echo $key; ?>" {{ $clientData['dept_id'] == $key ? 'selected="selected"' : '' }}><?php echo $value?></option>
                        <?php }?>
                        {{--<option value="0">Deleted</option>--}}
                    </select>
                </div>
            </div>

            <div class="space-4"></div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="dob">Current Office Joining Date</label>

                <div class="col-sm-9">
                    <div class="input-group col-xs-10 col-sm-5 ">
                        <input class="date-picker form-control " id="dob" type="text" name="dob"
                               value="{{ $clientData['dob'] }}"
                               data-date-format="dd-mm-yyyy"/>
                        <span class="input-group-addon">
                            <i class="fa fa-calendar bigger-110"></i>
                        </span>
                    </div>
                </div>
            </div>

            <div class="space-4"></div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Address *</label>

                <div class="col-sm-9">
                    <textarea id="form-field-1" placeholder="Address" name="address"
                              class="col-xs-10 col-sm-5"> {{ $clientData['address'] }} </textarea>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('address'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('address') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="space-4"></div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="form-field-1-1"> Can Print </label>

                <div class="col-sm-9">
                    <select name='can_print' class="col-xs-10 col-sm-5" id="form-field-select-1">
                        <option value="1" {{ $clientData['can_print'] == 1 ? 'selected="selected"' : '' }} >Yes
                        </option>
                        <option value="0" {{ $clientData['status'] == 0 ? 'selected="selected"' : '' }} >No
                        </option>
                    </select>
                </div>
            </div>

            <div class="space-4"></div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="form-field-1-1"> Status </label>

                <div class="col-sm-9">
                    <select name='status' class="col-xs-10 col-sm-5" id="form-field-select-1">
                        <option value="1" {{ $clientData['status'] == 1 ? 'selected="selected"' : '' }} >Active
                        </option>
                        <option value="2" {{ $clientData['status'] == 2 ? 'selected="selected"' : '' }} >Inactive
                        </option>
                        <option value="0" {{ $clientData['status'] == 0 ? 'selected="selected"' : '' }} >Deleted
                        </option>
                    </select>
                </div>
            </div>

            <div class="space-4"></div>

            <div class="form-group">
                <label class="control-label col-xs-12 col-sm-3 no-padding-right "
                       for="image">Client Images</label>

                <div class="col-xs-10 col-sm-5">
                    <input type="file" id="image" class="col-xs-10 col-sm-5" name="image"/>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('image'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('image') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
                <div class="col-xs-10 col-sm-5 col-md-4" style="margin-top:10px">
                    <?php if($clientData['image'] != ""){ ?>
                    <img src="{{ asset('storage/clients/'.$clientData['id'].'/'.$clientData['image']) }}"
                         class="msg-photo" alt="Kate's Avatar" width="120px" height="80"/>
                    <?php }?>

                </div>
            </div>

            <div class="space-4"></div>

            <div class="clearfix form-actions">
                <div class="col-md-offset-3 col-md-9">
                    <button class="btn btn-info" type="submit">
                        <i class="ace-icon fa fa-check bigger-110"></i>
                        Submit
                    </button>

                    &nbsp; &nbsp; &nbsp;
                    <button class="btn" type="reset">
                        <i class="ace-icon fa fa-undo bigger-110"></i>
                        Reset
                    </button>
                </div>
            </div>
        </form>
    </div>
@stop


@section('custom_style')

@stop

@section('custom_script')
    <script type="text/javascript">

        /****************************/
        /****************************/
        /*  File Upload Section */
        /****************************/
        /****************************/

        $('#image').ace_file_input({
            style: 'well',
            btn_choose: 'Drop files here or click to choose',
            btn_change: null,
            no_icon: 'ace-icon fa fa-cloud-upload',
            droppable: true,
            thumbnail: 'small'//large | fit
            //,icon_remove:null//set null, to hide remove/reset button
            /**,before_change:function(files, dropped) {
						//Check an example below
						//or examples/file-upload.html
						return true;
					}*/
            /**,before_remove : function() {
						return true;
					}*/
            ,
            preview_error: function (filename, error_code) {
                //name of the file that failed
                //error_code values
                //1 = 'FILE_LOAD_FAILED',
                //2 = 'IMAGE_LOAD_FAILED',
                //3 = 'THUMBNAIL_FAILED'
                //alert(error_code);
            }

        }).on('change', function () {
            //console.log($(this).data('ace_input_files'));
            //console.log($(this).data('ace_input_method'));
        });

        var whitelist_ext, whitelist_mime;
        var btn_choose;
        var no_icon;

        var file_input = $('#image');

        file_input
                .ace_file_input('update_settings',
                {
                    'btn_choose': btn_choose,
                    'no_icon': no_icon,
                    'allowExt': whitelist_ext,
                    'allowMime': whitelist_mime
                });

        file_input.ace_file_input('reset_input');

        file_input
                .off('file.error.ace')
                .on('file.error.ace', function (e, info) {
                    //console.log(info.file_count);//number of selected files
                    //console.log(info.invalid_count);//number of invalid files
                    //console.log(info.error_list);//a list of errors in the following format

                    //info.error_count['ext']
                    //info.error_count['mime']
                    //info.error_count['size']

                    //info.error_list['ext']  = [list of file names with invalid extension]
                    //info.error_list['mime'] = [list of file names with invalid mimetype]
                    //info.error_list['size'] = [list of file names with invalid size]


                    /**
                     if( !info.dropped ) {
							//perhapse reset file field if files have been selected, and there are invalid files among them
							//when files are dropped, only valid files will be added to our file array
							e.preventDefault();//it will rest input
						}
                     */


                    //if files have been selected (not dropped), you can choose to reset input
                    //because browser keeps all selected files anyway and this cannot be changed
                    //we can only reset file field to become empty again
                    //on any case you still should check files with your server side script
                    //because any arbitrary file can be uploaded by user and it's not safe to rely on browser-side measures
                });


        /****************************/
        /****************************/
        /*  End */
        /****************************/
        /****************************/

        //datepicker plugin
        $('.date-picker').datepicker({
            autoclose: true,
            todayHighlight: true
        })
    </script>
@stop