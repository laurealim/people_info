@extends('layouts.admin')

@section('title')
    Add Eligibility
@stop

@section('breadcrumb')
    <ul class="breadcrumb">
        <li>
            <i class="ace-icon fa fa-bars home-icon"></i>
            <a href="{{ route('eligibility.adminList') }}">Eligibility List</a>
        </li>

        <li>
            <a href="{{ route('eligibility.adminForm') }}">Eligibility Add</a>
        </li>
        {{--<li class="active">User Profile</li>--}}
    </ul>
@stop

@section('page_header')
    <h1>Add Eligibility</h1>
@stop

@section('content')
    <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->

        <h4 class="lighter">
            <i class="ace-icon fa fa-hand-o-right icon-animated-hand-pointer blue"></i>
            <a href="#modal-wizard" data-toggle="modal" class="pink"> Wizard Inside a Modal Box </a>
        </h4>

        <div class="hr hr-18 hr-double dotted"></div>

        <div class="widget-box">
            <div class="widget-header widget-header-blue widget-header-flat">
                <h4 class="widget-title lighter">New Item Wizard</h4>

                <div class="widget-toolbar">
                    <label>
                        <small class="green">
                            <b>Validation</b>
                        </small>

                        <input id="skip-validation" type="checkbox" class="ace ace-switch ace-switch-4"/>
                        <span class="lbl middle"></span>
                    </label>
                </div>
            </div>

            <div class="widget-body">
                <div class="widget-main">
                    <div id="fuelux-wizard-container">
                        <div>
                            <ul class="steps">
                                <li data-step="1" class="active">
                                    <span class="step">1</span>
                                    <span class="title">Basic Details</span>
                                </li>

                                <li data-step="2">
                                    <span class="step">2</span>
                                    <span class="title">About Opportunity</span>
                                </li>

                                <li data-step="3">
                                    <span class="step">3</span>
                                    <span class="title">Rounds Listing</span>
                                </li>

                                <li data-step="4">
                                    <span class="step">4</span>
                                    <span class="title">Other Details</span>
                                </li>

                                <li data-step="4">
                                    <span class="step">5</span>
                                    <span class="title">Registration Method</span>
                                </li>
                            </ul>
                        </div>

                        <hr/>

                        <div class="step-content pos-rel">
                            <div class="step-pane active" data-step="1">
                                {{--<h3 class="lighter block green">Enter the following information</h3>--}}

                                <form class="form-horizontal" id="validation-form" method="get">
                                    <div class="space-2"></div>
                                    <h3 class="header smaller lighter blue">
                                        Basic Information
                                    </h3>

                                    <div class="col-xs-12 col-sm-12 col-lg-6 col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label col-xs-12 col-sm-3 no-padding-right"
                                                   for="title">Title: <span class="required">*</span></label>

                                            <div class="col-xs-12 col-sm-9">
                                                <div class="clearfix">
                                                    <input type="text" name="title" id="title"
                                                           class="col-xs-12 col-sm-12"/>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-xs-12 col-sm-3 no-padding-right"
                                                   for="organisation">Organisation: <span
                                                        class="required">*</span></label>

                                            <div class="col-xs-12 col-sm-9">
                                                <select id="organisation" name="organisation"
                                                        class="select2 col-xs-12 col-sm-12"
                                                        data-placeholder="Click to Choose...">
                                                    <option value="">&nbsp;</option>
                                                    <option value="AL">Alabama</option>
                                                    <option value="AK">Alaska</option>
                                                    <option value="AZ">Arizona</option>
                                                    <option value="AR">Arkansas</option>
                                                    <option value="CA">California</option>
                                                    <option value="CO">Colorado</option>
                                                    <option value="CT">Connecticut</option>
                                                    <option value="DE">Delaware</option>
                                                    <option value="FL">Florida</option>
                                                    <option value="GA">Georgia</option>
                                                    <option value="HI">Hawaii</option>
                                                    <option value="ID">Idaho</option>
                                                    <option value="IL">Illinois</option>
                                                    <option value="IN">Indiana</option>
                                                    <option value="IA">Iowa</option>
                                                    <option value="KS">Kansas</option>
                                                    <option value="KY">Kentucky</option>
                                                    <option value="LA">Louisiana</option>
                                                    <option value="ME">Maine</option>
                                                    <option value="MD">Maryland</option>
                                                    <option value="MA">Massachusetts</option>
                                                    <option value="MI">Michigan</option>
                                                    <option value="MN">Minnesota</option>
                                                    <option value="MS">Mississippi</option>
                                                    <option value="MO">Missouri</option>
                                                    <option value="MT">Montana</option>
                                                    <option value="NE">Nebraska</option>
                                                    <option value="NV">Nevada</option>
                                                    <option value="NH">New Hampshire</option>
                                                    <option value="NJ">New Jersey</option>
                                                    <option value="NM">New Mexico</option>
                                                    <option value="NY">New York</option>
                                                    <option value="NC">North Carolina</option>
                                                    <option value="ND">North Dakota</option>
                                                    <option value="OH">Ohio</option>
                                                    <option value="OK">Oklahoma</option>
                                                    <option value="OR">Oregon</option>
                                                    <option value="PA">Pennsylvania</option>
                                                    <option value="RI">Rhode Island</option>
                                                    <option value="SC">South Carolina</option>
                                                    <option value="SD">South Dakota</option>
                                                    <option value="TN">Tennessee</option>
                                                    <option value="TX">Texas</option>
                                                    <option value="UT">Utah</option>
                                                    <option value="VT">Vermont</option>
                                                    <option value="VA">Virginia</option>
                                                    <option value="WA">Washington</option>
                                                    <option value="WV">West Virginia</option>
                                                    <option value="WI">Wisconsin</option>
                                                    <option value="WY">Wyoming</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-xs-12 col-sm-3 no-padding-right"
                                                   for="fromDate">From Date: <span
                                                        class="required">*</span></label>

                                            <div class="col-xs-12 col-sm-9">
                                                <div class="input-group">
																		<span class="input-group-addon">
																			<i class="ace-icon fa fa-clock-o"></i>
																		</span>

                                                    <input id="fromDate" name="fromDate" type="text"
                                                           class="form-control col-xs-12 col-sm-12 custDateTime"/>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-xs-12 col-sm-3 no-padding-right"
                                                   for="toDate">To Date: <span
                                                        class="required">*</span></label>

                                            <div class="col-xs-12 col-sm-9">
                                                <div class="input-group">
																		<span class="input-group-addon">
																			<i class="ace-icon fa fa-clock-o"></i>
																		</span>

                                                    <input id="toDate" name="toDate" type="text"
                                                           class="form-control col-xs-12 col-sm-12 custDateTime"/>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-xs-12 col-sm-3 no-padding-right"
                                                   for="region">Region: <span class="required">*</span></label>

                                            <div class="col-xs-12 col-sm-9">
                                                <select id="region" name="region"
                                                        class="select2 col-xs-12 col-sm-12"
                                                        data-placeholder="Click to Choose...">
                                                    <option value="">&nbsp;</option>
                                                    <option value="AL">Alabama</option>
                                                    <option value="AK">Alaska</option>
                                                    <option value="AZ">Arizona</option>
                                                    <option value="AR">Arkansas</option>
                                                    <option value="CA">California</option>
                                                    <option value="CO">Colorado</option>
                                                    <option value="CT">Connecticut</option>
                                                    <option value="DE">Delaware</option>
                                                    <option value="FL">Florida</option>
                                                    <option value="GA">Georgia</option>
                                                    <option value="HI">Hawaii</option>
                                                    <option value="ID">Idaho</option>
                                                    <option value="IL">Illinois</option>
                                                    <option value="IN">Indiana</option>
                                                    <option value="IA">Iowa</option>
                                                    <option value="KS">Kansas</option>
                                                    <option value="KY">Kentucky</option>
                                                    <option value="LA">Louisiana</option>
                                                    <option value="ME">Maine</option>
                                                    <option value="MD">Maryland</option>
                                                    <option value="MA">Massachusetts</option>
                                                    <option value="MI">Michigan</option>
                                                    <option value="MN">Minnesota</option>
                                                    <option value="MS">Mississippi</option>
                                                    <option value="MO">Missouri</option>
                                                    <option value="MT">Montana</option>
                                                    <option value="NE">Nebraska</option>
                                                    <option value="NV">Nevada</option>
                                                    <option value="NH">New Hampshire</option>
                                                    <option value="NJ">New Jersey</option>
                                                    <option value="NM">New Mexico</option>
                                                    <option value="NY">New York</option>
                                                    <option value="NC">North Carolina</option>
                                                    <option value="ND">North Dakota</option>
                                                    <option value="OH">Ohio</option>
                                                    <option value="OK">Oklahoma</option>
                                                    <option value="OR">Oregon</option>
                                                    <option value="PA">Pennsylvania</option>
                                                    <option value="RI">Rhode Island</option>
                                                    <option value="SC">South Carolina</option>
                                                    <option value="SD">South Dakota</option>
                                                    <option value="TN">Tennessee</option>
                                                    <option value="TX">Texas</option>
                                                    <option value="UT">Utah</option>
                                                    <option value="VT">Vermont</option>
                                                    <option value="VA">Virginia</option>
                                                    <option value="WA">Washington</option>
                                                    <option value="WV">West Virginia</option>
                                                    <option value="WI">Wisconsin</option>
                                                    <option value="WY">Wyoming</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-xs-12 col-sm-3 no-padding-right"
                                                   for="location">Location: <span class="required">*</span></label>

                                            <div class="col-xs-12 col-sm-9">
                                                <select id="location" name="location"
                                                        class="select2 col-xs-12 col-sm-12"
                                                        data-placeholder="Click to Choose...">
                                                    <option value="">&nbsp;</option>
                                                    <option value="AL">Alabama</option>
                                                    <option value="AK">Alaska</option>
                                                    <option value="AZ">Arizona</option>
                                                    <option value="AR">Arkansas</option>
                                                    <option value="CA">California</option>
                                                    <option value="CO">Colorado</option>
                                                    <option value="CT">Connecticut</option>
                                                    <option value="DE">Delaware</option>
                                                    <option value="FL">Florida</option>
                                                    <option value="GA">Georgia</option>
                                                    <option value="HI">Hawaii</option>
                                                    <option value="ID">Idaho</option>
                                                    <option value="IL">Illinois</option>
                                                    <option value="IN">Indiana</option>
                                                    <option value="IA">Iowa</option>
                                                    <option value="KS">Kansas</option>
                                                    <option value="KY">Kentucky</option>
                                                    <option value="LA">Louisiana</option>
                                                    <option value="ME">Maine</option>
                                                    <option value="MD">Maryland</option>
                                                    <option value="MA">Massachusetts</option>
                                                    <option value="MI">Michigan</option>
                                                    <option value="MN">Minnesota</option>
                                                    <option value="MS">Mississippi</option>
                                                    <option value="MO">Missouri</option>
                                                    <option value="MT">Montana</option>
                                                    <option value="NE">Nebraska</option>
                                                    <option value="NV">Nevada</option>
                                                    <option value="NH">New Hampshire</option>
                                                    <option value="NJ">New Jersey</option>
                                                    <option value="NM">New Mexico</option>
                                                    <option value="NY">New York</option>
                                                    <option value="NC">North Carolina</option>
                                                    <option value="ND">North Dakota</option>
                                                    <option value="OH">Ohio</option>
                                                    <option value="OK">Oklahoma</option>
                                                    <option value="OR">Oregon</option>
                                                    <option value="PA">Pennsylvania</option>
                                                    <option value="RI">Rhode Island</option>
                                                    <option value="SC">South Carolina</option>
                                                    <option value="SD">South Dakota</option>
                                                    <option value="TN">Tennessee</option>
                                                    <option value="TX">Texas</option>
                                                    <option value="UT">Utah</option>
                                                    <option value="VT">Vermont</option>
                                                    <option value="VA">Virginia</option>
                                                    <option value="WA">Washington</option>
                                                    <option value="WV">West Virginia</option>
                                                    <option value="WI">Wisconsin</option>
                                                    <option value="WY">Wyoming</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-lg-6 col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="web">Website:</label>

                                            <div class="col-xs-12 col-sm-9">
                                                <div class="clearfix">
                                                    <input type="text" name="web" id="web"
                                                           class="col-xs-12 col-sm-12"/>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-xs-12 col-sm-3 no-padding-right"
                                                   for="festival">Festival:</label>

                                            <div class="col-xs-12 col-sm-9">
                                                <select id="festival" name="festival"
                                                        class="select2 col-xs-12 col-sm-12"
                                                        data-placeholder="Click to Choose...">
                                                    <option value="">&nbsp;</option>
                                                    <option value="AL">Alabama</option>
                                                    <option value="AK">Alaska</option>
                                                    <option value="AZ">Arizona</option>
                                                    <option value="AR">Arkansas</option>
                                                    <option value="CA">California</option>
                                                    <option value="CO">Colorado</option>
                                                    <option value="CT">Connecticut</option>
                                                    <option value="DE">Delaware</option>
                                                    <option value="FL">Florida</option>
                                                    <option value="GA">Georgia</option>
                                                    <option value="HI">Hawaii</option>
                                                    <option value="ID">Idaho</option>
                                                    <option value="IL">Illinois</option>
                                                    <option value="IN">Indiana</option>
                                                    <option value="IA">Iowa</option>
                                                    <option value="KS">Kansas</option>
                                                    <option value="KY">Kentucky</option>
                                                    <option value="LA">Louisiana</option>
                                                    <option value="ME">Maine</option>
                                                    <option value="MD">Maryland</option>
                                                    <option value="MA">Massachusetts</option>
                                                    <option value="MI">Michigan</option>
                                                    <option value="MN">Minnesota</option>
                                                    <option value="MS">Mississippi</option>
                                                    <option value="MO">Missouri</option>
                                                    <option value="MT">Montana</option>
                                                    <option value="NE">Nebraska</option>
                                                    <option value="NV">Nevada</option>
                                                    <option value="NH">New Hampshire</option>
                                                    <option value="NJ">New Jersey</option>
                                                    <option value="NM">New Mexico</option>
                                                    <option value="NY">New York</option>
                                                    <option value="NC">North Carolina</option>
                                                    <option value="ND">North Dakota</option>
                                                    <option value="OH">Ohio</option>
                                                    <option value="OK">Oklahoma</option>
                                                    <option value="OR">Oregon</option>
                                                    <option value="PA">Pennsylvania</option>
                                                    <option value="RI">Rhode Island</option>
                                                    <option value="SC">South Carolina</option>
                                                    <option value="SD">South Dakota</option>
                                                    <option value="TN">Tennessee</option>
                                                    <option value="TX">Texas</option>
                                                    <option value="UT">Utah</option>
                                                    <option value="VT">Vermont</option>
                                                    <option value="VA">Virginia</option>
                                                    <option value="WA">Washington</option>
                                                    <option value="WV">West Virginia</option>
                                                    <option value="WI">Wisconsin</option>
                                                    <option value="WY">Wyoming</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-xs-12 col-sm-3 no-padding-right "
                                                   for="logo">Select Logo: <span class="required">*</span></label>

                                            <div class="col-xs-12 col-sm-9">
                                                <input type="file" id="id-input-file-3" name="logo"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>
                                    <div class="space-2"></div>
                                    <h3 class="header smaller lighter blue">
                                        Domains <span class="required">*</span>
                                    </h3>

                                    <div class="row">
                                        <div class="col-xs-12 col-sm-1 col-lg-1 col-sm-1"></div>
                                        <div class="col-xs-12 col-sm-10 col-lg-10 col-sm-10">
                                            <div class="control-group">
                                                @for($i = 0; $i < 23; $i++)
                                                    <?php
                                                    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                                                    $charactersLength = strlen($characters);
                                                    $randomString = '';
                                                    $length = rand(5, 40);
                                                    for ($j = 0; $j < $length; $j++) {
                                                        $randomString .= $characters[rand(0, $charactersLength - 1)];
                                                    }
                                                    ?>
                                                    <div class="checkbox noFixedWidth">
                                                        <label>
                                                            <input name="form-field-checkbox" class="ace ace-checkbox-3"
                                                                   type="checkbox"/>
                                                            <span class="lbl"> <?php echo $randomString; ?></span>
                                                        </label>
                                                    </div>
                                                @endfor
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-1 col-lg-1 col-sm-1"></div>
                                    </div>

                                    <div class="space-10"></div>

                                    <div class="clearfix"></div>
                                    <div class="space-2"></div>
                                    <h3 class="header smaller lighter blue">
                                        Categories <span class="required">*</span>
                                    </h3>

                                    <div class="row">
                                        <div class="col-xs-12 col-sm-1 col-lg-1 col-sm-1"></div>
                                        <div class="col-xs-12 col-sm-10 col-lg-10 col-sm-10">
                                            <div class="control-group">
                                                @for($i = 0; $i < 50; $i++)
                                                    <?php
                                                    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                                                    $charactersLength = strlen($characters);
                                                    $randomString = '';
                                                    $length = rand(5, 40);
                                                    for ($j = 0; $j < $length; $j++) {
                                                        $randomString .= $characters[rand(0, $charactersLength - 1)];
                                                    }
                                                    ?>
                                                    <div class="checkbox noFixedWidth">
                                                        <label>
                                                            <input name="form-field-checkbox" class="ace ace-checkbox-3"
                                                                   type="checkbox"/>
                                                            <span class="lbl"> <?php echo $randomString; ?></span>
                                                        </label>
                                                    </div>
                                                @endfor
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-1 col-lg-1 col-sm-1"></div>
                                    </div>

                                    <div class="space-10"></div>

                                    <div class="clearfix"></div>
                                    <div class="space-2"></div>
                                    <h3 class="header smaller lighter blue">
                                        Eligibility <span class="required">*</span>
                                    </h3>

                                    <div class="row">
                                        <div class="col-xs-12 col-sm-1 col-lg-1 col-sm-1"></div>
                                        <div class="col-xs-12 col-sm-10 col-lg-10 col-sm-10">
                                            <div class="control-group">
                                                @for($i = 0; $i < 7; $i++)
                                                    <?php
                                                    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                                                    $charactersLength = strlen($characters);
                                                    $randomString = '';
                                                    $length = rand(5, 40);
                                                    for ($j = 0; $j < $length; $j++) {
                                                        $randomString .= $characters[rand(0, $charactersLength - 1)];
                                                    }
                                                    ?>
                                                    <div class="checkbox noFixedWidth">
                                                        <label>
                                                            <input name="form-field-checkbox" class="ace ace-checkbox-3"
                                                                   type="checkbox"/>
                                                            <span class="lbl"> <?php echo $randomString; ?></span>
                                                        </label>
                                                    </div>
                                                @endfor
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-1 col-lg-1 col-sm-1"></div>
                                    </div>

                                    <div class="space-24"></div>
                                </form>
                            </div>

                            <div class="step-pane" data-step="2">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <h4 class="header blue clearfix">
                                            Create Opportunity
                                        </h4>

                                        <div class="wysiwyg-editor" id="editor1"></div>

                                        <div class="hr hr-double dotted"></div>
                                    </div>

                                    <div class="form-group">
                                        <h4 class="header blue clearfix">
                                            Upload Gallery
                                        </h4>

                                        <div class="col-xs-12 col-sm-12">
                                            <input multiple="" type="file" id="gallery" name="gallery"/>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="step-pane" data-step="3">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <h4 class="header smaller lighter blue">Important Dates</h4>
                                    </div>
                                </div>
                                <table id="simple-table" class="table  table-bordered table-hover">
                                    <tbody>
                                    @for($k = 0; $k < 1; $k++)
                                        <?php
                                        $rowId = "row_" . $k;
                                        ?>
                                        <tr id={{$rowId}}>
                                            <td class="center" width="47%">
                                                <div class="form-group">
                                                    <label class="control-label col-xs-12 col-sm-3 no-padding-right"
                                                           for="date_title">Date Title: </label>

                                                    <div class="col-xs-12 col-sm-9">
                                                        <div class="clearfix">
                                                            <input type="text" name="date_title_{{ $k }}"
                                                                   id="date_title"
                                                                   class="col-xs-12 col-sm-12"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="center" width="47%">
                                                <div class="form-group">
                                                    <label class="control-label col-xs-12 col-sm-3 no-padding-right"
                                                           for="date-timepicker2">Date: <span
                                                                class="required">*</span></label>

                                                    <div class="col-xs-12 col-sm-9">
                                                        <div class="input-group">
																		<span class="input-group-addon">
																			<i class="ace-icon fa fa-clock-o"></i>
																		</span>

                                                            <?php
                                                            $customDateTimeId = "custDateTime_" . $k;
                                                            ?>
                                                            <input id={{$customDateTimeId}} name="toDate_{{ $k }}"
                                                                   type="text"
                                                                   class="form-control col-xs-12 col-sm-12  custDateTime"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="center" width="6%">
                                                {{--<button class="btn btn-sm btn-danger removeRow">--}}
                                                {{--<i class="ace-icon fa fa-trash"></i>--}}
                                                {{--</button>--}}

                                                <button class="btn btn-sm btn-success addRow">
                                                    <i class="ace-icon fa fa-plus"></i>
                                                </button>
                                            </td>
                                        </tr>
                                    @endfor
                                    </tbody>
                                </table>

                                <div class="row">
                                    <div class="col-xs-12">
                                        <h4 class="header smaller lighter blue">Important Contacts</h4>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="space-2"></div>
                                @for($k = 0; $k < 2; $k++)
                                    <?php
                                    $rowId = "row_" . $k;
                                    ?>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="col-xs-4">
                                                <div class="form-group">
                                                    <label class="control-label col-xs-12 col-sm-3 no-padding-right"
                                                           for="contactName">Name {{$k+1}} :</label>

                                                    <div class="col-xs-12 col-sm-9">
                                                        <div class="clearfix">
                                                            <input type="text" name="contact_name_{{ $k }}"
                                                                   id="contact_name_{{ $k }}"
                                                                   class="col-xs-12 col-sm-12"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="form-group">
                                                    <label class="control-label col-xs-12 col-sm-3 no-padding-right"
                                                           for="contactEmail">Email {{$k+1}} :</label>

                                                    <div class="col-xs-12 col-sm-9">
                                                        <div class="clearfix">
                                                            <input type="text" name="contact_email_{{ $k }}"
                                                                   id="contact_email_{{ $k }}"
                                                                   class="col-xs-12 col-sm-12"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="form-group">
                                                    <label class="control-label col-xs-12 col-sm-3 no-padding-right"
                                                           for="contactNo">Contact No {{$k+1}} :</label>

                                                    <div class="col-xs-12 col-sm-9">
                                                        <div class="clearfix">
                                                            <input type="text" name="contact_no_{{ $k }}"
                                                                   id="contact_no_{{ $k }}"
                                                                   class="col-xs-12 col-sm-12"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="space-2"></div>
                                @endfor

                                <div class="row">
                                    <div class="col-xs-12">
                                        <h4 class="header smaller lighter blue">Prizes </h4>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="space-4"></div>
                                    <div class="col-xs-12">
                                        <div class="col-xs-2">
                                            <div class="form-group">
                                                <label class="control-label col-xs-12 no-padding-right"
                                                       for="contactName">Winner</label>
                                            </div>
                                        </div>
                                        <div class="col-xs-3">
                                            <div class="form-group">
                                                <label class="control-label col-xs-12 col-sm-3 no-padding-right"
                                                       for="currency">Currency :</label>

                                                <div class="col-xs-12 col-sm-9">
                                                    <div class="clearfix">
                                                        <select name='w_currency' class="col-xs-12 col-sm-12"
                                                                id="w_currency">
                                                            <option value="BDT">Tk. (BDT)</option>
                                                            <option value="USD">$ (USD)</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-3">
                                            <div class="form-group">
                                                <label class="control-label col-xs-12 col-sm-3 no-padding-right"
                                                       for="amount">Amount :</label>

                                                <div class="col-xs-12 col-sm-9">
                                                    <div class="clearfix">
                                                        <input type="text" name="w_amount" id="w_amount"
                                                               class="col-xs-12 col-sm-12"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label class="control-label col-xs-12 col-sm-3 no-padding-right"
                                                       for="currency">Allow Certificatre :</label>

                                                <div class="col-xs-12 col-sm-9">
                                                    <div class="clearfix">
                                                        <select name='w_alowCertif' class="col-xs-12"
                                                                id="w_alowCertif">
                                                            <option value=1>Yes</option>
                                                            <option value=0>No</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="col-xs-2">
                                            <div class="form-group">
                                                <label class="control-label col-xs-12 no-padding-right"
                                                       for="contactName">First Runner Up</label>
                                            </div>
                                        </div>
                                        <div class="col-xs-3">
                                            <div class="form-group">
                                                <label class="control-label col-xs-12 col-sm-3 no-padding-right"
                                                       for="currency">Currency :</label>

                                                <div class="col-xs-12 col-sm-9">
                                                    <div class="clearfix">
                                                        <select name='fr_currency' class="col-xs-12 col-sm-12"
                                                                id="fr_currency">
                                                            <option value="BDT">Tk. (BDT)</option>
                                                            <option value="USD">$ (USD)</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-3">
                                            <div class="form-group">
                                                <label class="control-label col-xs-12 col-sm-3 no-padding-right"
                                                       for="amount">Amount :</label>

                                                <div class="col-xs-12 col-sm-9">
                                                    <div class="clearfix">
                                                        <input type="text" name="fr_amount" id="fr_amount"
                                                               class="col-xs-12 col-sm-12"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label class="control-label col-xs-12 col-sm-3 no-padding-right"
                                                       for="currency">Allow Certificatre :</label>

                                                <div class="col-xs-12 col-sm-9">
                                                    <div class="clearfix">
                                                        <select name='fr_alowCertif' class="col-xs-12"
                                                                id="fr_alowCertif">
                                                            <option value=1>Yes</option>
                                                            <option value=0>No</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="col-xs-2">
                                            <div class="form-group">
                                                <label class="control-label col-xs-12 no-padding-right"
                                                       for="contactName">Second Runner Up</label>
                                            </div>
                                        </div>
                                        <div class="col-xs-3">
                                            <div class="form-group">
                                                <label class="control-label col-xs-12 col-sm-3 no-padding-right"
                                                       for="currency">Currency :</label>

                                                <div class="col-xs-12 col-sm-9">
                                                    <div class="clearfix">
                                                        <select name='sr_currency' class="col-xs-12 col-sm-12"
                                                                id="sr_currency">
                                                            <option value="BDT">Tk. (BDT)</option>
                                                            <option value="USD">$ (USD)</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-3">
                                            <div class="form-group">
                                                <label class="control-label col-xs-12 col-sm-3 no-padding-right"
                                                       for="amount">Amount :</label>

                                                <div class="col-xs-12 col-sm-9">
                                                    <div class="clearfix">
                                                        <input type="text" name="sr_amount" id="sr_amount"
                                                               class="col-xs-12 col-sm-12"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label class="control-label col-xs-12 col-sm-3 no-padding-right"
                                                       for="currency">Allow Certificatre :</label>

                                                <div class="col-xs-12 col-sm-9">
                                                    <div class="clearfix">
                                                        <select name='sr_alowCertif' class="col-xs-12 "
                                                                id="sr_alowCertif">
                                                            <option value=1>Yes</option>
                                                            <option value=0>No</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>
                                    <div class="space-4"></div>

                                    <div class="col-xs-12 col-sm-6">
                                        <div class="clearfix">
                                            <input type="text" name="prizDesc" id="prizDesc"
                                                   class="col-xs-12" placeholder="Short description about prizes"/>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="clearfix">
                                            <input type="text" name="prizOtherDetl" id="prizOtherDetl"
                                                   class="col-xs-12" placeholder="Other Details"/>
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>
                                    <div class="space-4"></div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12">
                                        <h4 class="header smaller lighter blue">Attachment </h4>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="space-4"></div>
                                    <div class="form-group">
                                        <div class="col-xs-12 col-sm-12">
                                            <input multiple="" type="file" id="prizeAttachment" name="prizeAttachment"/>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="step-pane" data-step="4">
                                <div class="center">
                                    <h3 class="green">Registration</h3>
                                </div>
                                <div class="row">
                                    <div class="clearfix"></div>
                                    <div class="space-4"></div>

                                    <div class="col-xs-12 checkReg">
                                        <div class="col-xs-12 col-xs-6">
                                            <h4>
                                                <div class="form-group">
                                                    <label class="control-label col-xs-12 col-sm-6 no-padding-right"
                                                           for="contactEmail">Open Registration on
                                                        <b><i>{{ config('app.name') }}</i></b> :</label>

                                                    <div class="col-xs-12 col-sm-6">
                                                        <div class="clearfix">
                                                            <?php
                                                            $ext = array();
                                                            /*if (!empty($category->activeRegistration) && ($category->activeRegistration == 1)) {
                                                                $ext = array('checked' => TRUE);
                                                            }*/
                                                            ?>
                                                            <input name="activeRegistration"
                                                                   class="ace-checkbox-1 isRegister" type="checkbox"
                                                                   value="1"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </h4>
                                        </div>

                                        <div class="col-xs-12 col-xs-6">
                                            &nbsp;
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="space-4"></div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="space-4"></div>

                                <div class="row regForm">
                                    <div class="col-xs-12">
                                        <div class="col-xs-12 col-sm-6">
                                            <div class="form-group">
                                                <label class="control-label col-xs-12 col-sm-3 no-padding-right"
                                                       for="regStartDate">Date: <span
                                                            class="required">*</span></label>

                                                <div class="col-xs-12 col-sm-9">
                                                    <div class="input-group">
																		<span class="input-group-addon">
																			<i class="ace-icon fa fa-clock-o"></i>
																		</span>
                                                        <input id="regStartDate" name="regStartDate" type="text"
                                                               class="form-control col-xs-12 col-sm-12  custDateTime"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6">
                                            <div class="form-group">
                                                <label class="control-label col-xs-12 col-sm-3 no-padding-right"
                                                       for="regEndDate">Date: <span
                                                            class="required">*</span></label>

                                                <div class="col-xs-12 col-sm-9">
                                                    <div class="input-group">
																		<span class="input-group-addon">
																			<i class="ace-icon fa fa-clock-o"></i>
																		</span>
                                                        <input id="regEndDate" name="regEndDate" type="text"
                                                               class="form-control col-xs-12 col-sm-12  custDateTime"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <hr/>
                    <div class="wizard-actions">
                        <button class="btn btn-prev">
                            <i class="ace-icon fa fa-arrow-left"></i>
                            Prev
                        </button>

                        <button class="btn btn-success btn-next" data-last="Finish">
                            Next
                            <i class="ace-icon fa fa-arrow-right icon-on-right"></i>
                        </button>
                    </div>
                </div>
                <!-- /.widget-main -->
            </div>
            <!-- /.widget-body -->
        </div>

    </div>
@stop


@section('custom_style')
    <style type="text/css">
        .required {
            color: red;
        }

        span.select2 {
            width: 100% !important;
        }

        .noFixedWidth {
            float: left;
            /*margin: 0px !important;*/
            padding: 5px !important;
            margin: 5px !important;
            /*border: 2px solid silver;*/
            border-radius: 15px;
            background-color: #DCE6F7;
        }

        .widget-body .wysiwyg-editor {
            border-width: 1px !important;
            /*border: 1px solid #BBC0CA !important;*/
        }

        .table {
            border: none;
        }

        .table tbody tr td {
            border: none;
        }

        .checkReg{
            background-color: #e0e0e0;
            margin-bottom: 10px;
            padding-bottom: 10px;
        }
    </style>
@stop

@section('custom_script')
    <script type="text/javascript">
        var rowCounter = 0;

        $(document).ready(function () {
            createDateTime();
        });

        /****************************/
        /****************************/
        /*  Add/Remove Row Section */
        /****************************/
        /****************************/

        $(document).on('click', ".addRow", function (e) {
            rowCounter++;
            e.preventDefault();
            var currentRowId = $(this).closest('tr').attr("id");
            var newRow = "<tr id='row_" + rowCounter + "'>" +
                    '<td class="center" width="47%"> <div class="form-group">' +
                    '<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="date_title">Date Title: </label>' +
                    '<div class="col-xs-12 col-sm-9"> <div class="clearfix">' +
                    '<input type="text" name="date_title_' + rowCounter + '" id="date_title" class="col-xs-12 col-sm-12"/>' +
                    '</div> </div> </div> </td>' +
                    '<td class="center" width="47%"> <div class="form-group">' +
                    '<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="date-timepicker2">Date: <span class="required">*</span></label>' +
                    '<div class="col-xs-12 col-sm-9"> <div class="input-group"> <span class="input-group-addon"> <i class="ace-icon fa fa-clock-o"></i> </span>' +
                    "<input id='custDateTime_" + rowCounter + "' name='toDate_" + rowCounter + "' type='text' class='form-control col-xs-12 col-sm-12  custDateTime'/>" +
                    '</div> </div> </div> </td>' +
                    '<td class="center" width="6%">' +
                    '<button class="btn btn-sm btn-danger removeRow"> <i class="ace-icon fa fa-trash"></i> </button>' +
                    '<button class="btn btn-sm btn-success addRow"> <i class="ace-icon fa fa-plus"></i> </button>' +
                    '</td> </tr>'

            $("table tbody").append(newRow);
        });

        $(document).on('click', ".removeRow", function (e) {
            e.preventDefault();
            var currentRowId = $(this).closest('tr').attr("id");
            $('#' + currentRowId + '').remove();
        })


        /****************************/
        /****************************/
        /*  End */
        /****************************/
        /****************************/

        /****************************/
        /****************************/
        /*  Form Validation Section  */
        /****************************/
        /****************************/
        jQuery(function ($) {

            $('[data-rel=tooltip]').tooltip();

            $('.select2').css('width', '400px').select2({allowClear: true})
                    .on('change', function () {
                        $(this).closest('form').validate().element($(this));
                    });


            var $validation = true;
            $('#fuelux-wizard-container')
                    .ace_wizard({
                        //step: 2 //optional argument. wizard will jump to step "2" at first
                        //buttons: '.wizard-actions:eq(0)'
                    })
                    .on('actionclicked.fu.wizard', function (e, info) {
                        if (info.step == 1 && $validation) {
                            if (!$('#validation-form').valid()) e.preventDefault();
                        }
                        if (info.step == 2 && $validation) {
                            if (!$('#validation-form').valid()) e.preventDefault();
                        }
                        if (info.step == 3 && $validation) {
                            if (!$('#validation-form').valid()) e.preventDefault();
                        }
                        if (info.step == 4 && $validation) {
                            if (!$('#validation-form').valid()) e.preventDefault();
                        }
                    })
                //.on('changed.fu.wizard', function() {
                //})
                    .on('finished.fu.wizard', function (e) {
                        bootbox.dialog({
                            message: "Thank you! Your information was successfully saved!",
                            buttons: {
                                "success": {
                                    "label": "OK",
                                    "className": "btn-sm btn-primary"
                                }
                            }
                        });
                    }).on('stepclick.fu.wizard', function (e) {
                        //e.preventDefault();//this will prevent clicking and selecting steps
                    });


            //jump to a step
            /**
             var wizard = $('#fuelux-wizard-container').data('fu.wizard')
             wizard.currentStep = 3;
             wizard.setState();
             */

                //determine selected step
                //wizard.selectedItem().step


                //hide or show the other form which requires validation
                //this is for demo only, you usullay want just one form in your application
//            $('#skip-validation').removeAttr('checked').on('click', function(){
//                $validation = this.checked;
//                if(this.checked) {
//                    $('#sample-form').hide();
//                    $('#validation-form').removeClass('hide');
//                }
//                else {
//                    $('#validation-form').addClass('hide');
//                    $('#sample-form').show();
//                }
//            })


                //documentation : http://docs.jquery.com/Plugins/Validation/validate


            $.mask.definitions['~'] = '[+-]';
            $('#phone').mask('(999) 999-9999');

            jQuery.validator.addMethod("phone", function (value, element) {
                return this.optional(element) || /^\(\d{3}\) \d{3}\-\d{4}( x\d{1,6})?$/.test(value);
            }, "Enter a valid phone number.");

            $('#validation-form').validate({
                errorElement: 'div',
                errorClass: 'help-block',
                focusInvalid: false,
                ignore: "",
                rules: {
                    email: {
                        required: true,
                        email: true
                    },
                    password: {
                        required: true,
                        minlength: 5
                    },
                    password2: {
                        required: true,
                        minlength: 5,
                        equalTo: "#password"
                    },
                    name: {
                        required: true
                    },
                    phone: {
                        required: true,
                        phone: 'required'
                    },
                    url: {
                        required: true,
                        url: true
                    },
                    comment: {
                        required: true
                    },
                    state: {
                        required: true
                    },
                    platform: {
                        required: true
                    },
                    subscription: {
                        required: true
                    },
                    gender: {
                        required: true,
                    },
                    agree: {
                        required: true,
                    },
                    title1: {
                        required: true,
                    }
                },

                messages: {
                    email: {
                        required: "Please provide a valid email.",
                        email: "Please provide a valid email."
                    },
                    password: {
                        required: "Please specify a password.",
                        minlength: "Please specify a secure password."
                    },
                    state: "Please choose state",
                    subscription: "Please choose at least one option",
                    gender: "Please choose gender",
                    agree: "Please accept our policy",
                    title1: "Please enter the title",
                },


                highlight: function (e) {
                    $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
                },

                success: function (e) {
                    $(e).closest('.form-group').removeClass('has-error');//.addClass('has-info');
                    $(e).remove();
                },

                errorPlacement: function (error, element) {
                    if (element.is('input[type=checkbox]') || element.is('input[type=radio]')) {
                        var controls = element.closest('div[class*="col-"]');
                        if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                        else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
                    }
                    else if (element.is('.select2')) {
                        error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
                    }
                    else if (element.is('.chosen-select')) {
                        error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
                    }
                    else error.insertAfter(element.parent());
                },

                submitHandler: function (form) {
                },
                invalidHandler: function (form) {
                }
            });


            $('#modal-wizard-container').ace_wizard();
            $('#modal-wizard .wizard-actions .btn[data-dismiss=modal]').removeAttr('disabled');


            /**
             $('#date').datepicker({autoclose:true}).on('changeDate', function(ev) {
					$(this).closest('form').validate().element($(this));
				});

             $('#mychosen').chosen().on('change', function(ev) {
					$(this).closest('form').validate().element($(this));
				});
             */


            $(document).one('ajaxloadstart.page', function (e) {
                //in ajax mode, remove remaining elements before leaving page
                $('[class*=select2]').remove();
            });
        })

        /****************************/
        /****************************/
        /*  End */
        /****************************/
        /****************************/

        /****************************/
        /****************************/
        /*  Date Time Picker Section    */
        /****************************/
        /****************************/

        function createDateTime() {
            $('.custDateTime').each(function () {
                var dateTimeId = $(this).attr("id");
                $('#' + dateTimeId + '').datetimepicker({
                    //format: 'MM/DD/YYYY h:mm:ss A',//use this option to display seconds
                    icons: {
                        time: 'fa fa-clock-o',
                        date: 'fa fa-calendar',
                        up: 'fa fa-chevron-up',
                        down: 'fa fa-chevron-down',
                        previous: 'fa fa-chevron-left',
                        next: 'fa fa-chevron-right',
                        today: 'fa fa-arrows ',
                        clear: 'fa fa-trash',
                        close: 'fa fa-times'
                    }
                }).next().on(ace.click_event, function () {
                    $(this).prev().focus();
                });
            });
        }

        $('#date-timepicker1').datetimepicker({
            //format: 'MM/DD/YYYY h:mm:ss A',//use this option to display seconds
            icons: {
                time: 'fa fa-clock-o',
                date: 'fa fa-calendar',
                up: 'fa fa-chevron-up',
                down: 'fa fa-chevron-down',
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-arrows ',
                clear: 'fa fa-trash',
                close: 'fa fa-times'
            }
        }).next().on(ace.click_event, function () {
            $(this).prev().focus();
        });

        $('#date-timepicker2').datetimepicker({
            //format: 'MM/DD/YYYY h:mm:ss A',//use this option to display seconds
            icons: {
                time: 'fa fa-clock-o',
                date: 'fa fa-calendar',
                up: 'fa fa-chevron-up',
                down: 'fa fa-chevron-down',
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-arrows ',
                clear: 'fa fa-trash',
                close: 'fa fa-times'
            }
        }).next().on(ace.click_event, function () {
            $(this).prev().focus();
        });

        /****************************/
        /****************************/
        /*  End */
        /****************************/
        /****************************/



        /****************************/
        /****************************/
        /*  File Upload Section */
        /****************************/
        /****************************/

        $('#id-input-file-3').ace_file_input({
            style: 'well',
            btn_choose: 'Drop files here or click to choose',
            btn_change: null,
            no_icon: 'ace-icon fa fa-cloud-upload',
            droppable: true,
            thumbnail: 'small'//large | fit
            //,icon_remove:null//set null, to hide remove/reset button
            /**,before_change:function(files, dropped) {
						//Check an example below
						//or examples/file-upload.html
						return true;
					}*/
            /**,before_remove : function() {
						return true;
					}*/
            ,
            preview_error: function (filename, error_code) {
                //name of the file that failed
                //error_code values
                //1 = 'FILE_LOAD_FAILED',
                //2 = 'IMAGE_LOAD_FAILED',
                //3 = 'THUMBNAIL_FAILED'
                //alert(error_code);
            }

        }).on('change', function () {
            //console.log($(this).data('ace_input_files'));
            //console.log($(this).data('ace_input_method'));
        });

        $('#gallery').ace_file_input({
            style: 'well',
            btn_choose: 'Drop files here or click to choose',
            btn_change: null,
            no_icon: 'ace-icon fa fa-cloud-upload',
            droppable: true,
            thumbnail: 'small'//large | fit
            //,icon_remove:null//set null, to hide remove/reset button
            /**,before_change:function(files, dropped) {
						//Check an example below
						//or examples/file-upload.html
						return true;
					}*/
            /**,before_remove : function() {
						return true;
					}*/
            ,
            preview_error: function (filename, error_code) {
                //name of the file that failed
                //error_code values
                //1 = 'FILE_LOAD_FAILED',
                //2 = 'IMAGE_LOAD_FAILED',
                //3 = 'THUMBNAIL_FAILED'
                //alert(error_code);
            }

        }).on('change', function () {
            //console.log($(this).data('ace_input_files'));
            //console.log($(this).data('ace_input_method'));
        });

        $('#prizeAttachment').ace_file_input({
            style: 'well',
            btn_choose: 'Drop files here or click to choose',
            btn_change: null,
            no_icon: 'ace-icon fa fa-cloud-upload',
            droppable: true,
            thumbnail: 'small'//large | fit
            //,icon_remove:null//set null, to hide remove/reset button
            /**,before_change:function(files, dropped) {
						//Check an example below
						//or examples/file-upload.html
						return true;
					}*/
            /**,before_remove : function() {
						return true;
					}*/
            ,
            preview_error: function (filename, error_code) {
                //name of the file that failed
                //error_code values
                //1 = 'FILE_LOAD_FAILED',
                //2 = 'IMAGE_LOAD_FAILED',
                //3 = 'THUMBNAIL_FAILED'
                //alert(error_code);
            }

        }).on('change', function () {
            //console.log($(this).data('ace_input_files'));
            //console.log($(this).data('ace_input_method'));
        });

        var whitelist_ext, whitelist_mime;
        var btn_choose;
        var no_icon;

        var file_input = $('#id-input-file-3');
        var gallery_input = $('#gallery');
        var prize_input = $('#prizeAttachment');

        file_input
                .ace_file_input('update_settings',
                {
                    'btn_choose': btn_choose,
                    'no_icon': no_icon,
                    'allowExt': whitelist_ext,
                    'allowMime': whitelist_mime
                });
        gallery_input
                .ace_file_input('update_settings',
                {
                    'btn_choose': btn_choose,
                    'no_icon': no_icon,
                    'allowExt': whitelist_ext,
                    'allowMime': whitelist_mime
                });
        prize_input
                .ace_file_input('update_settings',
                {
                    'btn_choose': btn_choose,
                    'no_icon': no_icon,
                    'allowExt': whitelist_ext,
                    'allowMime': whitelist_mime
                });

        file_input.ace_file_input('reset_input');
        gallery_input.ace_file_input('reset_input');
        prize_input.ace_file_input('reset_input');

        file_input
                .off('file.error.ace')
                .on('file.error.ace', function (e, info) {
                    //console.log(info.file_count);//number of selected files
                    //console.log(info.invalid_count);//number of invalid files
                    //console.log(info.error_list);//a list of errors in the following format

                    //info.error_count['ext']
                    //info.error_count['mime']
                    //info.error_count['size']

                    //info.error_list['ext']  = [list of file names with invalid extension]
                    //info.error_list['mime'] = [list of file names with invalid mimetype]
                    //info.error_list['size'] = [list of file names with invalid size]


                    /**
                     if( !info.dropped ) {
							//perhapse reset file field if files have been selected, and there are invalid files among them
							//when files are dropped, only valid files will be added to our file array
							e.preventDefault();//it will rest input
						}
                     */


                    //if files have been selected (not dropped), you can choose to reset input
                    //because browser keeps all selected files anyway and this cannot be changed
                    //we can only reset file field to become empty again
                    //on any case you still should check files with your server side script
                    //because any arbitrary file can be uploaded by user and it's not safe to rely on browser-side measures
                });

        gallery_input
                .off('file.error.ace')
                .on('file.error.ace', function (e, info) {
                    //console.log(info.file_count);//number of selected files
                    //console.log(info.invalid_count);//number of invalid files
                    //console.log(info.error_list);//a list of errors in the following format

                    //info.error_count['ext']
                    //info.error_count['mime']
                    //info.error_count['size']

                    //info.error_list['ext']  = [list of file names with invalid extension]
                    //info.error_list['mime'] = [list of file names with invalid mimetype]
                    //info.error_list['size'] = [list of file names with invalid size]


                    /**
                     if( !info.dropped ) {
							//perhapse reset file field if files have been selected, and there are invalid files among them
							//when files are dropped, only valid files will be added to our file array
							e.preventDefault();//it will rest input
						}
                     */


                    //if files have been selected (not dropped), you can choose to reset input
                    //because browser keeps all selected files anyway and this cannot be changed
                    //we can only reset file field to become empty again
                    //on any case you still should check files with your server side script
                    //because any arbitrary file can be uploaded by user and it's not safe to rely on browser-side measures
                });

        prize_input
                .off('file.error.ace')
                .on('file.error.ace', function (e, info) {
                    //console.log(info.file_count);//number of selected files
                    //console.log(info.invalid_count);//number of invalid files
                    //console.log(info.error_list);//a list of errors in the following format

                    //info.error_count['ext']
                    //info.error_count['mime']
                    //info.error_count['size']

                    //info.error_list['ext']  = [list of file names with invalid extension]
                    //info.error_list['mime'] = [list of file names with invalid mimetype]
                    //info.error_list['size'] = [list of file names with invalid size]


                    /**
                     if( !info.dropped ) {
							//perhapse reset file field if files have been selected, and there are invalid files among them
							//when files are dropped, only valid files will be added to our file array
							e.preventDefault();//it will rest input
						}
                     */


                    //if files have been selected (not dropped), you can choose to reset input
                    //because browser keeps all selected files anyway and this cannot be changed
                    //we can only reset file field to become empty again
                    //on any case you still should check files with your server side script
                    //because any arbitrary file can be uploaded by user and it's not safe to rely on browser-side measures
                });

        /****************************/
        /****************************/
        /*  End */
        /****************************/
        /****************************/


        /****************************/
        /****************************/
        /*  CK Editor JS Section    */
        /****************************/
        /****************************/

        function showErrorAlert(reason, detail) {
            var msg = '';
            if (reason === 'unsupported-file-type') {
                msg = "Unsupported format " + detail;
            }
            else {
                //console.log("error uploading file", reason, detail);
            }
            $('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>' +
                    '<strong>File upload error</strong> ' + msg + ' </div>').prependTo('#alerts');
        }

        $('#editor1').ace_wysiwyg({
            toolbar: [
                'font',
                null,
                'fontSize',
                null,
                {name: 'bold', className: 'btn-info'},
                {name: 'italic', className: 'btn-info'},
                {name: 'strikethrough', className: 'btn-info'},
                {name: 'underline', className: 'btn-info'},
                null,
                {name: 'insertunorderedlist', className: 'btn-success'},
                {name: 'insertorderedlist', className: 'btn-success'},
                {name: 'outdent', className: 'btn-purple'},
                {name: 'indent', className: 'btn-purple'},
                null,
                {name: 'justifyleft', className: 'btn-primary'},
                {name: 'justifycenter', className: 'btn-primary'},
                {name: 'justifyright', className: 'btn-primary'},
                {name: 'justifyfull', className: 'btn-inverse'},
                null,
                {name: 'createLink', className: 'btn-pink'},
                {name: 'unlink', className: 'btn-pink'},
                null,
                {name: 'insertImage', className: 'btn-success'},
                null,
                'foreColor',
                null,
                {name: 'undo', className: 'btn-grey'},
                {name: 'redo', className: 'btn-grey'}
            ],
            'wysiwyg': {
                fileUploadError: showErrorAlert
            }
        }).prev().addClass('wysiwyg-style2');

        var toolbar = $('#editor1').prev().get(0);
        toolbar.className = toolbar.className.replace(/wysiwyg\-style(1|2)/g, '');
        $(toolbar).addClass('wysiwyg-style2');

        /****************************/
        /****************************/
        /*  End */
        /****************************/
        /****************************/

        /****************************/
        /****************************/
        /*  Registration Section Show/Hide */
        /****************************/
        /****************************/

        $('.isRegister').on('click', function () {
            if ($(this).is(':checked')) {
                alert("checked");
            }
            else {
                alert("not checked");
            }
        });


        /****************************/
        /****************************/
        /*  End */
        /****************************/
        /****************************/

    </script>
@stop