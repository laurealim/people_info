                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
<div class="row">
    <div class="col-xs-12">
        @for($i = 0; $i < 20; $i++)
            <div class="media search-media">
                <div class="media-left">
                    <a href="#">
                        <img class="media-object eventImg" data-src="holder.js/72x72"/>
                    </a>
                </div>
                <div class="media-body">
                    <div class="">
                        <h3 class="media-heading">
                            <a href="#" class="blue"><strong>Event Title</strong></a>
                        </h3>

                        <div>
                            <span class="companyName">DivineIT Limited</span>
                            @if($i % 3 == 0)
                                <span class="label label-success arrowed eventStatus">Approved</span>
                            @elseif($i % 3 == 1)
                                <span class="label label-warning arrowed eventStatus">Pending</span>
                            @else
                                <span class="label label-danger arrowed eventStatus">Rejected</span>
                            @endif
                        </div>
                    </div>
                    <div class="event-body separator">
                        <div class="row">
                            <div class="col-xs-12 col-lg-4 col-sm-4">
                                <span class="label label-xlg label-primary arrowed-right">Domains</span> Dance,
                                Programming Contest, Finance, Online Quiz.
                            </div>
                            <div class="col-xs-12 col-lg-4 col-sm-4">
                                <span class="label label-xlg label-purple arrowed-right">Categories</span> Dance,
                                Programming Contest, Finance, Online Quiz.
                            </div>
                            <div class="col-xs-12 col-lg-4 col-sm-4">
                                <span class="label label-xlg label-info arrowed-right">Eligibility</span> Dance,
                                Programming Contest, Finance, Online Quiz.
                            </div>
                        </div>
                    </div>
                    <div class="event-body separator">
                        <div class="row">
                            <div class="col-xs-12 col-lg-4 col-sm-4">
                                <div class="startDate">
                                    <strong>From: &nbsp;</strong> Jan-12-2018
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    <strong>To: &nbsp;</strong> Jan-12-2018
                                </div>
                            </div>
                            <div class="col-xs-12 col-lg-4 col-sm-4">
                                <div class="deadline">
                                    <strong>Dead Line: &nbsp;</strong> Jan-12-2018
                                </div>
                            </div>

                            <div class="clear"></div>
                        </div>
                    </div>

                    {{--<div class="media-bottom">--}}
                    {{--<div class="eventDate">--}}


                    {{--<div class="clear"></div>--}}

                    {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="clear"></div>--}}


                    <div class="search-actions text-center ">
                        <span class="blue bolder bigger-150">#Event Type</span>

                        <div class="action-buttons bigger-125">
                            <a class="btns btn-primarys" href="{{ route('competition.adminEdit',1) }}">
                            <span class="label label-info label-white middle">
                                {{--<i class="ace-icon fa fa-exclamation-triangle bigger-120"></i>--}}
                                <i class="normal-icon ace-icon fa fa-eye gray bigger-130"></i>
                            View Registration
                            </span>
                            </a>
                        </div>
                        <div class="action-buttons bigger-125">
                            <a class="btns btn-primarys " href="{{ route('competition.adminDelete',1) }}" id=1>
                            <span class="label label-info label-white middle">
                                {{--<i class="ace-icon fa fa-exclamation-triangle bigger-120"></i>--}}
                                {{--<i class="normal-icon ace-icon fa fa-eye green bigger-130"></i>--}}
                                <i class="normal-icon ace-icon fa fa-pencil-square-o bigger-130"></i>
                            Edit
                            </span>
                            </a>
                        </div>
                        <div class="action-buttons bigger-125 timer">
                            <span class="middle " id="id_<?php echo $i;?>">
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        @endfor
    </div>
</div>


<div class="row">
    <div class="col-xs-6">
        <div class="dataTables_info" id="dynamic-table_info" role="status" aria-live="polite">
            {{--            Showing {{ (($categories->currentPage() - 1) * $categories->perPage()) + 1 }}--}}
            {{--            to {{ (($categories->currentPage() - 1) * $categories->perPage()) + $categories->perPage() }} of--}}
            {{--            {{ $categories->total() }} entries--}}
        </div>
    </div>
    <div class="col-xs-6">
        <div class="dataTables_paginate paging_simple_numbers" id="dynamic-table_paginate">
            {{--            {{ $categories->links("vendor.pagination.custom") }}--}}
        </div>
    </div>
</div>

@section('custom_style')
    <style type="text/css">
        .search-media {
            margin-top: 5px;
            padding: 12px 250px 12px 12px;
        }

        .search-media .search-actions {
            max-width: 250px;
            padding-top: 25px;
            border-bottom: 1px solid #EEEEEE;
        }

        .media-left {
            border-right: 2px dotted #EEEEEE;
            padding-top: 10px;
            padding-bottom: 10px;
        }

        img.eventImg {
            width: 100px;
            height: 100px;
            background-color: #EEEEEE;
        }

        .media-body {
            padding-left: 10px;
            padding-top: 10px;
            padding-bottom: 10px;
        }

        .separator {
            /*margin-bottom: 10px;*/
            margin-top: 10px;
        }

        .startDate {
            float: left;
        }

        .deadline {
            color: red;
            float: right;
        }

        .eventStatus {
            /*position: absolute;*/
            /*width: 10%;*/
            /*margin-right: 35px;*/
            float: right;
        }

        .event-body span {
            /*font-weight: bold;*/
            /*font-size: 15px;*/
            margin-bottom: 2px;
        }

        .action-buttons {
            margin-top: 5px;
        }
    </style>
@stop

@section('custom_script')
    <script>
        // Set the date we're counting down to
        //        var countDownDate = new Date("Jun 15, 2018 15:37:25").getTime();
        var countDownDate = new Date("6/8/2018 0:0:0").getTime();


        // Update the count down every 1 second
        $(".timer").each(function(){
            var thisId = $(this).find("span").attr('id');
            timeCounter(thisId);
//            timeCounter($(this));
//            console.log(thisId);
        });

        function timeCounter(e) {
            console.log(e);
            var x = setInterval(function () {

                // Get todays date and time
                var now = new Date().getTime();

                // Find the distance between now an the count down date
                var distance = countDownDate - now;

                // Time calculations for days, hours, minutes and seconds
                var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                var seconds = Math.floor((distance % (1000 * 60)) / 1000);

                // Display the result in the element with id="demo"
//                document.getElementById("timer").innerHTML = days + "d " + hours + "h "
//                        + minutes + "m " + seconds + "s ";

                document.getElementById(e).innerHTML = "<span style='color: #3A87AD; font-weight: bold'>"+days + "d " + hours + "h "
                        + minutes + "m " + seconds + "s </span>";

                // If the count down is finished, write some text
                if (distance < 0) {
                    clearInterval(x);
//                    document.getElementById("timer").innerHTML = "EXPIRED";
                    document.getElementById(e).innerHTML = "<span style='color: red; font-weight: bold'>EXPIRED</span>";
                }
            }, 1000);
        }
    </script>
@stop