<table id="dynamic-table" class="table table-striped table-bordered table-hover" style="margin-bottom: 0px;">
    <thead>
    <tr>
        <th class="center" width="5%">
            <label class="pos-rel">
                {{--<input type="checkbox" class="ace"/>--}}
                {{--<span class="lbl"></span>--}}
            </label>
        </th>
        <th width="30%">Domain Name</th>
        <th width="25%">Created By</th>
        <th width="25%">Status</th>
        <th class="center" width="15%">Action</th>
    </tr>
    </thead>

    <tbody>

    <?php $pageNo = (($domains->currentPage() - 1) * $domains->perPage()) + 1 ?>
    @foreach ($domains as $domain)
        <tr>
            <td class="center">
                <label class="pos-rel">
                    {{--<input type="checkbox" class="ace"/>--}}
                    <span class="lbl">{{ $pageNo++ }}</span>
                </label>
            </td>
            <td>
                {{ $domain->name }}
            </td>
            <td> {{ $domain->first_name}} </td>
            <td>
                <?php if($domain->status == 1){ ?>
                <span class='label label-success'>Active</span>
                <?php } else{ ?>
                <span class='label label-danger'>Inactive</span>
                <?php } ?>
            </td>
            <td class="center">
                <div class="hidden-sm hidden-xs action-buttons">
                    {{--<a class="blue" href="{{ route('') }}">--}}
                    {{--<i class="ace-icon fa fa-search-plus bigger-200"></i>--}}
                    {{--</a>--}}

                    {{--&nbsp;&nbsp;| &nbsp;&nbsp;--}}

                    <a class="green" href="{{ route('domain.adminEdit',$domain->id) }}">
                        <i class="ace-icon fa fa-pencil bigger-200"></i>
                    </a>

                    &nbsp;&nbsp;| &nbsp;&nbsp;

                    <a class="red deletebtn" href="{{ route('domain.adminDelete',$domain->id) }}" id={{ $domain->id }}>
                        <i class="ace-icon fa fa-trash-o bigger-200"></i>
                    </a>

                    {{--&nbsp;&nbsp;| &nbsp;&nbsp;--}}

                    {{--<a class="red deletebtn" href="{{ route('country.adminDelete',$country->id) }}" id={{ $country->id }}>--}}
                    {{--<i class="ace-icon fa fa-trash-o bigger-200"></i>--}}
                    {{--</a>--}}
                </div>

            </td>
        </tr>
    @endforeach
    </tbody>
</table>


<div class="row">
    <div class="col-xs-6">
        <div class="dataTables_info" id="dynamic-table_info" role="status" aria-live="polite">
            Showing {{ (($domains->currentPage() - 1) * $domains->perPage()) + 1 }}
            to {{ (($domains->currentPage() - 1) * $domains->perPage()) + $domains->perPage() }} of
            {{ $domains->total() }} entries
        </div>
    </div>
    <div class="col-xs-6">
        <div class="dataTables_paginate paging_simple_numbers" id="dynamic-table_paginate">
            {{ $domains->links("vendor.pagination.custom") }}
        </div>
    </div>
</div>
