<script type="text/javascript">
    try {
//        ace.settings.loadState('sidebar')
    } catch (e) {
    }
</script>

{{--<div class="sidebar-shortcuts" id="sidebar-shortcuts">--}}
{{--<div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">--}}
{{--<button class="btn btn-success">--}}
{{--<i class="ace-icon fa fa-signal"></i>--}}
{{--</button>--}}

{{--<button class="btn btn-info">--}}
{{--<i class="ace-icon fa fa-pencil"></i>--}}
{{--</button>--}}

{{--<button class="btn btn-warning">--}}
{{--<i class="ace-icon fa fa-users"></i>--}}
{{--</button>--}}

{{--<button class="btn btn-danger">--}}
{{--<i class="ace-icon fa fa-cogs"></i>--}}
{{--</button>--}}
{{--</div>--}}

{{--<div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">--}}
{{--<span class="btn btn-success"></span>--}}

{{--<span class="btn btn-info"></span>--}}

{{--<span class="btn btn-warning"></span>--}}

{{--<span class="btn btn-danger"></span>--}}
{{--</div>--}}
{{--</div><!-- /.sidebar-shortcuts -->--}}

<ul class="nav nav-list">
    <li class="active open">
        <a href="{{ route('admin.dashboard') }}">
            <i class="menu-icon fa fa-tachometer"></i>
            <span class="menu-text"> Dashboard </span>
        </a>

        <b class="arrow"></b>
    </li>

    {{--    <li class="">--}}
    {{--        <a href="#" class="dropdown-toggle">--}}
    {{--            <i class="menu-icon fa fa-briefcase"></i>--}}
    {{--							<span class="menu-text">--}}
    {{--								Data Content--}}
    {{--							</span>--}}

    {{--            <b class="arrow fa fa-angle-down"></b>--}}
    {{--        </a>--}}

    {{--        <b class="arrow"></b>--}}

    {{--        <ul class="submenu">--}}
    {{--            <li class="">--}}
    {{--                <a href="{{ route('event.adminList') }}">--}}
    {{--                    <i class="menu-icon fa fa-leanpub"></i>--}}
    {{--							<span class="menu-text">--}}
    {{--								Event Data--}}
    {{--							</span>--}}

    {{--                    --}}{{--<b class="arrow fa fa-angle-down"></b>--}}
    {{--                </a>--}}
    {{--            </li>--}}

    {{--            <li class="">--}}
    {{--                <a href="{{ route('category.adminList') }}">--}}
    {{--                    <i class="menu-icon fa fa-bars"></i>--}}
    {{--							<span class="menu-text">--}}
    {{--								Category Data--}}
    {{--							</span>--}}

    {{--                    --}}{{--<b class="arrow fa fa-angle-down"></b>--}}
    {{--                </a>--}}
    {{--            </li>--}}

    {{--            <li class="">--}}
    {{--                <a href="{{ route('domain.adminList') }}">--}}
    {{--                    <i class="menu-icon fa fa-archive"></i>--}}
    {{--							<span class="menu-text">--}}
    {{--								Domain Data--}}
    {{--							</span>--}}

    {{--                    --}}{{--<b class="arrow fa fa-angle-down"></b>--}}
    {{--                </a>--}}
    {{--            </li>--}}

    {{--            <li class="">--}}
    {{--                <a href="{{ route('eligibility.adminList') }}">--}}
    {{--                    <i class="menu-icon fa fa-check"></i>--}}
    {{--							<span class="menu-text">--}}
    {{--								Eligibility Data--}}
    {{--							</span>--}}

    {{--                    --}}{{--<b class="arrow fa fa-angle-down"></b>--}}
    {{--                </a>--}}
    {{--            </li>--}}
    {{--        </ul>--}}
    {{--    </li>--}}

{{--        <li class="">--}}
{{--            <a href="#" class="dropdown-toggle">--}}
{{--                <i class="menu-icon fa fa-map-marker"></i>--}}
{{--                <span class="menu-text"> Location </span>--}}

{{--                <b class="arrow fa fa-angle-down"></b>--}}
{{--            </a>--}}

{{--            <b class="arrow"></b>--}}

{{--            <ul class="submenu">--}}
{{--                <li class="">--}}
{{--                    <a href="{{ route('country.adminList') }}">--}}
{{--                        <i class="menu-icon fa fa-map"></i>--}}
{{--                        Country--}}
{{--                    </a>--}}

{{--                    <b class="arrow"></b>--}}
{{--                </li>--}}
{{--                <li class="">--}}
{{--                    <a href="{{ route('division.adminList') }}">--}}
{{--                        <i class="menu-icon fa fa-map"></i>--}}
{{--                        Division--}}
{{--                    </a>--}}
{{--                    <b class="arrow"></b>--}}
{{--                </li>--}}
{{--                <li class="">--}}
{{--                    <a href="{{ route('district.adminList') }}">--}}
{{--                        <i class="menu-icon fa fa-map"></i>--}}
{{--                        District--}}
{{--                    </a>--}}
{{--                    <b class="arrow"></b>--}}
{{--                </li>--}}

{{--                <li class="">--}}
{{--                    <a href="{{ route('city.adminList') }}">--}}
{{--                        <i class="menu-icon fa fa-building"></i>--}}
{{--                        City--}}
{{--                    </a>--}}

{{--                    <b class="arrow"></b>--}}
{{--                </li>--}}
{{--            </ul>--}}
{{--        </li>--}}

{{--        <li class="">--}}
{{--            <a href="{{ route('organization.adminList') }}">--}}
{{--                <i class="menu-icon fa fa-industry"></i>--}}
{{--                <span class="menu-text"> Organizations </span>--}}
{{--            </a>--}}

{{--            <b class="arrow"></b>--}}
{{--        </li>--}}

{{--        <li class="">--}}
{{--            <a href="{{ route('competition.adminList') }}">--}}
{{--                <i class="menu-icon fa fa-bullhorn"></i>--}}
{{--                <span class="menu-text"> Competitions </span>--}}
{{--            </a>--}}

{{--            <b class="arrow"></b>--}}
{{--        </li>--}}

{{--        <li class="">--}}
{{--            <a href="{{ route('property.adminList') }}">--}}
{{--                <i class="menu-icon fa fa-bullhorn"></i>--}}
{{--                <span class="menu-text"> Properties </span>--}}
{{--            </a>--}}

{{--            <b class="arrow"></b>--}}
{{--        </li>--}}
{{--        <li class="">--}}

{{--            <a href="#" class="dropdown-toggle">--}}
{{--                <i class="menu-icon fa fa-pencil-square-o"></i>--}}
{{--                <span class="menu-text"> Apartments </span>--}}
{{--                <b class="arrow fa fa-angle-down"></b>--}}
{{--            </a>--}}

{{--            <b class="arrow"></b>--}}
{{--            <ul class="submenu">--}}
{{--                <li class="">--}}
{{--                    <a href="{{ route('apt.adminList') }}">--}}
{{--                        <i class="menu-icon fa fa-caret-right"></i>--}}
{{--                        Apartment Lists--}}
{{--                    </a>--}}

{{--                    <b class="arrow"></b>--}}
{{--                </li>--}}

{{--                <li class="">--}}
{{--                    <a href="{{ route('apt.adminConfirmAssignClient') }}">--}}
{{--                        <i class="menu-icon fa fa-caret-right"></i>--}}
{{--                        Assign to a Client--}}
{{--                    </a>--}}

{{--                    <b class="arrow"></b>--}}
{{--                </li>--}}
{{--            </ul>--}}

{{--        </li>--}}

{{--    <li class="">--}}
{{--        <a href="{{ route('department.adminList') }}">--}}
{{--            <i class="menu-icon fa fa-university"></i>--}}
{{--            <span class="menu-text"> Department </span>--}}
{{--        </a>--}}

{{--        <b class="arrow"></b>--}}
{{--    </li>--}}
{{--    <li class="">--}}
{{--        <a href="{{ route('client.adminList') }}">--}}
{{--            <i class="menu-icon fa fa-users"></i>--}}
{{--            <span class="menu-text"> Users </span>--}}
{{--        </a>--}}

{{--        <b class="arrow"></b>--}}
{{--    </li>--}}
{{--    <li class="">--}}
{{--        <a href="{{ route('trade.adminList') }}">--}}
{{--            <i class="menu-icon fa fa-flag"></i>--}}
{{--            <span class="menu-text"> Trades </span>--}}
{{--        </a>--}}

{{--        <b class="arrow"></b>--}}
{{--    </li>--}}
{{--    <li class="">--}}
{{--        <a href="{{ route('training.adminList') }}">--}}
{{--            <i class="menu-icon fa fa-graduation-cap"></i>--}}
{{--            <span class="menu-text"> Training </span>--}}
{{--        </a>--}}

{{--        <b class="arrow"></b>--}}
{{--    </li>--}}
    <li class="">
        <a href="{{ route('registration.adminList') }}">
            <i class="menu-icon fa fa-users"></i>
            <span class="menu-text"> Registration </span>
        </a>

        <b class="arrow"></b>
    </li>
{{--    <li class="">--}}
{{--        <a href="{{ route('participant.adminList') }}">--}}
{{--            <i class="menu-icon fa fa-list"></i>--}}
{{--            <span class="menu-text"> Participant List </span>--}}
{{--        </a>--}}
{{--        <b class="arrow"></b>--}}
{{--    </li>--}}
    <li class="">
        <a href="#" class="dropdown-toggle">
            <i class="menu-icon fa fa-cogs"></i>
            <span class="menu-text"> Settings </span>
            <b class="arrow fa fa-angle-down"></b>
        </a>
        <b class="arrow"></b>
        <ul class="submenu">
            <li class="">
                <a href="#" class="dropdown-toggle">
                    <i class="menu-icon fa fa-box-open"></i>
                    <span class="menu-text"> General Settings </span>
                    <b class="arrow fa fa-angle-down"></b>
                </a>
                <b class="arrow"></b>
                <ul class="submenu">
{{--                    <li class="">--}}
{{--                        <a href="{{ route('division.adminList') }}">--}}
{{--                            <i class="menu-icon fa fa-caret-right"></i>--}}
{{--                            Division--}}
{{--                        </a>--}}
{{--                    </li>--}}
{{--                    <li class="">--}}
{{--                        <a href="{{ route('district.adminList') }}">--}}
{{--                            <i class="menu-icon fa fa-caret-right"></i>--}}
{{--                            Districts--}}
{{--                        </a>--}}
{{--                    </li>--}}
{{--                    <li class="">--}}
{{--                        <a href="{{ route('upazila.adminList') }}">--}}
{{--                            <i class="menu-icon fa fa-caret-right"></i>--}}
{{--                            Upazilas--}}
{{--                        </a>--}}
{{--                    </li>--}}
{{--                    <li class="">--}}
{{--                        <a href="{{ route('union.adminList') }}">--}}
{{--                            <i class="menu-icon fa fa-caret-right"></i>--}}
{{--                            Unions--}}
{{--                        </a>--}}
{{--                    </li>--}}
                    <li class="">
                        <a href="{{ route('package.adminList') }}">
                            <i class="menu-icon fa fa-cutlery"></i>
                            Package
                        </a>
                    </li>
                    <li class="">
                        <a href="{{ route('registration.adminDuplicateCheck') }}">
                            <i class="menu-icon fa fa-clone"></i>
                            Duplicate Check
                        </a>
                    </li>
                    <li class="">
                        <a href="{{ route('ssnp.adminList') }}">
                            <i class="menu-icon fa fa-clone"></i>
                            SSNP List
                        </a>
                    </li>
                </ul>
            </li>
{{--            <li class="">--}}
{{--                <a href="#" class="dropdown-toggle">--}}
{{--                    <i class="menu-icon fa fa-box-open"></i>--}}
{{--                    <span class="menu-text"> ACL Management </span>--}}
{{--                    <b class="arrow fa fa-angle-down"></b>--}}
{{--                </a>--}}
{{--                <b class="arrow"></b>--}}
{{--                <ul class="submenu">--}}
{{--                    <li class="">--}}
{{--                        <a href="{{ route('role.adminList') }}">--}}
{{--                            <i class="menu-icon fa fa-cutlery"></i>--}}
{{--                            Roles--}}
{{--                        </a>--}}
{{--                    </li>--}}
{{--                    <li class="">--}}
{{--                        <a href="{{ route('acl.adminList') }}">--}}
{{--                            <i class="menu-icon fa fa-cutlery"></i>--}}
{{--                            Actions--}}
{{--                        </a>--}}
{{--                    </li>--}}
{{--                    <li class="">--}}
{{--                        <a href="{{ route('permission.adminList') }}">--}}
{{--                            <i class="menu-icon fa fa-cutlery"></i>--}}
{{--                            Permission Set--}}
{{--                        </a>--}}
{{--                    </li>--}}
{{--                </ul>--}}
{{--            </li>--}}
        </ul>

    </li>
    <li class="">
        <a href="#" class="dropdown-toggle">
            <i class="menu-icon fa fa-line-chart"></i>
            <span class="menu-text">All Reports </span>
            <b class="arrow fa fa-angle-down"></b>
        </a>
        <b class="arrow"></b>
        <ul class="submenu">
{{--            <li class="">--}}
{{--                <a href="#" class="dropdown-toggle">--}}
{{--                    <i class="menu-icon fa fa-pencil-square-o"></i>--}}
{{--                    <span class="menu-text">Registration Report </span>--}}
{{--                    <b class="arrow fa fa-angle-down"></b>--}}
{{--                </a>--}}
{{--                <b class="arrow"></b>--}}
{{--                <ul class="submenu">--}}
{{--                    <li class="">--}}
{{--                        <a href="{{ route('report.adminRegistrationReport') }}">--}}
{{--                            <i class="menu-icon fa fa-caret-right"></i>--}}
{{--                            Registration Report--}}
{{--                        </a>--}}

{{--                        <b class="arrow"></b>--}}
{{--                    </li>--}}
{{--                    <li class="">--}}
{{--                        <a href="{{ route('report.adminMasterRollReport') }}">--}}
{{--                            <i class="menu-icon fa fa-caret-right"></i>--}}
{{--                            Master Roll Report--}}
{{--                        </a>--}}

{{--                        <b class="arrow"></b>--}}
{{--                    </li>--}}
{{--                </ul>--}}
{{--            </li>--}}
            <li class="">
                <a href="#" class="dropdown-toggle">
                    <i class="menu-icon fa fa-pencil-square-o"></i>
                    <span class="menu-text">External Report </span>
                    <b class="arrow fa fa-angle-down"></b>
                </a>
                <b class="arrow"></b>
                <ul class="submenu">
                    <li class="">
                        <a href="{{ route('report.adminBulkRegistrationReport') }}">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Bulk Registration Data
                        </a>

                        <b class="arrow"></b>
                    </li>

                </ul>
            </li>
            <li class="">
                <a href="#" class="dropdown-toggle">
                    <i class="menu-icon fa fa-pencil-square-o"></i>
                    <span class="menu-text">File Import </span>
                    <b class="arrow fa fa-angle-down"></b>
                </a>
                <b class="arrow"></b>
                <ul class="submenu">
                    <li class="">
                        <a href="{{ route('report.adminImportPackageAssign') }}">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Data Assign File Import
                        </a>

                        <b class="arrow"></b>
                    </li>
                    <li class="">
                        <a href="{{ route('report.adminImportPackageUnAssign') }}">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Data UnAssign File Import
                        </a>

                        <b class="arrow"></b>
                    </li>
                    <li class="">
                        <a href="{{ route('report.adminImportTribalAssign') }}">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Trible Assign File Import
                        </a>

                        <b class="arrow"></b>
                    </li>

                </ul>
            </li>
        </ul>
{{--        <ul class="submenu">--}}
{{--            <li class="">--}}
{{--                <a href="#" class="dropdown-toggle">--}}
{{--                    <i class="menu-icon fa fa-pencil-square-o"></i>--}}
{{--                    <span class="menu-text">Package wise Report </span>--}}
{{--                    <b class="arrow fa fa-angle-down"></b>--}}
{{--                </a>--}}
{{--                <b class="arrow"></b>--}}
{{--                <ul class="submenu">--}}
{{--                    <li class="">--}}
{{--                        <a href="{{ route('report.adminRegistrationReport') }}">--}}
{{--                            <i class="menu-icon fa fa-caret-right"></i>--}}
{{--                                Registration Report--}}
{{--                        </a>--}}

{{--                        <b class="arrow"></b>--}}
{{--                    </li>--}}
{{--                    <li class="">--}}
{{--                        <a href="{{ route('report.adminMasterRollReport') }}">--}}
{{--                            <i class="menu-icon fa fa-caret-right"></i>--}}
{{--                            Master Roll Report--}}
{{--                        </a>--}}

{{--                        <b class="arrow"></b>--}}
{{--                    </li>--}}
{{--                </ul>--}}
{{--            </li>--}}
{{--        </ul>--}}
    </li>
    <li class="">
        <a href="#" class="dropdown-toggle">
            <i class="fa fa-cloud-download" aria-hidden="true"></i>
            <span class="menu-text">Download App</span>
            <b class="arrow fa fa-angle-down"></b>
        </a>
        <b class="arrow"></b>
        <ul class="submenu">
            <li class="">
                <a href={{ asset('family_card_app.apk') }}>
                    <i class="fa fa-arrow-circle-down" aria-hidden="true"></i>
                    Mobile App Download
                </a>

                <b class="arrow"></b>
            </li>

        </ul>
    </li>

    {{--    <li class="">--}}
    {{--        <a href="#" class="dropdown-toggle">--}}
    {{--            <i class="menu-icon fa fa-pencil-square-o"></i>--}}
    {{--            <span class="menu-text"> Ledgers </span>--}}
    {{--            <b class="arrow fa fa-angle-down"></b>--}}
    {{--        </a>--}}
    {{--        <b class="arrow"></b>--}}
    {{--        <ul class="submenu">--}}
    {{--            <li class="">--}}
    {{--                <a href="{{ route('ledger.adminList') }}">--}}
    {{--                    <i class="menu-icon fa fa-caret-right"></i>--}}
    {{--                    Ledger Lists--}}
    {{--                </a>--}}

    {{--                <b class="arrow"></b>--}}
    {{--            </li>--}}
    {{--        </ul>--}}
    {{--    </li>--}}
    {{--    <li class="">--}}
    {{--        <a href="#" class="dropdown-toggle">--}}
    {{--            <i class="menu-icon fa fa-money"></i>--}}
    {{--            <span class="menu-text"> Bills </span>--}}
    {{--            <b class="arrow fa fa-angle-down"></b>--}}
    {{--        </a>--}}
    {{--        <b class="arrow"></b>--}}
    {{--        <ul class="submenu">--}}
    {{--            <li class="">--}}
    {{--                <a href="{{ route('bill.adminList') }}">--}}
    {{--                    <i class="menu-icon fa fa-caret-right"></i>--}}
    {{--                    Bill Lists--}}
    {{--                </a>--}}

    {{--                <b class="arrow"></b>--}}
    {{--            </li>--}}
    {{--        </ul>--}}
    {{--    </li>--}}
    {{--    <li class="">--}}
    {{--        <a href="#" class="dropdown-toggle">--}}
    {{--            <i class="menu-icon fa fa-credit-card" aria-hidden="true"></i>--}}
    {{--            <span class="menu-text"> Payment </span>--}}
    {{--            <b class="arrow fa fa-angle-down"></b>--}}
    {{--        </a>--}}
    {{--        <b class="arrow"></b>--}}
    {{--        <ul class="submenu">--}}
    {{--            <li class="">--}}
    {{--                <a href="{{ route('payment.adminList') }}">--}}
    {{--                    <i class="menu-icon fa fa-caret-right"></i>--}}
    {{--                    Payment Lists--}}
    {{--                </a>--}}

    {{--                <b class="arrow"></b>--}}
    {{--            </li>--}}
    {{--        </ul>--}}
    {{--    </li>--}}

    {{--    <li class="">--}}
    {{--        <a href="#" class="dropdown-toggle">--}}
    {{--            <i class="menu-icon fa fa-pencil-square-o"></i>--}}
    {{--            <span class="menu-text"> Forms </span>--}}

    {{--            <b class="arrow fa fa-angle-down"></b>--}}
    {{--        </a>--}}

    {{--        <b class="arrow"></b>--}}

    {{--        <ul class="submenu">--}}
    {{--            <li class="">--}}
    {{--                <a href="form-elements.html">--}}
    {{--                    <i class="menu-icon fa fa-caret-right"></i>--}}
    {{--                    Form Elements--}}
    {{--                </a>--}}

    {{--                <b class="arrow"></b>--}}
    {{--            </li>--}}

    {{--            <li class="">--}}
    {{--                <a href="form-elements-2.html">--}}
    {{--                    <i class="menu-icon fa fa-caret-right"></i>--}}
    {{--                    Form Elements 2--}}
    {{--                </a>--}}

    {{--                <b class="arrow"></b>--}}
    {{--            </li>--}}

    {{--            <li class="">--}}
    {{--                <a href="form-wizard.html">--}}
    {{--                    <i class="menu-icon fa fa-caret-right"></i>--}}
    {{--                    Wizard &amp; Validation--}}
    {{--                </a>--}}

    {{--                <b class="arrow"></b>--}}
    {{--            </li>--}}

    {{--            <li class="">--}}
    {{--                <a href="wysiwyg.html">--}}
    {{--                    <i class="menu-icon fa fa-caret-right"></i>--}}
    {{--                    Wysiwyg &amp; Markdown--}}
    {{--                </a>--}}

    {{--                <b class="arrow"></b>--}}
    {{--            </li>--}}

    {{--            <li class="">--}}
    {{--                <a href="dropzone.html">--}}
    {{--                    <i class="menu-icon fa fa-caret-right"></i>--}}
    {{--                    Dropzone File Upload--}}
    {{--                </a>--}}

    {{--                <b class="arrow"></b>--}}
    {{--            </li>--}}
    {{--        </ul>--}}
    {{--    </li>--}}

    {{--    <li class="">--}}
    {{--        <a href="widgets.html">--}}
    {{--            <i class="menu-icon fa fa-list-alt"></i>--}}
    {{--            <span class="menu-text"> Widgets </span>--}}
    {{--        </a>--}}

    {{--        <b class="arrow"></b>--}}
    {{--    </li>--}}

    {{--    <li class="">--}}
    {{--        <a href="calendar.html">--}}
    {{--            <i class="menu-icon fa fa-calendar"></i>--}}

    {{--							<span class="menu-text">--}}
    {{--								Calendar--}}

    {{--								<span class="badge badge-transparent tooltip-error" title="2 Important Events">--}}
    {{--									<i class="ace-icon fa fa-exclamation-triangle red bigger-130"></i>--}}
    {{--								</span>--}}
    {{--							</span>--}}
    {{--        </a>--}}

    {{--        <b class="arrow"></b>--}}
    {{--    </li>--}}

    {{--    <li class="">--}}
    {{--        <a href="gallery.html">--}}
    {{--            <i class="menu-icon fa fa-picture-o"></i>--}}
    {{--            <span class="menu-text"> Gallery </span>--}}
    {{--        </a>--}}

    {{--        <b class="arrow"></b>--}}
    {{--    </li>--}}

    {{--    <li class="">--}}
    {{--        <a href="#" class="dropdown-toggle">--}}
    {{--            <i class="menu-icon fa fa-tag"></i>--}}
    {{--            <span class="menu-text"> More Pages </span>--}}

    {{--            <b class="arrow fa fa-angle-down"></b>--}}
    {{--        </a>--}}

    {{--        <b class="arrow"></b>--}}

    {{--        <ul class="submenu">--}}
    {{--            <li class="">--}}
    {{--                <a href="profile.html">--}}
    {{--                    <i class="menu-icon fa fa-caret-right"></i>--}}
    {{--                    User Profile--}}
    {{--                </a>--}}

    {{--                <b class="arrow"></b>--}}
    {{--            </li>--}}

    {{--            <li class="">--}}
    {{--                <a href="inbox.html">--}}
    {{--                    <i class="menu-icon fa fa-caret-right"></i>--}}
    {{--                    Inbox--}}
    {{--                </a>--}}

    {{--                <b class="arrow"></b>--}}
    {{--            </li>--}}

    {{--            <li class="">--}}
    {{--                <a href="pricing.html">--}}
    {{--                    <i class="menu-icon fa fa-caret-right"></i>--}}
    {{--                    Pricing Tables--}}
    {{--                </a>--}}

    {{--                <b class="arrow"></b>--}}
    {{--            </li>--}}

    {{--            <li class="">--}}
    {{--                <a href="invoice.html">--}}
    {{--                    <i class="menu-icon fa fa-caret-right"></i>--}}
    {{--                    Invoice--}}
    {{--                </a>--}}

    {{--                <b class="arrow"></b>--}}
    {{--            </li>--}}

    {{--            <li class="">--}}
    {{--                <a href="timeline.html">--}}
    {{--                    <i class="menu-icon fa fa-caret-right"></i>--}}
    {{--                    Timeline--}}
    {{--                </a>--}}

    {{--                <b class="arrow"></b>--}}
    {{--            </li>--}}

    {{--            <li class="">--}}
    {{--                <a href="search.html">--}}
    {{--                    <i class="menu-icon fa fa-caret-right"></i>--}}
    {{--                    Search Results--}}
    {{--                </a>--}}

    {{--                <b class="arrow"></b>--}}
    {{--            </li>--}}

    {{--            <li class="">--}}
    {{--                <a href="email.html">--}}
    {{--                    <i class="menu-icon fa fa-caret-right"></i>--}}
    {{--                    Email Templates--}}
    {{--                </a>--}}

    {{--                <b class="arrow"></b>--}}
    {{--            </li>--}}

    {{--            <li class="">--}}
    {{--                <a href="login.html">--}}
    {{--                    <i class="menu-icon fa fa-caret-right"></i>--}}
    {{--                    Login &amp; Register--}}
    {{--                </a>--}}

    {{--                <b class="arrow"></b>--}}
    {{--            </li>--}}
    {{--        </ul>--}}
    {{--    </li>--}}

    {{--    <li class="">--}}
    {{--        <a href="#" class="dropdown-toggle">--}}
    {{--            <i class="menu-icon fa fa-file-o"></i>--}}

    {{--							<span class="menu-text">--}}
    {{--								Other Pages--}}

    {{--								<span class="badge badge-primary">5</span>--}}
    {{--							</span>--}}

    {{--            <b class="arrow fa fa-angle-down"></b>--}}
    {{--        </a>--}}

    {{--        <b class="arrow"></b>--}}

    {{--        <ul class="submenu">--}}
    {{--            <li class="">--}}
    {{--                <a href="faq.html">--}}
    {{--                    <i class="menu-icon fa fa-caret-right"></i>--}}
    {{--                    FAQ--}}
    {{--                </a>--}}

    {{--                <b class="arrow"></b>--}}
    {{--            </li>--}}

    {{--            <li class="">--}}
    {{--                <a href="error-404.html">--}}
    {{--                    <i class="menu-icon fa fa-caret-right"></i>--}}
    {{--                    Error 404--}}
    {{--                </a>--}}

    {{--                <b class="arrow"></b>--}}
    {{--            </li>--}}

    {{--            <li class="">--}}
    {{--                <a href="error-500.html">--}}
    {{--                    <i class="menu-icon fa fa-caret-right"></i>--}}
    {{--                    Error 500--}}
    {{--                </a>--}}

    {{--                <b class="arrow"></b>--}}
    {{--            </li>--}}

    {{--            <li class="">--}}
    {{--                <a href="grid.html">--}}
    {{--                    <i class="menu-icon fa fa-caret-right"></i>--}}
    {{--                    Grid--}}
    {{--                </a>--}}

    {{--                <b class="arrow"></b>--}}
    {{--            </li>--}}

    {{--            <li class="active">--}}
    {{--                <a href="blank.html">--}}
    {{--                    <i class="menu-icon fa fa-caret-right"></i>--}}
    {{--                    Blank Page--}}
    {{--                </a>--}}

    {{--                <b class="arrow"></b>--}}
    {{--            </li>--}}
    {{--        </ul>--}}
    {{--    </li>--}}
</ul><!-- /.nav-list -->

<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
    <i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state"
       data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
</div>