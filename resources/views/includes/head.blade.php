<meta http-equiv="Content-Type" content="text/html;"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta charset="UTF-8" />
<title>@yield('title')</title>

<meta name="description" content="" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

<!-- bootstrap & fontawesome -->
{{--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/css/bootstrap.min.css" integrity="sha384-y3tfxAZXuh4HwSYylfB+J125MxIs6mR5FOHamPBG064zB+AFeWH94NdvaCBm8qnd" crossorigin="anonymous">--}}
<link rel="stylesheet" href="{{ asset('css/template/bootstrap-4.min.css') }}" media="all" type="text/css"  />
<link rel="stylesheet" href="{{ asset('css/template/bootstrap.min.css') }}" media="all" type="text/css"  />
<link rel="stylesheet" href="{{ asset('font-awesome/4.5.0/css/font-awesome.min.css') }}" media="all" type="text/css"   />

<!-- page specific plugin styles -->
<link rel="stylesheet" href="{{ asset('css/template/jquery-ui.custom.min.css') }}" />
<link rel="stylesheet" href="{{ asset('css/template/jquery.gritter.min.css') }}" />
<link rel="stylesheet" href="{{ asset('css/template/select2.min.css') }}" />
<link rel="stylesheet" href="{{ asset('css/template/bootstrap-datepicker3.min.css') }}" />
<link rel="stylesheet" href="{{ asset('css/template/bootstrap-editable.min.css') }}" />
<link rel="stylesheet" href="{{ asset('css/template/jquery-ui.min.css') }}" />

<!-- text fonts -->
<link rel="stylesheet" href="{{ asset('css/template/fonts.googleapis.com.css') }}" media="all" type="text/css" />

<!-- ace styles -->
<link rel="stylesheet" href="{{ asset('css/template/ace.min.css')}}" class="ace-main-stylesheet" id="main-ace-style" media="all" type="text/css" />

<!--[if lte IE 9]>
<link rel="stylesheet" href="{{ asset('css/template/ace-part2.min.css') }}" class="ace-main-stylesheet" media="all" type="text/css" />
<![endif]-->
<link rel="stylesheet" href="{{ asset('css/template/ace-skins.min.css') }}" media="all" type="text/css" />
<link rel="stylesheet" href="{{ asset('css/template/ace-rtl.min.css') }}" media="all" type="text/css" />

<!--[if lte IE 9]>
<link rel="stylesheet" href="{{ asset('css/template/ace-ie.min.css') }}" media="all" type="text/css" />
<![endif]-->



<!-- inline styles related to this page -->

<!-- ace settings handler -->
<script src="{{ asset('js/template/ace-extra.min.js') }}"></script>

<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

<!--[if lte IE 8]>
<script src="{{ asset('js/template/html5shiv.min.js') }}"></script>
<script src="{{ asset('js/template/respond.min.js') }}"></script>
<![endif]-->