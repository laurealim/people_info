<div class="footer-inner">
    <div class="footer-contents">
        <span class="bigger-120">
            <div class="row-12">
            <span class="blue  align-center"><b>তথ্যাদি পরিবার প্রধানের স্বেচ্ছা প্রদত্ত, যা পরবর্তীতে প্রামাণিক দলিলাদির ভিত্তিতে সন্নিবেশ ও সংশোধনযোগ্য।</b></span><br><br>
                <div class="col-sm-6 col-md-6 align-left blue">পরিকল্পনা ও বাস্তবায়নে: <b>জনাব শাহিদা সুলতানা, জেলা প্রশাসক, গোপালগঞ্জ </b></div>
                <div class="col-sm-6 col-md-6 align-right blue">কারিগরি সহযোগিতায়: <b>আকিব মোঃ সাদিকুল ইসলাম, তথ্য ও যোগাযোগ প্রযুক্তি অধিদপ্তর.</b></div>
                <div class="clearfix"></div>
            </div>
        </span> &nbsp; &nbsp;
    </div>
</div>