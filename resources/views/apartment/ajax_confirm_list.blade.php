<table id="dynamic-table" class="table table-striped table-bordered table-hover" style="margin-bottom: 0px;">
    <thead>
    <tr>
        <th class="center" width="3%">
            {{--<label class="pos-rel">--}}
            {{--<input type="checkbox" class="ace"/>--}}
            {{--<span class="lbl"></span>--}}
            {{--</label>--}}
        </th>
        <th width="10%">Property Name</th>
        <th width="9%">Apartment Number</th>
        <th width="9%">Owner</th>
        <th width="9%">Tenant</th>
        <th width="9%">Monthly Rent Amount (BDT)</th>
        <th width="9%">Monthly Service Charge (BDT)</th>
        <th width="9%">Total Security Money (BDT)</th>
        <th width="7%">Total Security Month (BDT)</th>
        <th width="9%">Total Amount (BDT)</th>
        <th width="7%">Status</th>
        <th class="center" width="10%">Action</th>
    </tr>
    </thead>

    <tbody>

    <?php $pageNo = (($approveClients->currentPage() - 1) * $approveClients->perPage()) + 1; ?>
    @foreach ($approveClients as $approveClient)
        <?php //pr($approveClient);//die;?>
        <tr>
            <td class="center">
                <label class="pos-rel">
                    {{--<input type="checkbox" class="ace"/>--}}
                    <span class="lbl">{{ $pageNo++ }}</span>
                </label>
            </td>
            <td>
                {{ $approveClient->property_name }}
            </td>
            <td>
                {{ $approveClient->apt_number }}
            </td>
            <td>
                {{ \App\User::getDataById($approveClient->owner)->first_name.' '.\App\User::getDataById($approveClient->owner)->last_name }}
            </td>
            <td>
                {{ \App\User::getDataById($approveClient->client_id)->first_name.' '.\App\User::getDataById($approveClient->client_id)->last_name }}
            </td>
            <td>
                {{ number_format($approveClient->rent_amount, 2,'.',"") }}
            </td>
            <td>
                {{ number_format($approveClient->service_charge, 2,'.',"") }}
            </td>
            <td>
                {{ number_format(($approveClient->adv_rent * $approveClient->adv_month), 2,'.',"") }}
            </td>
            <td>
                {{ $approveClient->adv_month }}
            </td>
            <td>
                {{ number_format(($approveClient->adv_rent * $approveClient->adv_month) + $approveClient->rent_amount + $approveClient->service_charge, 2,'.',"") }}
            </td>

            <td>
                <?php if($approveClient->is_assigned == 1){ ?>
                <span class='label label-warning'>Not Confirmed</span>
                <?php } else if($approveClient->is_assigned == 2){ ?>
                <span class='label label-success'>Confirmed</span>
                <?php } else{?>
                <span class='label label-danger'>Deleted</span>
                <?php }?>
            </td>
            <td class="center">
                <div class="hidden-sm hidden-xs action-buttons">
                    {{--<a class="blue" href="{{ route('') }}">--}}
                    {{--<i class="ace-icon fa fa-search-plus bigger-200"></i>--}}
                    {{--</a>--}}

                    {{--&nbsp;&nbsp;| &nbsp;&nbsp;--}}




                    @if($approveClient->is_assigned == 1)

                        <a class="blue"
                           href="{{ route('apt.adminApproveClient',[$approveClient->id,$approveClient->client_id]) }}"
                           title="Accept">
                            <i class="ace-icon fa fa-check bigger-150"></i>
                        </a>

                        &nbsp;| &nbsp;

                        <a class="red rejectBtn" href="{{ route('apt.adminRejectClient',[$approveClient->id,$approveClient->client_id]) }}"
                           title="Reject"
                           id={{ $approveClient->id }}>
                            <i class="ace-icon fa fa-close bigger-150"></i>
                        </a>
                    @else
                        <i class="ace-icon fa fa-check-square-o bigger-150 green"></i>
                    @endif
                </div>

            </td>
        </tr>
    @endforeach
    </tbody>
</table>


<div class="row">
    <div class="col-xs-6">
        <div class="dataTables_info" id="dynamic-table_info" role="status" aria-live="polite">
            Showing {{ (($approveClients->currentPage() - 1) * $approveClients->perPage()) + 1 }}
            to {{ (($approveClients->currentPage() - 1) * $approveClients->perPage()) + $approveClients->perPage() }} of
            {{ $approveClients->total() }} entries
        </div>
    </div>
    <div class="col-xs-6">
        <div class="dataTables_paginate paging_simple_numbers" id="dynamic-table_paginate">
            {{ $approveClients->links("vendor.pagination.custom") }}
        </div>
    </div>
</div>

<style type="text/css">
    .facility {
        font-weight: bold;
    }
</style>