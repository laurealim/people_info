@extends('layouts.admin')

@section('title')
    Edit Apartment
@stop

@section('breadcrumb')
    <ul class="breadcrumb">
        <li>
            <i class="ace-icon fa fa-bars home-icon"></i>
            <a href="{{ route('apt.adminList') }}">Apartment List</a>
        </li>

        <li>
            <a href="{{ route('apt.adminEdit',$id) }}">Apartment Edit</a>
        </li>
        {{--<li class="active">User Profile</li>--}}
    </ul>
@stop

@section('page_header')
    <h1>Edit Apartment</h1>
@stop

@section('content')
    <div class="col-xs-12">
        <h3 class="header smaller lighter blue">
            Basic Information
        </h3>

        <form class="form-horizontal" role="form" action="{{ route('apt.adminUpdate', $id) }}" method="POST"
              enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="form-field-1-1"> Property List * </label>

                <div class="col-sm-9">
                    <select name='property_id' class="col-xs-10 col-sm-5" id="form-field-select-1">
                        <option value="0">---- Please Select One ----</option>
                        @foreach($propertyList as $p_id => $name)
                            <option value="{{ $p_id }}" {{ $apartmentData->property_id == $p_id ? 'selected="selected"' : '' }}>{{ $name }}</option>
                        @endforeach

                    </select>
                    @if ($errors->has('property_id'))
                        <span class="help-block middle">
                                <strong>{{ $errors->first('property_id') }}</strong>
                            </span>
                    @endif
                </div>
            </div>

            <div class="space-4"></div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="builders"> Apartment Number *</label>

                <div class="col-sm-9">
                    <input type="text" id="apt_number" placeholder="Apartment Number" name="apt_number"
                           value="{{ $apartmentData->apt_number }}"
                           class="col-xs-10 col-sm-5"/>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('apt_number'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('apt_number') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="space-4"></div>

            <h3 class="header smaller lighter blue">
                Apartment Details Information
            </h3>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="apt_size"> Apartment Size *</label>

                <div class="col-sm-9">
                    <input type="number" id="apt_size" placeholder="Apartment Size" name="apt_size"
                           value="{{ $apartmentData->apt_size }}"
                           class="col-xs-10 col-sm-5"/>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('apt_size'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('apt_size') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="apt_level"> Apartment Level *</label>

                <div class="col-sm-9">
                    <input type="number" id="apt_level" placeholder="1,2,3..." name="apt_level"
                           value="{{ $apartmentData->apt_level }}"
                           class="col-xs-10 col-sm-5"/>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('apt_level'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('apt_level') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="bed_room"> Total Bedroom *</label>

                <div class="col-sm-9">
                    <input type="number" id="bed_room" placeholder="10,20,30,40...." name="bed_room"
                           value="{{ $apartmentData->bed_room }}"
                           class="col-xs-10 col-sm-5"/>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('bed_room'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('bed_room') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="toilet"> Total Toilet *</label>

                <div class="col-sm-9">
                    <input type="number" id="toilet" placeholder="1,2,3..." name="toilet"
                           value="{{ $apartmentData->toilet }}"
                           class="col-xs-10 col-sm-5"/>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('toilet'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('toilet') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="veranda"> Total Veranda *</label>

                <div class="col-sm-9">
                    <input type="number" id="veranda" placeholder="1,2,3..." name="veranda"
                           value="{{ $apartmentData->veranda }}"
                           class="col-xs-10 col-sm-5"/>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('veranda'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('veranda') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right">Facilities </label>

                <div class="checkbox col-sm-2 col-lg-2 col-md-3">
                    <label>
                        <?php
                        $isChecked = "";
                        $isChecked = $apartmentData->intercom == 1 ? "checked" : "";
                        ?>

                        <input name="intercom" id="intercom" type="hidden" value="0"/>
                        <input name="intercom" id="intercom" type="checkbox" class="ace ace-checkbox-2"
                               value="1" <?php echo $isChecked; ?> />
                        <span class="lbl"> &nbsp; &nbsp;Intercom</span>
                    </label>
                </div>

                <div class="checkbox col-sm-3 col-lg-2 col-md-3">
                    <label>
                        <?php
                        $isChecked = "";
                        $isChecked = $apartmentData->geyser == 1 ? "checked" : "";
                        ?>

                        <input name="geyser" id="geyser" type="hidden" value="0"/>
                        <input name="geyser" id="geyser" type="checkbox" class="ace ace-checkbox-2"
                               value="1" <?php echo $isChecked; ?> />
                        <span class="lbl"> &nbsp; &nbsp;Geyser</span>
                    </label>
                </div>

                <div class="checkbox col-sm-3 col-lg-2 col-md-3">
                    <label>
                        <?php
                        $isChecked = "";
                        $isChecked = $apartmentData->car_parking == 1 ? "checked" : "";
                        ?>

                        <input name="car_parking" id="car_parking" type="hidden" value="0"/>
                        <input name="car_parking" id="car_parking" class="ace ace-checkbox-2" type="checkbox"
                               value="1" <?php echo $isChecked; ?> />
                        <span class="lbl"> &nbsp; &nbsp;Car Parking</span>
                    </label>
                </div>
            </div>

            <div class="space-4"></div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Apartment Description</label>

                <div class="col-sm-9">
                    <textarea id="form-field-1" placeholder="Apartment Description" name="apt_desc"
                              class="col-xs-10 col-sm-5"> {{ $apartmentData->apt_desc }}</textarea>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('apt_desc'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('apt_desc') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="space-4"></div>

            <div class="form-group">
                <label class="control-label col-xs-12 col-sm-3 no-padding-right "
                       for="logo">Apartment Images <span class="required">*</span></label>

                <div class="col-xs-12 col-sm-5">
                    <input multiple="" type="file" id="id-input-file-3" class="col-xs-10 col-sm-5" name="images[]"/>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <input type="hidden">
                </div>
            </div>

            <?php
            if (!empty($apartmentData->apt_images)) { ?>
            <div class="form-group">
                <div class="col-md-2">
                </div>
                <div class="col-md-8 col-sm-12">
                    <div class="team-img col-md-10 col-sm-12">
                        <?php
                        $apt_images = json_decode($apartmentData->apt_images, true);

                        if (sizeof($apt_images > 0)) {
                        $imgHas = false;
                        $imgDir = 'storage/apartments/';
                        $folderId = $id;

                        foreach ($apt_images as $key => $fileName) {
                        if (file_exists($imgDir . '/' . $folderId . '/' . $fileName)) {
                        ?>
                        <div class="team-img col-md-2 col-sm-3">
                            <img src="{{ asset('storage/apartments/'.$id.'/'.$fileName) }}" alt=""
                                 class="img-fluid hajjImg"
                                 width="100" height="100"><br>
                            <a href="#" class="imgRemove" id="<?php echo $fileName; ?>">Remove</a>
                        </div>
                        <?php } } } ?>
                    </div>
                </div>
                <div class="col-md-2">
                </div>
            </div>
            <br/>
            <br/>
            <?php } ?>

            <div class="space-4"></div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="form-field-1-1"> Status </label>

                <div class="col-sm-9">
                    <select name='status' class="col-xs-10 col-sm-5" id="form-field-select-1">
                        <option value="1" {{ $apartmentData->status == 1 ? 'selected="selected"' : '' }} >Active
                        </option>
                        <option value="2" {{ $apartmentData->status == 2 ? 'selected="selected"' : '' }} >Inactive
                        </option>
                        <option value="0" {{ $apartmentData->status == 0 ? 'selected="selected"' : '' }} >Deleted
                        </option>
                    </select>
                </div>
            </div>

            <div class="space-4"></div>

            <h3 class="header smaller lighter blue">
                Apartment Rent Details Information
            </h3>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="rent_amount"> Rent Amount *</label>

                <div class="col-sm-9">
                    <input type="number" id="rent_amount" placeholder="25000, 30000, 35000...." name="rent_amount"
                           value="{{ $apartmentData->rent_amount }}"
                           class="col-xs-10 col-sm-5"/>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('rent_amount'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('rent_amount') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="space-4"></div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="service_charge"> Service Charge
                    *</label>

                <div class="col-sm-9">
                    <input type="number" id="service_charge" placeholder="2000, 3000, 4000...."
                           name="service_charge"
                           value="{{ $apartmentData->service_charge }}"
                           class="col-xs-10 col-sm-5"/>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('service_charge'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('service_charge') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="space-4"></div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="adv_month"> Total Advance Months to pay
                    *</label>

                <div class="col-sm-9">
                    <input type="number" id="adv_month" placeholder="0, 1, 2..." name="adv_month"
                           value="{{ $apartmentData->adv_month }}"
                           class="col-xs-10 col-sm-5"/>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('adv_month'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('adv_month') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="space-4"></div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="adv_rent"> Advance Rent Amount *</label>

                <div class="col-sm-9">
                    <input type="number" id="adv_rent" placeholder="25000, 30000, 35000..." name="adv_rent"
                           value="{{ $apartmentData->adv_rent }}"
                           class="col-xs-10 col-sm-5"/>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('adv_rent'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('adv_rent') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="space-4"></div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right">Utility Bills (User have to pay) </label>

                <div class="checkbox col-sm-2 col-lg-2 col-md-3">
                    <label>
                        <?php
                        $isChecked = "";
                        $isChecked = $apartmentData->electricity == 1 ? "checked" : "";
                        ?>

                        <input name="electricity" id="electricity" type="hidden" value="0"/>
                        <input name="electricity" id="electricity" type="checkbox" class="ace ace-checkbox-2"
                               value="1" <?php echo $isChecked; ?> />
                        <span class="lbl"> &nbsp; &nbsp;Electricity Bill</span>
                    </label>
                </div>

                <div class="checkbox col-sm-3 col-lg-2 col-md-3">
                    <label>
                        <?php
                        $isChecked = "";
                        $isChecked = $apartmentData->gas == 1 ? "checked" : "";
                        ?>

                        <input name="gas" id="gas" type="hidden" value="0"/>
                        <input name="gas" id="gas" type="checkbox" class="ace ace-checkbox-2"
                               value="1" <?php echo $isChecked; ?> />
                        <span class="lbl"> &nbsp; &nbsp;Gas Bill</span>
                    </label>
                </div>

                <div class="checkbox col-sm-3 col-lg-2 col-md-3">
                    <label>
                        <?php
                        $isChecked = "";
                        $isChecked = $apartmentData->water == 1 ? "checked" : "";
                        ?>

                        <input name="water" id="water" type="hidden" value="0"/>
                        <input name="water" id="water" class="ace ace-checkbox-2" type="checkbox"
                               value="1" <?php echo $isChecked; ?> />
                        <span class="lbl"> &nbsp; &nbsp;Water Bill</span>
                    </label>
                </div>
            </div>

            <div class="space-4"></div>

            <div class="clearfix form-actions">
                <div class="col-md-offset-3 col-md-9">
                    <button class="btn btn-info" type="submit">
                        <i class="ace-icon fa fa-check bigger-110"></i>
                        Submit
                    </button>

                    &nbsp; &nbsp; &nbsp;
                    <button class="btn" type="reset">
                        <i class="ace-icon fa fa-undo bigger-110"></i>
                        Reset
                    </button>
                </div>
            </div>
        </form>
    </div>
@stop


@section('custom_style')

@stop

@section('custom_script')
    <script type="text/javascript">

        /****************************/
        /****************************/
        /*  File Upload Section */
        /****************************/
        /****************************/

        $('#id-input-file-3').ace_file_input({
            style: 'well',
            btn_choose: 'Drop files here or click to choose',
            btn_change: null,
            no_icon: 'ace-icon fa fa-cloud-upload',
            droppable: true,
            thumbnail: 'small'//large | fit
            //,icon_remove:null//set null, to hide remove/reset button
            /**,before_change:function(files, dropped) {
						//Check an example below
						//or examples/file-upload.html
						return true;
					}*/
            /**,before_remove : function() {
						return true;
					}*/
            ,
            preview_error: function (filename, error_code) {
                //name of the file that failed
                //error_code values
                //1 = 'FILE_LOAD_FAILED',
                //2 = 'IMAGE_LOAD_FAILED',
                //3 = 'THUMBNAIL_FAILED'
                //alert(error_code);
            }

        }).on('change', function () {
            //console.log($(this).data('ace_input_files'));
            //console.log($(this).data('ace_input_method'));
        });

        var whitelist_ext, whitelist_mime;
        var btn_choose;
        var no_icon;

        var file_input = $('#id-input-file-3');

        file_input
                .ace_file_input('update_settings',
                {
                    'btn_choose': btn_choose,
                    'no_icon': no_icon,
                    'allowExt': whitelist_ext,
                    'allowMime': whitelist_mime
                });

        file_input.ace_file_input('reset_input');

        file_input
                .off('file.error.ace')
                .on('file.error.ace', function (e, info) {
                    //console.log(info.file_count);//number of selected files
                    //console.log(info.invalid_count);//number of invalid files
                    //console.log(info.error_list);//a list of errors in the following format

                    //info.error_count['ext']
                    //info.error_count['mime']
                    //info.error_count['size']

                    //info.error_list['ext']  = [list of file names with invalid extension]
                    //info.error_list['mime'] = [list of file names with invalid mimetype]
                    //info.error_list['size'] = [list of file names with invalid size]


                    /**
                     if( !info.dropped ) {
							//perhapse reset file field if files have been selected, and there are invalid files among them
							//when files are dropped, only valid files will be added to our file array
							e.preventDefault();//it will rest input
						}
                     */


                    //if files have been selected (not dropped), you can choose to reset input
                    //because browser keeps all selected files anyway and this cannot be changed
                    //we can only reset file field to become empty again
                    //on any case you still should check files with your server side script
                    //because any arbitrary file can be uploaded by user and it's not safe to rely on browser-side measures
                });


        /****************************/
        /****************************/
        /*  End */
        /****************************/
        /****************************/


        $(".imgRemove").on('click', function (e) {
//            if (packageId) {
            e.preventDefault();
            var fileName = $(this).attr('id');
            var folderId = "<?php echo $id; ?>";
            dataType: "json",
                    $.ajax({
                        type: "delete",
                        url: '{{ url('/admin/apt/ajaxDelete') }}',
                        dataType: 'json',
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        data: {fileName: fileName, folderId: folderId, _token: "{{ csrf_token() }}"},
                        success: function (data) {
                            console.log(data.status);
                            if (data.status == 'success') {
                                location.reload();
                            }
                        }
                    });
//            }

        });
        
        
        $("#rent_amount").on('change', function () {
            var rentAmount = $(this).val();
            $("#adv_rent").val(rentAmount);

        });
    </script>
@stop