<?php
use App\Model\Ledger;
use App\Model\PendingLedger;
use App\Model\Bill;
use App\Model\Payment;
use App\Model\AdvanceDue;
use App\Model\Apartment;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

function is_initial_bill($aptId, $clientId, $ledgerFor)
{
    $ledgerModel = new Ledger();
    $resData = $ledgerModel->select('id', 'previous_amount', 'current_amount')
        ->where('apt_id', '=', $aptId)
        ->where('client_id', '=', $clientId)
        ->where('ledger_for', '=', $ledgerFor)
        ->where('status', '!=', config('constants.state.Rejected'))
        ->orderBy('id', 'desc')
        ->first();

    return $resData;
}

function getLedgerTypeById($id)
{
    $pendingLedgerModel = new PendingLedger();
    $resData = $pendingLedgerModel->select('id', 'ledger_type')
        ->where('id', '=', $id)
        ->where('status', '!=', config('constants.state.Rejected'))
        ->first()->toArray();

    return $resData;
}

function getLedgerForById($id)
{
    $pendingLedgerModel = new PendingLedger();
    $resData = $pendingLedgerModel->select('id', 'ledger_for')
        ->where('id', '=', $id)
        ->where('status', '!=', config('constants.state.Rejected'))
        ->first()->toArray();

    return $resData;
}

function getPreviousAmountFromLedger($apt_id, $client_id, $pendingLedgerId = 0)
{
    $advanceDueModel = new AdvanceDue();
    $resData = $advanceDueModel->select('id', 'previous_amount', 'current_amount')
        ->where('apt_id', '=', $apt_id)
        ->where('client_id', '=', $client_id)
        ->where('pending_ledger_id', '!=', $pendingLedgerId)
        ->orderBy('id', 'desc')
        ->first();
    return $resData;
}

function getPreviousAmount($apt_id, $client_id, $pendingLedgerId = 0)
{
    $ledgerModel = new Ledger();
    $resData = $ledgerModel->select('id', 'previous_amount', 'current_amount', 'ledger_type')
        ->where('apt_id', '=', $apt_id)
        ->where('client_id', '=', $client_id)
        ->where('pending_ledger_id', '!=', $pendingLedgerId)
        ->orderBy('id', 'desc')
        ->first();
    return $resData;
}

function checkPendingLedgers($apt_id, $client_id)
{
    $pendingLedgerModel = new PendingLedger();
    $has_pending = true;

    $resData = $pendingLedgerModel->where('apt_id', '=', $apt_id)
        ->where('client_id', '=', $client_id)
        ->where('status', '=', config('constants.state.Pending'))
        ->get()
        ->toArray();

    if (sizeof($resData) < 1) {
        $has_pending = false;
    }

    return $has_pending;
}

function pendingLedgerUpdate($id, $state)
{
    $apartmentModel = new Apartment();
    $ledgerModel = new Ledger();
    $pendingLedgerModel = new PendingLedger();
    $billModel = new Bill();
    $paymentModel = new Payment();
    $advanceDueModel = new AdvanceDue();
    $pendingLedgerDataArr = $pendingLedgerModel->where('id', '=', $id)
        ->where('status', '=', 1)
        ->first();

    $ledgerType = $pendingLedgerDataArr->ledger_type;
    $ledgerFor = $pendingLedgerDataArr->ledger_for;
    $aptId = $pendingLedgerDataArr->apt_id;
    $clientId = $pendingLedgerDataArr->client_id;
    $amount = $pendingLedgerDataArr->amount;
    $secure_amount = $pendingLedgerDataArr->secure_amount;
    $total_amount = $amount + $secure_amount;
    $previous_amount = $pendingLedgerDataArr->previous_amount;
    $current_amount = $pendingLedgerDataArr->current_amount;
    $updatedStatus = 0;
    $initialUpdateStatus = 2;
    $approvedBy = 0;
    $lastLedgerId = 0;

    $returnDate = [
        'status',
        'message'
    ];

    if ($pendingLedgerDataArr) {
        if ($state == config('constants.state.Approved')) {
            $updatedStatus = 2;
            $approvedBy = auth()->user()->id;
        } elseif ($state == config('constants.state.Rejected')) {
            $updatedStatus = 3;
            $initialUpdateStatus = 1;
        }

        DB::beginTransaction();
        try {
            if ($state == config('constants.state.Approved')) {
                $ledgerModel->apt_id = $pendingLedgerDataArr->apt_id;
                $ledgerModel->client_id = $pendingLedgerDataArr->client_id;
                $ledgerModel->pending_ledger_id = $pendingLedgerDataArr->id;
                $ledgerModel->amount = $pendingLedgerDataArr->amount;
                $ledgerModel->secure_amount = $pendingLedgerDataArr->secure_amount;
                $ledgerModel->previous_amount = $pendingLedgerDataArr->previous_amount;
                $ledgerModel->current_amount = $pendingLedgerDataArr->current_amount;
                $ledgerModel->ledger_for = $pendingLedgerDataArr->ledger_for;
                $ledgerModel->ledger_type = $pendingLedgerDataArr->ledger_type;
                $ledgerModel->created_by = $pendingLedgerDataArr->created_by;
                $ledgerModel->updated_by = $pendingLedgerDataArr->updated_by;
                $ledgerModel->approved_by = auth()->user()->id;
                $ledgerModel->status = config('constants.state.Approved');

                $ledgerModel->save();
                $lastLedgerId = $ledgerModel->id;
            }


            /*  Update Pending Ledger Status to 3 or 2  */
            $pendingLedgerDataArr->status = $updatedStatus;
            $pendingLedgerDataArr->approved_by = $approvedBy;
            $pendingLedgerDataArr->save();

            /*  If Ledger For is "Bill", Update Bill Status to 3 or 2 */
            if ($ledgerFor == config('constants.ledgerFor.Billing')) {
                $billModel->where('pending_ledger_id', '=', $id)
                    ->where('apt_id', '=', $aptId)
                    ->where('client_id', '=', $clientId)
                    ->update(['status' => $updatedStatus, 'ledger_id' => $lastLedgerId, 'approved_by' => $approvedBy]);
            }

            /*  If Ledger For is "Payment", Update Payment Status to 3 or 2 */
            if ($ledgerFor == config('constants.ledgerFor.Payment')) {
                $paymentModel->where('pending_ledger_id', '=', $id)
                    ->where('apt_id', '=', $aptId)
                    ->where('client_id', '=', $clientId)
                    ->update(['status' => $updatedStatus, 'ledger_id' => $lastLedgerId, 'approved_by' => $approvedBy]);
            }

            /*  Update Advance Due Status to 3 or 2 */
            if ($state == config('constants.state.Approved')) {
                $resData = $advanceDueModel->where('client_id', '=', $clientId)
                    ->where('apt_id', '=', $aptId)
                    ->where('status', '=', config('constants.state.Approved'))
                    ->first();

                if (!empty($resData)) {
                    $advanceDueModel->where('apt_id', '=', $aptId)
                        ->where('client_id', '=', $clientId)
                        ->update(['status' => $updatedStatus, 'approved_by' => $approvedBy, 'total_amount' => $total_amount, 'previous_amount' => $previous_amount, 'current_amount' => $current_amount, 'pending_ledger_id' => $id]);
                } else {
                    $advanceDueModel->where('pending_ledger_id', '=', $id)
                        ->where('apt_id', '=', $aptId)
                        ->where('client_id', '=', $clientId)
                        ->update(['status' => $updatedStatus, 'approved_by' => $approvedBy]);
                }
            }

            /*  If Ledger Type is 'Initial' and Ledger For 'Bill' Update Apartment Is Assigned to 1 */
            if (($ledgerType == config('constants.ledgerType.Initial')) && ($ledgerFor == config('constants.ledgerFor.Billing'))) {
                $apartmentModel->where('id', '=', $aptId)
                    ->where('client_id', '=', $clientId)
                    ->update(['is_assigned' => $initialUpdateStatus]);
            }

            DB::commit();
            $returnDate['status'] = 1;
            if ($state == config('constants.state.Approved')) {
                $returnDate['message'] = 'Pending Ledger Approved Successfully..';
            } else if ($state == config('constants.state.Rejected')) {
                $returnDate['message'] = 'Pending Ledger Rejected Successfully..';
            }
            return $returnDate;

        } catch (\Exception $ex) {
            DB::rollback();
            $returnDate['status'] = 2;
            if ($state == config('constants.state.Approved')) {
                $returnDate['message'] = 'Cann\'t Approved Pending Ledger..';
            } else if ($state == config('constants.state.Rejected')) {
                $returnDate['message'] = 'Cann\'t Reject Pending Ledger..';
            }
            return $returnDate;
        }
    } else {
        $returnDate['status'] = 2;
        $returnDate['message'] = 'No Data Found..';
        return $returnDate;
    }
}

function generateLedgerForPayment()
{
}

function getAdvanceDueAmount($clientId, $aptId, $bill_id)
{
    $advanceDueModel = new AdvanceDue();
    $billModel = new Bill();

    $pendingLedgerId = 0;
    if ($bill_id > 0) {
        $pendingLedgerId = $billModel->getPendingLedgerIdByBillId($bill_id);
    }

    $resData = getPreviousAmount($aptId, $clientId, $pendingLedgerId);

    $currentAmount = $resData->current_amount;
    $previousAmount = $resData->previous_amount;
    $ledgerType = $resData->ledger_type;

    return ['dueAmount' => $currentAmount, 'ledgerType' => $ledgerType, 'previousAmount' => $previousAmount];
}

?>