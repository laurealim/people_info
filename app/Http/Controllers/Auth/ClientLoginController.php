<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class ClientLoginController extends Controller
{
    use AuthenticatesUsers;

    protected $redirectTo = '/client';

    public function __construct()
    {
        $this->middleware('guest:client')->except('logout');
    }

    public function showLoginForm()
    {
        return view('auth.client_login');
    }

    public function logout(Request $request)
    {
        $this->guard("client")->logout();
        $request->session()->invalidate();
        return redirect('/client/login');
    }

    protected function guard()
    {
        return Auth::guard('client');
    }

    protected function credentials(Request $request)
    {
//        dd($request);
        $user_type = config('constants.userType.Owner');
        $user_status = config('constants.status.Active');
        return [
            "email" => $request->get($this->username()),
            'password' => $request->password,
            'user_type' => $user_type,
            'status' =>  $user_status,
        ];

    }

    protected function sendFailedLoginResponse(Request $request)
    {
        throw ValidationException::withMessages([
            $this->username() => [trans('auth.failed')],
            'login_error' => 'Errors in Login',
        ]);
    }
}
