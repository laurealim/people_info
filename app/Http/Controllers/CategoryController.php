<?php

namespace App\Http\Controllers;

use App\Model\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function adminList()
    {
        if (isset($_GET['displayValue'])) {
            $displayValue = $_GET['displayValue'] != '' ? $_GET['displayValue'] : "";
        } else {
            $displayValue = '';
        }

        if (isset($_GET['searchData'])) {
            $searchData = $_GET['searchData'] != '' ? $_GET['searchData'] : "";
        } else {
            $searchData = '';
        }

        $categories = new Category();

        $categories = $categories->where('categories.name', 'like', '%' . $searchData . '%')
            ->leftJoin('users', 'categories.created_by', 'users.id')
            ->select('categories.*', 'users.first_name')
            ->paginate($displayValue);


        if (request()->ajax()) {
            return view('category.ajax_list',compact('categories'));
        } else {
            return view('category.adminList',compact('categories'));
        }
    }

    public function adminForm()
    {
        return view('category.adminForm');
    }

    public function adminStore(Request $request)
    {
        $categoryModel = new Category();
        $data = $this->validate($request,[
            'name' => 'required',
        ]);

        $categoryModel->saveData($request);
        return redirect('admin/category/list')->with('success','New Category added successfully');
    }

    public function show(Category $category)
    {
        //
    }

    public function adminEdit($id, Category $category)
    {
        $categoryData = Category::where('id', $id)->first();
        return view('category.adminEdit', compact('categoryData', 'id'));
    }

    public function adminUpdate(Request $request, Category $category, $id)
    {
        $categoryModel = new Category();
        $data = $this->validate($request, [
            'name' => 'required',
        ]);

        $categoryModel->updateData($request);

        return redirect('admin/category/list')->with('success','Category edited successfully');
    }

    public function adminDestroy(Request $request, Category $category)
    {
        $eventInfo = Category::findOrFail($request->id);
        if(isset($request->id)){
            $eventInfo->delete();
            $request->session()->flash('success', 'Data Deleted Successfully..');
            return response()->json(['status'=>'success']);
        }
        else{
            $request->session()->flash('errors', 'Data Deleted Successfully..');
            return response()->json(['status'=>'error']);
        }
    }
}
