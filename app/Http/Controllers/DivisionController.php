<?php

namespace App\Http\Controllers;

use App\Model\Division;
use App\Model\Country;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DivisionController extends Controller
{
    public function adminList()
    {
        if (isset($_GET['displayValue'])) {
            $displayValue = $_GET['displayValue'] != '' ? $_GET['displayValue'] : "";
        } else {
            $displayValue = '';
        }

        if (isset($_GET['searchData'])) {
            $searchData = $_GET['searchData'] != '' ? $_GET['searchData'] : "";
        } else {
            $searchData = '';
        }

        $divisions = new Division();

        $divisions = $divisions->select('divisions.*')
            ->where('divisions.name', 'like', '%' . $searchData . '%')
            ->paginate($displayValue);
        if (request()->ajax()) {
            return view('division.ajax_list', compact('divisions'));
        } else {
            return view('division.adminList', compact('divisions'));
        }
    }

    public function adminForm()
    {
        $countryData = Country::all();
        return view('division.adminForm', compact('countryData'));
    }

    public function adminStore(Request $request)
    {
        $divisionModel = new Division();
        $data = $this->validate($request, [
            'name' => 'required',
            'bn_name' => 'required',
        ], [
            'name.required' => 'Division Name is required',
            'bn_name.required' => 'Division Name (Bangla) is required',

        ]);

        $divisionModel->saveData($request);
        return redirect('admin/division/list')->with('success', 'New Division added successfully');
    }

    public function show(Division $division)
    {
        //
    }

    public function adminEdit(Division $division, $id)
    {
        $countryData = Country::all();
        $divisionData = Division::where('id', $id)->first();
        return view('division.adminEdit', compact('countryData', 'divisionData', 'id'));
    }

    public function adminUpdate(Request $request, Division $division)
    {
        $divisionModel = new Division();
        $data = $this->validate($request, [
            'name' => 'required',
            'bn_name' => 'required',
        ], [
            'name.required' => 'Division Name is required',
            'bn_name.required' => 'Division Name (Bangla) is required',

        ]);

        $divisionModel->updateData($request);

        return redirect('admin/division/list')->with('success', 'Division edited successfully');
    }

    public function adminDelete(Request $request,Division $division)
    {
        $divisionInfo = Division::findOrFail($request->id);
        if(isset($request->id)){
            $divisionInfo->delete();
            $request->session()->flash('success', 'Division Deleted Successfully..');
            return response()->json(['status'=>'success']);
        }
        else{
            $request->session()->flash('errors', 'Division Can\'t Deleted Successfully..');
            return response()->json(['status'=>'error']);
        }
    }


    public function adminDivisionSelectAjaxList(Request $request)
    {
        return $this->_divisionSelectAjaxList($request);
    }

    public function clientDivisionSelectAjaxList(Request $request)
    {
        return $this->_divisionSelectAjaxList($request);
    }

    private function _divisionSelectAjaxList($request){
        $divisions = array();
        if($request->ajax()){

            $divisionData = new Division();
            $countryID = $request->countryID;
            $divisions = $divisionData->where("country_id", $countryID)->pluck("name", "id")->all();

//            $states = DB::table('states')->where('id_country',$request->id_country)->pluck("name","id")->all();
//            $data = view('ajax-select',compact('states'))->render();
//            return response()->json(['options'=>$data]);
            return json_encode($divisions);
        }
    }
}
