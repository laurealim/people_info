<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
//        $this->middleware('permission:role-list|role-create|role-edit|role-delete', ['only' => ['index', 'store']]);
//        $this->middleware('permission:role-create', ['only' => ['create', 'store']]);
//        $this->middleware('permission:role-edit', ['only' => ['edit', 'update']]);
//        $this->middleware('permission:role-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * //     * @return \Illuminate\Http\Response
     */
    public function adminList()
    {
        if (isset($_GET['displayValue'])) {
            $displayValue = $_GET['displayValue'] != '' ? $_GET['displayValue'] : "";
        } else {
            $displayValue = '';
        }

        if (isset($_GET['searchData'])) {
            $searchData = $_GET['searchData'] != '' ? $_GET['searchData'] : "";
        } else {
            $searchData = '';
        }

        $permissionList = Permission::where('permissions.name', 'like', '%' . $searchData . '%')
            ->orwhere('permissions.controller', 'like', '%' . $searchData . '%')
            ->orwhere('permissions.action', 'like', '%' . $searchData . '%')
            ->orwhere('permissions.guard_name', 'like', '%' . $searchData . '%')
            ->paginate($displayValue);

        if (request()->ajax()) {
            return view('permissions.ajax_list', compact('permissionList'));
        } else {
            return view('permissions.adminList', compact('permissionList'));
        }
    }

    public function indexxx()
    {
        $permissionLists = getPermissionList();
        $createData = [];

        DB::beginTransaction();
        try {
            foreach ($permissionLists as $script => $values) {
                foreach ($values as $controller => $actions) {
                    foreach ($actions as $key => $desc) {
                        $createData = [];
                        $createData['name'] = $script . '_' . $controller . '_' . $key;
                        $createData['controller'] = $controller;
                        $createData['action'] = $key;
                        $createData['desc'] = $desc;
                        $createData['guard_name'] = $script;
                        $permission = Permission::create($createData);
                    }
                }
            }
            DB::commit();
            dd('Done');
//            return redirect('admin/registration/list')->with('success', 'New Registration added successfully');
        } catch (\Exception $exception) {
            DB::rollback();
            dd($exception);
//            $request->session()->flash('errors', $exception);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function adminForm()
    {
        return view('permissions.adminForm');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function adminStore(Request $request)
    {
        try {
            DB::beginTransaction();
            $this->validate($request, [
                'name' => 'required',
                'guard_name' => 'required',
            ]);

            $saveData = [
                'name' => $request->post('name'),
                'guard_name' => $request->post('guard_name'),
                'controller' => $request->post('controller'),
                'action' => $request->post('action'),
                'desc' => $request->post('desc'),
            ];
            Permission::create($saveData);
            DB::commit();
            return redirect('admin/permission/list')->with('success', 'New Permission added successfully');
        } catch (\Exception $exception) {
            DB::rollback();
            dd($exception);
            $request->session()->flash('error', $exception);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function adminEdit($id)
    {
        $permission = Permission::find($id);

        return view('permissions.adminEdit', compact('permission',"id"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function adminUpdate(Request $request, $id)
    {
        try {
            $permission = Permission::find($id);

            DB::beginTransaction();
            $this->validate($request, [
                'name' => 'required',
                'guard_name' => 'required',
            ]);

            $saveData = [
                'name' => $request->post('name'),
                'guard_name' => $request->post('guard_name'),
                'controller' => $request->post('controller'),
                'action' => $request->post('action'),
                'desc' => $request->post('desc'),
            ];


            $permission->name = $request->input('name');
            $permission->guard_name = $request->input('guard_name');
            $permission->controller = $request->input('controller');
            $permission->action = $request->input('action');
            $permission->desc = $request->input('desc');

            $permission->save();
            DB::commit();
            return redirect('admin/permission/list')->with('success', 'Permission Updated successfully');
        } catch (\Exception $exception) {
            DB::rollback();
            dd($exception);
            $request->session()->flash('error', $exception);
        }


//        $role = Role::find($id);
//        $role->name = $request->input('name');
//        $role->save();

//        $role->syncPermissions($request->input('permission'));

//        return redirect()->route('permissions.index')->with('success', 'Role updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function adminDestroy($id)
    {
        DB::table("permissions")->where('id', $id)->delete();
        return redirect()->route('permissions.index')->with('success', 'Role deleted successfully');
    }

    private function get_guard()
    {
        if (Auth::guard('admin')->check()) {
            return "admin";
        } elseif (Auth::guard('user')->check()) {
            return "user";
        } elseif (Auth::guard('client')->check()) {
            return "client";
        } elseif (Auth::guard('web')->check()) {
            return "web";
        } elseif (Auth::guard('api')->check()) {
            return "api";
        } elseif (Auth::guard('admin-api')->check()) {
            return "admin-api";
        } elseif (Auth::guard('user-api')->check()) {
            return "user-api";
        } elseif (Auth::guard('client-api')->check()) {
            return "client-api";
        }
    }
}
