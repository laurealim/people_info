<?php

namespace App\Http\Controllers;

use App\Model\Department;
use App\Model\District;
use App\Model\Division;
use App\Model\Package;
use App\Model\Registration;
use App\Model\ReliefFamilySearch;
use App\Model\Ssnp;
use App\Model\Training;
use App\Model\Union;
use App\Model\Upazila;
use App\Model\Relief;
use App\Model\ReliefDetails;
use App\User;
use Auth;
use Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use \Milon\Barcode\DNS1D;
use \Milon\Barcode\DNS2D;
use mysql_xdevapi\Exception;
use SimpleSoftwareIO\QrCode\Facades\QrCode;


//use Maatwebsite\Excel\Excel;

class RegistrationController extends Controller
{
    public function adminList()
    {
        if (isset($_GET['displayValue'])) {
            $displayValue = $_GET['displayValue'] != '' ? $_GET['displayValue'] : "";
        } else {
            $displayValue = '';
        }

        if (isset($_GET['searchData'])) {
            $searchData = $_GET['searchData'] != '' ? $_GET['searchData'] : "";
        } else {
            $searchData = '';
        }

        $trainingModel = new Training();
        $departmentModel = new Department();
        $reliefModel = new Relief();
        $packageModel = new Package();

        $packages = $packageModel
            ->where('status', '!=', 0)
            ->pluck('title', 'id')
            ->all();
        //            ->where('user_type', '=', config('constants.userType.Tenant'))
//        dd($packages);

        $reliefList = $reliefModel->select("ssnps.ssnp_name AS ssnp_name", "districts.bn_name AS dis_name", "upazilas.bn_name AS upa_name", "unions.bn_name AS uni_name", "users.first_name", 'reliefs.*')
            ->where('is_deleted', '!=', config('constants.is_deleted.Deleted'))
            ->where('reliefs.name', 'like', '%' . $searchData . '%')
            ->orwhere('reliefs.age', 'like', '%' . $searchData . '%')
            ->orwhere('reliefs.phone', 'like', '%' . $searchData . '%')
            ->orwhere('reliefs.nid', 'like', '%' . $searchData . '%')
            ->orwhere('reliefs.f_nid', 'like', '%' . $searchData . '%')
            ->orwhere('reliefs.total_income', 'like', '%' . $searchData . '%')
            ->orwhere('reliefs.total_spend', 'like', '%' . $searchData . '%')
            ->orwhere('reliefs.card_no', 'like', '%' . $searchData . '%')
            ->leftJoin('users', 'reliefs.created_by', 'users.id')
            ->leftJoin('unions', 'unions.id', '=', 'reliefs.uni_pre')
            ->leftJoin('upazilas', 'upazilas.id', '=', 'reliefs.upo_pre')
            ->leftJoin('districts', 'districts.id', '=', 'reliefs.dist_pre')
            ->leftJoin('ssnps', 'ssnps.id', '=', 'reliefs.ssnp')
            ->orderBy('reliefs.id', 'DESC')
            //            ->toSql();
            ->paginate($displayValue);
        //        dd($reliefList);

        $divisionModel = new Division();
        $districtModel = new District();
        $upazilaModel = new Upazila();
        $unionModel = new Union();
        $divisionData = $divisionModel->pluck('bn_name', 'id')->all();
        $districtList = $districtModel->pluck('bn_name', 'id')->all();
        $upazilaList = $upazilaModel->pluck('bn_name', 'id')->all();
        $unionList = $unionModel->pluck('bn_name', 'id')->all();

        $addressArr = [$divisionData, $districtList, $upazilaList, $unionList];

        $uri = "";
        if (request()->ajax()) {
            $uri = request()->getUri();
            return view('registration.ajax_list', compact('reliefList', 'uri', 'addressArr', 'packages'));
        } else {
            return view('registration.adminList', compact('reliefList', 'uri', 'addressArr', 'packages'));
        }
    }

    public function adminListId($id)
    {
        if (isset($_GET['displayValue'])) {
            $displayValue = $_GET['displayValue'] != '' ? $_GET['displayValue'] : "";
        } else {
            $displayValue = '';
        }

        if (isset($_GET['searchData'])) {
            $searchData = $_GET['searchData'] != '' ? $_GET['searchData'] : "";
        } else {
            $searchData = '';
        }

        $trainingModel = new Training();
        $departmentModel = new Department();
        $registrationModel = new Registration();
        $trainingList = $trainingModel->where('status', '=', config('constants.training_status.Pending'))
            ->where('is_deleted', '=', config('constants.is_deleted.Active'))
            ->pluck('name', 'id')->all();
        //        dd($trainingList);

        $registrationList = $registrationModel->select("users.first_name", 'registrations.*')->where('is_deleted', '!=', config('constants.is_deleted.Deleted'))
            ->where('registrations.training_id', '=', $id)
            ->where('registrations.name_en', 'like', '%' . $searchData . '%')
            ->leftJoin('users', 'registrations.created_by', 'users.id')
            ->select('registrations.*', 'users.first_name')
            //            ->toSql();
            ->paginate($displayValue);

        //        dd($registrationList);

        $uri = "";
        if (request()->ajax()) {
            $uri = request()->getUri();
            return view('registration.ajax_list', compact('registrationList', 'trainingList', 'uri'));
        } else {
            return view('registration.adminList', compact('registrationList', 'trainingList', 'uri'));
        }
    }

    public function adminForm()
    {
        $trainingModel = new Training();
        $ssnpModel = new Ssnp();
        $trainingList = $trainingModel->where('status', '=', config('constants.training_status.Pending'))
            ->where('is_deleted', '=', config('constants.is_deleted.Active'))
            ->pluck('name', 'id')->all();


        $eduLevel_list = config('constants.edu_level.arr');
        $eduBoard_list = config('constants.edu_board.arr');
        $relrg_list = config('constants.religious.arr');
        $gender_list = config('constants.gender.arr');
        $marital_list = config('constants.marital_status.arr');
        $possi_help_list = config('constants.possible_help');
        $land_info = config('constants.land_info');

        $divisionModel = new Division();
        $districtModel = new District();
        $upazilaModel = new Upazila();
        $divisionData = $divisionModel->pluck('bn_name', 'id')->all();
        $districtList = $districtModel->pluck('bn_name', 'id')->all();
        $upazilaList = $upazilaModel->pluck('bn_name', 'id')->all();
        $ssnpList = $ssnpModel->pluck('ssnp_name', 'id')->all();

        return view('registration.adminForm', compact('trainingList', 'eduLevel_list', 'eduBoard_list', 'relrg_list', 'gender_list', 'marital_list', 'divisionData', 'districtList', 'upazilaList', 'ssnpList', 'possi_help_list', 'land_info'));
    }

    public function adminStore(Request $request)
    {
        $registrationModel = new Registration();
        $reliefModel = new Relief();
        $reliefDetailsModel = new ReliefDetails();
        $reliefFamilySearchModel = new ReliefFamilySearch();

        $formData = $request->all();
        //        $cardId = $this->__cardIdGen($formData, 4321);
//        dd($cardId);

        $request->request->add(['nidVal' => $this->bn2en($formData['nid'])]);
        $request->request->add(['phoneVal' => $this->bn2en($formData['phone'])]);
        $nidDup = json_decode($this->adminNidDuplicateCheck($request));
        $phonDup = json_decode($this->adminPhoneDuplicateCheck($request));
        try {

            if (($nidDup->status == 404) || ($phonDup->status == 404)) {
                if ($nidDup->status == 404) {
                    $message = $nidDup->data->card_no . ' Card Holder Has the Same NID.';
                    throw new \Exception(
                        'Duplicate NID. ' . $message
                    );
                }
                if ($phonDup->status == 404) {
                    $message = $phonDup->data->card_no . ' Card Holder Has the Same Phone No.';
                    throw new \Exception(
                        'Duplicate Phone. ' . $message
                    );
                }
            }
        } catch (\Exception $e) {
            return redirect('admin/registration/add')->with('error', $e->getMessage());
        }
        unset($request['nidVal']);
        unset($request['phoneVal']);

        $request->request->add(['nid' => $this->bn2en($formData['nid'])]);
        $request->request->add(['phone' => $this->bn2en($formData['phone'])]);

        if (isset($formData['same_add'])) {
            $request->request->add(['vill_per' => $formData['house_pre']]);
            $request->request->add(['post_per' => $formData['road_pre']]);
            $request->request->add(['word_per' => $formData['word_pre']]);
            $request->request->add(['holding_per' => $formData['holding_pre']]);
            $request->request->add(['dist_per' => $formData['dist_pre']]);
            $request->request->add(['upo_per' => $formData['upo_pre']]);
            $request->request->add(['uni_per' => $formData['uni_pre']]);

            unset($request['same_add']);
        }
        //        dd($request);

        $id_deleted = 0;

        if (empty($formData['phone'])) {
            $id_deleted = 1;
        }
        if (empty($formData['nid'])) {
            $id_deleted = 1;
        }
        if (empty($formData['profession'])) {
            $id_deleted = 1;
        }

        $request->request->add(['is_deleted' => $id_deleted]);

        $memberCount = 1;
        foreach ($formData['tblData']['age'] as $sr => $numb) {
            if (!empty($numb))
                $memberCount++;
        }
        $request->request->add(['total_member' => $memberCount]);
        $request->request->add(['tot_f_member' => $memberCount]);

        //        $formData['dist_per'] = $formData['dist_pre'];
//        $formData['up_per'] = $formData['up_pre'];
//        $formData['post_per'] = $formData['post_pre'];
//        $formData['vill_per'] = $formData['vill_pre'];
//        dd($formData);
        $request->request->add(['card_no' => time()]);

        //        $data = $this->validate($request, [
//            'training_id' => 'required',
//            'name_bn' => 'required',
//            'name_en' => 'required',
//            'f_name_bn' => 'required',
//            'm_name_bn' => 'required',
//            'vill_pre' => 'required',
//            'post_pre' => 'required',
//            'up_pre' => 'required',
//            'dist_pre' => 'required',
//            'vill_per' => 'required',
//            'post_per' => 'required',
//            'up_per' => 'required',
//            'dist_per' => 'required',
//            'edu_lvl' => 'required',
//            'phone' => 'required',
//            'nid' => 'required',
//            'dob' => 'required',
//            'relg' => 'required',
//            'nation' => 'required',
//            'gender' => 'required',
//            'married' => 'required',
//            'image' => 'required|file|image|mimes:jpeg,jpg,png,gif,webp|max:2048'
//        ],
//            [
//                'training_id.required' => 'Please Select a Training',
//                'name_bn.required' => 'Bengoli Name is required',
//                'name_en.required' => 'English Name is required',
//                'f_name_bn.required' => 'Bengoli Name is required',
//                'm_name_bn.required' => 'Bengoli Name is required',
//                'vill_pre.required' => 'Present Village is required',
//                'post_pre.required' => 'Present Post Office is required',
//                'up_pre.required' => 'Present Upozilla is required',
//                'dist_pre.required' => 'Present District is required',
//                'vill_per.required' => 'Permanent Village is required',
//                'post_per.required' => 'Permanent Post Office is required',
//                'up_per.required' => 'Permanent Upozilla is required',
//                'dist_per.required' => 'Permanent District is required',
//                'edu_lvl.required' => 'Education Level is required',
//                'phone.required' => 'Phone is required',
//                'nid.required' => 'Valid ID is required',
//                'dob.required' => 'Date of Birth is required',
//                'relg.required' => 'Please Select a Religious',
//                'nation.required' => 'Please Select Your Nationality',
//                'gender.required' => 'Please Select your gender',
//                'married.required' => 'Please Select your Marital Status',
//                'image' => 'required|file|image|mimes:jpeg,jpg,png,gif,webp|max:6144'
//            ]);

        $detailsData = $formData['tblData'];

        unset($formData['tblData']);
        DB::beginTransaction();
        try {
            try {
                $lastInsertedId = $reliefModel->saveData($request);
            } catch (\Exception $exc) {
                dd($exc);
            }

            $request->request->add(['lastInsertedId' => $lastInsertedId]);
            $res = $reliefDetailsModel->saveData($request);

            try {
                $cardId = $this->__cardIdGen($formData, $lastInsertedId);
                Relief::where('id', $lastInsertedId)->update(['card_no' => $cardId]);
            } catch (\Exception $exc) {
                dd($exc);
            }

            try {
                $reliefFamilySearchModel;
                $male = $female = $childern = $transgen = 0;
                $f_nid = $f_ssnp = '';
                $familySearchArr = [];
                foreach ($detailsData as $k_name => $k_values) {
                    if ($k_name === 'age') {
                        foreach ($k_values as $ageData) {
                            if (!empty($ageData) && ($ageData < 6)) {
                                $childern++;
                            }
                        }
                        $familySearchArr['tot_child'] = $childern;
                    }
                    if ($k_name === 'f_gender') {
                        foreach ($k_values as $genData) {
                            if (!empty($genData) && ($genData == 1)) {
                                $male++;
                            }
                            if (!empty($genData) && ($genData == 2)) {
                                $female++;
                            }
                            if (!empty($genData) && ($genData == 3)) {
                                $transgen++;
                            }
                        }
                        $familySearchArr['tot_male'] = $male;
                        $familySearchArr['tot_female'] = $female;
                        $familySearchArr['tot_transgen'] = $transgen;
                    }
                    if ($k_name === 'nid') {
                        foreach ($k_values as $nidData) {
                            if (!empty($nidData)) {
                                $f_nid .= $nidData . ',';
                            }
                        }
                        $familySearchArr['f_nid'] = $f_nid;
                    }
                    if ($k_name === 'ssnp') {
                        foreach ($k_values as $ssnpData) {
                            if (!empty($ssnpData)) {
                                $f_ssnp .= $ssnpData . ',';
                            }
                        }
                        $familySearchArr['f_ssnp'] = $f_ssnp;
                    }
                }
                Relief::where('id', $lastInsertedId)->update($familySearchArr);
                $request->request->add(['familySearchData' => $familySearchArr]);
                $res = $reliefFamilySearchModel->saveData($request);

            } catch (\Exception $serchDataSaveException) {
                dd($serchDataSaveException);
            }

            /*  File Manipulation   */
            $filename = '';
            try {
                if ($request->hasFile('image')) {
                    $client_id = $lastInsertedId;
                    $path = "family";

                    // cache the file
                    $file = $request->file('image');

                    // generate a new filename. getClientOriginalExtension() for the file extension
                    $filename = $client_id . '.' . $file->getClientOriginalExtension();
                    $filePath = $path . '/' . $client_id;

                    if (Storage::disk('public')->exists($filePath . '/' . $filename)) {
                        //                Storage::deleteDirectory($filePath);
                        Storage::delete($filePath . '/' . $filename);
                    }

                    // save to storage_1/app/public/clients as the new $filename
                    $path = $file->storeAs($filePath, $filename);

                    Relief::where('id', $lastInsertedId)->update(['image' => $filename]);
                }
            } catch (\Exception $exception) {
                var_dump($exception);
            }

            DB::commit();
            return redirect('admin/registration/list')->with('success', 'New Registration added successfully');

        } catch (\Exception $mainException) {
            DB::rollback();
            $request->session()->flash('errors', $mainException);
        }
    }

    public function show($id)
    {
        //
    }

    public function adminEdit($id, Request $request)
    {
        $registrationModel = new Registration();
        $trainingModel = new Training();
        $districtModel = new District();
        $upazilaModel = new Upazila();
        $unionModel = new Union();
        $reliefModel = new Relief();
        $ssnpModel = new Ssnp();
        $reliefDetailsModel = new ReliefDetails();
        $tblData = array();

        try {
            if (auth()->user()->id == 532 || auth()->user()->id == 534) {
                throw new \Exception('Permission Deny');
            }

            $eduLevel_list = config('constants.edu_level.arr');
            $eduBoard_list = config('constants.edu_board.arr');
            $relrg_list = config('constants.religious.arr');
            $gender_list = config('constants.gender.arr');
            $marital_list = config('constants.marital_status.arr');
            $possi_help_list = config('constants.possible_help');
            $land_info = config('constants.land_info');

            $reliefData = $reliefModel->where('reliefs.id', $id)
                ->leftJoin('users', 'reliefs.created_by', 'users.id')
                ->leftJoin('upazilas', 'upazilas.id', '=', 'reliefs.upo_pre')
                ->leftJoin('districts', 'districts.id', '=', 'reliefs.dist_pre')
                ->select('reliefs.*', 'users.first_name')
                //                ->tosql();
                ->firstOrFail();

            $detailsData = $reliefDetailsModel->where('relief_details.rlf_id', $id)
                ->firstOrFail();
            $tblData['name'] = json_decode($detailsData->name, true);
            $tblData['dob'] = json_decode($detailsData->dob, true);
            $tblData['age'] = json_decode($detailsData->age, true);
            $tblData['nid'] = json_decode($detailsData->nid, true);
            $tblData['profession'] = json_decode($detailsData->profession, true);
            $tblData['relation'] = json_decode($detailsData->relation, true);
            $tblData['ssnp'] = json_decode($detailsData->ssnp, true);
            $tblData['f_gender'] = json_decode($detailsData->f_gender, true);

            //            $districtListPre = $districtModel->where('id', $reliefData->dist_pre)->pluck('bn_name', 'id')->all();
//            $districtListPer = $districtModel->where('id', $reliefData->dist_per)->pluck('bn_name', 'id')->all();
            $districtListPre = $districtModel->pluck('bn_name', 'id')->all();
            $districtListPer = $districtModel->pluck('bn_name', 'id')->all();
            $upazilaListPre = $upazilaModel->where('district_id', $reliefData->dist_pre)->pluck('bn_name', 'id')->all();
            $upazilaListPer = $upazilaModel->where('district_id', $reliefData->dist_per)->pluck('bn_name', 'id')->all();
            $unionListPre = $unionModel->where('upazilla_id', $reliefData->upo_pre)->pluck('bn_name', 'id')->all();
            $unionListPer = $unionModel->where('upazilla_id', $reliefData->upo_per)->pluck('bn_name', 'id')->all();
            $ssnpList = $ssnpModel->pluck('ssnp_name', 'id')->all();

            //            dd($unionListPer);

            $hideenData = $reliefData['home_type'];

            $listArr = [
                $districtListPre,
                $districtListPer,
                $upazilaListPre,
                $upazilaListPer,
                $unionListPre,
                $unionListPer
            ];
            return view('registration.adminEdit', compact('id', 'gender_list', 'marital_list', 'reliefData', 'listArr', 'tblData', 'hideenData', 'ssnpList', 'possi_help_list', 'land_info'));
            //            return view('training.adminEdit', compact('deptList', 'id', 'trainingData'));
        } catch (\Exception $ex) {
            //            $request->session()->flash('error', 'No Data Found...');
            $request->session()->flash('error', $ex->getMessage());
            return redirect()->back();
            //            return redirect('client/apt/list')->with('errors', 'No Data Found...');
//            return redirect()->back()->withErrors(['msg', 'The Message']);    When has multiple errors and go back to the previous url...
        }
    }

    public function adminUpdate(Request $request, $id)
    {
        $registrationModel = new Registration();
        $reliefModel = new Relief();
        $reliefDetailsModel = new ReliefDetails();
        $ssnpModel = new Ssnp();
        $reliefFamilySearchModel = new ReliefFamilySearch();

        $formData = $request->all();
        $memberCount = 1;
        $memberNid = [];
        foreach ($formData['tblData']['age'] as $sr => $numb) {
            if (!empty($numb)) {
                $memberCount++;
            }
        }
        foreach ($formData['tblData']['nid'] as $sr => $numb) {
            if (!empty($numb)) {
                $memberNid[] = $numb;
            }
        }

        $request->request->add(['total_member' => $memberCount]);
        $request->request->add(['tot_f_member' => $memberCount]);

        $request->request->add(['nid' => $this->bn2en($formData['nid'])]);
        $request->request->add(['phone' => $this->bn2en($formData['phone'])]);

        $request->request->add(['nidVal' => $this->bn2en($formData['nid'])]);
        $request->request->add(['phoneVal' => $this->bn2en($formData['phone'])]);
        $request->request->add(['id' => $id]);

        $request->request->add(['memberNid' => $memberNid]);

        $phonDup = json_decode($this->adminPhoneDuplicateCheck($request));
        $nidDup = json_decode($this->adminNidDuplicateCheck($request));

        try {
            if (($nidDup->status == 404) || ($phonDup->status == 404)) {
                if ($nidDup->status == 404) {
                    $message = $nidDup->data->card_no . ' Card Holder Has the Same NID.';
                    throw new \Exception(
                        'Duplicate NID. ' . $message
                    );
                }
                if ($phonDup->status == 404) {
                    $message = $phonDup->data->card_no . ' Card Holder Has the Same Phone No.';
                    throw new \Exception(
                        'Duplicate Phone. ' . $message
                    );
                }
            }
        } catch (\Exception $e) {
            return redirect('admin/registration/' . $id)->with('error', $e->getMessage());
        }
        unset($request['nidVal']);
        unset($request['phoneVal']);
        unset($request['id']);
        unset($request['memberNid']);

        //        $data = $this->validate($request, [
//            'training_id' => 'required',
//            'name_bn' => 'required',
//            'name_en' => 'required',
//            'f_name_bn' => 'required',
//            'm_name_bn' => 'required',
//            'vill_pre' => 'required',
//            'post_pre' => 'required',
//            'up_pre' => 'required',
//            'dist_pre' => 'required',
//            'vill_per' => 'required',
//            'post_per' => 'required',
//            'up_per' => 'required',
//            'dist_per' => 'required',
//            'edu_lvl' => 'required',
//            'phone' => 'required',
//            'nid' => 'required',
//            'dob' => 'required',
//            'relg' => 'required',
//            'nation' => 'required',
//            'gender' => 'required',
//            'married' => 'required',
////            'image' => 'required|file|image|mimes:jpeg,jpg,png,gif,webp|max:2048'
//        ],
//            [
//                'training_id.required' => 'Please Select a Training',
//                'name_bn.required' => 'Bengoli Name is required',
//                'name_en.required' => 'English Name is required',
//                'f_name_bn.required' => 'Bengoli Name is required',
//                'm_name_bn.required' => 'Bengoli Name is required',
//                'vill_pre.required' => 'Present Village is required',
//                'post_pre.required' => 'Present Post Office is required',
//                'up_pre.required' => 'Present Upozilla is required',
//                'dist_pre.required' => 'Present District is required',
//                'vill_per.required' => 'Permanent Village is required',
//                'post_per.required' => 'Permanent Post Office is required',
//                'up_per.required' => 'Permanent Upozilla is required',
//                'dist_per.required' => 'Permanent District is required',
//                'edu_lvl.required' => 'Education Level is required',
//                'phone.required' => 'Phone is required',
//                'nid.required' => 'Valid ID is required',
//                'dob.required' => 'Date of Birth is required',
//                'relg.required' => 'Please Select a Religious',
//                'nation.required' => 'Please Select Your Nationality',
//                'gender.required' => 'Please Select your gender',
//                'married.required' => 'Please Select your Marital Status',
////                'image' => 'required|file|image|mimes:jpeg,jpg,png,gif,webp|max:6144'
//            ])

        DB::beginTransaction();
        try {
            $detailsData = $formData['tblData'];
            try {
                $reliefModel->updateData($request);
                $cardId = $this->__cardIdGen($formData, $id);
                $request->request->add(['card_no' => time()]);
                Relief::where('id', $id)->update(['card_no' => $cardId]);
            } catch (\Exception $exc) {
                dd($exc);
            }


            try {
                $res = $reliefDetailsModel->updateData($request);
                $reliefFamilySearchModel;
                $male = $female = $childern = $transgen = 0;
                $f_nid = $f_ssnp = '';
                $familySearchArr = [];

                foreach ($detailsData as $k_name => $k_values) {
                    if ($k_name === 'age') {
                        foreach ($k_values as $ageData) {
                            if (!empty($ageData) && ($ageData < 6)) {
                                $childern++;
                            }
                        }
                        $familySearchArr['tot_child'] = $childern;
                    }
                    if ($k_name === 'f_gender') {
                        foreach ($k_values as $genData) {
                            if (!empty($genData) && ($genData == 1)) {
                                $male++;
                            }
                            if (!empty($genData) && ($genData == 2)) {
                                $female++;
                            }
                            if (!empty($genData) && ($genData == 3)) {
                                $transgen++;
                            }
                        }
                        $familySearchArr['tot_male'] = $male;
                        $familySearchArr['tot_female'] = $female;
                        $familySearchArr['tot_transgen'] = $transgen;
                    }
                    if ($k_name === 'nid') {
                        foreach ($k_values as $nidData) {
                            if (!empty($nidData)) {
                                $f_nid .= $nidData . ',';
                            }
                        }
                        $familySearchArr['f_nid'] = $f_nid;
                    }
                    if ($k_name === 'ssnp') {
                        foreach ($k_values as $ssnpData) {
                            if (!empty($ssnpData)) {
                                $f_ssnp .= $ssnpData . ',';
                            }
                        }
                        $familySearchArr['f_ssnp'] = $f_ssnp;
                    }
                }
                Relief::where('id', $id)->update($familySearchArr);
                $request->request->add(['familySearchData' => $familySearchArr]);
                $familySearchData = $reliefFamilySearchModel->where('relf_id', $id)->first();
                if (empty($familySearchData)) {
                    $request->request->add(['lastInsertedId' => $id]);
                    $res = $reliefFamilySearchModel->saveData($request);
                } else {
                    $res = $reliefFamilySearchModel->updateData($request);
                }

            } catch (\Exception $serchDataSaveException) {
                dd($serchDataSaveException);
            }

            /*  File Manipulation   */
            $filename = '';
            //        die("adasdad");
            try {
                if ($request->hasFile('image')) {

                    $path = "family";

                    // cache the file
                    $file = $request->file('image');

                    // generate a new filename. getClientOriginalExtension() for the file extension
                    $filename = $id . '.' . $file->getClientOriginalExtension();
                    $filePath = $path . '/' . $id;

                    if (Storage::disk('public')->exists($filePath . '/' . $filename)) {
                        //                Storage::deleteDirectory($filePath);
                        Storage::delete($filePath . '/' . $filename);
                    }

                    // save to storage_1/app/public/properties as the new $filename
                    $path = $file->storeAs($filePath, $filename);
                    //            $request->request->add(['p_images' => $filename]);

                    Relief::where('id', $id)->update(['image' => $filename]);
                }
            } catch (\Exception $exception) {
                var_dump($exception);
            }

            DB::commit();
            return redirect('admin/registration/list')->with('success', 'Registration Updated successfully');
        } catch (\Exception $mainException) {
            DB::rollback();
            $request->session()->flash('errors', $mainException);
        }
    }

    public function adminDestroy(Request $request, $id)
    {

        if (auth()->user()->id == 532 || auth()->user()->id == 534) {
            throw new \Exception('Permission Deny');
        }
        $registrationModel = new Registration();
        $reliefModel = new Relief();
        $reliefDetailsModel = new ReliefDetails();

        DB::beginTransaction();
        $updateData = [
            'is_deleted' => config('constants.is_deleted.Deleted'),
        ];

        if (isset($request->id)) {
            $reliefModel->where('id', '=', $id)->update($updateData);
            $reliefDetailsModel->where('rlf_id', '=', $id)->update($updateData);
            DB::commit();
            $request->session()->flash('success', 'Data Deleted Successfully..');
            return response()->json(['status' => 'success']);
        } else {
            DB::rollback();
            $request->session()->flash('errors', 'Data Deleted Successfully..');
            return response()->json(['status' => 'error']);
        }
    }

    public function adminRegStatusChange(Request $request, $id, $status)
    {
        $registrationModel = new Registration();

        DB::beginTransaction();
        $updateData = [
            'status' => $status,
        ];

        if (isset($request->id)) {
            $registrationModel->where('id', '=', $id)->update($updateData);
            DB::commit();
            $request->session()->flash('success', 'Trainee Status has changed Successfully..');
            return response()->json(['status' => 'success']);
        } else {
            DB::rollback();
            $request->session()->flash('errors', 'Could not change trainee status Successfully..');
            return response()->json(['status' => 'error']);
        }
    }

    public function adminExportRegister()
    {
        $trainingModel = new Training();
        $departmentModel = new Department();
        $registrationModel = new Registration();
        $reliefModel = new Relief();
        $reliefDetailsModel = new ReliefDetails();
        $trainingList = $trainingModel->where('status', '=', config('constants.training_status.Pending'))
            ->where('is_deleted', '=', config('constants.is_deleted.Active'))
            ->pluck('name', 'id')->all();
        //        dd($trainingList);

        $reliefList = $reliefModel->select("users.first_name", 'reliefs.*', 'relief_details.name as d_name', 'relief_details.age as d_age', 'relief_details.nid as d_nid', 'relief_details.profession as d_profession', 'relief_details.relation as d_relation', 'relief_details.ssnp as d_ssnp', "districts.bn_name AS dis_name", "upazilas.bn_name AS upa_name")->where('reliefs.is_deleted', '!=', config('constants.is_deleted.Deleted'))
            ->leftJoin('users', 'reliefs.created_by', 'users.id')
            ->leftJoin('relief_details', 'relief_details.rlf_id', 'reliefs.id')
            ->leftJoin('upazilas', 'upazilas.id', '=', 'reliefs.upo_pre')
            ->leftJoin('districts', 'districts.id', '=', 'reliefs.dist_pre')
            //            ->select('reliefs.*', 'users.first_name')
//            ->tosql();
            ->get();
        //            ->toArray();

        $this->__exportRegList($reliefList, $trainingList);
    }

    public function adminPrintCard(Request $request)
    {
        $id = $request->id;
        //
//
//        if (Storage::disk('public')->exists($filePath . '/' . $filename)) {
////                Storage::deleteDirectory($filePath);
//            Storage::delete($filePath . '/' . $filename);
//        }

        // save to storage_1/app/public/clients as the new $filename
//        $path = $file->storeAs($filePath, $filename);

        //        User::where('id', $lastInsertedId)->update(['image' => $filename]);

        //        dd($image);

        $registrationModel = new Registration();
        $trainingModel = new Training();
        $districtModel = new District();
        $upazilaModel = new Upazila();
        $unionModel = new Union();
        $reliefModel = new Relief();
        $reliefDetailsModel = new ReliefDetails();
        $tblData = array();

        try {
            $eduLevel_list = config('constants.edu_level.arr');
            $eduBoard_list = config('constants.edu_board.arr');
            $relrg_list = config('constants.religious.arr');
            $gender_list = config('constants.gender.arr');
            $marital_list = config('constants.marital_status.arr');

            $reliefData = $reliefModel->where('reliefs.id', $id)
                ->leftJoin('users', 'reliefs.created_by', 'users.id')
                ->leftJoin('upazilas', 'upazilas.id', '=', 'reliefs.upo_pre')
                ->leftJoin('districts', 'districts.id', '=', 'reliefs.dist_pre')
                ->leftJoin('unions', 'unions.id', '=', 'reliefs.uni_pre')
                ->select('reliefs.*', 'users.first_name', "districts.bn_name AS dis_name", "upazilas.bn_name AS upa_name", "unions.bn_name AS uni_name")
                //                ->tosql();
                ->firstOrFail();
            $str = "name.: " . utf8_encode($reliefData['name']) . ", ";
            if (!empty($reliefData['g_name']))
                $str .= "Father Name: " . utf8_encode($reliefData['g_name']) . ", ";
            else
                $str .= "Husband/Wife Name: " . utf8_encode($reliefData['hw_name']) . ", ";
            $str .= "Card No.: " . $reliefData['card_no'] . ", ";
            $str .= "Phone: " . utf8_encode($reliefData['phone']) . ", ";
            $str .= "Age: " . $reliefData['age'] . ", ";
            $str .= "NID: " . utf8_encode($reliefData['nid']) . ", ";
            if ($reliefData['gender'] == 1)
                $str .= "Gender: Male, ";
            elseif ($reliefData['gender'] == 2)
                $str .= "Gender: Female, ";
            else
                $str .= "Gender: Others, ";


            $path = "images/qr_code";
            $filename = $id . ".png";
            $filePath = $path . '/' . $id;

            $uploadPath = public_path($filePath);

            try {
                if (!is_dir($uploadPath)) {
                    mkdir($uploadPath, 0755, true);
                } else {
                    if (file_exists($uploadPath . $filename)) {
                        unlink($uploadPath . $filename);
                    }
                }
                QrCode::format('png')->size(600)->color(40, 40, 40)->generate($str, $uploadPath . '/' . $filename);
            } catch (\Exception $excp) {
                dd($excp);
            }


            //            $barcode = $datasss = DNS2D::getBarcodeHTML($str, 'QRCODE', 2, 2);

            $detailsData = $reliefDetailsModel->where('relief_details.rlf_id', $id)
                ->firstOrFail();
            $tblData['name'] = json_decode($detailsData->name, true);
            $tblData['age'] = json_decode($detailsData->age, true);
            $tblData['nid'] = json_decode($detailsData->nid, true);
            $tblData['profession'] = json_decode($detailsData->profession, true);
            $tblData['relation'] = json_decode($detailsData->relation, true);
            $tblData['ssnp'] = json_decode($detailsData->ssnp, true);

            $districtListPre = $districtModel->where('id', $reliefData->dist_pre)->pluck('bn_name', 'id')->all();
            $districtListPer = $districtModel->where('id', $reliefData->dist_per)->pluck('bn_name', 'id')->all();
            $upazilaListPre = $upazilaModel->where('district_id', $reliefData->dist_pre)->pluck('bn_name', 'id')->all();
            $upazilaListPer = $upazilaModel->where('district_id', $reliefData->dist_per)->pluck('bn_name', 'id')->all();

            $hideenData = $reliefData['home_type'];

            $listArr = [
                $districtListPre,
                $districtListPer,
                $upazilaListPre,
                $upazilaListPer
            ];

            return view('registration.adminPrintcard', compact('id', 'gender_list', 'marital_list', 'reliefData', 'listArr', 'tblData', 'hideenData'));
            //            return view('registration.adminPrintcard', compact('trainingList', 'eduLevel_list', 'eduBoard_list', 'relrg_list', 'gender_list', 'marital_list', 'divisionData', 'districtList', 'upazilaList'));
//            return view('training.adminEdit', compact('deptList', 'id', 'trainingData'));
        } catch (\Exception $ex) {
            $request->session()->flash('error', 'No Data Found...');
            return redirect()->back();
            //            return redirect('client/apt/list')->with('errors', 'No Data Found...');
//            return redirect()->back()->withErrors(['msg', 'The Message']);    When has multiple errors and go back to the previous url...
        }

        //        return view('registration.adminPrintcard', compact('trainingList', 'eduLevel_list', 'eduBoard_list', 'relrg_list', 'gender_list', 'marital_list', 'divisionData', 'districtList', 'upazilaList'));
    }

    public function userPrintCard(Request $request)
    {
        $id = $request->id;
        //
//
//        if (Storage::disk('public')->exists($filePath . '/' . $filename)) {
////                Storage::deleteDirectory($filePath);
//            Storage::delete($filePath . '/' . $filename);
//        }

        // save to storage_1/app/public/clients as the new $filename
//        $path = $file->storeAs($filePath, $filename);

        //        User::where('id', $lastInsertedId)->update(['image' => $filename]);

        //        dd($image);

        $registrationModel = new Registration();
        $trainingModel = new Training();
        $districtModel = new District();
        $upazilaModel = new Upazila();
        $unionModel = new Union();
        $reliefModel = new Relief();
        $reliefDetailsModel = new ReliefDetails();
        $tblData = array();

        try {
            $eduLevel_list = config('constants.edu_level.arr');
            $eduBoard_list = config('constants.edu_board.arr');
            $relrg_list = config('constants.religious.arr');
            $gender_list = config('constants.gender.arr');
            $marital_list = config('constants.marital_status.arr');

            $reliefData = $reliefModel->where('reliefs.id', $id)
                ->leftJoin('users', 'reliefs.created_by', 'users.id')
                ->leftJoin('upazilas', 'upazilas.id', '=', 'reliefs.upo_pre')
                ->leftJoin('districts', 'districts.id', '=', 'reliefs.dist_pre')
                ->leftJoin('unions', 'unions.id', '=', 'reliefs.uni_pre')
                ->select('reliefs.*', 'users.first_name', "districts.bn_name AS dis_name", "upazilas.bn_name AS upa_name", "unions.bn_name AS uni_name")
                //                ->tosql();
                ->firstOrFail();
            $str = "name.: " . utf8_encode($reliefData['name']) . ", ";
            if (!empty($reliefData['g_name']))
                $str .= "Father Name: " . utf8_encode($reliefData['g_name']) . ", ";
            else
                $str .= "Husband/Wife Name: " . utf8_encode($reliefData['hw_name']) . ", ";
            $str .= "Card No.: " . $reliefData['card_no'] . ", ";
            $str .= "Phone: " . utf8_encode($reliefData['phone']) . ", ";
            $str .= "Age: " . $reliefData['age'] . ", ";
            $str .= "NID: " . utf8_encode($reliefData['nid']) . ", ";
            if ($reliefData['gender'] == 1)
                $str .= "Gender: Male, ";
            elseif ($reliefData['gender'] == 2)
                $str .= "Gender: Female, ";
            else
                $str .= "Gender: Others, ";


            $path = "images/qr_code";
            $filename = $id . ".png";
            $filePath = $path . '/' . $id;

            $uploadPath = public_path($filePath);

            try {
                if (!is_dir($uploadPath)) {
                    mkdir($uploadPath, 0755, true);
                } else {
                    if (file_exists($uploadPath . $filename)) {
                        unlink($uploadPath . $filename);
                    }
                }
                QrCode::format('png')->size(600)->color(40, 40, 40)->generate($str, $uploadPath . '/' . $filename);
            } catch (\Exception $excp) {
                dd($excp);
            }


            //            $barcode = $datasss = DNS2D::getBarcodeHTML($str, 'QRCODE', 2, 2);

            $detailsData = $reliefDetailsModel->where('relief_details.rlf_id', $id)
                ->firstOrFail();
            $tblData['name'] = json_decode($detailsData->name, true);
            $tblData['age'] = json_decode($detailsData->age, true);
            $tblData['nid'] = json_decode($detailsData->nid, true);
            $tblData['profession'] = json_decode($detailsData->profession, true);
            $tblData['relation'] = json_decode($detailsData->relation, true);
            $tblData['ssnp'] = json_decode($detailsData->ssnp, true);

            $districtListPre = $districtModel->where('id', $reliefData->dist_pre)->pluck('bn_name', 'id')->all();
            $districtListPer = $districtModel->where('id', $reliefData->dist_per)->pluck('bn_name', 'id')->all();
            $upazilaListPre = $upazilaModel->where('district_id', $reliefData->dist_pre)->pluck('bn_name', 'id')->all();
            $upazilaListPer = $upazilaModel->where('district_id', $reliefData->dist_per)->pluck('bn_name', 'id')->all();

            $hideenData = $reliefData['home_type'];

            $listArr = [
                $districtListPre,
                $districtListPer,
                $upazilaListPre,
                $upazilaListPer
            ];

            return view('user.registration.userPrintcard', compact('id', 'gender_list', 'marital_list', 'reliefData', 'listArr', 'tblData', 'hideenData'));
            //            return view('registration.adminPrintcard', compact('trainingList', 'eduLevel_list', 'eduBoard_list', 'relrg_list', 'gender_list', 'marital_list', 'divisionData', 'districtList', 'upazilaList'));
//            return view('training.adminEdit', compact('deptList', 'id', 'trainingData'));
        } catch (\Exception $ex) {
            // dd($ex->getMessage());
            $request->session()->flash('error', 'No Data Found...');
            return redirect()->back();
            //            return redirect('client/apt/list')->with('errors', 'No Data Found...');
//            return redirect()->back()->withErrors(['msg', 'The Message']);    When has multiple errors and go back to the previous url...
        }

        //        return view('registration.adminPrintcard', compact('trainingList', 'eduLevel_list', 'eduBoard_list', 'relrg_list', 'gender_list', 'marital_list', 'divisionData', 'districtList', 'upazilaList'));
    }

    public function adminCardGen()
    {
        ini_set('memory_limit', '2048M');
        set_time_limit(120);
        $reliefModel = new Relief();
        $reliefList = $reliefModel->select('reliefs.id', 'reliefs.card_no', 'reliefs.dist_pre', 'reliefs.upo_pre', 'reliefs.uni_pre', 'reliefs.word_pre')->where('reliefs.upo_pre', 389)->get();

        foreach ($reliefList->toArray() as $key => $value) {
            $length = strlen($value['card_no']);
            //            echo $value['id'] .' => '.$value['card_no'].'<br/>';
            $dataForm = array();
            $dataForm['dist_pre'] = $value['dist_pre'];
            $dataForm['upo_pre'] = $value['upo_pre'];
            $dataForm['uni_pre'] = $value['uni_pre'];
            $dataForm['word_pre'] = $value['word_pre'];
            if ($length < 13) {
                $cardNo = $this->__cardIdGen($dataForm, $value['id']);
                //                pr($cardNo);
                try {
                    //                    $request->request->add(['card_no' => time()]);
                    Relief::where('id', $value['id'])->update(['card_no' => $cardNo]);
                } catch (\Exception $exc) {
                    dd($exc);
                }
            }
        }
        die('done');
    }

    public function adminRegConfirm()
    {
    }

    public function adminAssignPackage()
    {
        $apartmentModel = new Apartment();
        $aptId = $_POST['id'];
        $clientId = $_POST['clientId'];
        $message = array();

        if (isset($aptId) && isset($clientId)) {
            DB::beginTransaction();
            try {
                $apartmentModel->where('id', $aptId)->update(['is_assigned' => 1, 'client_id' => $clientId]);
                $message = array('message' => 'Successfully Deleted.', 'status' => 'success');
                DB::commit();
            } catch (\Exception $ex) {
                $message = array('message' => 'No File Found to Delete', 'status' => 'error');
                DB::rollback();
            }
        }
        echo json_encode($message);
        die;
    }

    public function adminManagePackage(Request $request, $id)
    {
        $apartmentModel = new Apartment();
        $aptId = $_POST['id'];
        $clientId = $_POST['clientId'];
        $message = array();

        if (isset($aptId) && isset($clientId)) {
            DB::beginTransaction();
            try {
                $apartmentModel->where('id', $aptId)->update(['is_assigned' => 1, 'client_id' => $clientId]);
                $message = array('message' => 'Successfully Deleted.', 'status' => 'success');
                DB::commit();
            } catch (\Exception $ex) {
                $message = array('message' => 'No File Found to Delete', 'status' => 'error');
                DB::rollback();
            }
        }
        echo json_encode($message);
        die;
    }

    public function adminDuplicateCheck()
    {

        ini_set('memory_limit', '1024M');
        set_time_limit(120);
        $reliefModel = new Relief();
        $queryString = $reliefModel::query();
        $isDisplay = 1;
        $titleName = "Duplicate Check";

        $queryString->select("reliefs.id", "reliefs.card_no", "reliefs.name", 'reliefs.poss_help', "reliefs.nid", "reliefs.phone", DB::raw("count(reliefs.id) as cnt", "is_deleted"))
            ->havingRaw('cnt > 1');
        //dd($isDisplay);
        $groupBy = "";
        if (request()->ajax()) {

            //            dd($_GET['duplicate']);
            if (isset($_GET['duplicate']) && !empty($_GET['duplicate'])) {
                $duplicateData = $_GET['duplicate'];
                if ($duplicateData === 'nid') {
                    $groupBy = 'nid';
                    $queryString->where('reliefs.nid', '!=', '3333333333');
                } elseif ($duplicateData === 'phone') {
                    $groupBy = 'phone';
                    $queryString->where('reliefs.phone', '!=', '77777777777');
                }
            }

            $queryString->groupBy($groupBy);
            $reliefData = $queryString->get();

            $count = $reliefData->count();
            $isDisplay = 2;
            return view('registration.ajax_duplicate_list', compact('reliefData', 'titleName', 'isDisplay', 'count', 'groupBy'));
            //            return view('registration.ajax_duplicate_list', compact('reliefData', 'titleName', 'isDisplay', 'count', 'groupBy'));
        } else {
            //            $reliefData = $queryString->get();
            $reliefData = array();
            return view('registration.adminDuplicateList', compact('reliefData', 'titleName', 'isDisplay', 'groupBy'));
            //            return view('registration.adminDuplicateList', compact('reliefData', 'titleName', 'isDisplay', 'groupBy'));
//            return view('report.adminRegistrationReport', compact('registrationList', 'departmentList', 'trainingList', 'training_id', 'reg_status', 'trade_name', 'registrationStatusArr', 'titleName', 'tradeList'));
        }
    }

    public function adminDuplicateList($id, $type)
    {
        $reliefModel = new Relief();
        $packagesModel = new Package();
        $queryString = $reliefModel::query();

        $queryString->select("districts.bn_name AS dis_name", "upazilas.bn_name AS upa_name", "unions.bn_name AS uni_name", "users.first_name", 'reliefs.*', 'relief_mappings.reg_id', 'relief_mappings.packeges', 'relief_mappings.delv_date', 'relief_mappings.last_give_pac')
            ->leftJoin('users', 'reliefs.created_by', 'users.id')
            ->leftJoin('unions', 'unions.id', '=', 'reliefs.uni_pre')
            ->leftJoin('upazilas', 'upazilas.id', '=', 'reliefs.upo_pre')
            ->leftJoin('districts', 'districts.id', '=', 'reliefs.dist_pre')
            ->leftJoin('relief_mappings', 'relief_mappings.reg_id', '=', 'reliefs.id')
            ->leftJoin('ssnps', 'ssnps.id', '=', 'reliefs.ssnp');

        if ($id === '0') {
            $queryString->where('reliefs.' . $type, '=', $id);
        } elseif ($id === "emp") {
            $queryString->whereNull('reliefs.' . $type);
        } else {
            $queryString->where('reliefs.' . $type, '=', $id);
        }
        $reliefList = $queryString->get();
        //        $reliefList = $queryString->toSql();
//        dd($reliefList);

        $packageList = $packagesModel->pluck('title', 'id')->all();

        return view('registration.adminDuplicateDelete', compact('reliefList', 'packageList'));
    }

    public function adminDestroyDuplicate(Request $request, $id)
    {
        $registrationModel = new Registration();
        $reliefModel = new Relief();
        $reliefDetailsModel = new ReliefDetails();

        DB::beginTransaction();

        if (isset($id)) {
            try {
                $reliedData = Relief::findOrFail($id);
                $reliedData->delete();

                ReliefDetails::where('rlf_id', $id)->delete();
                DB::commit();
                $request->session()->flash('success', 'Data Deleted Successfully..');
                return response()->json(['status' => 'success']);
            } catch (\Exception $excp) {
                DB::rollback();
                $request->session()->flash('errors', 'Data Deleted Successfully..');
                return response()->json(['status' => 'error']);
                dd($excp);
            }
        } else {
            DB::rollback();
            $request->session()->flash('errors', 'Data Deleted Successfully..');
            return response()->json(['status' => 'error']);
        }
    }

    public function adminTotalRegistrationUpazillaPiChart()
    {
        $reliefModel = new Relief();
        $upazilaModel = new Upazila();
        //        $departmentModel = new Department();
//        $registrationModel = new Registration();
        $upazilaList = $upazilaModel->where('district_id', '=', 51)
            ->pluck('bn_name', 'id')->all();

        $reliefList = $reliefModel
            ->select('upazilas.bn_name', 'reliefs.upo_pre', DB::raw('COUNT(reliefs.id) AS total_registration'))
            ->where('reliefs.is_deleted', '!=', config('constants.is_deleted.Deleted'))
            ->whereNotNull('reliefs.upo_pre')
            ->where('reliefs.dist_pre', '=', 51)
            ->leftJoin('upazilas', 'upazilas.id', '=', 'reliefs.upo_pre')
            ->groupBy('reliefs.upo_pre')
            ->get()
            ->toArray();
        //        ->toSql();

        $totalRow = 0;
        $counter = 0;
        $resArray = [];
        $reliefArr = [];
        foreach ($reliefList as $key => $val) {
            $reliefArr[$val['upo_pre']] = $val['total_registration'];
        }

        foreach ($upazilaList as $disId => $disName) {
            $resArray[$counter]['label'] = $disName;
            if (array_key_exists($disId, $reliefArr)) {
                $totalRow += intval($reliefArr[$disId]);
                $resArray[$counter]['total_trainings'] = $reliefArr[$disId];
            } else {
                $resArray[$counter]['total_trainings'] = 0;
            }
            $counter++;
        }

        $percentage = number_format((100 / $totalRow), 4, '.', '');
        foreach ($resArray as $keys => $values) {
            $resArray[$keys]['data'] = number_format(($percentage * $values['total_trainings']), 4, '.', '');
            $color = $this->random_color();
            $resArray[$keys]['color'] = "#" . strtoupper($color);
        }

        echo json_encode($resArray);
        die;
    }

    public function adminGetFamilyStatus()
    {
        $reliefModel = new Relief();
        $upazilaModel = new Upazila();
        $upazilaList = $upazilaModel->where('district_id', '=', 51)
            ->pluck('bn_name', 'id')->all();

        $reliefList = $reliefModel
            ->select('upazilas.bn_name', 'reliefs.upo_pre', 'reliefs.card_type', DB::raw('COUNT(reliefs.card_type) AS card_type_count'))
            ->where('reliefs.is_deleted', '!=', config('constants.is_deleted.Deleted'))
            ->whereNotNull('reliefs.upo_pre')
            ->where('reliefs.dist_pre', '=', 51)
            ->leftJoin('upazilas', 'upazilas.id', '=', 'reliefs.upo_pre')
            ->groupBy('reliefs.upo_pre', 'reliefs.card_type')
            ->groupBy('reliefs.upo_pre')
            ->get()
            ->toArray();
        $finalDataArr = [];
        foreach ($reliefList as $key => $value) {
            if (!isset($finalDataArr[$value['bn_name']])) {
                $finalDataArr[$value['bn_name']] = [0, 0, 0, 0, 0, 0];
            }
            $finalDataArr[$value['bn_name']][0] = $value['bn_name'];

            $finalDataArr[$value['bn_name']][$value['card_type']] = $value['card_type_count'];

        }

        echo json_encode($finalDataArr);
        die;
    }

    public function adminMemberCount()
    {
        ini_set('max_execution_time', 300);
        $reliefModel = new Relief();
        $reliefDetailsModel = new ReliefDetails();

        $memberName = $reliefDetailsModel->get()->toArray();
        $nameArr = [];
        try {
            foreach ($memberName as $key => $val) {
                $memberCount = 1;
                $name = json_decode($val['name']);
                $id = $val['rlf_id'];

                foreach ($name as $sr => $numb) {
                    if (!empty($numb))
                        $memberCount++;
                    echo $memberCount;
                }
                Relief::where('id', $id)->update(['total_member' => $memberCount]);
                echo "Family Member =>" . $memberCount . "</br>";
            }
        } catch (\Exception $exception) {
            dd($exception);
        }
        echo "Done....";
        dd();
    }

    public function adminEngConvert()
    {
        ini_set('memory_limit', '1024M');
        set_time_limit(600);
        $reliefModel = new Relief();
        try {
            $reliefList = $reliefModel->select('reliefs.nid', 'reliefs.phone', 'reliefs.id', 'reliefs.card_no')->get()->toArray();
            //        dd($reliefList);
            $count = 0;
            foreach ($reliefList as $key => $value) {
                $nid = $value['nid'];
                $phone = $value['phone'];
                $id = $value['id'];

                $en_nid = $this->bn2en($nid);
                $en_phone = $this->bn2en($phone);

                //                echo "ID = ".$id.", NID = ",$en_nid.", Phone = ".$en_phone.". <br/>";

                Relief::where('id', $id)->update(['nid' => $en_nid, 'phone' => $en_phone]);
                $count++;
            }

            dd('Finished...');
        } catch (\Exception $exception) {
            dd($exception);
        }
        dd('Done');
    }

    public function adminSetDob()
    {
        ini_set('memory_limit', '1024M');
        set_time_limit(1200);
        $reliefModel = new Relief();
        $upo_code = [385, 386, 387, 388, 389];
        $word_code = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];

        $monthArr = [
            'JAN' => "01",
            'FEB' => "02",
            'MAR' => "03",
            'APR' => "04",
            'MAY' => "05",
            'JUN' => "06",
            'JUL' => "07",
            'AUG' => "08",
            'SEP' => "09",
            'OCT' => "10",
            'NOV' => "11",
            'DEC' => "12"
        ];

        DB::beginTransaction();
        try {
            $offset = 0;
            $limit = 10000;
            //            $queryStr = "SELECT `reliefs`.`nid`, `reliefs`.`upo_per`, `reliefs`.`word_per`, `reliefs`.`uni_per`, `reliefs`.`card_no`, `reliefs`.`dob`, `reliefs`.`id` FROM `reliefs` WHERE `reliefs`.`is_deleted` != 1 AND `reliefs`.`nid` IS NOT NULL AND upo_per = 388";

            //            AND upo_per = 388
            $queryStr = "SELECT BIRTH, NEW_NID, OLD_NID FROM combine_data WHERE NEW_NID IN ( SELECT `nid` FROM `reliefs` WHERE `is_deleted` != 1 AND `nid` IS NOT NULL AND upo_per = 385)";
            //            $queryStr = "SELECT BIRTH, NEW_NID, OLD_NID FROM combine_data WHERE OLD_NID IN ( SELECT `nid` FROM `reliefs` WHERE `is_deleted` != 1 AND `nid` IS NOT NULL AND upo_per = 385)";
//
//            $queryStr = "SELECT BIRTH, NEW_NID, OLD_NID FROM combine_data WHERE NEW_NID IN ( SELECT `nid` FROM `reliefs` WHERE `is_deleted` != 1 AND `nid` IS NOT NULL AND upo_per = 386)";
//            $queryStr = "SELECT BIRTH, NEW_NID, OLD_NID FROM combine_data WHERE OLD_NID IN ( SELECT `nid` FROM `reliefs` WHERE `is_deleted` != 1 AND `nid` IS NOT NULL AND upo_per = 386)";
//
//            $queryStr = "SELECT BIRTH, NEW_NID, OLD_NID FROM combine_data WHERE NEW_NID IN ( SELECT `nid` FROM `reliefs` WHERE `is_deleted` != 1 AND `nid` IS NOT NULL AND upo_per = 387)";
//            $queryStr = "SELECT BIRTH, NEW_NID, OLD_NID FROM combine_data WHERE OLD_NID IN ( SELECT `nid` FROM `reliefs` WHERE `is_deleted` != 1 AND `nid` IS NOT NULL AND upo_per = 387)";
//
//            $queryStr = "SELECT BIRTH, NEW_NID, OLD_NID FROM combine_data_kashiyani WHERE NEW_NID IN ( SELECT `nid` FROM `reliefs` WHERE `is_deleted` != 1 AND `nid` IS NOT NULL AND upo_per = 388)";
//            $queryStr = "SELECT BIRTH, NEW_NID, OLD_NID FROM combine_data_kashiyani WHERE OLD_NID IN ( SELECT `nid` FROM `reliefs` WHERE `is_deleted` != 1 AND `nid` IS NOT NULL AND upo_per = 388)";
//
//            $queryStr = "SELECT BIRTH, NEW_NID, OLD_NID FROM combine_data WHERE NEW_NID IN ( SELECT `nid` FROM `reliefs` WHERE `is_deleted` != 1 AND `nid` IS NOT NULL AND upo_per = 389)";
//            $queryStr = "SELECT BIRTH, NEW_NID, OLD_NID FROM combine_data WHERE OLD_NID IN ( SELECT `nid` FROM `reliefs` WHERE `is_deleted` != 1 AND `nid` IS NOT NULL AND upo_per = 389)";

            $reliefList = DB::select(DB::raw($queryStr));
            //            dd($reliefList);
            $count = 1;
            $strUp = "";
            if (!empty($reliefList)) {
                foreach ($reliefList as $key => $value) {
                    $new_nid = $value->NEW_NID;
                    $old_nid = $value->OLD_NID;
                    $dob = $value->BIRTH;
                    $dobaArr = explode("-", $dob);

                    $date = $dobaArr[0];
                    $month = $dobaArr[1];
                    $year = $dobaArr[2];
                    $dateOfBirth = '19' . $year . '-' . $monthArr[$month] . '-' . $date;
                    //                    echo $new_nid . ' => ' . $old_nid . ' => ' . $dateOfBirth . "<br/>";

                    //                    $strUp .= "Update reliefs set dob = '".$dateOfBirth."' where nid like '%".$new_nid."%' OR nid like '%".$old_nid."%';";
                    $strUp .= "Update reliefs set dob = '" . $dateOfBirth . "' where (SUBSTRING(nid,1) like '" . $new_nid . "') OR (SUBSTRING(nid,1) like '" . $old_nid . "');";

                    //                    DB::table('reliefs')
//                        ->where('nid', 'like', '%' . $new_nid . '%')
//                        ->orwhere('nid', 'like', '%' . $old_nid . '%')
//                        ->update(['dob' => $dateOfBirth]);


                    //                        $queryStr2 = "SELECT `OLD_NID`,`NEW_NID`,`BIRTH` FROM combine_data_kashiyani WHERE `OLD_NID` LIKE '%".$nid."%' OR `NEW_NID` LIKE '%".$nid."%';";
//                        $dataList = DB::select(DB::raw($queryStr2));

                    //                        if(!empty($dataList)){
//                            foreach ($dataList as $keys => $values){
//                                $dob = $values->BIRTH;
//                            }

                    //                        }

                    //                    $dobaArr = explode("-", $birth);
////                                        dd($dobaArr);
//                    $date = $dobaArr[0];
//                    $month = $dobaArr[1];
//                    $year = $dobaArr[2];
//                    $dateOfBirth = '19' . $year . '-' . $monthArr[$month] . '-' . $date;

                    //                    echo $nid . "=> " . $dateOfBirth . '<br/>';
//                    $updateStr = "Update reliefs set reliefs.dob = ".$dateOfBirth." Where id = ".$id.";<br/>";
//                    echo $updateStr;
//                    echo $queryStr2."<br/>";
                    //Relief::where('id', $id)->update(['dob' => $dateOfBirth]);
                    $count++;
                    //                    }
//                            $en_nid = $this->bn2en($nid);
//                            $en_phone = $this->bn2en($phone);
//                    if (!empty($nid)) {
//                        $results = DB::select(DB::raw("SELECT id,FORM_NO,BIRTH,OLD_NID,NEW_NID FROM combine_data WHERE ITEM_ID like '$nid' or OLD_NID like '$nid' or NEW_NID like '$nid'"));
//                        if (!empty($results)) {
//                            foreach ($results as $keys => $values) {
//
//                                $dob = $values->FORM_NO;
//                                $len = strlen($dob);
//                                if ($len != 9) {
//                                    $dob = $values->BIRTH;
//                                }
//
//                                $dobaArr = explode("-", $dob);
//                                $date = $dobaArr[0];
//                                $month = $dobaArr[1];
//                                $year = $dobaArr[2];
//                                $dateOfBirth = '19' . $year . '-' . $monthArr[$month] . '-' . $date;
//                            }
//                            echo $nid . "=> " . $dateOfBirth . '<br/>';
//                            Relief::where('id', $id)->update(['dob' => $dateOfBirth]);
//                            $count++;
//                        }
//                    }
                    //Relief::where('id', $id)->update(['nid'=>$en_nid, 'phone'=>$en_phone]);
//                        }
//                    }
                }
            }
            echo $strUp;
            //            echo $count;
            DB::commit();
            dd();
            //            dd('Finished...');
        } catch (\Exception $exception) {
            DB::rollback();
            dd($exception);
        }
        dd('Done');


    }

    public function adminSetDobs()
    {
        ini_set('memory_limit', '1024M');
        set_time_limit(1200);
        $reliefModel = new Relief();
        $upo_code = [385, 386, 387, 388, 389];
        $word_code = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];

        $monthArr = [
            'JAN' => "01",
            'FEB' => "02",
            'MAR' => "03",
            'APR' => "04",
            'MAY' => "05",
            'JUN' => "06",
            'JUL' => "07",
            'AUG' => "08",
            'SEP' => "09",
            'OCT' => "10",
            'NOV' => "11",
            'DEC' => "12"
        ];

        DB::beginTransaction();
        try {
            $offset = 0;
            $limit = 10000;
            //            `reliefs`.`nid` LIKE `combine_datas`.`NEW_NID` AND
//            $reliefList = $reliefModel->select('reliefs.nid', 'reliefs.dob', 'reliefs.id', 'reliefs.card_no', 'combine_datas.NEW_NID', 'combine_datas.OLD_NID', 'combine_datas.BIRTH')
//                ->where('reliefs.is_deleted', '!=', config('constants.is_deleted.Deleted'))
//                ->where('reliefs.nid', 'like','combine_datas.OLD_NID' )
//                ->whereNotNull('reliefs.nid')
//                ->leftJoin('combine_datas', 'reliefs.nid', 'combine_datas.OLD_NID')
//                ->get()
//                ->toArray();
            $queryStr = "SELECT `reliefs`.`nid`, `reliefs`.`upo_per`, `reliefs`.`word_per`, `reliefs`.`uni_per`, `reliefs`.`dist_per`, `reliefs`.`card_no`, `reliefs`.`dob`,`reliefs`.`dob`, `reliefs`.`id`, `reliefs`.`card_no`, `kashyanis`.`NEW_NID`, `kashyanis` .`OLD_NID`, `kashyanis`.`BIRTH` FROM `reliefs` LEFT JOIN `kashyanis` ON `reliefs`.`nid` = `kashyanis`.`NEW_NID` WHERE `reliefs`.`is_deleted` != 1 AND `reliefs`.`nid` IS NOT NULL AND `reliefs`.`nid` LIKE `kashyanis`.`NEW_NID` AND upo_per = 388";
            $reliefList = DB::select(DB::raw($queryStr));
            //            dd($results);
            $count = 1;
            if (!empty($reliefList)) {
                foreach ($reliefList as $key => $value) {
                    //                    dd($value->nid);
//                    if (empty($value['dob'])) {
//                        if ($count < 1) {
//                            break;
//                        } else {
                    $nid = $value->nid;
                    $dob = $value->dob;
                    $birth = $value->BIRTH;
                    $old_nid = $value->OLD_NID;
                    $new_nid = $value->NEW_NID;
                    $id = $value->id;
                    $dateOfBirth = '';

                    $dobaArr = explode("-", $birth);
                    //                                        dd($dobaArr);
                    $date = $dobaArr[0];
                    $month = $dobaArr[1];
                    $year = $dobaArr[2];
                    $dateOfBirth = '19' . $year . '-' . $monthArr[$month] . '-' . $date;

                    //                    echo $nid . "=> " . $dateOfBirth . '<br/>';
                    $updateStr = "Update reliefs set reliefs.dob = " . $dateOfBirth . " Where id = " . $id . ";<br/>";
                    echo $updateStr;
                    //Relief::where('id', $id)->update(['dob' => $dateOfBirth]);
                    $count++;

                    //                            $en_nid = $this->bn2en($nid);
//                            $en_phone = $this->bn2en($phone);
//                    if (!empty($nid)) {
//                        $results = DB::select(DB::raw("SELECT id,FORM_NO,BIRTH,OLD_NID,NEW_NID FROM combine_data WHERE ITEM_ID like '$nid' or OLD_NID like '$nid' or NEW_NID like '$nid'"));
//                        if (!empty($results)) {
//                            foreach ($results as $keys => $values) {
//
//                                $dob = $values->FORM_NO;
//                                $len = strlen($dob);
//                                if ($len != 9) {
//                                    $dob = $values->BIRTH;
//                                }
//
//                                $dobaArr = explode("-", $dob);
//                                $date = $dobaArr[0];
//                                $month = $dobaArr[1];
//                                $year = $dobaArr[2];
//                                $dateOfBirth = '19' . $year . '-' . $monthArr[$month] . '-' . $date;
//                            }
//                            echo $nid . "=> " . $dateOfBirth . '<br/>';
//                            Relief::where('id', $id)->update(['dob' => $dateOfBirth]);
//                            $count++;
//                        }
//                    }
                    //Relief::where('id', $id)->update(['nid'=>$en_nid, 'phone'=>$en_phone]);
//                        }
//                    }
                }
            }
            echo $count;
            DB::commit();
            dd('Finished...');
        } catch (\Exception $exception) {
            DB::rollback();
            dd($exception);
        }
        dd('Done');


    }

    public function adminNidDuplicateCheck(Request $request)
    {
        $type = 'nid';
        return $this->_duplicateCheck($request, $type);
    }

    public function adminPhoneDuplicateCheck(Request $request)
    {
        $type = 'phone';
        return $this->_duplicateCheck($request, $type);
    }

    public function adminImportExportExcelORCSV()
    {
        return view('registration.file_import_export');
    }

    public function adminManobikSohayotaExcelImport(Request $request)
    {
        if ($request->hasFile('sample_file')) {
            try {
                $arr = [];
                $reliefModel = new Relief();
                $path = $request->file('sample_file')->getRealPath();
                //            dd($path);
                $data = \Excel::load($path)->get();
                //            $dataRow = \Excel::assertImported($path, 'diskName');

                //            pr($data);
                DB::beginTransaction();
                if ($data->count()) {
                    $count = 0;
                    foreach ($data as $key => $value) {
                        //                    pr($value);
                        $arr = ['poss_help' => 1, 'phone' => $value->mob, 'nid' => $value->nid, 'dob' => date('Y-m-d', strtotime($value->dob))];
                        $card_no = $value->card_no;

                        if (!empty($arr)) {
                            $res = Relief::where('card_no', 'like', $card_no)->update($arr); //->toSql();
//                            echo $res;
//                            dd();
                            echo "Updated card = " . $card_no . ', ' . $value->mob . ', ' . $value->nid . ', ' . $value->dob . ' <br/>';
                            //                    \DB::table('products')->insert($arr);
//                    dd('Insert Record successfully.');
                            $count++;
                        }
                        //                    pr($arr);
                    }
                    pr($count);
                    DB::commit();
                    dd('Done....');
                }
            } catch (\Exception $exception) {
                DB::rollback();
                dd($exception);
            }
        }
    }

    public function adminCountPrintHit(Request $request)
    {
        if ($request->ajax()) {
            try {

                $h_id = $request->post('h_id');
                $reliefModel = new Relief();
                $queryString = $reliefModel->select("id", "card_no", 'print_hit', "phone")
                    ->where('id', $h_id)->get()->toArray();

                $prev_hit = intval($queryString[0]['print_hit']);
                $update_data = [
                    'print_hit' => intval($prev_hit + 1),
                    'print_hit_by' => auth()->user()->id
                ];

                Relief::where('id', $h_id)->update($update_data);
                echo json_encode(['status' => 202]);
                die;
            } catch (\Exception $exception) {

                echo json_encode(['status' => 404]);
                die;
            }
            dd();
        }
    }

    public function userCountPrintHit(Request $request)
    {
        if ($request->ajax()) {
            try {

                $h_id = $request->post('h_id');
                $reliefModel = new Relief();
                $queryString = $reliefModel->select("id", "card_no", 'print_hit', "phone")
                    ->where('id', $h_id)->get()->toArray();

                $prev_hit = intval($queryString[0]['print_hit']);
                $update_data = [
                    'print_hit' => intval($prev_hit + 1),
                    'print_hit_by' => auth()->user()->id
                ];

                Relief::where('id', $h_id)->update($update_data);
                echo json_encode(['status' => 202]);
                die;
            } catch (\Exception $exception) {

                echo json_encode(['status' => 404]);
                die;
            }
            dd();
        }
    }

    public function adminGetPopulationStatus()
    {
        $reliefModel = new Relief();
        $upazilaModel = new Upazila();
        $upazilaList = $upazilaModel->where('district_id', '=', 51)
            ->pluck('bn_name', 'id')->all();

        //        dd($upazilaList);
        $reliefList = $reliefModel->select('upazilas.bn_name', 'reliefs.upo_pre', 'reliefs.card_type', DB::raw('sum(reliefs.tot_f_member) AS tot_f_member'))
            ->where('reliefs.is_deleted', '!=', config('constants.is_deleted.Deleted'))
            ->whereNotNull('reliefs.upo_pre')
            ->where('reliefs.dist_pre', '=', 51)
            ->leftJoin('upazilas', 'upazilas.id', '=', 'reliefs.upo_pre')
            ->groupBy('reliefs.card_type', 'reliefs.upo_pre')
            //            ->toSql();
            ->get()
            ->toArray();
        //        dd($reliefList);
        $finalDataArr = [];
        foreach ($reliefList as $key => $value) {
            if (!isset($finalDataArr[$value['bn_name']])) {
                $finalDataArr[$value['bn_name']] = [0, 0, 0, 0, 0, 0];
            }
            $finalDataArr[$value['bn_name']][0] = $value['bn_name'];

            $finalDataArr[$value['bn_name']][$value['card_type']] = $value['tot_f_member'];

        }
        //        pr($finalDataArr);
//        dd();

        echo json_encode($finalDataArr);
        die;
    }

    public function adminGetTribleDataList()
    {
        $reliefModel = new Relief();
        $upazilaModel = new Upazila();
        $upazilaList = $upazilaModel->where('district_id', '=', 51)
            ->pluck('bn_name', 'id')->all();

        $reliefList = $reliefModel->select('upazilas.bn_name', 'reliefs.upo_pre', 'reliefs.card_type', DB::raw('count(reliefs.id) AS tot_trible'))
            ->where('reliefs.is_deleted', '!=', config('constants.is_deleted.Deleted'))
            ->whereNotNull('reliefs.upo_pre')
            ->where('reliefs.trible', '>=', 1)
            ->where('reliefs.dist_pre', '=', 51)
            ->leftJoin('upazilas', 'upazilas.id', '=', 'reliefs.upo_pre')
            ->groupBy('reliefs.card_type', 'reliefs.upo_pre')
            // ->toSql();
            ->get()
            ->toArray();

        $finalDataArr = [];
        foreach ($reliefList as $key => $value) {
            if (!isset($finalDataArr[$value['bn_name']])) {
                $finalDataArr[$value['bn_name']] = [0, 0, 0, 0, 0, 0];
            }
            $finalDataArr[$value['bn_name']][0] = $value['bn_name'];

            $finalDataArr[$value['bn_name']][$value['card_type']] = $value['tot_trible'];

        }

        echo json_encode($finalDataArr);
        die;
    }

    private function random_color()
    {
        return $this->random_color_part() . $this->random_color_part() . $this->random_color_part();
    }

    private function random_color_part()
    {
        return str_pad(dechex(mt_rand(0, 255)), 2, '0', STR_PAD_LEFT);
    }


    /*  Trainee Registration Head portal portion    */
    public function clientList()
    {
        if (isset($_GET['displayValue'])) {
            $displayValue = $_GET['displayValue'] != '' ? $_GET['displayValue'] : "";
        } else {
            $displayValue = '';
        }

        if (isset($_GET['searchData'])) {
            $searchData = $_GET['searchData'] != '' ? $_GET['searchData'] : "";
        } else {
            $searchData = '';
        }

        $trainingModel = new Training();
        $departmentModel = new Department();
        $registrationModel = new Registration();
        $trainingList = $trainingModel->where('status', '=', config('constants.training_status.Pending'))
            ->where('is_deleted', '=', config('constants.is_deleted.Active'))
            ->where('dept_id', '=', Auth::user()->dept_id)
            ->pluck('name', 'id')->all();
        //        dd($trainingList);

        $queryString = $registrationModel::query();
        $queryString->select("users.first_name", 'registrations.*')->where('is_deleted', '!=', config('constants.is_deleted.Deleted'))
            ->where('registrations.name_en', 'like', '%' . $searchData . '%')
            ->leftJoin('users', 'registrations.created_by', 'users.id');

        if (Auth::user()->created_by == config('constants.userType.SuperUser')) {
            $queryString->where('registrations.dept_id', '=', Auth::user()->dept_id);
        } else {
            $queryString->where('registrations.dept_id', '=', Auth::user()->dept_id);
            $queryString->where('registrations.created_by', '=', Auth::user()->id);
        }
        $registrationList = $queryString->paginate($displayValue);
        //            ->select('registrations.*', 'users.first_name')
//            ->tosql();
//        dd($registrationList);

        $divisionModel = new Division();
        $districtModel = new District();
        $upazilaModel = new Upazila();
        $unionModel = new Union();
        $divisionData = $divisionModel->pluck('bn_name', 'id')->all();
        $districtList = $districtModel->pluck('bn_name', 'id')->all();
        $upazilaList = $upazilaModel->pluck('bn_name', 'id')->all();
        $unionList = $unionModel->pluck('bn_name', 'id')->all();

        $addressArr = [$divisionData, $districtList, $upazilaList, $unionList];

        if (request()->ajax()) {
            return view('user.registration.ajax_list', compact('registrationList', 'trainingList', 'addressArr'));
        } else {
            return view('user.registration.clientList', compact('registrationList', 'trainingList', 'addressArr'));
        }
    }

    public function clientListId($id)
    {
        if (isset($_GET['displayValue'])) {
            $displayValue = $_GET['displayValue'] != '' ? $_GET['displayValue'] : "";
        } else {
            $displayValue = '';
        }

        if (isset($_GET['searchData'])) {
            $searchData = $_GET['searchData'] != '' ? $_GET['searchData'] : "";
        } else {
            $searchData = '';
        }

        $trainingModel = new Training();
        $departmentModel = new Department();
        $registrationModel = new Registration();
        $trainingList = $trainingModel->where('status', '=', config('constants.training_status.Pending'))
            ->where('is_deleted', '=', config('constants.is_deleted.Active'))
            ->pluck('name', 'id')->all();
        //        dd($trainingList);

        $queryString = $registrationModel::query();
        $queryString->select("users.first_name", 'registrations.*')->where('is_deleted', '!=', config('constants.is_deleted.Deleted'))
            ->where('registrations.training_id', '=', $id)
            ->where('registrations.name_en', 'like', '%' . $searchData . '%')
            ->leftJoin('users', 'registrations.created_by', 'users.id');


        if (Auth::user()->created_by == config('constants.userType.SuperUser')) {
            $queryString->where('registrations.dept_id', '=', Auth::user()->dept_id);
        } else {
            $queryString->where('registrations.dept_id', '=', Auth::user()->dept_id);
            $queryString->where('registrations.created_by', '=', Auth::user()->id);
        }
        //        $queryString->toSql();
        $registrationList = $queryString->paginate($displayValue);

        //        dd($registrationList->toSql());

        if (request()->ajax()) {
            return view('user.registration.ajax_list', compact('registrationList', 'trainingList'));
        } else {
            return view('user.registration.clientList', compact('registrationList', 'trainingList'));
        }
    }

    public function clientForm()
    {
        $trainingModel = new Training();
        $queryString = $trainingModel::query();
        $queryString->where('status', '=', config('constants.training_status.Pending'))->where('is_deleted', '=', config('constants.is_deleted.Active'));

        if (Auth::user()->created_by == config('constants.userType.SuperUser')) {
            $queryString->where('dept_id', '=', Auth::user()->dept_id);
        } else {
            $queryString->where('created_by', '=', Auth::user()->id);
        }
        $trainingList = $queryString->pluck('name', 'id')->all();

        $eduLevel_list = config('constants.edu_level.arr');
        $eduBoard_list = config('constants.edu_board.arr');
        $relrg_list = config('constants.religious.arr');
        $gender_list = config('constants.gender.arr');
        $marital_list = config('constants.marital_status.arr');

        $divisionModel = new Division();
        $districtModel = new District();
        $upazilaModel = new Upazila();
        $divisionData = $divisionModel->pluck('bn_name', 'id')->all();
        $districtList = $districtModel->pluck('bn_name', 'id')->all();
        $upazilaList = $upazilaModel->pluck('bn_name', 'id')->all();
        return view('user.registration.clientForm', compact('trainingList', 'eduLevel_list', 'eduBoard_list', 'relrg_list', 'gender_list', 'marital_list', 'divisionData', 'districtList', 'upazilaList'));
    }

    public function clientStore(Request $request)
    {
        $registrationModel = new Registration();

        $formData = $request->all();
        if (isset($formData['same_add'])) {
            $request->request->add(['dist_per' => $formData['dist_pre']]);
            $request->request->add(['up_per' => $formData['up_pre']]);
            $request->request->add(['post_per' => $formData['post_pre']]);
            $request->request->add(['vill_per' => $formData['vill_pre']]);

            unset($request['same_add']);
        }

        $data = $this->validate(
            $request,
            [
                'training_id' => 'required',
                'name_bn' => 'required',
                'name_en' => 'required',
                'f_name_bn' => 'required',
                'm_name_bn' => 'required',
                'vill_pre' => 'required',
                'post_pre' => 'required',
                'up_pre' => 'required',
                'dist_pre' => 'required',
                'vill_per' => 'required',
                'post_per' => 'required',
                'up_per' => 'required',
                'dist_per' => 'required',
                'edu_lvl' => 'required',
                'phone' => 'required',
                'nid' => 'required',
                'dob' => 'required',
                'relg' => 'required',
                'nation' => 'required',
                'gender' => 'required',
                'married' => 'required',
                'image' => 'required|file|image|mimes:jpeg,jpg,png,gif,webp|max:2048'
            ],
            [
                'training_id.required' => 'Please Select a Training',
                'name_bn.required' => 'Bengoli Name is required',
                'name_en.required' => 'English Name is required',
                'f_name_bn.required' => 'Bengoli Name is required',
                'm_name_bn.required' => 'Bengoli Name is required',
                'vill_pre.required' => 'Present Village is required',
                'post_pre.required' => 'Present Post Office is required',
                'up_pre.required' => 'Present Upozilla is required',
                'dist_pre.required' => 'Present District is required',
                'vill_per.required' => 'Permanent Village is required',
                'post_per.required' => 'Permanent Post Office is required',
                'up_per.required' => 'Permanent Upozilla is required',
                'dist_per.required' => 'Permanent District is required',
                'edu_lvl.required' => 'Education Level is required',
                'phone.required' => 'Phone is required',
                'nid.required' => 'Valid ID is required',
                'dob.required' => 'Date of Birth is required',
                'relg.required' => 'Please Select a Religious',
                'nation.required' => 'Please Select Your Nationality',
                'gender.required' => 'Please Select your gender',
                'married.required' => 'Please Select your Marital Status',
                'image' => 'required|file|image|mimes:jpeg,jpg,png,gif,webp|max:6144'
            ]
        );

        $lastInsertedId = $registrationModel->saveData($request);

        /*  File Manipulation   */
        $filename = '';
        try {
            if ($request->hasFile('image')) {
                $client_id = $lastInsertedId;
                $path = "trainee";

                // cache the file
                $file = $request->file('image');

                // generate a new filename. getClientOriginalExtension() for the file extension
                $filename = $client_id . '.' . $file->getClientOriginalExtension();
                $filePath = $path . '/' . $client_id;

                if (Storage::disk('public')->exists($filePath . '/' . $filename)) {
                    //                Storage::deleteDirectory($filePath);
                    Storage::delete($filePath . '/' . $filename);
                }

                // save to storage_1/app/public/clients as the new $filename
                $path = $file->storeAs($filePath, $filename);

                Registration::where('id', $lastInsertedId)->update(['image' => $filename]);
            }
        } catch (\Exception $exception) {
            var_dump($exception);
        }


        return redirect('client/registration/list')->with('success', 'New Registration added successfully');
    }

    public function clientshow($id)
    {
        //
    }

    public function clientEdit($id)
    {
        $registrationModel = new Registration();
        $trainingModel = new Training();
        $districtModel = new District();
        $upazilaModel = new Upazila();
        $unionModel = new Union();
        try {

            $queryString = $trainingModel::query();
            $queryString->where('status', '=', config('constants.training_status.Pending'))->where('is_deleted', '=', config('constants.is_deleted.Active'));

            if (Auth::user()->created_by == config('constants.userType.SuperUser')) {
                $queryString->where('dept_id', '=', Auth::user()->dept_id);
            } else {
                $queryString->where('created_by', '=', Auth::user()->id);
            }
            $trainingList = $queryString->pluck('name', 'id')->all();

            $eduLevel_list = config('constants.edu_level.arr');
            $eduBoard_list = config('constants.edu_board.arr');
            $relrg_list = config('constants.religious.arr');
            $gender_list = config('constants.gender.arr');
            $marital_list = config('constants.marital_status.arr');

            $registrationData = $registrationModel->where('registrations.id', $id)
                ->where('is_deleted', '!=', config('constants.is_deleted.Deleted'))
                ->leftJoin('users', 'registrations.created_by', 'users.id')
                ->select('registrations.*', 'users.first_name')
                ->firstOrFail();

            $districtListPre = $districtModel->pluck('bn_name', 'id')->all();
            $districtListPer = $districtModel->pluck('bn_name', 'id')->all();
            $upazilaListPre = $upazilaModel->where('district_id', $registrationData->dist_pre)->pluck('bn_name', 'id')->all();
            $upazilaListPer = $upazilaModel->where('district_id', $registrationData->dist_per)->pluck('bn_name', 'id')->all();
            $unionListPre = $unionModel->where('upazilla_id', $registrationData->up_pre)->pluck('bn_name', 'id')->all();
            $unionListPer = $unionModel->where('upazilla_id', $registrationData->up_per)->pluck('bn_name', 'id')->all();

            $listArr = [
                $districtListPre,
                $districtListPer,
                $upazilaListPre,
                $upazilaListPer,
                $unionListPre,
                $unionListPer
            ];
            return view('user.registration.clientEdit', compact('trainingList', 'id', 'eduLevel_list', 'eduBoard_list', 'relrg_list', 'gender_list', 'marital_list', 'registrationData', 'listArr'));
            //            return view('training.adminEdit', compact('deptList', 'id', 'trainingData'));
        } catch (\Exception $ex) {
            $request->session()->flash('error', 'No Data Found...');
            return redirect()->back();
            //            return redirect('client/apt/list')->with('errors', 'No Data Found...');
//            return redirect()->back()->withErrors(['msg', 'The Message']);    When has multiple errors and go back to the previous url...
        }
    }

    public function clientUpdate(Request $request, $id)
    {
        $registrationModel = new Registration();

        $formData = $request->all();
        if (isset($formData['same_add'])) {
            $request->request->add(['dist_per' => $formData['dist_pre']]);
            $request->request->add(['up_per' => $formData['up_pre']]);
            $request->request->add(['post_per' => $formData['post_pre']]);
            $request->request->add(['vill_per' => $formData['vill_pre']]);

            unset($request['same_add']);
        }

        $data = $this->validate(
            $request,
            [
                'training_id' => 'required',
                'name_bn' => 'required',
                'name_en' => 'required',
                'f_name_bn' => 'required',
                'm_name_bn' => 'required',
                'vill_pre' => 'required',
                'post_pre' => 'required',
                'up_pre' => 'required',
                'dist_pre' => 'required',
                'vill_per' => 'required',
                'post_per' => 'required',
                'up_per' => 'required',
                'dist_per' => 'required',
                'edu_lvl' => 'required',
                'phone' => 'required',
                'nid' => 'required',
                'dob' => 'required',
                'relg' => 'required',
                'nation' => 'required',
                'gender' => 'required',
                'married' => 'required',
                //            'image' => 'required|file|image|mimes:jpeg,jpg,png,gif,webp|max:2048'
            ],
            [
                'training_id.required' => 'Please Select a Training',
                'name_bn.required' => 'Bengoli Name is required',
                'name_en.required' => 'English Name is required',
                'f_name_bn.required' => 'Bengoli Name is required',
                'm_name_bn.required' => 'Bengoli Name is required',
                'vill_pre.required' => 'Present Village is required',
                'post_pre.required' => 'Present Post Office is required',
                'up_pre.required' => 'Present Upozilla is required',
                'dist_pre.required' => 'Present District is required',
                'vill_per.required' => 'Permanent Village is required',
                'post_per.required' => 'Permanent Post Office is required',
                'up_per.required' => 'Permanent Upozilla is required',
                'dist_per.required' => 'Permanent District is required',
                'edu_lvl.required' => 'Education Level is required',
                'phone.required' => 'Phone is required',
                'nid.required' => 'Valid ID is required',
                'dob.required' => 'Date of Birth is required',
                'relg.required' => 'Please Select a Religious',
                'nation.required' => 'Please Select Your Nationality',
                'gender.required' => 'Please Select your gender',
                'married.required' => 'Please Select your Marital Status',
                //                'image' => 'required|file|image|mimes:jpeg,jpg,png,gif,webp|max:6144'
            ]
        );

        $registrationModel->updateData($request);

        /*  File Manipulation   */
        $filename = '';
        try {
            if ($request->hasFile('image')) {
                $path = "trainee";

                // cache the file
                $file = $request->file('image');

                // generate a new filename. getClientOriginalExtension() for the file extension
                $filename = $id . '.' . $file->getClientOriginalExtension();
                $filePath = $path . '/' . $id;

                if (Storage::disk('public')->exists($filePath . '/' . $filename)) {
                    //                Storage::deleteDirectory($filePath);
                    Storage::delete($filePath . '/' . $filename);
                }

                // save to storage_1/app/public/properties as the new $filename
                $path = $file->storeAs($filePath, $filename);
                //            $request->request->add(['p_images' => $filename]);

                Registration::where('id', $id)->update(['image' => $filename]);
            }
        } catch (\Exception $exception) {
            var_dump($exception);
        }


        return redirect('client/registration/list')->with('success', 'New Registration added successfully');
    }

    public function clientDestroy(Request $request, $id)
    {
        $registrationModel = new Registration();

        DB::beginTransaction();
        $updateData = [
            'is_deleted' => config('constants.is_deleted.Deleted'),
        ];

        if (isset($request->id)) {
            $registrationModel->where('id', '=', $id)->update($updateData);
            DB::commit();
            $request->session()->flash('success', 'Data Deleted Successfully..');
            return response()->json(['status' => 'success']);
        } else {
            DB::rollback();
            $request->session()->flash('errors', 'Data Deleted Successfully..');
            return response()->json(['status' => 'error']);
        }
    }

    public function clientRegStatusChange(Request $request, $id, $status)
    {
        $registrationModel = new Registration();

        DB::beginTransaction();
        $updateData = [
            'status' => $status,
        ];

        if (isset($request->id)) {
            $registrationModel->where('id', '=', $id)->update($updateData);
            DB::commit();
            $request->session()->flash('success', 'Trainee Status has changed Successfully..');
            return response()->json(['status' => 'success']);
        } else {
            DB::rollback();
            $request->session()->flash('errors', 'Could not change trainee status Successfully..');
            return response()->json(['status' => 'error']);
        }
    }

    public function clientExportRegister()
    {
        $trainingModel = new Training();
        $departmentModel = new Department();
        $registrationModel = new Registration();
        $trainingList = $trainingModel->where('status', '=', config('constants.training_status.Pending'))
            ->where('is_deleted', '=', config('constants.is_deleted.Active'))
            ->where('dept_id', '=', Auth::user()->dept_id)
            ->pluck('name', 'id')->all();
        //        dd($trainingList);

        $registrationList = $registrationModel->select("users.first_name", 'registrations.*')
            ->where('is_deleted', '!=', config('constants.is_deleted.Deleted'))
            ->where('registrations.dept_id', '=', Auth::user()->dept_id)
            ->leftJoin('users', 'registrations.created_by', 'users.id')
            ->select('registrations.*', 'users.first_name')
            //            ->tosql();
            ->get();
        //            ->toArray();

        $this->__exportRegList($registrationList, $trainingList);
    }


    /*  Trainee Registration User portal portion    */
    public function userList()
    {
        if (isset($_GET['displayValue'])) {
            $displayValue = $_GET['displayValue'] != '' ? $_GET['displayValue'] : "";
        } else {
            $displayValue = '';
        }

        if (isset($_GET['searchData'])) {
            $searchData = $_GET['searchData'] != '' ? $_GET['searchData'] : "";
        } else {
            $searchData = '';
        }

        $trainingModel = new Training();
        $departmentModel = new Department();
        $reliefModel = new Relief();

        $reliefList = $reliefModel->select("districts.bn_name AS dis_name", "upazilas.bn_name AS upa_name", "unions.bn_name AS uni_name", "users.first_name", 'reliefs.*')->where('is_deleted', '!=', config('constants.is_deleted.Deleted'))
            ->where('reliefs.created_by', '=', Auth::user()->id)
            ->where('reliefs.name', 'like', '%' . $searchData . '%')
            ->orwhere('reliefs.age', 'like', '%' . $searchData . '%')
            ->orwhere('reliefs.phone', 'like', '%' . $searchData . '%')
            ->orwhere('reliefs.nid', 'like', '%' . $searchData . '%')
            ->orwhere('reliefs.f_nid', 'like', '%' . $searchData . '%')
            ->orwhere('reliefs.total_income', 'like', '%' . $searchData . '%')
            ->orwhere('reliefs.total_spend', 'like', '%' . $searchData . '%')
            ->orwhere('reliefs.card_no', 'like', '%' . $searchData . '%')
            ->leftJoin('users', 'reliefs.created_by', 'users.id')
            ->leftJoin('unions', 'unions.id', '=', 'reliefs.uni_pre')
            ->leftJoin('upazilas', 'upazilas.id', '=', 'reliefs.upo_pre')
            ->leftJoin('districts', 'districts.id', '=', 'reliefs.dist_pre')
            //            ->get();
//            ->tosql();
            ->paginate($displayValue);
        //        dd(Auth::user()->id);
//        dd($reliefList);

        $divisionModel = new Division();
        $districtModel = new District();
        $upazilaModel = new Upazila();
        $unionModel = new Union();
        $divisionData = $divisionModel->pluck('bn_name', 'id')->all();
        $districtList = $districtModel->pluck('bn_name', 'id')->all();
        $upazilaList = $upazilaModel->pluck('bn_name', 'id')->all();
        $unionList = $unionModel->pluck('bn_name', 'id')->all();

        $addressArr = [$divisionData, $districtList, $upazilaList, $unionList];

        $uri = "";
        //        if (request()->ajax()) {
//            $uri = request()->getUri();
//            return view('registration.ajax_list', compact('reliefList',  'uri', 'addressArr'));
//        } else {
//            return view('registration.adminList', compact('reliefList', 'uri', 'addressArr'));
//        }

        if (request()->ajax()) {
            return view('user.registration.user_ajax_list', compact('reliefList', 'addressArr', 'uri'));
        } else {
            return view('user.registration.userList', compact('reliefList', 'addressArr', 'uri'));
        }
    }

    public function userListId($id)
    {
        if (isset($_GET['displayValue'])) {
            $displayValue = $_GET['displayValue'] != '' ? $_GET['displayValue'] : "";
        } else {
            $displayValue = '';
        }

        if (isset($_GET['searchData'])) {
            $searchData = $_GET['searchData'] != '' ? $_GET['searchData'] : "";
        } else {
            $searchData = '';
        }

        $trainingModel = new Training();
        $departmentModel = new Department();
        $registrationModel = new Registration();
        $trainingList = $trainingModel->where('status', '=', config('constants.training_status.Pending'))
            ->where('is_deleted', '=', config('constants.is_deleted.Active'))
            ->pluck('name', 'id')->all();
        //        dd($trainingList);

        $queryString = $registrationModel::query();
        $queryString->select("users.first_name", 'registrations.*')->where('is_deleted', '!=', config('constants.is_deleted.Deleted'))
            ->where('registrations.training_id', '=', $id)
            ->where('registrations.name_en', 'like', '%' . $searchData . '%')
            ->leftJoin('users', 'registrations.created_by', 'users.id');


        if (Auth::user()->created_by == config('constants.userType.SuperUser')) {
            $queryString->where('registrations.dept_id', '=', Auth::user()->dept_id);
        } else {
            $queryString->where('registrations.dept_id', '=', Auth::user()->dept_id);
            $queryString->where('registrations.created_by', '=', Auth::user()->id);
        }
        //        $queryString->toSql();
        $registrationList = $queryString->paginate($displayValue);

        //        dd($registrationList->toSql());

        if (request()->ajax()) {
            return view('user.registration.user_ajax_list', compact('registrationList', 'trainingList'));
        } else {
            return view('user.registration.userList', compact('registrationList', 'trainingList'));
        }
    }

    public function userForm()
    {
        $trainingModel = new Training();
        $ssnpModel = new Ssnp();
        $queryString = $trainingModel::query();
        $queryString->where('status', '=', config('constants.training_status.Pending'))->where('is_deleted', '=', config('constants.is_deleted.Active'));

        $queryString->where('dept_id', '=', Auth::user()->dept_id);
        //        if(Auth::user()->created_by == config('constants.userType.SuperUser')){
//        }else{
//            $queryString->where('created_by', '=', Auth::user()->id);
//        }
        $trainingList = $queryString->pluck('name', 'id')->all();

        $eduLevel_list = config('constants.edu_level.arr');
        $eduBoard_list = config('constants.edu_board.arr');
        $relrg_list = config('constants.religious.arr');
        $gender_list = config('constants.gender.arr');
        $marital_list = config('constants.marital_status.arr');
        $possi_help_list = config('constants.possible_help');
        $land_info = config('constants.land_info');

        $divisionModel = new Division();
        $districtModel = new District();
        $upazilaModel = new Upazila();
        $divisionData = $divisionModel->pluck('bn_name', 'id')->all();
        $districtList = $districtModel->pluck('bn_name', 'id')->all();
        $upazilaList = $upazilaModel->pluck('bn_name', 'id')->all();
        $ssnpList = $ssnpModel->pluck('ssnp_name', 'id')->all();

        return view('user.registration.userForm', compact('trainingList', 'eduLevel_list', 'eduBoard_list', 'relrg_list', 'gender_list', 'marital_list', 'divisionData', 'districtList', 'upazilaList', 'ssnpList', 'possi_help_list', 'land_info'));
    }

    public function userStore(Request $request)
    {
        $registrationModel = new Registration();
        $reliefModel = new Relief();
        $reliefDetailsModel = new ReliefDetails();
        $reliefFamilySearchModel = new ReliefFamilySearch();


        //        dd("sadad");
        $memberNid = [];
        $formData = $request->all();
        $request->request->add(['nid' => $this->bn2en($formData['nid'])]);
        $request->request->add(['phone' => $this->bn2en($formData['phone'])]);

        $request->request->add(['nidVal' => $this->bn2en($formData['nid'])]);
        $request->request->add(['phoneVal' => $this->bn2en($formData['phone'])]);

        //        foreach ($formData['tblData']['nid'] as $sr => $numb) {
//            if (!empty($numb)) {
//                $memberNid[] = $numb;
//            }
//        }
//
//        $request->request->add(['memberNid' => $memberNid]);

        $nidDup = json_decode($this->adminNidDuplicateCheck($request));
        $phonDup = json_decode($this->adminPhoneDuplicateCheck($request));
        try {

            if (($nidDup->status == 404) || ($phonDup->status == 404)) {
                if ($nidDup->status == 404) {
                    $message = $nidDup->data->card_no . ' Card Holder Has the Same NID.';
                    throw new \Exception(
                        'Duplicate NID. ' . $message
                    );
                }
                if ($phonDup->status == 404) {
                    $message = $phonDup->data->card_no . ' Card Holder Has the Same Phone No.';
                    throw new \Exception(
                        'Duplicate Phone. ' . $message
                    );
                }
            }
        } catch (\Exception $e) {
            return redirect('user/registration/add')->with('error', $e->getMessage());
        }
        unset($request['nidVal']);
        unset($request['phoneVal']);

        //        dd($formData);
        if (isset($formData['same_add'])) {
            $request->request->add(['vill_per' => $formData['house_pre']]);
            $request->request->add(['post_per' => $formData['road_pre']]);
            $request->request->add(['word_per' => $formData['word_pre']]);
            $request->request->add(['holding_per' => $formData['holding_pre']]);
            $request->request->add(['dist_per' => $formData['dist_pre']]);
            $request->request->add(['upo_per' => $formData['upo_pre']]);
            $request->request->add(['uni_per' => $formData['uni_pre']]);

            unset($request['same_add']);
        }
        $memberCount = 1;
        foreach ($formData['tblData']['name'] as $sr => $numb) {
            if (!empty($numb))
                $memberCount++;
        }

        //        $data = $this->validate($request, [
//            'training_id' => 'required',
//            'name_bn' => 'required',
//            'name_en' => 'required',
//            'f_name_bn' => 'required',
//            'm_name_bn' => 'required',
//            'vill_pre' => 'required',
//            'post_pre' => 'required',
//            'up_pre' => 'required',
//            'dist_pre' => 'required',
//            'vill_per' => 'required',
//            'post_per' => 'required',
//            'up_per' => 'required',
//            'dist_per' => 'required',
//            'edu_lvl' => 'required',
//            'phone' => 'required',
//            'nid' => 'required',
//            'dob' => 'required',
//            'relg' => 'required',
//            'nation' => 'required',
//            'gender' => 'required',
//            'married' => 'required',
//            'image' => 'required|file|image|mimes:jpeg,jpg,png,gif,webp|max:2048'
//        ],
//            [
//                'training_id.required' => 'Please Select a Training',
//                'name_bn.required' => 'Bengoli Name is required',
//                'name_en.required' => 'English Name is required',
//                'f_name_bn.required' => 'Bengoli Name is required',
//                'm_name_bn.required' => 'Bengoli Name is required',
//                'vill_pre.required' => 'Present Village is required',
//                'post_pre.required' => 'Present Post Office is required',
//                'up_pre.required' => 'Present Upozilla is required',
//                'dist_pre.required' => 'Present District is required',
//                'vill_per.required' => 'Permanent Village is required',
//                'post_per.required' => 'Permanent Post Office is required',
//                'up_per.required' => 'Permanent Upozilla is required',
//                'dist_per.required' => 'Permanent District is required',
//                'edu_lvl.required' => 'Education Level is required',
//                'phone.required' => 'Phone is required',
//                'nid.required' => 'Valid ID is required',
//                'dob.required' => 'Date of Birth is required',
//                'relg.required' => 'Please Select a Religious',
//                'nation.required' => 'Please Select Your Nationality',
//                'gender.required' => 'Please Select your gender',
//                'married.required' => 'Please Select your Marital Status',
//                'image' => 'required|file|image|mimes:jpeg,jpg,png,gif,webp|max:6144'
//            ]);

        $detailsData = $formData['tblData'];

        unset($formData['tblData']);

        DB::beginTransaction();
        try {
            try {
                $lastInsertedId = $reliefModel->saveData($request);
            } catch (\Exception $exc) {
                dd($exc);
            }

            $request->request->add(['lastInsertedId' => $lastInsertedId]);
            $res = $reliefDetailsModel->saveData($request);

            try {
                $cardId = $this->__cardIdGen($formData, $lastInsertedId);
                Relief::where('id', $lastInsertedId)->update(['card_no' => $cardId]);
            } catch (\Exception $exc) {
                dd($exc);
            }

            try {
                $reliefFamilySearchModel;
                $male = $female = $childern = $transgen = 0;
                $f_nid = $f_ssnp = '';
                $familySearchArr = [];
                foreach ($detailsData as $k_name => $k_values) {
                    if ($k_name === 'age') {
                        foreach ($k_values as $ageData) {
                            if (!empty($ageData) && ($ageData < 6)) {
                                $childern++;
                            }
                        }
                        $familySearchArr['tot_child'] = $childern;
                    }
                    if ($k_name === 'f_gender') {
                        foreach ($k_values as $genData) {
                            if (!empty($genData) && ($genData == 1)) {
                                $male++;
                            }
                            if (!empty($genData) && ($genData == 2)) {
                                $female++;
                            }
                            if (!empty($genData) && ($genData == 3)) {
                                $transgen++;
                            }
                        }
                        $familySearchArr['tot_male'] = $male;
                        $familySearchArr['tot_female'] = $female;
                        $familySearchArr['tot_transgen'] = $transgen;
                    }
                    if ($k_name === 'nid') {
                        foreach ($k_values as $nidData) {
                            if (!empty($nidData)) {
                                $f_nid .= $nidData . ',';
                            }
                        }
                        $familySearchArr['f_nid'] = $f_nid;
                    }
                    if ($k_name === 'ssnp') {
                        foreach ($k_values as $ssnpData) {
                            if (!empty($ssnpData)) {
                                $f_ssnp .= $ssnpData . ',';
                            }
                        }
                        $familySearchArr['f_ssnp'] = $f_ssnp;
                    }
                }
                Relief::where('id', $lastInsertedId)->update($familySearchArr);
                $request->request->add(['familySearchData' => $familySearchArr]);
                $res = $reliefFamilySearchModel->saveData($request);

            } catch (\Exception $serchDataSaveException) {
                dd($serchDataSaveException);
            }

            /*  File Manipulation   */
            $filename = '';
            try {
                if ($request->hasFile('image')) {
                    $client_id = $lastInsertedId;
                    $path = "family";

                    // cache the file
                    $file = $request->file('image');

                    // generate a new filename. getClientOriginalExtension() for the file extension
                    $filename = $client_id . '.' . $file->getClientOriginalExtension();
                    $filePath = $path . '/' . $client_id;

                    if (Storage::disk('public')->exists($filePath . '/' . $filename)) {
                        //                Storage::deleteDirectory($filePath);
                        Storage::delete($filePath . '/' . $filename);
                    }

                    // save to storage_1/app/public/clients as the new $filename
                    $path = $file->storeAs($filePath, $filename);

                    Relief::where('id', $lastInsertedId)->update(['image' => $filename]);
                }
            } catch (\Exception $exception) {
                var_dump($exception);
            }

            DB::commit();
            return redirect('user/registration/list')->with('success', 'New Registration added successfully');

        } catch (\Exception $mainException) {
            DB::rollback();
            $request->session()->flash('errors', $mainException);
        }

        //        $lastInsertedId = $reliefModel->saveData($request);
//
//        $request->request->add(['lastInsertedId' => $lastInsertedId]);
//        $res = $reliefDetailsModel->saveData($request);
//        try {
//            $cardId = $this->__cardIdGen($formData, $lastInsertedId);
//            Relief::where('id', $lastInsertedId)->update(['card_no' => $cardId]);
//        } catch (\Exception $exc) {
//            dd($exc);
//        }
//
//        /*  File Manipulation   */
//        $filename = '';
//        try {
//            if ($request->hasFile('image')) {
//                $client_id = $lastInsertedId;
//                $path = "trainee";
//
//                // cache the file
//                $file = $request->file('image');
//
//                // generate a new filename. getClientOriginalExtension() for the file extension
//                $filename = $client_id . '.' . $file->getClientOriginalExtension();
//                $filePath = $path . '/' . $client_id;
//
//                if (Storage::disk('public')->exists($filePath . '/' . $filename)) {
////                Storage::deleteDirectory($filePath);
//                    Storage::delete($filePath . '/' . $filename);
//                }
//
//                // save to storage_1/app/public/clients as the new $filename
//                $path = $file->storeAs($filePath, $filename);
//
//                Registration::where('id', $lastInsertedId)->update(['image' => $filename]);
//            }
//        } catch (\Exception $e) {
//            var_dump($e);
//        }

        return redirect('user/registration/list')->with('success', 'New Registration added successfully');
    }

    public function userhow($id)
    {
        //
    }

    public function userEdit(Request $request, $id)
    {
        $registrationModel = new Registration();
        $trainingModel = new Training();
        $districtModel = new District();
        $upazilaModel = new Upazila();
        $unionModel = new Union();
        $reliefModel = new Relief();
        $reliefDetailsModel = new ReliefDetails();
        $ssnpModel = new Ssnp();
        $tblData = array();
        try {

            $eduLevel_list = config('constants.edu_level.arr');
            $eduBoard_list = config('constants.edu_board.arr');
            $relrg_list = config('constants.religious.arr');
            $gender_list = config('constants.gender.arr');
            $marital_list = config('constants.marital_status.arr');
            $possi_help_list = config('constants.possible_help');

            $reliefData = $reliefModel->where('reliefs.id', $id)
                ->where('reliefs.created_by', '=', Auth::user()->id)
                ->leftJoin('users', 'reliefs.created_by', 'users.id')
                ->leftJoin('upazilas', 'upazilas.id', '=', 'reliefs.upo_pre')
                ->leftJoin('districts', 'districts.id', '=', 'reliefs.dist_pre')
                ->select('reliefs.*', 'users.first_name')
                //                ->tosql();
                ->firstOrFail();

            $detailsData = $reliefDetailsModel->where('relief_details.rlf_id', $id)
                ->firstOrFail();
            $tblData['name'] = json_decode($detailsData->name, true);
            $tblData['age'] = json_decode($detailsData->age, true);
            $tblData['nid'] = json_decode($detailsData->nid, true);
            $tblData['profession'] = json_decode($detailsData->profession, true);
            $tblData['relation'] = json_decode($detailsData->relation, true);
            $tblData['ssnp'] = json_decode($detailsData->ssnp, true);

            $districtListPre = $districtModel->where('id', $reliefData->dist_pre)->pluck('bn_name', 'id')->all();
            $districtListPer = $districtModel->where('id', $reliefData->dist_per)->pluck('bn_name', 'id')->all();
            $upazilaListPre = $upazilaModel->where('district_id', $reliefData->dist_pre)->pluck('bn_name', 'id')->all();
            $upazilaListPer = $upazilaModel->where('district_id', $reliefData->dist_per)->pluck('bn_name', 'id')->all();
            $unionListPre = $unionModel->where('upazilla_id', $reliefData->upo_pre)->pluck('bn_name', 'id')->all();
            $unionListPer = $unionModel->where('upazilla_id', $reliefData->upo_per)->pluck('bn_name', 'id')->all();
            $ssnpList = $ssnpModel->pluck('ssnp_name', 'id')->all();

            $hideenData = $reliefData['home_type'];

            $listArr = [
                $districtListPre,
                $districtListPer,
                $upazilaListPre,
                $upazilaListPer,
                $unionListPre,
                $unionListPer
            ];
            //            return view('user.registration.userEdit', compact('trainingList', 'id', 'eduLevel_list', 'eduBoard_list', 'relrg_list', 'gender_list', 'marital_list', 'registrationData', 'listArr'));
            return view('user.registration.userEdit', compact('id', 'gender_list', 'marital_list', 'reliefData', 'listArr', 'tblData', 'hideenData', 'ssnpList', 'possi_help_list'));
            //            return view('training.adminEdit', compact('deptList', 'id', 'trainingData'));
        } catch (\Exception $ex) {
            $request->session()->flash('error', 'No Data Found...');
            return redirect()->back();
            //            return redirect('client/apt/list')->with('errors', 'No Data Found...');
//            return redirect()->back()->withErrors(['msg', 'The Message']);    When has multiple errors and go back to the previous url...
        }
    }

    public function userUpdate(Request $request, $id)
    {
        $registrationModel = new Registration();
        $reliefModel = new Relief();
        $reliefDetailsModel = new ReliefDetails();
        $reliefFamilySearchModel = new ReliefFamilySearch();

        $formData = $request->all();
        if (isset($formData['same_add'])) {
            $request->request->add(['dist_per' => $formData['dist_pre']]);
            $request->request->add(['up_per' => $formData['up_pre']]);
            $request->request->add(['post_per' => $formData['post_pre']]);
            $request->request->add(['vill_per' => $formData['vill_pre']]);

            unset($request['same_add']);
        }

        //        $data = $this->validate($request, [
//            'training_id' => 'required',
//            'name_bn' => 'required',
//            'name_en' => 'required',
//            'f_name_bn' => 'required',
//            'm_name_bn' => 'required',
//            'vill_pre' => 'required',
//            'post_pre' => 'required',
//            'up_pre' => 'required',
//            'dist_pre' => 'required',
//            'vill_per' => 'required',
//            'post_per' => 'required',
//            'up_per' => 'required',
//            'dist_per' => 'required',
//            'edu_lvl' => 'required',
//            'phone' => 'required',
//            'nid' => 'required',
//            'dob' => 'required',
//            'relg' => 'required',
//            'nation' => 'required',
//            'gender' => 'required',
//            'married' => 'required',
////            'image' => 'required|file|image|mimes:jpeg,jpg,png,gif,webp|max:2048'
//        ],
//            [
//                'training_id.required' => 'Please Select a Training',
//                'name_bn.required' => 'Bengoli Name is required',
//                'name_en.required' => 'English Name is required',
//                'f_name_bn.required' => 'Bengoli Name is required',
//                'm_name_bn.required' => 'Bengoli Name is required',
//                'vill_pre.required' => 'Present Village is required',
//                'post_pre.required' => 'Present Post Office is required',
//                'up_pre.required' => 'Present Upozilla is required',
//                'dist_pre.required' => 'Present District is required',
//                'vill_per.required' => 'Permanent Village is required',
//                'post_per.required' => 'Permanent Post Office is required',
//                'up_per.required' => 'Permanent Upozilla is required',
//                'dist_per.required' => 'Permanent District is required',
//                'edu_lvl.required' => 'Education Level is required',
//                'phone.required' => 'Phone is required',
//                'nid.required' => 'Valid ID is required',
//                'dob.required' => 'Date of Birth is required',
//                'relg.required' => 'Please Select a Religious',
//                'nation.required' => 'Please Select Your Nationality',
//                'gender.required' => 'Please Select your gender',
//                'married.required' => 'Please Select your Marital Status',
////                'image' => 'required|file|image|mimes:jpeg,jpg,png,gif,webp|max:6144'
//            ]);

        DB::beginTransaction();
        try {
            $detailsData = $formData['tblData'];
            try {
                $reliefModel->updateData($request);
                $cardId = $this->__cardIdGen($formData, $id);
                $request->request->add(['card_no' => time()]);
                Relief::where('id', $id)->update(['card_no' => $cardId]);
            } catch (\Exception $exc) {
                dd($exc);
            }

            try {
                $res = $reliefDetailsModel->updateData($request);
                $reliefFamilySearchModel;
                $male = $female = $childern = $transgen = 0;
                $f_nid = $f_ssnp = '';
                $familySearchArr = [];

                foreach ($detailsData as $k_name => $k_values) {
                    if ($k_name === 'age') {
                        foreach ($k_values as $ageData) {
                            if (!empty($ageData) && ($ageData < 6)) {
                                $childern++;
                            }
                        }
                        $familySearchArr['tot_child'] = $childern;
                    }
                    if ($k_name === 'f_gender') {
                        foreach ($k_values as $genData) {
                            if (!empty($genData) && ($genData == 1)) {
                                $male++;
                            }
                            if (!empty($genData) && ($genData == 2)) {
                                $female++;
                            }
                            if (!empty($genData) && ($genData == 3)) {
                                $transgen++;
                            }
                        }
                        $familySearchArr['tot_male'] = $male;
                        $familySearchArr['tot_female'] = $female;
                        $familySearchArr['tot_transgen'] = $transgen;
                    }
                    if ($k_name === 'nid') {
                        foreach ($k_values as $nidData) {
                            if (!empty($nidData)) {
                                $f_nid .= $nidData . ',';
                            }
                        }
                        $familySearchArr['f_nid'] = $f_nid;
                    }
                    if ($k_name === 'ssnp') {
                        foreach ($k_values as $ssnpData) {
                            if (!empty($ssnpData)) {
                                $f_ssnp .= $ssnpData . ',';
                            }
                        }
                        $familySearchArr['f_ssnp'] = $f_ssnp;
                    }
                }
                Relief::where('id', $id)->update($familySearchArr);
                $request->request->add(['familySearchData' => $familySearchArr]);
                $familySearchData = $reliefFamilySearchModel->where('relf_id', $id)->first();
                if (empty($familySearchData)) {
                    $request->request->add(['lastInsertedId' => $id]);
                    $res = $reliefFamilySearchModel->saveData($request);
                } else {
                    $res = $reliefFamilySearchModel->updateData($request);
                }

            } catch (\Exception $serchDataSaveException) {
                dd($serchDataSaveException);
            }

            /*  File Manipulation   */
            $filename = '';
            //        die("adasdad");
            try {
                if ($request->hasFile('image')) {

                    $path = "family";

                    // cache the file
                    $file = $request->file('image');

                    // generate a new filename. getClientOriginalExtension() for the file extension
                    $filename = $id . '.' . $file->getClientOriginalExtension();
                    $filePath = $path . '/' . $id;

                    if (Storage::disk('public')->exists($filePath . '/' . $filename)) {
                        //                Storage::deleteDirectory($filePath);
                        Storage::delete($filePath . '/' . $filename);
                    }

                    // save to storage_1/app/public/properties as the new $filename
                    $path = $file->storeAs($filePath, $filename);
                    //            $request->request->add(['p_images' => $filename]);

                    Relief::where('id', $id)->update(['image' => $filename]);
                }
            } catch (\Exception $exception) {
                var_dump($exception);
            }

            DB::commit();
            return redirect('user/registration/list')->with('success', 'Registration Updated successfully');
        } catch (\Exception $mainException) {
            DB::rollback();
            $request->session()->flash('errors', $mainException);
        }


        //        $detailsData = $formData['tblData'];
//        $reliefModel->updateData($request);
//
//        $res = $reliefDetailsModel->updateData($request);
//
//
//        /*  File Manipulation   */
//        $filename = '';
//        try {
//            if ($request->hasFile('image')) {
//
//                $path = "trainee";
//
//                // cache the file
//                $file = $request->file('image');
//
//                // generate a new filename. getClientOriginalExtension() for the file extension
//                $filename = $id . '.' . $file->getClientOriginalExtension();
//                $filePath = $path . '/' . $id;
//
//                if (Storage::disk('public')->exists($filePath . '/' . $filename)) {
////                Storage::deleteDirectory($filePath);
//                    Storage::delete($filePath . '/' . $filename);
//                }
//
//                // save to storage_1/app/public/properties as the new $filename
//                $path = $file->storeAs($filePath, $filename);
////            $request->request->add(['p_images' => $filename]);
//
//                Registration::where('id', $id)->update(['image' => $filename]);
//            }
//        } catch (\Exception $exception) {
//            var_dump($exception);
//        }
//
//        return redirect('user/registration/list')->with('success', 'New Registration added successfully');
    }

    public function userDestroy(Request $request, $id)
    {
        $registrationModel = new Registration();
        $reliefModel = new Relief();
        $reliefDetailsModel = new ReliefDetails();

        DB::beginTransaction();
        $updateData = [
            'is_deleted' => config('constants.is_deleted.Deleted'),
        ];

        if (isset($request->id)) {
            $reliefModel->where('id', '=', $id)->update($updateData);
            $reliefDetailsModel->where('rlf_id', '=', $id)->update($updateData);
            DB::commit();
            $request->session()->flash('success', 'Data Deleted Successfully..');
            return response()->json(['status' => 'success']);
        } else {
            DB::rollback();
            $request->session()->flash('errors', 'Data Deleted Successfully..');
            return response()->json(['status' => 'error']);
        }
    }

    public function userRegStatusChange(Request $request, $id, $status)
    {
        $registrationModel = new Registration();

        DB::beginTransaction();
        $updateData = [
            'status' => $status,
        ];

        if (isset($request->id)) {
            $registrationModel->where('id', '=', $id)->update($updateData);
            DB::commit();
            $request->session()->flash('success', 'Trainee Status has changed Successfully..');
            return response()->json(['status' => 'success']);
        } else {
            DB::rollback();
            $request->session()->flash('errors', 'Could not change trainee status Successfully..');
            return response()->json(['status' => 'error']);
        }
    }

    public function userNidDuplicateCheck(Request $request)
    {
        $type = 'nid';
        return $this->_duplicateCheck($request, $type);
    }

    public function userPhoneDuplicateCheck(Request $request)
    {
        $type = 'phone';
        return $this->_duplicateCheck($request, $type);
    }


    /*  Common Function */

    private function __exportRegList($registrationList, $trainingList)
    {
        //        dd($registrationList);
        $registration_array[] = array('Sr. No.', 'পরিবার প্রধানের নাম', 'পিতার নাম', 'স্বামীর নাম', 'বয়স', 'লিঙ্গ', 'বর্তমান ঠিকানা', 'পেশা', 'মোবাইল নাম্বার', 'জন্ম নিবন্ধন / ভোটার আইডি নাম্বার / NID', 'মাসিক আয়', 'মাসিক ব্যয়');
        $relif_arr[] = array('Sr. No.', 'পরিবার প্রধানের নাম', 'পিতার নাম', 'স্বামীর/স্ত্রীর নাম', 'বয়স', 'লিঙ্গ', 'বর্তমান ঠিকানা', 'পেশা', 'মোবাইল নাম্বার', 'জন্ম নিবন্ধন / ভোটার আইডি নাম্বার / NID', 'মাসিক আয়', 'মাসিক ব্যয়', 'SSNP');

        $relief_dtl_arr[] = array('সদস্যের নাম', 'বয়স', 'জন্ম নিবন্ধন / ভোটার আইডি নাম্বার / NID', 'পেশা', 'সম্পর্ক', 'SSNP');

        foreach ($registrationList as $srNo => $registration) {
            $d_name = json_decode($registration['d_name'], true);
            $d_age = json_decode($registration['d_age'], true);
            $d_nid = json_decode($registration['d_nid'], true);
            $d_profession = json_decode($registration['d_profession'], true);
            $d_relation = json_decode($registration['d_relation'], true);
            $d_ssnp = json_decode($registration['d_ssnp'], true);
            $otherDataArr = array();
            for ($infodet = 1; $infodet <= 8; $infodet++) {
                if (!empty($d_name[$infodet]) || !empty($d_age[$infodet]) || !empty($d_nid[$infodet]) || !empty($d_profession[$infodet]) || !empty($d_relation[$infodet]) || !empty($d_ssnp[$infodet])) {
                    $otherDataArr[] = array(
                        'সদস্যের নাম' => $d_name[$infodet],
                        'বয়স' => $d_age[$infodet],
                        'জন্ম নিবন্ধন / ভোটার আইডি নাম্বার / NID' => $d_nid[$infodet],
                        'পেশা' => $d_profession[$infodet],
                        'সম্পর্ক' => $d_relation[$infodet],
                        'SSNP' => $d_ssnp[$infodet]
                    );
                }
            }
            //            dd($registration);
            $registration_array[] = array(
                'Sr. No.' => $srNo + 1,
                'পরিবার প্রধানের নাম' => $registration->name,
                'পিতার নাম' => $registration->g_name,
                'স্বামীর/স্ত্রীর নাম' => $registration->hw_name,
                'বয়স' => $registration->age,
                'লিঙ্গ' => $registration->gender = 1 ? 'পুরুষ' : ($registration->gender = 2 ? 'মহিলা' : 'অন্যান্য'),
                'বর্তমান ঠিকানা' => $registration->holding_pre . ', ' . $registration->house_pre . ', ' . $registration->road_pre . ', ' . $registration->word_pre . ', ' . $registration->uni_pre . ', ' . $registration->upa_name . ', ' . $registration->dis_name,
                'পেশা' => $registration->profession,
                'মোবাইল নাম্বার' => $registration->phone,
                'জন্ম নিবন্ধন / ভোটার আইডি নাম্বার' => $registration->nid,
                'মাসিক আয়' => $registration->totoal_income,
                'মাসিক ব্যয়' => $registration->total_spand,
                'SSNP' => $registration->ssnp,
                'পরিবারের সদস্য' => ""
            );
        }

        Excel::create("Registration List", function ($excel) use ($registration_array) {
            $excel->setTitle('Registration List');
            $excel->sheet(
                'Registration List',
                function ($sheet) use ($registration_array) {
                    $sheet->fromArray($registration_array, null, 'A1', false, false);
                }
            );
        })->download('xlsx');
    }

    private function __cardIdGen($formData, $lastInsertedId)
    {
        $cardId = config('constants.location_code.dist.' . $formData['dist_pre']);
        $cardId .= config('constants.location_code.upo.' . $formData['upo_pre']);
        //        $uni_pre = strlen(config('constants.location_code.uni.'.$formData['uni_pre']));
        $uni_pre = $formData['uni_pre'];
        if ($uni_pre < 1) {
            $cardId .= '00';
        } else {

            $uniLen = strlen($uni_pre);
            if (($uni_pre > 1) && ($uniLen == 2)) {
                $cardId .= config('constants.location_code.uni.' . $formData['uni_pre']);
            } else {
                $cardId .= '0';
                $cardId .= config('constants.location_code.uni.' . $formData['uni_pre']);
            }
        }
        $cardId .= $formData['word_pre'];
        $length = strlen($lastInsertedId);
        for ($count = 6; $count > $length; $count--) {
            $cardId .= '0';
        }
        $cardId .= $lastInsertedId;
        return $cardId;
    }

    private function _duplicateCheck($request, $type)
    {
        //        if ($request->ajax()) {
        $reliefModel = new Relief();


        $queryString = $reliefModel::query();
        $queryString->select("id", "card_no", "nid", "phone");

        if ($type === 'nid') {
            $nidVal = $request->post('nidVal');
            $check = true;
            $memberNid = $request->post('memberNid');
            if (!empty($memberNid)) {
                $check = false;
            }

            $queryString->where('nid', 'not like', "%3333333333%")
                ->Where(function ($query) use ($nidVal, $check) {
                    if ($check) {
                        $query->where('nid', 'like', "%" . $nidVal . "%");
                        $query->orWhere('f_nid', 'like', "%" . $nidVal . "%");
                    }
                })
                ->Where(function ($query) use ($memberNid, $check) {
                    if (!$check) {
                        foreach ($memberNid as $key => $mNid) {
                            $query->orWhere(
                                function ($query) use ($mNid) {
                                    $query->where('nid', 'like', "%" . $mNid . "%");
                                    $query->orWhere('f_nid', 'like', "%" . $mNid . "%");
                                }
                            );
                        }
                    }
                });
        }
        if ($type === 'phone') {
            $phoneVal = $request->post('phoneVal');
            $queryString->where('phone', 'like', "%" . $phoneVal . "%");
            $queryString->where('phone', 'not like', "%77777777777%");
        }

        $id = $request->post('id');
        if (!empty($id)) {
            $queryString->where('id', '!=', $id);
        }

        $queryString->where('is_deleted', '!=', 1);

        $reliefData = $queryString->get()->toArray();
        //            echo $reliefDatas = $queryString->toSql();
//            dd($reliefData);
        $resData = array();
        if (empty($reliefData)) {
            $resData['status'] = 200;
            return json_encode($resData);
        } else {
            $resData['status'] = 404;
            $resData['data'] = $reliefData[0];
            return json_encode($resData);
        }
        //        }
    }

    public static $bn = array("১", "২", "৩", "৪", "৫", "৬", "৭", "৮", "৯", "০");
    public static $en = array("1", "2", "3", "4", "5", "6", "7", "8", "9", "0");

    public static function bn2en($number)
    {
        return str_replace(self::$bn, self::$en, $number);
    }
}