<?php

namespace App\Http\Controllers;

use App\Model\Trade;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TradeController extends Controller
{
    public function adminList()
    {
        if (isset($_GET['displayValue'])) {
            $displayValue = $_GET['displayValue'] != '' ? $_GET['displayValue'] : "";
        } else {
            $displayValue = '';
        }

        if (isset($_GET['searchData'])) {
            $searchData = $_GET['searchData'] != '' ? $_GET['searchData'] : "";
        } else {
            $searchData = '';
        }

        $tradeModel = new Trade();

        $trades = $tradeModel->where('trades.status', '!=', config('constants.status.Deleted'))
            ->leftJoin('users', 'trades.created_by', 'users.id')
            ->select('users.first_name', 'users.last_name', 'trades.*')
            ->paginate($displayValue);
        if (request()->ajax()) {
            return view('trade.ajax_list', compact('trades'));
        } else {
            return view('trade.adminList', compact('trades'));
        }
    }

    public function adminForm()
    {
        return view('trade.adminForm');
    }

    public function adminStore(Request $request)
    {
        $tradeModel = new Trade();

        $data = $this->validate($request, [
            'trade_name' => 'required',
        ], [
            'trade_name.required' => 'Trade Name is required',

        ]);

        $tradeModel->saveData($request);
        return redirect('admin/trade/list')->with('success', 'New Trade added successfully...');
    }


    public function adminshow(Trade $trade)
    {
        //
    }


    public function adminEdit(Trade $trade, $id)
    {
        $tradeData = Trade::where('id', $id)->first();
        return view('trade.adminEdit', compact('tradeData', 'id'));
    }


    public function adminUpdate(Request $request, Trade $trade, $id)
    {
        $tradeModel = new Trade();
        $data = $this->validate($request, [
            'trade_name' => 'required',
        ], [
            'trade_name.required' => 'Trade Name is required',

        ]);

        $tradeModel->updateData($request);

        return redirect('admin/trade/list')->with('success', 'Trade edited successfully');
    }


    public function adminDestroy(Trade $trade, $id)
    {
        $tradeModel = new Trade();

        DB::beginTransaction();
        $updateData = [
            'status' => config('constants.status.Deleted'),
        ];

        $status = "success";
        $message = "Trade Delete successfully.";

        if (!empty($id)) {
            try {
                $tradeModel->where('id', '=', $id)
                    ->update($updateData);
                DB::commit();
            } catch (\Exception $exPledger) {
                DB::rollback();
                $status = "error";
                $message = "Trade Delete failed!!!";
            }
        } else {
            $status = "error";
            $message = "No data has been found to delete!!!";
        }
        return response()->json(['status' => $status, 'message' => $message]);
    }

    /*  Department Head Trade Section  */
    public function clientList()
    {
        if (isset($_GET['displayValue'])) {
            $displayValue = $_GET['displayValue'] != '' ? $_GET['displayValue'] : "";
        } else {
            $displayValue = '';
        }

        if (isset($_GET['searchData'])) {
            $searchData = $_GET['searchData'] != '' ? $_GET['searchData'] : "";
        } else {
            $searchData = '';
        }

        $tradeModel = new Trade();

        $trades = $tradeModel->where('trades.status', '!=', config('constants.status.Deleted'))
            ->leftJoin('users', 'trades.created_by', 'users.id')
            ->select('users.first_name', 'users.last_name', 'trades.*')
            ->paginate($displayValue);
        if (request()->ajax()) {
            return view('user.trade.ajax_list', compact('trades'));
        } else {
            return view('user.trade.clientList', compact('trades'));
        }
    }

    public function clientForm()
    {
        return view('user.trade.clientForm');
    }

    public function clientStore(Request $request)
    {
        $tradeModel = new Trade();

        $data = $this->validate($request, [
            'trade_name' => 'required',
        ], [
            'trade_name.required' => 'Trade Name is required',

        ]);

        $tradeModel->saveData($request);
        return redirect('client/trade/list')->with('success', 'New Trade added successfully...');
    }

    public function clientEdit(Trade $trade, $id)
    {
        $tradeData = Trade::where('id', $id)->first();
        return view('user.trade.clientEdit', compact('tradeData', 'id'));
    }

    public function clientUpdate(Request $request, Trade $trade, $id)
    {
        $tradeModel = new Trade();
        $data = $this->validate($request, [
            'trade_name' => 'required',
        ], [
            'trade_name.required' => 'Trade Name is required',

        ]);

        $tradeModel->updateData($request);

        return redirect('client/trade/list')->with('success', 'Trade edited successfully');
    }

    public function clientDestroy(Trade $trade, $id)
    {
        $tradeModel = new Trade();

        DB::beginTransaction();
        $updateData = [
            'status' => config('constants.status.Deleted'),
        ];

        $status = "success";
        $message = "Trade Delete successfully.";

        if (!empty($id)) {
            try {
                $tradeModel->where('id', '=', $id)
                    ->update($updateData);
                DB::commit();
            } catch (\Exception $exPledger) {
                DB::rollback();
                $status = "error";
                $message = "Trade Delete failed!!!";
            }
        } else {
            $status = "error";
            $message = "No data has been found to delete!!!";
        }
        return response()->json(['status' => $status, 'message' => $message]);
    }
}
