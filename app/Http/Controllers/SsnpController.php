<?php

namespace App\Http\Controllers;

use App\Model\Ssnp;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SsnpController extends Controller
{
    public function adminList()
    {
        if (isset($_GET['displayValue'])) {
            $displayValue = $_GET['displayValue'] != '' ? $_GET['displayValue'] : "";
        } else {
            $displayValue = '';
        }

        if (isset($_GET['searchData'])) {
            $searchData = $_GET['searchData'] != '' ? $_GET['searchData'] : "";
        } else {
            $searchData = '';
        }

        $snnpModel = new Ssnp();

        $snnpList = $snnpModel->where('ssnps.status', '!=', config('constants.status.Deleted'))
            ->leftJoin('users', 'ssnps.created_by', 'users.id')
            ->select('users.first_name', 'users.last_name', 'ssnps.*')
            ->paginate($displayValue);
        if (request()->ajax()) {
            return view('ssnp.ajax_list', compact('snnpList'));
        } else {
            return view('ssnp.adminList', compact('snnpList'));
        }
    }

    public function adminForm()
    {
        return view('ssnp.adminForm');
    }

    public function adminStore(Request $request)
    {
        $snnpModel = new Ssnp();

        $data = $this->validate($request, [
            'ssnp_name' => 'required',
        ], [
            'ssnp_name.required' => 'SNNP Name is required',
        ]);

        $snnpModel->saveData($request);
        return redirect('admin/ssnp/list')->with('success', 'New SSNP added successfully...');
    }

    public function show($id)
    {
        //
    }

    public function adminEdit($id)
    {
        if (auth()->user()->id == 532 || auth()->user()->id == 534) {
            throw new \Exception('Permission Deny');
        }
        $snnpData = Ssnp::where('id', $id)->first();
        return view('ssnp.adminEdit', compact('snnpData', 'id'));
    }

    public function adminUpdate(Request $request, $id)
    {
        $snnpModel = new Ssnp();
        $data = $this->validate($request, [
            'ssnp_name' => 'required',
        ], [
            'ssnp_name.required' => 'SNNP Name is required',
        ]);

        $snnpModel->updateData($request);

        return redirect('admin/ssnp/list')->with('success', 'SSNP edited successfully');
    }

    public function adminDestroy($id)
    {
        $snnpModel = new Ssnp();

        DB::beginTransaction();
        $updateData = [
            'status' => config('constants.status.Deleted'),
        ];

        $status = "success";
        $message = "SSNP Delete successfully.";

        if (!empty($id)) {
            try {
                $snnpModel->where('id', '=', $id)
                    ->update($updateData);
                DB::commit();
            } catch (\Exception $exPledger) {
                DB::rollback();
                $status = "error";
                $message = "SSNP Delete failed!!!";
            }
        } else {
            $status = "error";
            $message = "No data has been found to delete!!!";
        }
        return response()->json(['status' => $status, 'message' => $message]);
    }
}
