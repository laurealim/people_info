<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\DB;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
//        $this->middleware('permission:role-list|role-create|role-edit|role-delete', ['only' => ['index', 'store']]);
//        $this->middleware('permission:role-create', ['only' => ['create', 'store']]);
//        $this->middleware('permission:role-edit', ['only' => ['edit', 'update']]);
//        $this->middleware('permission:role-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * //     * @return \Illuminate\Http\Response
     */
    public function adminList()
    {
        if (isset($_GET['displayValue'])) {
            $displayValue = $_GET['displayValue'] != '' ? $_GET['displayValue'] : "";
        } else {
            $displayValue = '';
        }

        if (isset($_GET['searchData'])) {
            $searchData = $_GET['searchData'] != '' ? $_GET['searchData'] : "";
        } else {
            $searchData = '';
        }

        $roles = Role::orderBy('id', 'DESC')
            ->paginate($displayValue);
//        dd($reliefList);

        $uri = "";
        if (request()->ajax()) {
            $uri = request()->getUri();
                return view('permissions.ajax_list', compact('roles', 'uri'));
//            return view('permissions.ajax_list', compact('reliefList', 'uri', 'addressArr', 'packages'));
        } else {
            return view('permissions.adminList', compact('roles', 'uri'));
//            return view('permissions.adminList', compact('reliefList', 'uri', 'addressArr', 'packages'));
        }
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function adminForm()
    {
        $controllers = [];

        foreach (Route::getRoutes()->getRoutes() as $route)
        {
            $action = $route->getAction();

            if (array_key_exists('controller', $action))
            {
                $strArray = explode('\\', $action['controller']);
                $lastElement = end($strArray);
                $prefix = $action['prefix'];


                $lastSectionArr = explode('@',$lastElement);
                $controllerName = $lastSectionArr[0];
                $actionName = $lastSectionArr[1];

                // You can also use explode('@', $action['controller']); here
                // to separate the class name from the method
                if(!isset($controllers[$prefix]))
                    $controllers[$prefix] = [];

                if(!isset($controllers[$prefix][$controllerName]))
                    $controllers[$prefix][$controllerName] = [];

                $controllers[$prefix][$controllerName][] = $actionName;
            }
        }
//        pr($controllers);
        return view('permissions.adminForm');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function adminStore(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:permissions,name',
        ]);


        $role = Role::create(['name' => $request->input('name')]);
//        $role->syncPermissions($request->input('permission'));


        return redirect()->route('role.adminList')->with('success', 'Role created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role = Role::find($id);
        $rolePermissions = Permission::join("role_has_permissions", "role_has_permissions.permission_id", "=", "permissions.id")
            ->where("role_has_permissions.role_id", $id)
            ->get();


        return view('permissions.show', compact('role', 'rolePermissions'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function adminEdit($id)
    {
        $role = Role::find($id);
        $permission = Permission::get();
        $rolePermissions = DB::table("role_has_permissions")->where("role_has_permissions.role_id", $id)
            ->pluck('role_has_permissions.permission_id', 'role_has_permissions.permission_id')
            ->all();


        return view('permissions.edit', compact('role', 'permission', 'rolePermissions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function adminUpdate(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'permission' => 'required',
        ]);


        $role = Role::find($id);
        $role->name = $request->input('name');
        $role->save();


        $role->syncPermissions($request->input('permission'));


        return redirect()->route('permissions.index')->with('success', 'Role updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function adminDestroy($id)
    {
        DB::table("permissions")->where('id', $id)->delete();
        return redirect()->route('permissions.index')->with('success', 'Role deleted successfully');
    }
}
