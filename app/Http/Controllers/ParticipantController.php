<?php

namespace App\Http\Controllers;

use App\Model\Department;
use App\Model\Participant;
use App\Model\Registration;
use App\Model\Training;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ParticipantController extends Controller
{
    public function adminList()
    {
        if (isset($_GET['displayValue'])) {
            $displayValue = $_GET['displayValue'] != '' ? $_GET['displayValue'] : "";
        } else {
            $displayValue = '';
        }

        if (isset($_GET['searchData'])) {
            $searchData = $_GET['searchData'] != '' ? $_GET['searchData'] : "";
        } else {
            $searchData = '';
        }

        $trainingModel = new Training();
        $departmentModel = new Department();
        $registrationModel = new Registration();
        $participantModel = new Participant();
        $trainingList = $trainingModel->where('is_deleted', '=', config('constants.is_deleted.Active'))
            ->pluck('name', 'id')->all();

        $departmentList = $departmentModel->where('status', '=', config('constants.status.Active'))
            ->pluck('name', 'id')->all();
//        dd($trainingList);

        $registrationList = $registrationModel->select("users.first_name", 'registrations.*')
            ->where('registrations.status','!=',config('constants.registration_status.Waiting'))
            ->leftJoin('users', 'registrations.created_by', 'users.id')
//            ->tosql();
            ->paginate($displayValue);
//        dd($registrationList);

        $uri = "";
        if (request()->ajax()) {
            $uri = request()->getUri();
            return view('participant.ajax_list', compact('registrationList', 'departmentList','trainingList','uri'));
        } else {
            return view('participant.adminList', compact('registrationList', 'departmentList','trainingList','uri'));
        }
    }

    public function adminListId($id)
    {
        if (isset($_GET['displayValue'])) {
            $displayValue = $_GET['displayValue'] != '' ? $_GET['displayValue'] : "";
        } else {
            $displayValue = '';
        }

        if (isset($_GET['searchData'])) {
            $searchData = $_GET['searchData'] != '' ? $_GET['searchData'] : "";
        } else {
            $searchData = '';
        }

        $trainingModel = new Training();
        $departmentModel = new Department();
        $registrationModel = new Registration();
        $participantModel = new Participant();
        $trainingList = $trainingModel->where('is_deleted', '=', config('constants.is_deleted.Active'))
            ->pluck('name', 'id')->all();

        $departmentList = $departmentModel->where('status', '=', config('constants.status.Active'))
            ->pluck('name', 'id')->all();
//        dd($trainingList);

        $registrationList = $registrationModel->select("users.first_name", 'registrations.*')
            ->where('registrations.status','!=',config('constants.registration_status.Waiting'))
            ->where('registrations.training_id','=',$id)
            ->leftJoin('users', 'registrations.created_by', 'users.id')
//            ->tosql();
            ->paginate($displayValue);
//        dd($registrationList);

        $uri = "";
        if (request()->ajax()) {
            $uri = request()->getUri();
            return view('participant.ajax_list', compact('registrationList', 'departmentList','trainingList','uri'));
        } else {
            return view('participant.adminList', compact('registrationList', 'departmentList','trainingList','uri'));
        }
    }

    public function userRegStatusChange(Request $request, $id, $status)
    {
        $registrationModel = new Registration();

        DB::beginTransaction();
        $updateData = [
            'status' => $status,
        ];

        if (isset($request->id)) {
            $registrationModel->where('id', '=', $id)->update($updateData);
            DB::commit();
            $request->session()->flash('success', 'Trainee Status has changed Successfully..');
            return response()->json(['status' => 'success']);
        } else {
            DB::rollback();
            $request->session()->flash('errors', 'Could not change trainee status Successfully..');
            return response()->json(['status' => 'error']);
        }
    }

    public function adminRegConfirm(Request $request, $id)
    {
        $registrationModel = new Registration();
        $participantModel = new Participant();

        DB::beginTransaction();
        $registrationListData = $registrationModel->find($id);
//        dd($registrationListData);
        $updateData = [
            'status' => config('constants.registration_status.Confirmed'),
        ];

        if (isset($request->id)) {
            $registrationModel->where('id', '=', $id)->update($updateData);

            $participantModel->reg_id = $registrationListData->id;
            $participantModel->training_id = $registrationListData->training_id;
            $participantModel->dept_id = $registrationListData->dept_id;
            $participantModel->created_by = Auth::user()->id;
//            $participantModel->is_top = $registrationListData->id;
            $participantModel->save();
            DB::commit();
            $request->session()->flash('success', 'Trainee has been Confirmed Successfully..');
            return response()->json(['status' => 'success']);
        } else {
            DB::rollback();
            $request->session()->flash('errors', 'Could not confirm trainee Successfully..');
            return response()->json(['status' => 'error']);
        }
    }


    public function clientList()
    {
        if (isset($_GET['displayValue'])) {
            $displayValue = $_GET['displayValue'] != '' ? $_GET['displayValue'] : "";
        } else {
            $displayValue = '';
        }

        if (isset($_GET['searchData'])) {
            $searchData = $_GET['searchData'] != '' ? $_GET['searchData'] : "";
        } else {
            $searchData = '';
        }

        $trainingModel = new Training();
        $departmentModel = new Department();
        $registrationModel = new Registration();
        $participantModel = new Participant();
        $trainingList = $trainingModel->where('is_deleted', '=', config('constants.is_deleted.Active'))
            ->where('trainings.dept_id', '=', Auth::user()->dept_id)
            ->pluck('name', 'id')->all();

        $departmentList = $departmentModel->where('status', '=', config('constants.status.Active'))
            ->where('departments.id', '=', Auth::user()->dept_id)
            ->pluck('name', 'id')->all();
//        dd($trainingList);

        $registrationList = $registrationModel->select("users.first_name", 'registrations.*')
            ->where('registrations.status','!=',config('constants.registration_status.Waiting'))
            ->where('registrations.dept_id', '=', Auth::user()->dept_id)
            ->leftJoin('users', 'registrations.created_by', 'users.id')
//            ->tosql();
            ->paginate($displayValue);
//        dd($registrationList);

        $uri = "";
        if (request()->ajax()) {
            $uri = request()->getUri();
            return view('user.participant.ajax_list', compact('registrationList', 'departmentList','trainingList','uri'));
        } else {
            return view('user.participant.clientList', compact('registrationList', 'departmentList','trainingList','uri'));
        }
    }

    public function clientListId($id)
    {
        if (isset($_GET['displayValue'])) {
            $displayValue = $_GET['displayValue'] != '' ? $_GET['displayValue'] : "";
        } else {
            $displayValue = '';
        }

        if (isset($_GET['searchData'])) {
            $searchData = $_GET['searchData'] != '' ? $_GET['searchData'] : "";
        } else {
            $searchData = '';
        }

        $trainingModel = new Training();
        $departmentModel = new Department();
        $registrationModel = new Registration();
        $participantModel = new Participant();
        $trainingList = $trainingModel->where('is_deleted', '=', config('constants.is_deleted.Active'))
            ->where('trainings.dept_id', '=', Auth::user()->dept_id)
            ->pluck('name', 'id')->all();

        $departmentList = $departmentModel->where('status', '=', config('constants.status.Active'))
            ->where('departments.id', '=', Auth::user()->dept_id)
            ->pluck('name', 'id')->all();
//        dd($trainingList);

        $registrationList = $registrationModel->select("users.first_name", 'registrations.*')
            ->where('registrations.status','!=',config('constants.registration_status.Waiting'))
            ->where('registrations.training_id','=',$id)
            ->where('registrations.dept_id', '=', Auth::user()->dept_id)
            ->leftJoin('users', 'registrations.created_by', 'users.id')
//            ->tosql();
            ->paginate($displayValue);
//        dd($registrationList);

        $uri = "";
        if (request()->ajax()) {
            $uri = request()->getUri();
            return view('user.participant.ajax_list', compact('registrationList', 'departmentList','trainingList','uri'));
        } else {
            return view('user.participant.clientList', compact('registrationList', 'departmentList','trainingList','uri'));
        }
    }

    public function clientUserRegStatusChange(Request $request, $id, $status)
    {
        $registrationModel = new Registration();

        DB::beginTransaction();
        $updateData = [
            'status' => $status,
        ];

        if (isset($request->id)) {
            $registrationModel->where('id', '=', $id)
                ->where('registrations.dept_id', '=', Auth::user()->dept_id)
                ->update($updateData);
            DB::commit();
            $request->session()->flash('success', 'Trainee Status has changed Successfully..');
            return response()->json(['status' => 'success']);
        } else {
            DB::rollback();
            $request->session()->flash('errors', 'Could not change trainee status Successfully..');
            return response()->json(['status' => 'error']);
        }
    }

    public function clientRegConfirm(Request $request, $id)
    {
        $registrationModel = new Registration();
        $participantModel = new Participant();

        DB::beginTransaction();
        $registrationListData = $registrationModel->find($id);
//        dd($registrationListData);
        $updateData = [
            'status' => config('constants.registration_status.Confirmed'),
        ];

        if (isset($request->id)) {
            $registrationModel->where('id', '=', $id)->update($updateData);

            $participantModel->reg_id = $registrationListData->id;
            $participantModel->training_id = $registrationListData->training_id;
            $participantModel->dept_id = $registrationListData->dept_id;
            $participantModel->created_by = Auth::user()->id;
//            $participantModel->is_top = $registrationListData->id;
            $participantModel->save();
            DB::commit();
            $request->session()->flash('success', 'Trainee has been Confirmed Successfully..');
            return response()->json(['status' => 'success']);
        } else {
            DB::rollback();
            $request->session()->flash('errors', 'Could not confirm trainee Successfully..');
            return response()->json(['status' => 'error']);
        }
    }
}
