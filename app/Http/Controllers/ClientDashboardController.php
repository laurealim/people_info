<?php

namespace App\Http\Controllers;

use App\Model\Department;
use App\Model\Registration;
use App\Model\Training;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;

class ClientDashboardController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:client');
    }

    public function index()
    {
        $departmentModel = new Department();
        $departmentList = $departmentModel->where('status', '=', config('constants.status.Active'))
            ->pluck('name', 'id')->all();
        $dept_id = $departmentList[Auth::user()->dept_id];
        return view('user.dashboard', ["user" => "client", 'dept_id' =>$dept_id]);
    }

    public function clientGetRegistration()
    {
        $trainingModel = new Training();
        $departmentModel = new Department();
        $registrationModel = new Registration();
        $departmentList = $departmentModel->where('status', '=', config('constants.status.Active'))
            ->pluck('name', 'id')->all();

        $registrationList = $registrationModel->where('registrations.is_deleted', '!=', config('constants.is_deleted.Deleted'))
            ->where('trainings.is_deleted', '!=', config('constants.is_deleted.Deleted'))
            ->where('registrations.dept_id', '=', Auth::user()->dept_id)
            ->leftJoin('users', 'registrations.created_by', 'users.id')
            ->leftJoin('departments', 'registrations.dept_id', 'departments.id')
            ->leftJoin('trainings', 'registrations.training_id', 'trainings.id')
            ->select('users.first_name', 'departments.name', 'trainings.name as training_name', DB::raw('COUNT(registrations.id) AS total_reg'),
                DB::raw('SUM(if(registrations.status = 3, 1,0)) AS reg_confirm')
            )
            ->limit(10)
            ->orderBy('registrations.id', 'asc')
            ->groupBy('trainings.id')
            ->get()
            ->toArray();

//        pr($registrationList);
        echo json_encode($registrationList);
        die;
    }

    public function clientGetLastTraining()
    {
        $trainingModel = new Training();
        $departmentModel = new Department();
        $registrationModel = new Registration();
        $departmentList = $departmentModel->where('status', '=', config('constants.status.Active'))
            ->pluck('name', 'id')->all();

        $trainingList = $trainingModel
            ->where('trainings.is_deleted', '!=', config('constants.is_deleted.Deleted'))
            ->where('trainings.dept_id', '=', Auth::user()->dept_id)
            ->leftJoin('users', 'trainings.created_by', 'users.id')
            ->leftJoin('departments', 'trainings.dept_id', 'departments.id')
            ->select('users.first_name', 'departments.name', 'trainings.name as training_name', 'trainings.total_day', 'trainings.start_date', 'trainings.exam_date', 'trainings.training_place', 'trainings.budget'
            )
            ->limit(10)
            ->orderBy('trainings.start_date', 'desc')
            ->get()
            ->toArray();

//        pr($trainingList);
        echo json_encode($trainingList);
        die;
    }
}
