<?php

namespace App\Http\Controllers;

use App\Model\Eligibility;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EligibilityController extends Controller
{
    public function adminList()
    {
        if (isset($_GET['displayValue'])) {
            $displayValue = $_GET['displayValue'] != '' ? $_GET['displayValue'] : "";
        } else {
            $displayValue = '';
        }

        if (isset($_GET['searchData'])) {
            $searchData = $_GET['searchData'] != '' ? $_GET['searchData'] : "";
        } else {
            $searchData = '';
        }

        $eligibilities = new Eligibility();

        $eligibilities = $eligibilities->where('eligibilities.name', 'like', '%' . $searchData . '%')
            ->leftJoin('users', 'eligibilities.created_by', 'users.id')
            ->select('eligibilities.*', 'users.first_name')
            ->paginate($displayValue);


        if (request()->ajax()) {
            return view('eligibility.ajax_list',compact('eligibilities'));
        } else {
            return view('eligibility.adminList',compact('eligibilities'));
        }
    }

    public function adminForm()
    {
        return view('eligibility.adminForm');
    }

    public function adminStore(Request $request)
    {
        $eligibilityModel = new Eligibility();
        $data = $this->validate($request,[
            'name' => 'required',
        ]);

        $eligibilityModel->saveData($request);
        return redirect('admin/eligibility/list')->with('success','New Eligibility type added successfully');
    }

    public function show(Eligibility $eligibility)
    {
        //
    }

    public function adminEdit($id, Eligibility $eligibility)
    {
        $eligibilityData = Eligibility::where('id', $id)->first();
        return view('eligibility.adminEdit', compact('eligibilityData', 'id'));
    }

    public function adminUpdate(Request $request, Eligibility $eligibility, $id)
    {
        $eligibilityModel = new Eligibility();
        $data = $this->validate($request, [
            'name' => 'required',
        ]);

        $eligibilityModel->updateData($request);

        return redirect('admin/eligibility/list')->with('success','Eligibility type edited successfully');
    }

    public function adminDestroy(Request $request, Eligibility $eligibility)
    {
        $eligibilityInfo = Eligibility::findOrFail($request->id);
        if(isset($request->id)){
            $eligibilityInfo->delete();
            $request->session()->flash('success', 'Data Deleted Successfully..');
            return response()->json(['status'=>'success']);
        }
        else{
            $request->session()->flash('errors', 'Data Deleted Successfully..');
            return response()->json(['status'=>'error']);
        }
    }
}
