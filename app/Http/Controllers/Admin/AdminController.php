<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{
    public function showProfile()
    {
        $userId = Auth::user()->id;
        $adminInfo = Admin::find($userId);
        return view('admin.profile', compact('adminInfo'));
    }

    public function updateProfile(Request $request)
    {
        $userId = Auth::user()->id;
        $adminInfo = Admin::find($userId);

        $request->validate([
            'first_name' => 'required',
        ]);

        $adminInfo->first_name = $request->first_name;
        $adminInfo->last_name = $request->last_name;
        $adminInfo->dob = $request->dob;
        $adminInfo->email = $request->email;
        $adminInfo->website = $request->website;
        $adminInfo->phone = $request->phone;
        $adminInfo->gender = $request->gender;

        if ($request->hasFile('profileImg')) {
            $file = $request->file('profileImg');
            $uploadPath = public_path('images/avatars/');
            $fileName = strtolower(Auth::user()->first_name . '_' . Auth::user()->id . '.' . $file->getClientOriginalExtension());
            $fileExtension = $file->getClientOriginalExtension();

            if (!is_dir($uploadPath)) {
                mkdir($uploadPath, 0755);
            } else {
                if (file_exists($uploadPath . $fileName)) {
                    unlink($uploadPath . $fileName);
                    $file->move($uploadPath, $fileName);
                } else {
                    $file->move($uploadPath, $fileName);
//                    Image::make($file)->resize(300, 300)->save( $uploadPath . $fileName  );
                }
            }
//            $array_push(['image'=>$fileName]);
            $adminInfo->image = $fileName;
        }

        if(!empty($request->crnt_password)){
            $request->validate([
                'crnt_password' => 'required',
                'password' => 'required|string|min:6|confirmed',
                'password_confirmation' => 'required',
            ]);

            $result = $this->changePassword($request);
            if($result[0] === 'error'){
                return redirect()->back()->with("error",$result[1]);
            }
            else{
                $adminInfo->password = bcrypt($request->get('password'));
            }
        }

//        dd($request);
        $adminInfo->save();

        return redirect('admin/profile')->with('success', 'Profile Updated Successfully');
    }

    public function changePassword($request){
        $messageType = '';
        $message = '';

        if($request->post('password') !== $request->post('password_confirmation')){
            //Current password and confirm password are not same
            $message = "New Password and confirm password mismatch. Please try again.";
            $messageType = "error";
            return ([$messageType,$message]);

//            return redirect()->back()->with("error","New Password and confirm password mismatch. Please try again.");
        }

        if (!(Hash::check($request->post('crnt_password'), Auth::user()->password))) {
            // The passwords matches
            $message = "Your current password does not matches with the password you provided. Please try again.";
            $messageType = "error";
            return ([$messageType,$message]);

//            return redirect()->back()->with("error","Your current password does not matches with the password you provided. Please try again.");
        }

        if(strcmp($request->get('crnt_password'), $request->get('password')) == 0){
            //Current password and new password are same
            $message = "New Password cannot be same as your current password. Please choose a different password.";
            $messageType = "error";
            return ([$messageType,$message]);

//            return redirect()->back()->with("error","New Password cannot be same as your current password. Please choose a different password.");
        }

//        $validatedData = $request->validate([
//            'crnt_password' => 'required',
//            'new_password' => 'required|string|min:6|confirmed',
//            'conf_password' => 'required',
//        ]);

        //Change Password
//        $user = Auth::user();
//        $user->password = bcrypt($request->get('new-password'));
//        $user->save();

        $message = "Password changed successfully !";
        $messageType = "success";
        return ([$messageType,$message]);

//        return redirect()->back()->with("success","Password changed successfully !");

    }
}
