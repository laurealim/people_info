<?php

namespace App\Http\Controllers\Api;

use App\Model\District;
use App\Model\Package;
use App\Model\PackageMapping;
use App\Model\Registration;
use App\Model\Relief;
use App\Model\ReliefDetails;
use App\Model\ReliefFamilySearch;
use App\Model\ReliefMapping;
use App\Model\Ssnp;
use App\Model\Upazila;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use JWTAuth;
use JWTAuthException;
use App\User;
use Monolog\Formatter\FlowdockFormatter;

class ApiController extends Controller
{

    public function __construct()
    {
        $this->user = new User();
    }

    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');
        $token = null;
        $user = null;
        try {
            //$credentials['user_type'] = 1;
            $credentials['app_access'] = 1;
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json([
                    'response' => 'error',
                    'message' => 'invalid_email_or_password_or_user_type',
                ]);
            } else {
//                $request->request->add(['token', $token]);
                $user = JWTAuth::user();
            }
        } catch (JWTAuthException $e) {
            return response()->json([
                'response' => 'error',
                'message' => 'failed_to_create_token',
            ]);
        }


        return response()->json([
            'status' => 200,
            'result' => [
                'token' => $token,
                'user' => $user
            ]
        ]);
    }

    public function getAuthUser(Request $request)
    {

//        dd($request->token);
        try {
            $user = JWTAuth::toUser();
            return response()->json(['status' => 200, 'result' => $user]);
        } catch (\Exception $exception) {
            dd($exception);
        }
    }

    public function logout(Request $request)
    {

//        $this->validate($request, ['token' => 'required']);

        try {
            $user = JWTAuth::toUser($request->token);
            JWTAuth::invalidate($request->input('token'));
            return response()->json(['success' => true, 'message' => "You have successfully logged out."]);
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['success' => false, 'error' => 'Failed to logout, please try again.'], 500);
        }
    }

    public function getFamilyInfoByAnyId(Request $request)
    {
        $districtModel = new District();
        $upazilaModel = new Upazila();
        $reliefModel = new Relief();
        $reliefDetailsModel = new ReliefDetails();
        $ssnpModel = new Ssnp();
        $packagesModel = new Package();
        $reliefMappingModel = new ReliefMapping();

        try {
            $queryString = $reliefModel::query();
            $queryString->select("districts.bn_name AS dis_name", "upazilas.bn_name AS upa_name", "unions.bn_name AS uni_name", "users.first_name", 'reliefs.id', 'reliefs.card_no', 'reliefs.card_type', 'reliefs.name', 'reliefs.g_name', 'reliefs.total_member', 'reliefs.hw_name', 'reliefs.age', 'reliefs.dob', 'reliefs.gender', 'reliefs.house_pre', 'reliefs.road_pre', 'reliefs.word_pre', 'reliefs.upo_pre', 'reliefs.uni_pre', 'reliefs.holding_pre', 'reliefs.dist_pre', 'reliefs.profession', 'reliefs.phone', 'reliefs.nid', 'reliefs.f_nid', 'reliefs.ssnp', 'reliefs.total_income', 'reliefs.poss_help', 'reliefs.total_spend')
                ->where('reliefs.is_deleted', '!=', config('constants.is_deleted.Deleted'))
                ->leftJoin('users', 'reliefs.created_by', 'users.id')
                ->leftJoin('upazilas', 'upazilas.id', '=', 'reliefs.upo_pre')
                ->leftJoin('unions', 'unions.id', '=', 'reliefs.uni_pre')
                ->leftJoin('districts', 'districts.id', '=', 'reliefs.dist_pre');
//                ->leftJoin('relief_details', 'relief_details.rlf_id', '=', 'reliefs.id')
//                ->leftJoin('relief_mappings', 'relief_mappings.reg_id', '=', 'reliefs.id')
//                ->leftJoin('ssnps', 'ssnps.id', '=', 'reliefs.ssnp');

            if ($request->has('phone') && !empty($request->phone)) {
                $phone = $request->phone;
                $queryString->where('reliefs.phone', 'like', "%" . $phone . "%");
            }
            if ($request->has('nid') && !empty($request->nid)) {
                $nid = $request->nid;
                $queryString->where('reliefs.nid', 'like', "%" . $nid . "%");
                $queryString->orWhere('reliefs.f_nid', 'like', "%" . $nid . "%");
//                $queryString->where('nid', 'like', "%" . $nidVal . "%");
            }
            if ($request->has('card_no') && !empty($request->card_no)) {
                $card_no = $request->card_no;
                $queryString->where('reliefs.card_no', '=', $card_no);
            }

            $reliefData = $queryString->get()->toArray();
//            $reliefData = $queryString->toSql();

//            dd($reliefData[0]);
//            dd($reliefData[0]['id']);
            if (!empty($reliefData)) {

                $reliefId = $reliefData[0]['id'];
                $rlfDetails = $reliefDetailsModel->select('id', 'rlf_id', 'name', 'age', 'f_gender', 'nid', 'profession', 'relation', 'ssnp')
                    ->where('rlf_id', '=', $reliefId)
                    ->get()
                    ->toArray();

                $reliefMappingData = $reliefMappingModel->select('packeges', 'last_give_pac', 'delv_date')
                    ->where('reg_id', '=', $reliefId)
                    ->get()
                    ->toArray();

                $ssnpList = $ssnpModel->pluck('ssnp_name', 'id')->all();
                $packageList = $packagesModel->pluck('title', 'id')->all();
                $relationList = config('constants.relation');
//                dd($relationList);
                $resData = [
                    'status' => 200,
                    'result' => [
                        'data' => $reliefData[0],
                        'family_data' => $rlfDetails[0],
                        'relief_mapping_data' => !empty($reliefMappingData) ? $reliefMappingData[0] : array(),
                        'ssnpList' => $ssnpList,
                        'packageList' => $packageList,
                        'relationList' => $relationList,
                    ]
                ];
                return response()->json($resData);
            } else {
                return response()->json(['status' => 404, 'result' => 'No Data Found']);
            }
        } catch (\Exception $exception) {
            dd($exception->getMessage());
        }
    }

    public function saveFamilyInfo(Request $request)
    {
        $jsonPostData = "";
        $reliefModel = new Relief();
        $reliefDetailsModel = new ReliefDetails();
        $packageMappingModel = new PackageMapping();


        /*To Do Task
        1. Have to update the family member list. Have to update relief_details, relief_family_search and also relief table.
        2. Update Image and the Date of Birth if provided. Have to update the Relied table.
        3. Save Packege Data. Have to update the relief_mapping table.

        Work Flow
        1.1. Get Family Member post data. OK
        1.2. Update relief_details table with the json format. OK
        1.3. Calculate and update the relif_family_search table. OK
        1.4. Update the relief table.

        2.1. Get the dob post data. OK
        2.2. Get the image_file and prepare the image from base64 encoded value if it has.
        2.3. Update the reliefs table.(can do the point 1.4 point in this point)

        3.1. Get the Package post data. OK
        3.2. Prepare the data. OK
        3.3. Save the data in package_mapping and also relief_mapping table. OK(not package_mapping)
        */


//        Storage::put('attempt1.txt', $card_no);
        DB::beginTransaction();
        try {
            $postFamilyData = $postPackageData = $postDobData = $postUserInfoData = $postImageFileData = '';
            if (!empty($request->post('family')))
                $postFamilyData = json_decode($request->post('family'), true);
            if (!empty($request->post('packages')))
                $postPackageData = json_decode($request->post('packages'),true);
            if (!empty($request->post('dob')))
                $postDobData = $request->post('dob');
            if (!empty($request->post('user_info')))
                $postUserInfoData = json_decode($request->post('user_info'), true);
            if (!empty($request->post('image_file')))
                $postImageFileData = json_decode($request->post('image_file'), true);

            $relief_id = $postUserInfoData['id'];
            $family_card_no = $postUserInfoData['card_no'];

            /*  Todo List 1 Task Starts */

            $familyDetailsData = [];
            $familySearchArr = [];
            $reliefData = [];
            $male = $female = $childern = $transgen = 0;
            $f_nid = $f_ssnp = '';
            $name = [];
            $age = [];
            $f_gender = [];
            $nid = [];
            $profession = [];
            $relation = [];
            $ssnp = [];
            $rlf_id = 0;
            $id = 0;


            for ($i = 0; $i <= 7; $i++) {
                if (!empty($postFamilyData[$i])) {
                    $id = $postFamilyData[$i]['id'];
                    $rlf_id = $postFamilyData[$i]['rlf_id'];
                    $name[$i + 1] = $postFamilyData[$i]['name'];
                    $age[$i + 1] = $postFamilyData[$i]['age'];
                    $f_gender[$i + 1] = $postFamilyData[$i]['f_gender'];
                    $nid[$i + 1] = $postFamilyData[$i]['nid'];
                    $profession[$i + 1] = $postFamilyData[$i]['profession'];
                    $relation[$i + 1] = $postFamilyData[$i]['relation'];
                    $ssnp[$i + 1] = $postFamilyData[$i]['ssnp'];

                    /*  Children Count  */
                    if (!empty($postFamilyData[$i]['age']) && ($postFamilyData[$i]['age'] < 6)) {
                        $childern++;
                    }
                    /*  Gender Count    */
                    if (!empty($postFamilyData[$i]['f_gender']) && ($postFamilyData[$i]['f_gender'] == 1)) {
                        $male++;
                    }
                    if (!empty($postFamilyData[$i]['f_gender']) && ($postFamilyData[$i]['f_gender'] == 2)) {
                        $female++;
                    }
                    if (!empty($postFamilyData[$i]['f_gender']) && ($postFamilyData[$i]['f_gender'] == 3)) {
                        $transgen++;
                    }
                    /*  NID Add */
                    if (!empty($postFamilyData[$i]['nid'])) {
                        $f_nid .= $postFamilyData[$i]['nid'] . ',';
                    }
                    /*  SSNP List   */
                    if (!empty($postFamilyData[$i]['ssnp'])) {
                        $f_ssnp .= $postFamilyData[$i]['ssnp'] . ',';
                    }

                } else {
                    $name[$i + 1] = '';
                    $age[$i + 1] = '';
                    $f_gender[$i + 1] = '';
                    $nid[$i + 1] = '';
                    $profession[$i + 1] = '';
                    $relation[$i + 1] = '';
                    $ssnp[$i + 1] = '';
                }
            }

            /*  Have to save in relied_details table    */
//            $familyDetailsData['id'] = $id;
//            $familyDetailsData['rlf_id'] = $rlf_id;
            $familyDetailsData['name'] = json_encode($name);
            $familyDetailsData['age'] = json_encode($age);
            $familyDetailsData['f_gender'] = json_encode($f_gender);
            $familyDetailsData['nid'] = json_encode($nid);
            $familyDetailsData['profession'] = json_encode($profession);
            $familyDetailsData['relation'] = json_encode($relation);
            $familyDetailsData['ssnp'] = json_encode($ssnp);


            /*  Have to Update relies_family_search and relief table    */
            $familySearchArr['tot_child'] = $childern;
            $familySearchArr['tot_male'] = $male;
            $familySearchArr['tot_female'] = $female;
            $familySearchArr['tot_transgen'] = $transgen;
            $familySearchArr['f_nid'] = $f_nid;
            $familySearchArr['f_ssnp'] = $f_ssnp;

            /*  Todo List 1 Task Ends */

            /*  Update Relief Details Data  */
            ReliefDetails::where(['rlf_id' => $relief_id])->update($familyDetailsData);

            /*  Update Family Search Data   */
            ReliefFamilySearch::where(['relf_id' => $relief_id])->update($familySearchArr);

            if (!empty($postDobData) && ($postDobData != '')) {
                $reliefData = $familySearchArr;
                $reliefData['dob'] = date('Y-m-d',strtotime($postDobData));
            }
            if (!empty($postUserInfoData)) {
                $reliefData['ssnp'] = $postUserInfoData['ssnp'];
                $reliefData['card_type'] = $postUserInfoData['card_type'];
            }

            /*  Update Relief Data  */
            Relief::where(['id' => $relief_id])->update($reliefData);

            /*  Update Relief Mapping Data  */
            if(!empty($postPackageData)){
                $packageArr = [];
                $pkj_id = $delv_date = $last_give_pac = '';
                foreach ($postPackageData as $key => $packages) {
                    $curDate = date("Y-m-d",time());
                    $curDateTime = date("Y-m-d h:i:s", time());
                    $pkj_id .= $packages['package_id'] . ',';
                    if(!empty($packages['delivery_date'])) {
                        $curDate = date("Y-m-d", strtotime($packages['delivery_date']));
                    }

                    $last_give_pac = $curDateTime;
                    $delv_date .= $curDate.',';
                }
                $packageArr['packeges'] = $pkj_id;
                $packageArr['delv_date'] = $delv_date;
                $packageArr['last_give_pac'] = $last_give_pac;

                $previousData = ReliefMapping::where(['reg_id' => $relief_id])->get()->toArray();

                if(!empty($previousData)){
                    ReliefMapping::where(['reg_id' => $relief_id])->update($packageArr);
                }else{
                    $releafMapping = new ReliefMapping();
                    $releafMapping->packeges = $packageArr['packeges'];
                    $releafMapping->delv_date = $packageArr['delv_date'];
                    $releafMapping->last_give_pac = $packageArr['last_give_pac'];
                    $releafMapping->reg_id = $relief_id;
                    $releafMapping->status = 1;

                    $releafMapping->save();
                }
            }

            /*  Image Save  */
            try{
                if(!empty($postImageFileData)){

                    $file_data = $postImageFileData['file_b64'];
                    $path = "images/family";
                    $filename = $relief_id.".jpg";
                    $filePath = $path . '/' . $relief_id;

                    $uploadPath = public_path($filePath);
                    try {
                        if (!is_dir($uploadPath)) {
                            mkdir($uploadPath, 0755, true);
                        } else {
                            if (file_exists($uploadPath.'/' . $filename)) {
                                unlink($uploadPath.'/' . $filename);
                            }
                        }
                        file_put_contents($uploadPath.'/'.$filename,base64_decode($file_data));
                    } catch (\Exception $excp) {
                        file_put_contents('dataset1.txt',$excp);
                        dd($excp);
                    }
                }

            }catch (\Exception $excp){
                file_put_contents('dataset1.txt',$excp);
                dd($excp);
            }

//            $package_ids = explode(',', $pkj_id);
//            foreach ($package_ids as $pacids => $pacid) {
//                if (!empty($pacid)) {
//                    $package_mapping_data = $packageMappingModel->where(['pack_id' => $pacid])->get()->toArray();
//                    $package_mapping_data = $package_mapping_data[0];
//                    pr($package_mapping_data);
//                    if (!empty($package_mapping_data)) {
//                        echo $relief_id;
//                        $regData = $package_mapping_data['registrations'];
//                        if(strpos($regData, $relief_id) !== false){
//                            echo "Word Found!";
//                        } else{
//                            echo "Word Not Found!";
//                        }
//                    }
//                }
//            }

            DB::commit();
            return response()->json(['status' => 200, 'result' => 'Data Saved Successfully!!!']);
        } catch (\Exception $exception) {
            DB::rollback();
            file_put_contents('dataset1.txt',$exception);
            return response()->json(['status' => 400, 'result' => $exception]);
        }
    }

}