<?php

namespace App\Http\Controllers;

use App\Model\Department;
use App\Model\Registration;
use App\Model\Training;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        return view('admin.dashboard', ["user" => "admin"]);
    }

    public function adminGetTrainingStatus()
    {
        $trainingModel = new Training();
        $departmentModel = new Department();
        $departmentList = $departmentModel->where('status', '=', config('constants.status.Active'))
            ->pluck('name', 'id')->all();

        $trainingList = $trainingModel
            ->where('is_deleted', '!=', config('constants.is_deleted.Deleted'))
            ->leftJoin('users', 'trainings.created_by', 'users.id')
            ->leftJoin('departments', 'trainings.dept_id', 'departments.id')
            ->select(
                'users.first_name',
                'departments.name', DB::raw('COUNT(trainings.id) AS total_Training'),
                DB::raw('SUM(if(trainings.status = 2, 1,0)) AS total_finished'),
                DB::raw('SUM(if(trainings.status = 1, 1,0)) AS total_pending')
            )
            ->groupBy('departments.id')
            ->get()
            ->toArray();
        //        pr($trainingList);
        echo json_encode($trainingList);
        die;
    }

    public function adminGetRegistration()
    {
        $trainingModel = new Training();
        $departmentModel = new Department();
        $registrationModel = new Registration();
        $departmentList = $departmentModel->where('status', '=', config('constants.status.Active'))
            ->pluck('name', 'id')->all();

        $registrationList = $registrationModel->where('registrations.is_deleted', '!=', config('constants.is_deleted.Deleted'))
            ->where('departments.status', '!=', config('constants.status.Deleted'))
            ->leftJoin('users', 'registrations.created_by', 'users.id')
            ->leftJoin('departments', 'registrations.dept_id', 'departments.id')
            ->select(
                'users.first_name',
                'departments.name', DB::raw('COUNT(registrations.id) AS total_reg'),
                DB::raw('SUM(if(registrations.status = 3, 1,0)) AS reg_confirm')
            )
            ->groupBy('departments.id')
            ->get()
            ->toArray();

        //        pr($registrationList);
        echo json_encode($registrationList);
        die;
    }

    public function adminGetLastTraining()
    {
        $trainingModel = new Training();
        $departmentModel = new Department();
        $registrationModel = new Registration();
        $departmentList = $departmentModel->where('status', '=', config('constants.status.Active'))
            ->pluck('name', 'id')->all();

        $trainingList = $trainingModel
            ->where('trainings.is_deleted', '!=', config('constants.is_deleted.Deleted'))
            ->leftJoin('users', 'trainings.created_by', 'users.id')
            ->leftJoin('departments', 'trainings.dept_id', 'departments.id')
            ->select(
                'users.first_name',
                'departments.name',
                'trainings.name as training_name',
                'trainings.total_day',
                'trainings.start_date',
                'trainings.exam_date',
                'trainings.training_place',
                'trainings.budget'
            )
            ->limit(5)
            ->orderBy('trainings.start_date', 'desc')
            ->get()
            ->toArray();

        //        pr($trainingList);
        echo json_encode($trainingList);
        die;
    }

    public function adminTrainingPiChart()
    {
        $trainingModel = new Training();
        $departmentModel = new Department();
        $registrationModel = new Registration();
        $departmentList = $departmentModel->where('status', '=', config('constants.status.Active'))
            ->pluck('name', 'id')->all();

        $trainingList = $trainingModel
            ->where('trainings.is_deleted', '!=', config('constants.is_deleted.Deleted'))
            ->where('departments.status', '=', config('constants.status.Active'))
            ->leftJoin('departments', 'trainings.dept_id', 'departments.id')
            ->select(
                'departments.name',
                'trainings.name as training_name', DB::raw('COUNT(trainings.id) AS total_trainings')
            )
            ->groupBy('departments.id')
            ->get()
            ->toArray();

        $totalRow = 0;
        $resArray = [];
        foreach ($trainingList as $key => $value) {
            $totalRow += $value['total_trainings'];
            $resArray[$key]['label'] = $value['name'];
            $resArray[$key]['total_trainings'] = $value['total_trainings'];
        }

        foreach ($resArray as $key => $value) {
            $percentage = floor(100 / $totalRow);
            $resArray[$key]['data'] = floor($percentage * $value['total_trainings']);
            $color = $this->random_color();
            $resArray[$key]['color'] = "#" . strtoupper($color);
        }

        echo json_encode($resArray);
        die;
    }

    private function random_color_part()
    {
        return str_pad(dechex(mt_rand(0, 255)), 2, '0', STR_PAD_LEFT);
    }

    private function random_color()
    {
        return $this->random_color_part() . $this->random_color_part() . $this->random_color_part();
    }

}