<?php

namespace App\Http\Controllers;

use App\Model\AdvanceDue;
use App\Model\Apartment;
use App\Model\ApartmentRent;
use App\Model\Bill;
use App\Model\Ledger;
use App\Model\PendingLedger;
use App\Model\Property;
use App\User;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class BillController extends Controller
{
    public function adminList()
    {
        if (isset($_GET['displayValue'])) {
            $displayValue = $_GET['displayValue'] != '' ? $_GET['displayValue'] : "";
        } else {
            $displayValue = '';
        }

        if (isset($_GET['searchData'])) {
            $searchData = $_GET['searchData'] != '' ? $_GET['searchData'] : "";
        } else {
            $searchData = '';
        }

        $apartments = new Apartment();
        $clients = new User();
        $billModel = new Bill();

        $billLists = $billModel->where('bills.status', '!=', config('constants.state.Rejected'))
            ->leftJoin('users', 'bills.created_by', 'users.id')
            ->leftJoin('apartments', 'bills.apt_id', 'apartments.id')
            ->select('users.first_name', 'users.last_name', 'apartments.apt_number', 'bills.*')
            ->paginate($displayValue);

        $users = $clients->select('first_name', 'last_name', 'id')
            ->where('status', '=', 1)
            ->where('user_type', '=', config('constants.userType.Tenant'))
            ->get();

        if (request()->ajax()) {
            return view('bill.ajax_list', compact('billLists', 'users'));
        } else {
            return view('bill.adminList', compact('billLists', 'users'));
        }
    }

    public function adminForm()
    {
        $tenantList = User::where('user_type', '=', config('constants.userType.Tenant'))->get()->pluck('full_name', 'id')->toArray();
        $yearList = dynamicYearList();
        $monthList = dynamicMonthList();

        return view('bill.adminForm', compact('tenantList', 'yearList', 'monthList'));
    }

    public function adminStore(Request $request)
    {
        $apt_id = $request->apt_id;
        $client_id = $request->client_id;

        $data = $this->validate($request, [
            'client_id' => 'required',
            'apt_id' => 'required',
            'rent_amount' => 'required',
            'service_charge' => 'required',
            'total_bill' => 'required',
            'billing_month' => 'required',
            'billing_year' => 'required',
        ], [
            'client_id.required' => 'Please Select a Client',
            'apt_id.required' => 'Please Select an Apartment',
            'rent_amount.required' => 'Rent amount is required',
            'service_charge.required' => 'Service Charge is required',
            'total_bill.required' => 'Total amount is required',
            'billing_month.required' => 'Billing Month is required',
            'billing_year.required' => 'Billing Year is required',
        ]);

        $result = $this->billGenerate($request);

        if ($result) {
            return redirect('admin/bill/list')->with('success', 'Bill Generated Successfully...');
        } else {
            return redirect('admin/bill/list')->with('error', 'Can`t create bill....');
        }
    }

    public function show($id)
    {
        //
    }

    public function adminEdit($id)
    {
        $billModel = new Bill();
        $apartmentModel = new Apartment();

        $yearList = dynamicYearList();
        $monthList = dynamicMonthList();

        $billData = $billModel->where('id', '=', $id)
            ->where('status', '!=', config('constants.state.Rejected'))
            ->first();
        $apartmentList = $apartmentModel->where('client_id', '=', $billData->client_id)->get()->pluck('apt_number', 'id')->toArray();
        $tenantList = User::where('user_type', '=', config('constants.userType.Tenant'))->get()->pluck('full_name', 'id')->toArray();

//        dd($billData);
        return view('bill.adminEdit', compact('billData', 'apartmentList', 'tenantList', 'id', 'yearList', 'monthList'));

    }

    public function adminUpdate(Request $request, $id)
    {
        $billModel = new Bill();
        $data = $this->validate($request, [
            'client_id' => 'required',
            'apt_id' => 'required',
            'rent_amount' => 'required',
            'service_charge' => 'required',
            'total_bill' => 'required',
            'billing_month' => 'required',
            'billing_year' => 'required',
        ], [
            'client_id.required' => 'Please Select a Client',
            'apt_id.required' => 'Please Select an Apartment',
            'rent_amount.required' => 'Rent amount is required',
            'service_charge.required' => 'Service Charge is required',
            'total_bill.required' => 'Total amount is required',
            'billing_month.required' => 'Billing Month is required',
            'billing_year.required' => 'Billing Year is required',
        ]);

        $updateData = [
            'comments' => $request->comments,
            'billing_year' => $request->billing_year,
            'billing_month' => $request->billing_month,
        ];

        DB::beginTransaction();
        try {
            $billModel->where('id', '=', $id)
                ->update($updateData);
            DB::commit();
            return redirect('admin/bill/list')->with('success', 'Bill Updated successfully');
        } catch (\Exception $exPledger) {
            dd($exPledger);
            DB::rollback();
            return redirect('admin/bill/list')->with('error', 'Bill Update failed!!!');
        }
    }

    public function adminDestroy($id, Request $request)
    {
        $advanceDueModel = new AdvanceDue();
        $pendingLedgerModel = new PendingLedger();
        $billModel = new Bill();
        $apartmentModel = new Apartment();


        DB::beginTransaction();
        $updateData = [
            'status' => config('constants.state.Rejected'),
        ];

        $status = "success";
        $message = "Bill Delete successfully.";

//        return response()->json(['options' => $optionData, 'dataValue' => $dataListObj]);

        if (!empty($id)) {
            $billData = $billModel->where('id', '=', $id)
                ->where('status', '!=', config('constants.state.Rejected'))
                ->get()
                ->toArray();

            $billType = $billData[0]['bill_type'];
            $client_id = $billData[0]['client_id'];
            $apt_id = $billData[0]['apt_id'];
            $pending_ledger_id = $billData[0]['pending_ledger_id'];

//            try {
//                $advanceDueModel->where('pending_ledger_id', '=', $pending_ledger_id)
//                    ->update($updateData);
//            } catch (\Exception $exPledger) {
//                DB::rollback();
//                $status = "error";
//                $message = "Bill Delete failed!!!";
//            }

            try {
                $billModel->where('id', '=', $id)
                    ->update($updateData);
            } catch (\Exception $exPledger) {
                DB::rollback();
                $status = "error";
                $message = "Bill Delete failed!!!";
            }

            try {
                $pendingLedgerModel->where('id', '=', $pending_ledger_id)
                    ->update($updateData);
                DB::commit();
            } catch (\Exception $exPledger) {
                DB::rollback();
                $status = "error";
                $message = "Bill Delete failed!!!";
            }

            if ($billType == config('constants.ledgerType.Initial')) {
                $apartmentModel->where('id', '=', $apt_id)
                    ->where('client_id', '=', $client_id)
                    ->update(['is_assigned' => config('constants.is_assigned.Assigned')]);
            }

        } else {
            $status = "error";
            $message = "No data has been found to delete!!!";
        }
        return response()->json(['status' => $status, 'message' => $message]);


//        $apartmentRentModel = ApartmentRent::where('apt_id', '=', $id)->firstOrFail();
//        if (isset($id)) {
//            $apartmentRentModel->delete();
//            $apartmentModel->delete();
//
//            $request->session()->flash('success', 'Data Deleted Successfully..');
//            return response()->json(['status' => 'success']);
//        } else {
//            $request->session()->flash('errors', 'Data Cann\'t Deleted Successfully..');
//            return response()->json(['status' => 'error']);
//        }
    }

    public function selectAjax(Request $request)
    {
        if ($request->ajax()) {
            $client_id = $request->client_id;
            $resData = getAptListByClientId($client_id);
            return response()->json($resData);
        }
    }

    public function getAptDataAjax(Request $request)
    {
        if ($request->ajax()) {
            $client_id = $request->client_id;
            $aptId = $request->aptId;
            $bill_id = 0;

            if (!empty($request->bill_id)) {
                $bill_id = $request->bill_id;
            }

            $has_pending_ledgers = checkPendingLedgers($aptId, $client_id);

            $dueAmount = 0;
            $aptData = getAptDetailsByAptAndClientId($client_id, $aptId);

            $dataSize = sizeof($aptData);


            if (!$has_pending_ledgers) {
                $resData = getAdvanceDueAmount($client_id, $aptId, $bill_id);
                $dueAmount = $resData['dueAmount'];
                $ledgerType = $resData['ledgerType'];
                $previousAmount = $resData['previousAmount'];
                return response()->json(['aptData' => $aptData[0], 'size' => $dataSize, 'dueAmount' => $dueAmount, 'ledgerType' => $ledgerType, 'previousAmount' => $previousAmount, 'error' => 0]);

            } else {
                return response()->json(['aptData' => $aptData[0], 'size' => $dataSize, 'dueAmount' => $dueAmount, 'error' => 1]);
            }

        }
    }

    private function billGenerate($request)
    {

        $pendingLedgerModel = new PendingLedger();
        $billModel = new Bill();
        $advanceDueModel = new AdvanceDue();

        $returnData = true;

        $apt_id = $request->apt_id;
        $client_id = $request->client_id;
        $rent_amount = $request->rent_amount;
        $service_charge = $request->service_charge;
        $security_bill_month = $request->security_bill_month;
        $due_amount = $request->due_amount;
        $total_bill = $request->total_bill;
        $comments = $request->comments;

        /*  Calculations    */
        $amount = (($rent_amount + $service_charge) * (-1));
        $current_amount = $amount + ($due_amount * (-1));
        $previous_amount = $due_amount * (-1);


        DB::beginTransaction();

        /*  Save Data in Pending Ledger Table Start */
        $pendingLedgerModel->apt_id = $apt_id;
        $pendingLedgerModel->client_id = $client_id;
        $pendingLedgerModel->amount = $amount;
        $pendingLedgerModel->secure_amount = 0;
        $pendingLedgerModel->previous_amount = $previous_amount;
        $pendingLedgerModel->current_amount = $current_amount;
        $pendingLedgerModel->ledger_for = config('constants.ledgerFor.Billing');
        $pendingLedgerModel->ledger_type = config('constants.ledgerType.Regular');
        $pendingLedgerModel->created_by = auth()->user()->id;
        $pendingLedgerModel->status = config('constants.state.Pending');

        try {
            $pendingLedgerModel->save();
            $lastPendingLedgerId = $pendingLedgerModel->id;
        } catch (\Exception $exPledger) {
            dd($exPledger);
            DB::rollback();
            $returnData = false;
        }
        /*  Save Data in Pending Ledger Table Ends */

        /*  Save Data in Bill Table Start */
        $billModel->apt_id = $apt_id;
        $billModel->client_id = $client_id;
        $billModel->pending_ledger_id = $lastPendingLedgerId;
        $billModel->rent_amount = $rent_amount;
        $billModel->service_charge = $service_charge;
        $billModel->security_bill = 0;
        $billModel->security_bill_month = 0;
        $billModel->total_bill = $amount;
        $billModel->current_amount = $current_amount;
        $billModel->bill_type = config('constants.ledgerType.Regular');
        $billModel->created_by = auth()->user()->id;
        $billModel->comments = $comments;
        $billModel->status = config('constants.state.Pending');
        $billModel->billing_month = $request->billing_month;
        $billModel->billing_year = $request->billing_year;

        try {
            $billModel->save();
            DB::commit();
        } catch (\Exception $exBill) {
            dd($exBill);
            DB::rollback();
            $returnData = false;
        }
        /*  Save Data in Bill Table End */

        /*  Save Data in Advance-Due Table Starts */
//        $advanceDueModel->apt_id = $apt_id;
//        $advanceDueModel->client_id = $client_id;
//        $advanceDueModel->pending_ledger_id = $lastPendingLedgerId;
//        $advanceDueModel->total_amount = $amount;
//        $advanceDueModel->previous_amount = $previous_amount;
//        $advanceDueModel->current_amount = $current_amount;
//        $advanceDueModel->amount_for = config('constants.ledgerFor.Billing');
//        $advanceDueModel->type = config('constants.ledgerType.Regular');
//        $advanceDueModel->created_by = auth()->user()->id;
//        $advanceDueModel->status = config('constants.state.Pending');

//        try {
//            $advanceDueModel->save();
//            DB::commit();
//        } catch (\Exception $exAdDu) {
//            dd($exAdDu);
//            DB::rollback();
//            $returnData = false;
//        }
        /*  Save Data in Advance-Due Table End */

        return $returnData;
    }
}
