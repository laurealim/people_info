<?php

namespace App\Http\Controllers;

use App\Model\Package;
use App\Model\PackageMapping;
use App\Model\ReliefMapping;
use Egulias\EmailValidator\Exception\ExpectingCTEXT;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PackageController extends Controller
{
    public function adminList()
    {
        if (isset($_GET['displayValue'])) {
            $displayValue = $_GET['displayValue'] != '' ? $_GET['displayValue'] : "";
        } else {
            $displayValue = '';
        }

        if (isset($_GET['searchData'])) {
            $searchData = $_GET['searchData'] != '' ? $_GET['searchData'] : "";
        } else {
            $searchData = '';
        }

        $packageModel = new Package();

        $packageList = $packageModel->where('packages.status', '!=', config('constants.package_status.Deleted'))
            ->leftJoin('users', 'packages.created_by', 'users.id')
            ->select('users.first_name', 'users.last_name', 'packages.*')
            ->orderBy('packages.id', 'DESC')
            ->paginate($displayValue);
        if (request()->ajax()) {
            return view('package.ajax_list', compact('packageList'));
        } else {
            return view('package.adminList', compact('packageList'));
        }
    }

    public function adminForm()
    {
        return view('package.adminForm');
    }

    public function adminStore(Request $request)
    {
        $packageModel = new Package();

        $data = $this->validate($request, [
            'title' => 'required',
            'p_code' => 'required',
        ], [
            'title.required' => 'Title is required',
            'p_code.required' => 'Short Code is required',

        ]);

        $packageModel->saveData($request);
        return redirect('admin/package/list')->with('success', 'New Package added successfully...');
    }

    public function show($id)
    {
        //
    }

    public function adminEdit($id)
    {
        if (auth()->user()->id == 532 || auth()->user()->id == 534) {
            throw new \Exception('Permission Deny');
        }
        $packageData = Package::where('id', $id)->first();
        return view('package.adminEdit', compact('packageData', 'id'));
    }

    public function adminUpdate(Request $request, $id)
    {
        $packageModel = new Package();
        $data = $this->validate($request, [
            'title' => 'required',
            'p_code' => 'required',
        ], [
            'title.required' => 'Title is required',
            'p_code.required' => 'Short Code is required',

        ]);

        $packageModel->updateData($request);

        return redirect('admin/package/list')->with('success', 'Package edited successfully');
    }

    public function adminDestroy($id)
    {
        $packageModel = new Package();

        DB::beginTransaction();
        $updateData = [
            'status' => config('constants.package_status.Deleted'),
        ];

        $status = "success";
        $message = "Package Delete successfully.";

        if (!empty($id)) {
            try {
                $packageModel->where('id', '=', $id)
                    ->update($updateData);
                DB::commit();
            } catch (\Exception $exPledger) {
                DB::rollback();
                $status = "error";
                $message = "Package Delete failed!!!";
            }
        } else {
            $status = "error";
            $message = "No data has been found to delete!!!";
        }
        return response()->json(['status' => $status, 'message' => $message]);
    }

    public function adminAssignPackage(Request $request)
    {
        if ($request->ajax()) {
            $packageModel = new Package();
            $reliefMapModel = new ReliefMapping();
            $packageMapModel = new PackageMapping();

            $regID = $request->post('regId');
            $packID = $request->post('packageId');
            if (empty($request->post('last_give_pac'))) {
                $last_give_pac = date('Y-m-d', time());
            } else {
                $last_give_pac = date('Y-m-d', strtotime($request->post('last_give_pac')));
            }

            try {

                $reliefMapList = ReliefMapping::where('reg_id', $regID)->first();
                $packMapList = PackageMapping::where('pack_id', $packID)->first();

                $rlfStr = "";
                $rlfDateStr = "";
                $pacStr = "";
                if (!empty($reliefMapList)) {
                    $rlfStr .= $reliefMapList['packeges'];
                    $rlfStr .= $packID . ',';
                    $rlfDateStr .= $reliefMapList['delv_date'];
                    $rlfDateStr .= $last_give_pac . ',';
                    ReliefMapping::where('reg_id', $regID)->update(['packeges' => $rlfStr, 'last_give_pac' => $last_give_pac, 'delv_date' => $rlfDateStr]);
                } else {
                    $rlfStr .= $packID . ',';
                    $rlfDateStr .= $last_give_pac . ',';
                    $insertDataRlf = [
                        'reg_id' => $regID,
                        'packeges' => $rlfStr,
                        'last_give_pac' => $last_give_pac,
                        'delv_date' => $rlfDateStr
                    ];
                    ReliefMapping::insert($insertDataRlf);
                }

                if (!empty($packMapList)) {
                    $pacStr .= $packMapList['registrations'];
                    $pacStr .= $regID . ',';
                    PackageMapping::where('pack_id', $packID)->update(['registrations' => $pacStr]);
                } else {
                    $pacStr .= $regID . ',';
                    $insertDatapac = [
                        'pack_id' => $packID,
                        'registrations' => $pacStr,
                    ];
                    PackageMapping::insert($insertDatapac);
                }

                $resData = [
                    'status' => '1',
                    'message' => 'Successfully Assign to the Family',
                ];

                return json_encode($resData);

            } catch (\Exception $exception) {
                $resData = [
                    'status' => '2',
                    'message' => 'Error',
                ];

                dd($exception);
                return json_encode($resData);
            }


        }
    }

    public function adminPackageListByRegId(Request $request)
    {
        return $this->_unionSelectAjaxList($request);
    }

    public function adminFamilyWiseList(Request $request, $id)
    {

        $packageModel = new Package();
        $reliefMappingModel = new ReliefMapping();
        $packageList = $packageModel->pluck('title', 'id')->all();

        $queryString = $reliefMappingModel::query();
        $queryString->select('relief_mappings.id', 'relief_mappings.reg_id', 'relief_mappings.packeges', 'relief_mappings.last_give_pac', 'relief_mappings.delv_date', 'reliefs.card_no', 'reliefs.nid', 'reliefs.phone', 'reliefs.name')
            ->where('reliefs.is_deleted', '!=', config('constants.is_deleted.Deleted'))
            ->where('reliefs.id', '=', $id)
            ->leftJoin('reliefs', 'reliefs.id', '=', 'relief_mappings.reg_id');

        $packageData = $queryString->get();
        $packageDataArr = $queryString->get()->toArray();
        $finalPacListArr = [];
        if (!empty($packageDataArr)) {
            $packArr = array_filter(explode(',', $packageDataArr[0]['packeges']));
            $dateDataArr = array_filter(explode(',', $packageDataArr[0]['delv_date']));

            foreach ($packArr as $key => $value) {
                $finalPacListArr[$value] = $dateDataArr[$key];
            }
        }
        return view('package.adminFamilyWiseList', compact('finalPacListArr', 'packageList', 'packageData'));
    }

    public function adminSaveFamilyWiseList(Request $request)
    {
        $reg_id = $request->post('reg_id');
        if (!empty($request->post('pac'))) {
            try {
                DB::beginTransaction();
                $postData = $request->post('pac');
                $packeges = '';
                $last_give_pac = '1970-01-01';
                $delv_date = '';
                $updated_by = auth()->user()->id;

                foreach ($postData as $pacId => $date) {
                    $packeges .= $pacId . ',';
                    $delv_date .= $date . ',';
                    if ($last_give_pac < $date)
                        $last_give_pac = $date;
                }
                $updateDataArr = [
                    'packeges' => $packeges,
                    'last_give_pac' => $last_give_pac,
                    'delv_date' => $delv_date,
                    'updated_by' => $updated_by,
                ];

                ReliefMapping::where('reg_id', '=', $reg_id)->update($updateDataArr);

                DB::commit();
//                $request->session()->flash('success', 'Success full');
                return redirect()->back()->with('success', 'Package Update Successfully.');
            } catch (\Exception $exception) {
                DB::rollback();
//                return redirect()->back()->with('success', 'Update Successfully.');
                $request->session()->flash('error', $exception);
            }
        } else {
            try {
                DB::beginTransaction();

                ReliefMapping::where('reg_id', $reg_id)->delete();

                DB::commit();
                return redirect()->back()->with('success', 'Package Update Successfully.');
            } catch (\Exception $delExc) {
                DB::rollback();
                $request->session()->flash('error', $delExc);
            }
        }
    }

    private function _unionSelectAjaxList($request)
    {
        if ($request->ajax()) {

            $packageModel = new Package();
            $reliefMapModel = new ReliefMapping();

            $regID = $request->post('regId');
            $packageList = array();
            $packageData = ReliefMapping::where('reg_id', $regID)->first();
            $pacList = [];
            $delvDateList = [];
            if (!empty($packageData)) {
                $pacList = $packageData['packeges'];
                $delvDateList = $packageData['delv_date'];
                $result = array_filter(explode(',', $pacList));

                $pacList = array_filter(explode(',', $pacList));
                $delvDateList = array_filter(explode(',', $delvDateList));

                $packageList = $packageModel
                    ->where('status', '!=', 0)
                    ->whereNotIn('id', $result)
                    ->pluck('title', 'id')
                    ->all();
                $fullPackageList = $packageModel
                    ->where('status', '!=', 0)
                    ->pluck('title', 'id')
                    ->all();

                return json_encode(['package_list' => $packageList, 'assign_package' => $pacList, 'delv_date' => $delvDateList, 'full_package_list' => $fullPackageList]);
            } else {
                $packageList = $packageModel
                    ->where('status', '!=', 0)
                    ->pluck('title', 'id')
                    ->all();
                return json_encode(['package_list' => $packageList, 'assign_package' => $pacList, 'delv_date' => $delvDateList]);
            }
        }
    }
}
