<?php

namespace App\Http\Controllers;

use App\Model\Event;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EventController extends Controller
{
    public function adminList()
    {
        if (isset($_GET['displayValue'])) {
            $displayValue = $_GET['displayValue'] != '' ? $_GET['displayValue'] : "";
        } else {
            $displayValue = '';
        }

        if (isset($_GET['searchData'])) {
            $searchData = $_GET['searchData'] != '' ? $_GET['searchData'] : "";
        } else {
            $searchData = '';
        }

        $events = new Event();

        $events = $events->where('events.name', 'like', '%' . $searchData . '%')
            ->leftJoin('users', 'events.created_by', 'users.id')
            ->select('events.*', 'users.first_name')
            ->paginate($displayValue);


        if (request()->ajax()) {
            return view('event.ajax_list', compact('events'));
        } else {
            return view('event.adminList', compact('events'));
        }
    }

    public function adminForm()
    {
        return view('event.adminForm');
    }

    public function adminStore(Request $request)
    {
        $eventModel = new Event();
        $data = $this->validate($request,[
            'name' => 'required',
        ]);

        $eventModel->saveData($request);
        return redirect('admin/event/list')->with('success','New Event type added successfully');
    }

    public function show(Event $event)
    {
        //
    }

    public function adminEdit($id)
    {
        $eventData = Event::where('id', $id)->first();

        return view('event.adminEdit', compact('eventData', 'id'));
    }

    public function adminUpdate(Request $request, $id)
    {
        $eventModel = new Event();
        $data = $this->validate($request, [
            'name' => 'required',
        ]);

        $eventModel->updateData($request);

        return redirect('admin/event/list')->with('success','Event type edited successfully');
    }

    public function adminDestroy(Request $request, Event $event)
    {
        $eventInfo = Event::findOrFail($request->id);
        if(isset($request->id)){
            $eventInfo->delete();
            $request->session()->flash('success', 'Data Deleted Successfully..');
            return response()->json(['status'=>'success']);
        }
        else{
            $request->session()->flash('errors', 'Data Deleted Successfully..');
            return response()->json(['status'=>'error']);
        }
    }
}
