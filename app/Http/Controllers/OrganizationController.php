<?php

namespace App\Http\Controllers;

use App\Model\Organization;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrganizationController extends Controller
{
    public function adminList()
    {
        if (isset($_GET['displayValue'])) {
            $displayValue = $_GET['displayValue'] != '' ? $_GET['displayValue'] : "";
        } else {
            $displayValue = '';
        }

        if (isset($_GET['searchData'])) {
            $searchData = $_GET['searchData'] != '' ? $_GET['searchData'] : "";
        } else {
            $searchData = '';
        }

        $organizations = new Organization();

        $organizations = $organizations->where('organizations.name', 'like', '%' . $searchData . '%')
            ->orwhere('organizations.phone', 'like', '%' . $searchData . '%')
            ->leftJoin('users', 'organizations.created_by', 'users.id')
            ->select('organizations.*', 'users.first_name')
            ->paginate($displayValue);


        if (request()->ajax()) {
            return view('organization.ajax_list',compact('organizations'));
        } else {
            return view('organization.adminList',compact('organizations'));
        }
    }

    public function adminForm()
    {
        return view('organization.adminForm');
    }

    public function adminStore(Request $request)
    {
        $organizationyModel = new Organization();
        $data = $this->validate($request,[
            'name' => 'required',
            'type' => 'required',
            'address' => 'required',
            'phone' => 'required',
        ]);

        $organizationyModel->saveData($request);
        return redirect('admin/organization/list')->with('success','New Organization Added Successfully');
    }


    public function show(Organization $organization)
    {
        //
    }

    public function adminEdit($id, Organization $organization)
    {
        $organizationData = Organization::where('id', $id)->first();
//        dd($organizationData);
        return view('organization.adminEdit', compact('organizationData', 'id'));
    }

    public function adminUpdate(Request $request, Organization $organization)
    {
        $organizationModel = new Organization();
        $data = $this->validate($request, [
            'name' => 'required',
            'type' => 'required',
            'address' => 'required',
            'phone' => 'required',
        ]);

        $organizationModel->updateData($request);

        return redirect('admin/organization/list')->with('success','Organization Edited successfully');
    }

    public function adminDestroy(Organization $organization)
    {
        //
    }
}
