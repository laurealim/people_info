<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Payment;
use App\Model\AdvanceDue;
use App\Model\PendingLedger;
use App\Model\Bill;
use App\Model\Apartment;
use App\User;
use Illuminate\Support\Facades\DB;

class PaymentController extends Controller
{
    public function adminList()
    {
        if (isset($_GET['displayValue'])) {
            $displayValue = $_GET['displayValue'] != '' ? $_GET['displayValue'] : "";
        } else {
            $displayValue = '';
        }

        if (isset($_GET['searchData'])) {
            $searchData = $_GET['searchData'] != '' ? $_GET['searchData'] : "";
        } else {
            $searchData = '';
        }

        $clients = new User();
        $advanceDueModel = new AdvanceDue();
        $paymentModel = new Payment();

        $paymentLists = $paymentModel->where('payments.status', '!=', config('constants.state.Rejected'))
            ->leftJoin('users', 'payments.client_id', 'users.id')
            ->leftJoin('apartments', 'payments.apt_id', 'apartments.id')
            ->leftJoin('advance_dues', function ($join) {
                $join->on('advance_dues.apt_id', '=', 'payments.apt_id')
                    ->on('advance_dues.client_id', '=', 'payments.client_id');
            })
            ->select('users.first_name', 'users.last_name', 'apartments.apt_number', 'advance_dues.current_amount', 'advance_dues.previous_amount', 'advance_dues.total_amount', 'payments.*')
            ->paginate($displayValue);

        $users = $clients->select('first_name', 'last_name', 'id')
            ->where('status', '=', 1)
            ->where('user_type', '=', config('constants.userType.Tenant'))
            ->get();

        if (request()->ajax()) {
            return view('payment.ajax_list', compact('paymentLists', 'users'));
        } else {
            return view('payment.adminList', compact('paymentLists', 'users'));
        }
    }

    public function adminForm()
    {
        $tenantList = User::where('user_type', '=', config('constants.userType.Tenant'))->get()->pluck('full_name', 'id')->toArray();
        $yearList = dynamicYearList();
        $monthList = dynamicMonthList();
        $ledgerType = ledgerType();

        return view('payment.adminForm', compact('tenantList', 'yearList', 'monthList', 'ledgerType'));
    }

    public function adminStore(Request $request)
    {
        $apt_id = $request->apt_id;
        $client_id = $request->client_id;

        $data = $this->validate($request, [
            'client_id' => 'required',
            'apt_id' => 'required',
            'amount' => 'required',
            'pre_ad_du' => 'required',
            'payable' => 'required',
            'total_bill_payment' => 'required',
            'payment_type' => 'required',
            'payment_month' => 'required',
            'payment_year' => 'required',
        ], [
            'client_id.required' => 'Please Select a Client',
            'apt_id.required' => 'Please Select an Apartment',
            'amount.required' => 'Rent amount is required',
            'pre_ad_du.required' => 'Previous amount is required',
            'payable.required' => 'Total Payable amount is required',
            'total_bill_payment.required' => 'Total payment amount is required',
            'payment_type.required' => 'Payment Type is required',
            'payment_month.required' => 'Payment Month is required',
            'payment_year.required' => 'Payment Year is required',
        ]);

        $result = $this->billGenerate($request);

        if ($result) {
            return redirect('admin/payment/list')->with('success', 'Payment Generated Successfully...');
        } else {
            return redirect('admin/payment/list')->with('error', 'Can`t create payment....');
        }
    }

    public function adminEdit($id)
    {
        $paymentModel = new Payment();
        $apartmentModel = new Apartment();
        $advanceDueModel = new AdvanceDue();

        $yearList = dynamicYearList();
        $monthList = dynamicMonthList();
        $ledgerType = ledgerType();

        $paymentData = $paymentModel->where('id', '=', $id)
            ->where('status', '!=', config('constants.state.Rejected'))
            ->first();
        $apartmentList = $apartmentModel->where('client_id', '=', $paymentData->client_id)->get()->pluck('apt_number', 'id')->toArray();
        $tenantList = User::where('user_type', '=', config('constants.userType.Tenant'))->get()->pluck('full_name', 'id')->toArray();
        $billAmountData = $advanceDueModel->where('advance_dues.client_id', '=', $paymentData->client_id)
            ->where('advance_dues.apt_id', '=', $paymentData->apt_id)
            ->where('advance_dues.status', '=', config('constants.state.Approved'))
            ->first();

        return view('payment.adminEdit', compact('paymentData', 'apartmentList', 'tenantList', 'id', 'yearList', 'monthList', 'ledgerType', 'billAmountData'));

    }

    public function adminUpdate(Request $request, $id)
    {
        $paymentModel = new Payment();
        $pendingLedgerModel = new PendingLedger();
        $data = $this->validate($request, [
            'client_id' => 'required',
            'apt_id' => 'required',
            'amount' => 'required',
            'pre_ad_du' => 'required',
            'payable' => 'required',
            'total_bill_payment' => 'required',
            'payment_type' => 'required',
            'payment_month' => 'required',
            'payment_year' => 'required',
        ], [
            'client_id.required' => 'Please Select a Client',
            'apt_id.required' => 'Please Select an Apartment',
            'amount.required' => 'Rent amount is required',
            'pre_ad_du.required' => 'Previous amount is required',
            'payable.required' => 'Total Payable amount is required',
            'total_bill_payment.required' => 'Total payment amount is required',
            'payment_type.required' => 'Payment Type is required',
            'payment_month.required' => 'Payment Month is required',
            'payment_year.required' => 'Payment Year is required',
        ]);

        $paymentData = $paymentModel->where('id', '=', $id)
            ->where('status', '!=', config('constants.state.Rejected'))
            ->first();

        $pendingLedgerId = $paymentData->pending_ledger_id;

        $updateData = [
            'comments' => $request->comments,
            'total_bill_payment' => $request->total_bill_payment,
            'payment_year' => $request->payment_year,
            'payment_month' => $request->payment_month,
        ];

        $updatePendingLedgerData = [
            'amount' => $request->total_bill_payment,
            'current_amount' => $request->total_bill_payment - $request->payable,
        ];

        DB::beginTransaction();

        try {
            $pendingLedgerModel->where('id', '=', $pendingLedgerId)
                ->update($updatePendingLedgerData);
        } catch (\Exception $ex) {
            dd($ex);
            DB::rollback();
            return redirect('admin/payment/list')->with('error', 'Payment Update failed!!!');
        }

        try {
            $paymentModel->where('id', '=', $id)
                ->update($updateData);
            DB::commit();
            return redirect('admin/payment/list')->with('success', 'Payment Updated successfully');
        } catch (\Exception $exPledger) {
            dd($exPledger);
            DB::rollback();
            return redirect('admin/payment/list')->with('error', 'Payment Update failed!!!');
        }
    }

    public function adminDestroy($id, Request $request)
    {
        $advanceDueModel = new AdvanceDue();
        $pendingLedgerModel = new PendingLedger();
        $paymentModel = new Payment();
        $apartmentModel = new Apartment();


        DB::beginTransaction();
        $updateData = [
            'status' => config('constants.state.Rejected'),
        ];

        $status = "success";
        $message = "Payment Delete successfully.";

//        return response()->json(['options' => $optionData, 'dataValue' => $dataListObj]);

        if (!empty($id)) {
            $paymentData = $paymentModel->where('id', '=', $id)
                ->where('status', '!=', config('constants.state.Rejected'))
                ->get()
                ->toArray();

            $paymentType = $paymentData[0]['payment_type'];
            $client_id = $paymentData[0]['client_id'];
            $apt_id = $paymentData[0]['apt_id'];
            $pending_ledger_id = $paymentData[0]['pending_ledger_id'];

//            try {
//                $advanceDueModel->where('pending_ledger_id', '=', $pending_ledger_id)
//                    ->update($updateData);
//            } catch (\Exception $exPledger) {
//                DB::rollback();
//                $status = "error";
//                $message = "Bill Delete failed!!!";
//            }

            try {
                $paymentModel->where('id', '=', $id)
                    ->update($updateData);
            } catch (\Exception $exPledger) {
                DB::rollback();
                $status = "error";
                $message = "Payment Delete failed!!!";
            }

            try {
                $pendingLedgerModel->where('id', '=', $pending_ledger_id)
                    ->update($updateData);
                DB::commit();
            } catch (\Exception $exPledger) {
                DB::rollback();
                $status = "error";
                $message = "Payment Delete failed!!!";
            }

        } else {
            $status = "error";
            $message = "No data has been found to delete!!!";
        }
        return response()->json(['status' => $status, 'message' => $message]);
    }

    private function billGenerate($request)
    {

        $pendingLedgerModel = new PendingLedger();
        $billModel = new Bill();
        $paymentModel = new Payment();
        $advanceDueModel = new AdvanceDue();

        $returnData = true;

//        pr($request);
//        dd($request);

        $apt_id = $request->apt_id;
        $client_id = $request->client_id;

        $payment_month = $request->payment_month;
        $payment_year = $request->payment_year;

        $rent_amount = $request->amount;
        $pre_ad_du = $request->pre_ad_du;
        $total_bill = $request->payable;

        $total_payment = $request->total_bill_payment;
        $payment_type = $request->payment_type;
        $comments = $request->comments;


        /*  Calculations    */
        $amount = $total_payment;
        $current_amount = ($total_bill * (-1) + $total_payment);
        $previous_amount = $total_bill * (-1);


        DB::beginTransaction();

        /*  Save Data in Pending Ledger Table Start */
        $pendingLedgerModel->apt_id = $apt_id;
        $pendingLedgerModel->client_id = $client_id;
        $pendingLedgerModel->amount = $amount;
        $pendingLedgerModel->secure_amount = 0;
        $pendingLedgerModel->previous_amount = $previous_amount;
        $pendingLedgerModel->current_amount = $current_amount;
        $pendingLedgerModel->ledger_for = config('constants.ledgerFor.Payment');
        $pendingLedgerModel->ledger_type = $payment_type;
        $pendingLedgerModel->created_by = auth()->user()->id;
        $pendingLedgerModel->status = config('constants.state.Pending');

        try {
            $pendingLedgerModel->save();
            $lastPendingLedgerId = $pendingLedgerModel->id;
        } catch (\Exception $exPledger) {
            dd($exPledger);
            DB::rollback();
            $returnData = false;
        }
        /*  Save Data in Pending Ledger Table Ends */

        /*  Save Data in Payment Table Start */
        $paymentModel->apt_id = $apt_id;
        $paymentModel->client_id = $client_id;
        $paymentModel->total_bill_payment = $amount;
        $paymentModel->payment_month = $payment_month;
        $paymentModel->payment_year = $payment_year;
        $paymentModel->pending_ledger_id = $lastPendingLedgerId;
        $paymentModel->ledger_id = 0;
        $paymentModel->payment_type = $payment_type;
        $paymentModel->comments = $comments;
        $paymentModel->created_by = auth()->user()->id;
        $paymentModel->status = config('constants.state.Pending');

        try {
            $paymentModel->save();
            DB::commit();
        } catch (\Exception $exPayment) {
            dd($exPayment);
            DB::rollback();
            $returnData = false;
        }
        /*  Save Data in Bill Table End */

        /*  Save Data in Advance-Due Table Starts */
//        $advanceDueModel->apt_id = $apt_id;
//        $advanceDueModel->client_id = $client_id;
//        $advanceDueModel->pending_ledger_id = $lastPendingLedgerId;
//        $advanceDueModel->total_amount = $amount;
//        $advanceDueModel->previous_amount = $previous_amount;
//        $advanceDueModel->current_amount = $current_amount;
//        $advanceDueModel->amount_for = config('constants.ledgerFor.Billing');
//        $advanceDueModel->type = config('constants.ledgerType.Regular');
//        $advanceDueModel->created_by = auth()->user()->id;
//        $advanceDueModel->status = config('constants.state.Pending');

//        try {
//            $advanceDueModel->save();
//            DB::commit();
//        } catch (\Exception $exAdDu) {
//            dd($exAdDu);
//            DB::rollback();
//            $returnData = false;
//        }
        /*  Save Data in Advance-Due Table End */

        return $returnData;
    }

    public function selectAjax(Request $request)
    {
        if ($request->ajax()) {
            $client_id = $request->client_id;
            $resData = getAptListByClientId($client_id);
            return response()->json($resData);
        }
    }

    public function getAptDataAjax(Request $request)
    {
        if ($request->ajax()) {
            $client_id = $request->client_id;
            $aptId = $request->aptId;
            $bill_id = 0;

            if (!empty($request->bill_id)) {
                $bill_id = $request->bill_id;
            }

            $has_pending_ledgers = checkPendingLedgers($aptId, $client_id);

            $dueAmount = 0;
            $aptData = getAptDetailsByAptAndClientId($client_id, $aptId);

            $dataSize = sizeof($aptData);


            if (!$has_pending_ledgers) {
                $resData = getAdvanceDueAmount($client_id, $aptId, $bill_id);
                $dueAmount = $resData['dueAmount'];
                $ledgerType = $resData['ledgerType'];
                $previousAmount = $resData['previousAmount'];
                return response()->json(['aptData' => $aptData[0], 'size' => $dataSize, 'dueAmount' => $dueAmount, 'ledgerType' => $ledgerType, 'previousAmount' => $previousAmount, 'error' => 0]);

            } else {
                return response()->json(['aptData' => $aptData[0], 'size' => $dataSize, 'dueAmount' => $dueAmount, 'error' => 1]);
            }

        }
    }
}
