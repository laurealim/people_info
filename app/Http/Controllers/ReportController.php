<?php

namespace App\Http\Controllers;

use App\Model\Department;
use App\Model\District;
use App\Model\Package;
use App\Model\Registration;
use App\Model\Relief;
use App\Model\ReliefDetails;
use App\Model\ReliefFamilySearch;
use App\Model\ReliefMapping;
use App\Model\Ssnp;
use App\Model\Trade;
use App\Model\Training;
use App\Model\Upazila;
use App\Model\VwKashiyani;
use App\Model\VwKotalipara;
use App\Model\VwMuksudpur;
use App\Model\VwSadar;
use App\Model\VwTungipara;
use Auth;
use Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

ini_set('memory_limit', '1024M');
set_time_limit(0);

class ReportController extends Controller
{

    /*  Admin Section   */
    public function adminRegistrationReport()
    {
        $districtModel = new District();
        $upazilaModel = new Upazila();
        $reliefModel = new Relief();
        $ssnpModel = new Ssnp();
        $packagesModel = new Package();


        $districtList = $districtModel->where('id', '=', 51)->pluck('bn_name', 'id')->all();
        $upazilaList = $upazilaModel->where('district_id', '=', 51)->pluck('bn_name', 'id')->all();
        $ssnpList = $ssnpModel->pluck('ssnp_name', 'id')->all();
        $packageList = $packagesModel->where('status', '!=', config('constants.package_status.Deleted'))->pluck('title', 'id')->all();

        $queryString = $reliefModel::query();
        $isDisplay = 1;

        $queryString->select("ssnps.ssnp_name AS ssnp_name", "districts.bn_name AS dis_name", "upazilas.bn_name AS upa_name", "unions.bn_name AS uni_name", "users.first_name", 'reliefs.id', 'reliefs.card_no', 'reliefs.card_type', 'reliefs.trible', 'reliefs.name', 'reliefs.g_name', 'reliefs.total_member', 'reliefs.hw_name', 'reliefs.age', 'reliefs.gender', 'reliefs.house_pre', 'reliefs.road_pre', 'reliefs.word_pre', 'reliefs.upo_pre', 'reliefs.uni_pre', 'reliefs.holding_pre', 'reliefs.dist_pre', 'reliefs.profession', 'reliefs.phone', 'reliefs.nid', 'reliefs.ssnp', 'reliefs.total_income', 'reliefs.poss_help', 'reliefs.total_spend', 'reliefs.dist_pre', 'relief_mappings.reg_id', 'relief_mappings.packeges', 'relief_mappings.delv_date', 'relief_mappings.last_give_pac', 'reliefs.tot_child')
            ->where('reliefs.is_deleted', '!=', config('constants.is_deleted.Deleted'))
            ->leftJoin('users', 'reliefs.created_by', 'users.id')
            ->leftJoin('upazilas', 'upazilas.id', '=', 'reliefs.upo_pre')
            ->leftJoin('unions', 'unions.id', '=', 'reliefs.uni_pre')
            ->leftJoin('districts', 'districts.id', '=', 'reliefs.dist_pre')
            ->leftJoin('relief_mappings', 'relief_mappings.reg_id', '=', 'reliefs.id')
            ->leftJoin('ssnps', 'ssnps.id', '=', 'reliefs.ssnp');

        if (request()->ajax()) {
            if (isset($_GET['dist']) && !empty($_GET['dist'])) {
                $dist = $_GET['dist'];
                $queryString->where('reliefs.dist_pre', '=', $_GET['dist']);
            }
            if (isset($_GET['upo']) && !empty($_GET['upo'])) {
                $upo = $_GET['upo'];
                $queryString->where('reliefs.upo_pre', '=', $_GET['upo']);
            }
            if (isset($_GET['uni_per'])) {
                $uni_per = $_GET['uni_per'];
                if (!empty($_GET['uni_per']))
                    $queryString->where('reliefs.uni_pre', '=', $_GET['uni_per']);
                if ($_GET['uni_per'] === '0') {
                    $queryString->where('reliefs.uni_pre', '=', $_GET['uni_per']);
                }
            }
            if (isset($_GET['word_no']) && !empty($_GET['word_no'])) {
                $word_no = $_GET['word_no'];
                $queryString->where('reliefs.word_pre', '=', $_GET['word_no']);
            }
            if (isset($_GET['name']) && !empty($_GET['name'])) {
                $name = $_GET['name'];
                $queryString->where('reliefs.name', 'like', "%" . $_GET['name'] . "%");
            }
            if (isset($_GET['phone']) && !empty($_GET['phone'])) {
                $phone = $_GET['phone'];
                $queryString->where('reliefs.phone', 'like', "%" . $_GET['phone'] . "%");
            }
            if (isset($_GET['nid']) && !empty($_GET['nid'])) {
                $nid = $_GET['nid'];
                $queryString->where('reliefs.nid', 'like', "%" . $_GET['nid'] . "%");
            }
            if (isset($_GET['card_type']) && !empty($_GET['card_type'])) {
                $card_type = $_GET['card_type'];
                $queryString->where('reliefs.card_type', '=', $_GET['card_type']);
            }
            if (isset($_GET['trible']) && !empty($_GET['trible'])) {
                $trible = $_GET['trible'];
                if ($trible == -1) {
                    $queryString->where('reliefs.trible', '=', 0);
                } else {
                    $queryString->Where(function ($query) use ($trible) {
                        $query->where('reliefs.trible', '=', $trible);
                    });
                }
            }
            if (isset($_GET['card_no']) && !empty($_GET['card_no'])) {
                $card_no = $_GET['card_no'];
                $queryString->where('reliefs.card_no', 'like', "%" . $_GET['card_no'] . "%");
            }
            if (isset($_GET['poss_help']) && !empty($_GET['poss_help'])) {
                $poss_help = $_GET['poss_help'];
                if ($poss_help == -1) {
                    $queryString->where('reliefs.poss_help', '!=', 1);
                } else {
                    $queryString->where('reliefs.poss_help', '=', $_GET['poss_help']);
                }
            }
            if (isset($_GET['ssnp']) && !empty($_GET['ssnp'])) {
                $ssnp = $_GET['ssnp'];
                if ($ssnp == -1)
                    $queryString->where('reliefs.ssnp', '=', 0);
                else
                    $queryString->where('reliefs.ssnp', '=', $_GET['ssnp']);
            }
            if (isset($_GET['packages']) && !empty($_GET['packages'])) {
                $packages = $_GET['packages'];
                if ($packages == -1)
                    $queryString->whereNull('relief_mappings.packeges');
                else
                    $queryString->whereRaw("find_in_set({$_GET['packages']},relief_mappings.packeges)");

            }
            if (isset($_GET['baby_food']) && !empty($_GET['baby_food'])) {
                $baby_food = $_GET['baby_food'];
                if ($baby_food == 1)
                    $queryString->where('reliefs.tot_child', '>=', 1);
                else
                    $queryString->where('reliefs.tot_child', '=', 0);

            }
            $reliefData = $queryString->get();
            //            $reliefData = $queryString->toSql();
//            dd($reliefData);
            $count = $reliefData->count();
            $isDisplay = 2;
            return view('report.ajax_registration_report', compact('reliefData', 'districtList', 'upazilaList', 'isDisplay', 'count', 'ssnpList', 'packageList'));
        } else {
            //            $reliefData = $queryString->get();
//            return view('report.adminRegistrationReport', compact('reliefData', 'districtList', 'upazilaList', 'training_id', 'reg_status', 'trade_name', 'registrationStatusArr', 'titleName', 'tradeList', 'isDisplay', 'ssnpList', 'packageList'));
            return view('report.adminRegistrationReport', compact('districtList', 'upazilaList', 'isDisplay', 'ssnpList', 'packageList'));
        }
    }

    public function adminMasterRollReport()
    {
        $districtModel = new District();
        $upazilaModel = new Upazila();
        $reliefModel = new Relief();
        $ssnpModel = new Ssnp();
        $packagesModel = new Package();

        try {
            $districtList = $districtModel->where('id', '=', 51)->pluck('bn_name', 'id')->all();
            $upazilaList = $upazilaModel->where('district_id', '=', 51)->pluck('bn_name', 'id')->all();
            $ssnpList = $ssnpModel->pluck('ssnp_name', 'id')->all();
            $packageList = $packagesModel->where('status', '!=', config('constants.package_status.Deleted'))->pluck('title', 'id')->all();

            $queryString = $reliefModel::query();

            $isDisplay = 1;

            $queryString->select("ssnps.ssnp_name AS ssnp_name", "districts.bn_name AS dis_name", "upazilas.bn_name AS upa_name", "unions.bn_name AS uni_name", "users.first_name", 'reliefs.id', 'reliefs.card_no', 'reliefs.card_type', 'reliefs.name', 'reliefs.g_name', 'reliefs.total_member', 'reliefs.hw_name', 'reliefs.age', 'reliefs.gender', 'reliefs.house_pre', 'reliefs.road_pre', 'reliefs.word_pre', 'reliefs.upo_pre', 'reliefs.uni_pre', 'reliefs.holding_pre', 'reliefs.dist_pre', 'reliefs.trible', 'reliefs.profession', 'reliefs.phone', 'reliefs.nid', 'reliefs.ssnp', 'reliefs.total_income', 'reliefs.poss_help', 'reliefs.total_spend', 'reliefs.dist_pre', 'relief_mappings.reg_id', 'relief_mappings.packeges', 'relief_mappings.delv_date', 'relief_mappings.last_give_pac')
                ->where('reliefs.is_deleted', '!=', config('constants.is_deleted.Deleted'))
                ->leftJoin('users', 'reliefs.created_by', 'users.id')
                ->leftJoin('upazilas', 'upazilas.id', '=', 'reliefs.upo_pre')
                ->leftJoin('unions', 'unions.id', '=', 'reliefs.uni_pre')
                ->leftJoin('districts', 'districts.id', '=', 'reliefs.dist_pre')
                ->leftJoin('relief_mappings', 'relief_mappings.reg_id', '=', 'reliefs.id')
                ->leftJoin('ssnps', 'ssnps.id', '=', 'reliefs.ssnp');

            //dd($isDisplay);
            if (request()->ajax()) {
                if (isset($_GET['dist']) && !empty($_GET['dist'])) {
                    $dist = $_GET['dist'];
                    $queryString->where('reliefs.dist_pre', '=', $_GET['dist']);
                }
                if (isset($_GET['upo']) && !empty($_GET['upo'])) {
                    $upo = $_GET['upo'];
                    $queryString->where('reliefs.upo_pre', '=', $_GET['upo']);
                }
                if (isset($_GET['uni_per'])) {
                    $uni_per = $_GET['uni_per'];
                    if (!empty($_GET['uni_per']))
                        $queryString->where('reliefs.uni_pre', '=', $_GET['uni_per']);
                    if ($_GET['uni_per'] === '0') {
                        $queryString->where('reliefs.uni_pre', '=', $_GET['uni_per']);
                    }
                }
                if (isset($_GET['word_no']) && !empty($_GET['word_no'])) {
                    $word_no = $_GET['word_no'];
                    $queryString->where('reliefs.word_pre', '=', $_GET['word_no']);
                }
                if (isset($_GET['name']) && !empty($_GET['name'])) {
                    $name = $_GET['name'];
                    $queryString->where('reliefs.name', 'like', "%" . $_GET['name'] . "%");
                }
                if (isset($_GET['phone']) && !empty($_GET['phone'])) {
                    $phone = $_GET['phone'];
                    $queryString->where('reliefs.phone', 'like', "%" . $_GET['phone'] . "%");
                }
                if (isset($_GET['nid']) && !empty($_GET['nid'])) {
                    $nid = $_GET['nid'];
                    $queryString->where('reliefs.nid', 'like', "%" . $_GET['nid'] . "%");
                }
                if (isset($_GET['card_type']) && !empty($_GET['card_type'])) {
                    $card_type = $_GET['card_type'];
                    $queryString->where('reliefs.card_type', '=', $_GET['card_type']);
                }
                if (isset($_GET['trible']) && !empty($_GET['trible'])) {
                    $trible = $_GET['trible'];
                    if ($trible == -1) {
                        $queryString->where('reliefs.trible', '=', 0);
                    } else {
                        $queryString->Where(function ($query) use ($trible) {
                            $query->where('reliefs.trible', '=', $trible);
                        });
                    }
                }
                if (isset($_GET['poss_help']) && !empty($_GET['poss_help'])) {
                    $poss_help = $_GET['poss_help'];
                    $queryString->where('reliefs.poss_help', '=', $_GET['poss_help']);
                }
                if (isset($_GET['card_no']) && !empty($_GET['card_no'])) {
                    $card_no = $_GET['card_no'];
                    $queryString->where('reliefs.card_no', 'like', "%" . $_GET['card_no'] . "%");
                }
                if (isset($_GET['ssnp']) && !empty($_GET['ssnp'])) {
                    $ssnp = $_GET['ssnp'];
                    if ($ssnp == -1)
                        $queryString->where('reliefs.ssnp', '=', 0);
                    else
                        $queryString->where('reliefs.ssnp', '=', $_GET['ssnp']);
                }
                if (isset($_GET['packages']) && !empty($_GET['packages'])) {
                    $packages = $_GET['packages'];
                    if ($packages == -1)
                        $queryString->whereNull('relief_mappings.packeges');
                    else
                        $queryString->whereRaw("find_in_set({$_GET['packages']},relief_mappings.packeges)");

                }
                $reliefData = $queryString->get();
                $count = $reliefData->count();
                $isDisplay = 2;
                return view('report.ajax_masterroll_report', compact('reliefData', 'districtList', 'upazilaList', 'isDisplay', 'count', 'ssnpList', 'packageList'));
                //                return view('report.ajax_masterroll_report', compact('reliefData', 'districtList', 'upazilaList', 'training_id', 'reg_status', 'trade_name', 'registrationStatusArr', 'titleName', 'tradeList', 'isDisplay', 'count', 'ssnpList', 'packageList'));
            } else {
                //            $reliefData = $queryString->get();
                $reliefData = array();
                return view('report.adminMasterRollReport', compact('reliefData', 'districtList', 'upazilaList', 'isDisplay', 'ssnpList', 'packageList'));
                //                return view('report.adminMasterRollReport', compact('reliefData', 'districtList', 'upazilaList', 'training_id', 'reg_status', 'trade_name', 'registrationStatusArr', 'titleName', 'tradeList', 'isDisplay', 'ssnpList', 'packageList'));
            }
        } catch
        (\Exception $exception) {
            dd($exception);
        }

    }

    public function adminBulkRegistrationReport_main()
    {
        $districtModel = new District();
        $upazilaModel = new Upazila();
        $reliefModel = new Relief();
        $ssnpModel = new Ssnp();
        $packagesModel = new Package();

        try {
            $districtList = $districtModel->where('id', '=', 51)->pluck('bn_name', 'id')->all();
            $upazilaList = $upazilaModel->where('district_id', '=', 51)->pluck('bn_name', 'id')->all();
            $ssnpList = $ssnpModel->pluck('ssnp_name', 'id')->all();
            $packageList = $packagesModel->where('status', '!=', config('constants.package_status.Deleted'))->pluck('title', 'id')->all();
            $professionList = $reliefModel->where('profession', 'like', '%ক্ষুদ্র%')->pluck('profession', 'id')->groupby('profession')->all();
            $land_info = config('constants.land_info');
            $professionList = $professionList[''];

            //            pr($packageList);
            $queryString = $reliefModel::query();
            $queryCountString = $reliefModel::query();
            $isDisplay = 1;

            $queryString->select("ssnps.ssnp_name AS ssnp_name", "districts.bn_name AS dis_name", "upazilas.bn_name AS upa_name", "unions.bn_name AS uni_name", "users.first_name", 'reliefs.id', 'reliefs.card_no', 'reliefs.card_type', 'reliefs.name', 'reliefs.trible', 'reliefs.g_name', 'reliefs.total_member', 'reliefs.hw_name', 'reliefs.age', 'reliefs.dob', 'reliefs.gender', 'reliefs.house_pre', 'reliefs.road_pre', 'reliefs.word_pre', 'reliefs.upo_pre', 'reliefs.uni_pre', 'reliefs.holding_pre', 'reliefs.dist_pre', 'reliefs.profession', 'reliefs.phone', 'reliefs.nid', 'reliefs.ssnp', 'reliefs.total_income', 'reliefs.firm_land', 'reliefs.poss_help', 'reliefs.total_spend', 'reliefs.dist_pre', 'relief_mappings.reg_id', 'relief_mappings.packeges', 'relief_mappings.delv_date', 'relief_mappings.last_give_pac', 'reliefs.tot_child')
                ->where('reliefs.is_deleted', '!=', config('constants.is_deleted.Deleted'))
                ->leftJoin('users', 'reliefs.created_by', 'users.id')
                ->leftJoin('upazilas', 'upazilas.id', '=', 'reliefs.upo_pre')
                ->leftJoin('unions', 'unions.id', '=', 'reliefs.uni_pre')
                ->leftJoin('districts', 'districts.id', '=', 'reliefs.dist_pre')
                ->leftJoin('relief_mappings', 'relief_mappings.reg_id', '=', 'reliefs.id')
                ->leftJoin('ssnps', 'ssnps.id', '=', 'reliefs.ssnp');

            //        pr(sizeof($array_length));

            if (request()->ajax()) {
                $search_by_id = $_GET['search_by_id'];
                $array_length = explode(",", $search_by_id);

                if (isset($_GET['dist']) && !empty($_GET['dist'])) {
                    $dist = $_GET['dist'];
                    $queryString->where('reliefs.dist_pre', '=', $_GET['dist']);
                }
                if (isset($_GET['upo']) && !empty($_GET['upo'])) {
                    $upo = $_GET['upo'];
                    $queryString->where('reliefs.upo_pre', '=', $_GET['upo']);
                }
                if (isset($_GET['uni_per'])) {
                    $uni_per = $_GET['uni_per'];
                    if (!empty($_GET['uni_per']))
                        $queryString->where('reliefs.uni_pre', '=', $_GET['uni_per']);
                    if ($_GET['uni_per'] === '0') {
                        $queryString->where('reliefs.uni_pre', '=', $_GET['uni_per']);
                    }
                }
                if (isset($_GET['word_no']) && !empty($_GET['word_no'])) {
                    $word_no = $_GET['word_no'];
                    $queryString->where('reliefs.word_pre', '=', $_GET['word_no']);
                }
                if (isset($_GET['name']) && !empty($_GET['name'])) {
                    $name = $_GET['name'];
                    $queryString->where('reliefs.name', 'like', "%" . $_GET['name'] . "%");
                }
                if (isset($_GET['phone']) && !empty($_GET['phone'])) {
                    $phone = $_GET['phone'];
                    $queryString->where('reliefs.phone', 'like', "%" . $_GET['phone'] . "%");
                }
                if (isset($_GET['nid']) && !empty($_GET['nid'])) {
                    $nid = $_GET['nid'];
                    $queryString->where('reliefs.nid', 'like', "%" . $_GET['nid'] . "%");
                }
                if (isset($_GET['card_type']) && !empty($_GET['card_type'])) {
                    $card_type = $_GET['card_type'];
                    $queryString->where('reliefs.card_type', '=', $_GET['card_type']);
                }
                if (isset($_GET['trible']) && !empty($_GET['trible'])) {
                    $trible = $_GET['trible'];
                    if ($trible == -1) {
                        $queryString->where('reliefs.trible', '=', 0);
                    } else {
                        $queryString->Where(function ($query) use ($trible) {
                            $query->where('reliefs.trible', '=', $trible);
                        });
                    }
                }
                if (isset($_GET['poss_help']) && !empty($_GET['poss_help'])) {
                    $poss_help = $_GET['poss_help'];
                    if ($poss_help == -1) {
                        $queryString->where('reliefs.poss_help', '!=', 1);
                    } else {
                        $queryString->where('reliefs.poss_help', '=', $_GET['poss_help']);
                    }
                }
                if (isset($_GET['card_no']) && !empty($_GET['card_no'])) {
                    $card_no = $_GET['card_no'];
                    $queryString->where('reliefs.card_no', 'like', "%" . $_GET['card_no'] . "%");
                }
                if (isset($_GET['search_by_id']) && !empty($_GET['search_by_id'])) {
                    $search_by_id = $_GET['search_by_id'];
                    $datas = explode(",", $search_by_id);
                    $queryString->whereIn('reliefs.id', $datas);
                }
                if (isset($_GET['ssnp']) && !empty($_GET['ssnp'])) {
                    $ssnp = $_GET['ssnp'];
                    if ($ssnp == -1) {
                        $queryString->where('reliefs.ssnp', '=', 0);
                    } else {
                        $queryString->Where(function ($query) use ($ssnp) {
                            $query->where('reliefs.ssnp', '=', $ssnp);
                            $query->orWhereRaw("find_in_set($ssnp,reliefs.f_ssnp)");
                        });
                    }
                }
                if (isset($_GET['packages']) && !empty($_GET['packages'])) {
                    $packages = $_GET['packages'];
                    if ($packages == -1) {
                        $queryString->whereNull('relief_mappings.packeges');
                    } else {
                        $queryString->whereRaw("find_in_set({$_GET['packages']},relief_mappings.packeges)");
                    }

                }
                if (isset($_GET['firm_land']) && !empty($_GET['firm_land'])) {
                    $firm_land = $_GET['firm_land'];
                    $queryString->where('reliefs.firm_land', '=', $_GET['firm_land']);
                }
                if (isset($_GET['baby_food']) && !empty($_GET['baby_food'])) {
                    $baby_food = $_GET['baby_food'];
                    if ($baby_food == 1)
                        $queryString->where('reliefs.tot_child', '>=', 1);
                    else
                        $queryString->where('reliefs.tot_child', '=', 0);

                }
                if (isset($_GET['proffs']) && !empty($_GET['proffs'])) {
                    $baby_food = $_GET['proffs'];
                    if ($baby_food == -99)
                        $queryString->Where(function ($query) {
                            $query->where('reliefs.profession', 'LIKE', '%ক্ষুদ্র%');
                            $query->orWhere('reliefs.profession', 'LIKE', '%ক্ষৃদ%');
                            $query->orWhere('reliefs.profession', 'LIKE', '%খুদ্%');
                            $query->orWhere('reliefs.profession', 'LIKE', '%খুদ%');
                            $query->orWhere('reliefs.profession', 'LIKE', '%ক্ষদ%');
                        });
                    else
                        $queryString->where('reliefs.profession', '=', 0);

                }
                $reliefData = $queryString->get();
                //            $reliefData = $queryString->toSql();
//            dd($reliefData);
                $count = $reliefData->count();
                $isDisplay = 2;
                return view('report.ajax_bulk_registration_report', compact('reliefData', 'districtList', 'upazilaList', 'isDisplay', 'count', 'ssnpList', 'packageList', 'array_length', 'land_info', 'professionList'));
            } else {
                //            $reliefData = $queryString->get();
                return view('report.adminBulkRegistrationReport', compact('districtList', 'upazilaList', 'isDisplay', 'ssnpList', 'packageList', 'land_info', 'professionList'));
                //            return view('report.adminRegistrationReport', compact('registrationList', 'departmentList', 'trainingList', 'training_id', 'reg_status', 'trade_name', 'registrationStatusArr', 'titleName', 'tradeList'));
            }
        } catch (\Exception $exception) {
            dd($exception);
        }
    }

    public function adminBulkRegistrationReport()
    {
        $districtModel = new District();
        $upazilaModel = new Upazila();
        $ssnpModel = new Ssnp();
        $packagesModel = new Package();

        try {
            $districtList = $districtModel->where('id', '=', 51)->pluck('bn_name', 'id')->all();
            $upazilaList = $upazilaModel->where('district_id', '=', 51)->pluck('bn_name', 'id')->all();
            $ssnpList = $ssnpModel->pluck('ssnp_name', 'id')->all();
            $packageList = $packagesModel->where('status', '!=', config('constants.package_status.Deleted'))->pluck('title', 'id')->all();
            //            $professionList = $reliefModel->where('profession','like','%ক্ষুদ্র%')->pluck('profession', 'id')->groupby('profession')->all();
            $land_info = config('constants.land_info');
            //            $professionList = $professionList[''];
            $professionList = [];

            //            pr($packageList);

            $isDisplay = 1;
            if (request()->ajax()) {
                //                pr($_GET);
//                if(!empty($_GET['word_no'])){
//                    echo "data has";
//                }else{
//                    echo 'No data';
//                }
//                dd();
                $reliefModel = '';
                $reliefModelVw = '';
                if (isset($_GET['upo']) && !empty($_GET['upo'])) {
                    $upo = $_GET['upo'];
                    switch ($upo) {
                        case 385:
                            $reliefModel = new VwSadar();
                            $reliefModelVw = 'sadar_vw';
                            break;
                        case 386:
                            $reliefModel = new VwKashiyani();
                            $reliefModelVw = 'kasiyani_vw';
                            break;
                        case 387:
                            $reliefModel = new VwTungipara();
                            $reliefModelVw = 'tungipara_vw';
                            break;
                        case 388:
                            $reliefModel = new VwKotalipara();
                            $reliefModelVw = 'kotalipara_vw';
                            break;
                        case 389:
                            $reliefModel = new VwMuksudpur();
                            $reliefModelVw = 'muksudpur_vw';
                            break;
                        default:
                            $reliefModel = new Relief();
                            $reliefModelVw = 'reliefs';
                            break;
                    }
                }
                //                pr($reliefModelVw);
//                die();

                $queryString = $reliefModel::query();
                //                $queryCountString = $reliefModel::query();

                $queryString->select("ssnps.ssnp_name AS ssnp_name", "districts.bn_name AS dis_name", "upazilas.bn_name AS upa_name", "unions.bn_name AS uni_name", $reliefModelVw . '.id', $reliefModelVw . '.card_no', $reliefModelVw . '.card_type', $reliefModelVw . '.name', $reliefModelVw . '.g_name', $reliefModelVw . '.total_member', $reliefModelVw . '.hw_name', $reliefModelVw . '.trible', $reliefModelVw . '.age', $reliefModelVw . '.dob', $reliefModelVw . '.gender', $reliefModelVw . '.house_pre', $reliefModelVw . '.road_pre', $reliefModelVw . '.word_pre', $reliefModelVw . '.upo_pre', $reliefModelVw . '.uni_pre', $reliefModelVw . '.dist_pre', $reliefModelVw . '.holding_pre', $reliefModelVw . '.profession', $reliefModelVw . '.phone', $reliefModelVw . '.nid', $reliefModelVw . '.ssnp', $reliefModelVw . '.poss_help')
                    ->where($reliefModelVw . '.is_deleted', '!=', config('constants.is_deleted.Deleted'))
                    //                ->leftJoin('users', 'reliefs.created_by', 'users.id')
                    ->leftJoin('upazilas', 'upazilas.id', '=', $reliefModelVw . '.upo_pre')
                    ->leftJoin('unions', 'unions.id', '=', $reliefModelVw . '.uni_pre')
                    ->leftJoin('districts', 'districts.id', '=', $reliefModelVw . '.dist_pre')
                    ->leftJoin('ssnps', 'ssnps.id', '=', $reliefModelVw . '.ssnp');

                //        pr(sizeof($array_length));

                $search_by_id = $_GET['search_by_id'];
                $array_length = explode(",", $search_by_id);

                if (isset($_GET['dist']) && !empty($_GET['dist'])) {
                    $dist = $_GET['dist'];
                    $queryString->where($reliefModelVw . '.dist_pre', '=', 51);
                }
                if (isset($_GET['upo']) && !empty($_GET['upo'])) {
                    $upo = $_GET['upo'];
                    $queryString->where($reliefModelVw . '.upo_pre', '=', $_GET['upo']);
                }
                if (isset($_GET['uni_per'])) {
                    $uni_per = $_GET['uni_per'];
                    if (!empty($_GET['uni_per']))
                        $queryString->where($reliefModelVw . '.uni_pre', '=', $_GET['uni_per']);
                    if ($_GET['uni_per'] === '0') {
                        $queryString->where($reliefModelVw . '.uni_pre', '=', $_GET['uni_per']);
                    }
                }
                if (isset($_GET['word_no']) && !empty($_GET['word_no'])) {
                    $word_no = $_GET['word_no'];
                    //                    pr()
//                    $queryString->where($reliefModelVw . '.word_pre', '=', $_GET['word_no']);
                    $queryString->whereIn($reliefModelVw . '.word_pre', $word_no);
                }
                if (isset($_GET['name']) && !empty($_GET['name'])) {
                    $name = $_GET['name'];
                    $queryString->where($reliefModelVw . '.name', 'like', "%" . $_GET['name'] . "%");
                }
                if (isset($_GET['phone']) && !empty($_GET['phone'])) {
                    $phone = $_GET['phone'];
                    $queryString->where($reliefModelVw . '.phone', 'like', "%" . $_GET['phone'] . "%");
                }
                if (isset($_GET['nid']) && !empty($_GET['nid'])) {
                    $nid = $_GET['nid'];
                    $queryString->where($reliefModelVw . '.nid', 'like', "%" . $_GET['nid'] . "%");
                }
                if (isset($_GET['card_type']) && !empty($_GET['card_type'])) {
                    $card_type = $_GET['card_type'];
                    //                    $queryString->where($reliefModelVw . '.card_type', '=', $_GET['card_type']);
                    $queryString->whereIn($reliefModelVw . '.card_type', $card_type);
                }
                if (isset($_GET['trible']) && !empty($_GET['trible'])) {
                    $trible = $_GET['trible'];
                    if ($trible == -1) {
                        $queryString->where($reliefModelVw . '.trible', '=', 0);
                    } else {
                        $queryString->Where(function ($query) use ($trible, $reliefModelVw) {
                            $query->where($reliefModelVw . '.trible', '=', $trible);
                        });
                    }
                    //                    $queryString->where($reliefModelVw . '.card_type', '=', $_GET['card_type']);
                    // $queryString->where($reliefModelVw . '.trible', '=', $trible);
                }
                if (isset($_GET['poss_help']) && !empty($_GET['poss_help'])) {
                    $poss_help = $_GET['poss_help'];
                    if ($poss_help == -1) {
                        $queryString->where($reliefModelVw . '.poss_help', '!=', 1);
                    } else {
                        $queryString->where($reliefModelVw . '.poss_help', '=', $_GET['poss_help']);
                    }
                }
                if (isset($_GET['card_no']) && !empty($_GET['card_no'])) {
                    $card_no = $_GET['card_no'];
                    $queryString->where($reliefModelVw . '.card_no', 'like', "%" . $_GET['card_no'] . "%");
                }
                if (isset($_GET['search_by_id']) && !empty($_GET['search_by_id'])) {
                    $search_by_id = $_GET['search_by_id'];
                    $datas = explode(",", $search_by_id);
                    $queryString->whereIn($reliefModelVw . '.id', $datas);
                }
                if (isset($_GET['ssnp']) && !empty($_GET['ssnp'])) {
                    $ssnp = $_GET['ssnp'];
                    if ($ssnp == -1) {
                        $queryString->where($reliefModelVw . '.ssnp', '=', 0);
                    } else {
                        $queryString->Where(function ($query) use ($ssnp, $reliefModelVw) {
                            $query->where($reliefModelVw . '.ssnp', '=', $ssnp);
                            $query->orWhereRaw("find_in_set($ssnp,'f_ssnp')");
                        });
                    }
                }
                if (isset($_GET['packages']) && !empty($_GET['packages'])) {
                    $packages = $_GET['packages'];
                    if ($packages == -1) {
                        //                        $queryString->whereNull('relief_mappings.packeges');
                        $queryString->whereNotIn($reliefModelVw . '.id', function ($query) {
                            $query->select("relief_mappings.reg_id")
                                ->from('relief_mappings');
                            //                            $query->select(DB::raw(1))
//                            $query->whereRaw("find_in_set({$_GET['packages']},relief_mappings.packeges)");
                        });
                    } else {
                        $queryString->whereRaw("find_in_set({$_GET['packages']},relief_mappings.packeges)");
                    }

                }
                //                if (isset($_GET['firm_land']) && !empty($_GET['firm_land'])) {
//                    $firm_land = $_GET['firm_land'];
//                    $queryString->where('reliefs.firm_land', '=', $_GET['firm_land']);
//                }
                if (isset($_GET['baby_food']) && !empty($_GET['baby_food'])) {
                    $baby_food = $_GET['baby_food'];
                    if ($baby_food == 1)
                        $queryString->where($reliefModelVw . '.tot_child', '>=', 1);
                    else
                        $queryString->where($reliefModelVw . '.tot_child', '=', 0);

                }
                if (isset($_GET['proffs']) && !empty($_GET['proffs'])) {
                    $baby_food = $_GET['proffs'];
                    if ($baby_food == -99)
                        $queryString->Where(function ($query) {
                            $query->where($reliefModelVw . '.profession', 'LIKE', '%ক্ষুদ্র%');
                            $query->orWhere($reliefModelVw . '.profession', 'LIKE', '%ক্ষৃদ%');
                            $query->orWhere($reliefModelVw . '.profession', 'LIKE', '%খুদ্%');
                            $query->orWhere($reliefModelVw . '.profession', 'LIKE', '%খুদ%');
                            $query->orWhere($reliefModelVw . '.profession', 'LIKE', '%ক্ষদ%');
                        });
                    else
                        $queryString->where($reliefModelVw . '.profession', '=', 0);

                }

                if (isset($_GET['packages']) && !empty($_GET['packages'])) {
                    if ($packages == -1) {
                    } else {
                        $queryString->addSelect('relief_mappings.reg_id', 'relief_mappings.packeges', 'relief_mappings.delv_date', 'relief_mappings.last_give_pac')
                            ->leftJoin('relief_mappings', 'relief_mappings.reg_id', '=', $reliefModelVw . '.id');
                    }
                }
                $reliefData = $queryString->get();
                //            $reliefData = $queryString->toSql();
//            dd($reliefData);
                $count = $reliefData->count();
                $isDisplay = 2;
                return view('report.ajax_bulk_registration_report', compact('reliefData', 'districtList', 'upazilaList', 'isDisplay', 'count', 'ssnpList', 'packageList', 'array_length', 'land_info', 'professionList'));
            } else {
                //            $reliefData = $queryString->get();
                return view('report.adminBulkRegistrationReport', compact('districtList', 'upazilaList', 'isDisplay', 'ssnpList', 'packageList', 'land_info', 'professionList'));
                //            return view('report.adminRegistrationReport', compact('registrationList', 'departmentList', 'trainingList', 'training_id', 'reg_status', 'trade_name', 'registrationStatusArr', 'titleName', 'tradeList'));
            }
        } catch (\Exception $exception) {
            dd($exception);
        }
    }

    public function adminExportRegistrationReport(Request $request)
    {
        $reliefModel = new Relief();

        $queryString = $reliefModel::query();

        $queryString->select("districts.bn_name AS dis_name", "upazilas.bn_name AS upa_name", "upazilas.up_code AS upa_code", "unions.bn_name AS uni_name", 'reliefs.id', 'reliefs.card_no', 'reliefs.card_type', 'reliefs.name', 'reliefs.g_name', 'reliefs.poss_help', 'reliefs.total_member', 'reliefs.hw_name', 'reliefs.age', 'reliefs.trible', 'reliefs.gender', 'reliefs.house_pre', 'reliefs.road_pre', 'reliefs.word_pre', 'reliefs.upo_pre', 'reliefs.uni_pre', 'reliefs.holding_pre', 'reliefs.dist_pre', 'reliefs.profession', 'reliefs.phone', 'reliefs.nid', 'reliefs.ssnp', 'reliefs.tot_child', 'reliefs.tot_male', 'reliefs.tot_female', 'reliefs.tot_transgen', 'reliefs.tot_handicap', 'reliefs.f_nid', 'reliefs.f_ssnp', 'ssnps.ssnp_name', 'relief_mappings.reg_id', 'relief_mappings.packeges', 'relief_mappings.delv_date', 'relief_mappings.last_give_pac', 'reliefs.tot_child')
            ->where('reliefs.is_deleted', '!=', config('constants.is_deleted.Deleted'))
            ->leftJoin('users', 'reliefs.created_by', 'users.id')
            ->leftJoin('upazilas', 'upazilas.id', '=', 'reliefs.upo_pre')
            ->leftJoin('unions', 'unions.id', '=', 'reliefs.uni_pre')
            ->leftJoin('districts', 'districts.id', '=', 'reliefs.dist_pre')
            ->leftJoin('relief_mappings', 'relief_mappings.reg_id', '=', 'reliefs.id')
            ->leftJoin('ssnps', 'ssnps.id', '=', 'reliefs.ssnp');

        //        dd($_POST);

        if (isset($_POST['dist']) && !empty($_POST['dist'])) {
            $dist = $_POST['dist'];
            $queryString->where('reliefs.dist_pre', '=', $_POST['dist']);
        }
        if (isset($_POST['upo']) && !empty($_POST['upo'])) {
            $upo = $_POST['upo'];
            $queryString->where('reliefs.upo_pre', '=', $_POST['upo']);
        }
        if (isset($_POST['uni_per'])) {
            $uni_per = $_POST['uni_per'];
            if (!empty($_POST['uni_per']))
                $queryString->where('reliefs.uni_pre', '=', $_POST['uni_per']);
            if ($_POST['uni_per'] === '0') {
                $queryString->where('reliefs.uni_pre', '=', $_POST['uni_per']);
            }
        }
        if (isset($_POST['word_no']) && !empty($_POST['word_no'])) {
            $word_no = $_POST['word_no'];
            $queryString->where('reliefs.word_pre', '=', $_POST['word_no']);
        }
        if (isset($_POST['name']) && !empty($_POST['name'])) {
            $name = $_POST['name'];
            $queryString->where('reliefs.name', 'like', "%" . $_POST['name'] . "%");
        }
        if (isset($_POST['phone']) && !empty($_POST['phone'])) {
            $phone = $_POST['phone'];
            $queryString->where('reliefs.phone', 'like', "%" . $_POST['phone'] . "%");
        }
        if (isset($_POST['nid']) && !empty($_POST['nid'])) {
            $nid = $_POST['nid'];
            $queryString->where('reliefs.nid', 'like', "%" . $_POST['nid'] . "%");
        }
        if (isset($_POST['card_type']) && !empty($_POST['card_type'])) {
            $card_type = $_POST['card_type'];
            $queryString->where('reliefs.card_type', '=', $_POST['card_type']);
        }
        if (isset($_POST['trible']) && !empty($_POST['trible'])) {
            $trible = $_POST['trible'];
            if ($trible == -1) {
                $queryString->where('reliefs.trible', '=', 0);
            } else {
                $queryString->Where(function ($query) use ($trible) {
                    $query->where('reliefs.trible', '=', $trible);
                });
            }
        }
        // if (isset($_POST['trible']) && !empty($_POST['trible'])) {
        //     $trible = $_POST['trible'];
        //     $queryString->where('reliefs.trible', '=', $_POST['trible']);
        // }
        if (isset($_POST['card_no']) && !empty($_POST['card_no'])) {
            $card_no = $_POST['card_no'];
            $queryString->where('reliefs.card_no', '=', $_POST['card_no']);
        }
        if (isset($_POST['poss_help']) && !empty($_POST['poss_help'])) {
            $poss_help = $_POST['poss_help'];
            if ($poss_help == -1) {
                $queryString->where('reliefs.poss_help', '!=', 1);
            } else {
                $queryString->where('reliefs.poss_help', '=', $_POST['poss_help']);
            }
        }
        if (isset($_POST['ssnp']) && !empty($_POST['ssnp'])) {
            $ssnp = $_POST['ssnp'];
            if ($ssnp == -1)
                $queryString->where('reliefs.ssnp', '=', 0);
            else
                $queryString->where('reliefs.ssnp', '=', $_POST['ssnp']);
        }
        if (isset($_POST['packages']) && !empty($_POST['packages'])) {
            $packages = $_POST['packages'];
            $queryString->whereRaw("find_in_set({$_POST['packages']},relief_mappings.packeges)");
        }
        if (isset($_POST['baby_food']) && !empty($_POST['baby_food'])) {
            $baby_food = $_POST['baby_food'];
            if ($baby_food == 1)
                $queryString->where('reliefs.tot_child', '>=', 1);
            else
                $queryString->where('reliefs.tot_child', '=', 0);

        }
        if (isset($_POST['search_by_id']) && !empty($_POST['search_by_id'])) {
            $search_by_id = $_POST['search_by_id'];
            $datas = explode(",", $search_by_id);
            $queryString->whereIn('reliefs.id', $datas);
        }

        //        if (isset($_POST['training_id']) && !empty($_POST['training_id'])) {
//            $training_id = $_POST['training_id'];
//            $queryString->where('registrations.training_id', '=', $_POST['training_id']);
//        }
//        if (isset($_POST['reg_status']) && !empty($_POST['reg_status'])) {
//            $reg_status = $_POST['reg_status'];
//            $queryString->where('registrations.status', '=', $_POST['reg_status']);
//        }
//        if (isset($_POST['trade_name']) && !empty($_POST['trade_name'])) {
//            $trade_name = $_POST['trade_name'];
//            $queryString->where('trainings.trade_name', '=', $_POST['trade_name']);
//        }
        $reliefData = $queryString->get();
        //        dd($reliefData);
        $this->__exportRegList($reliefData);
    }

    public function adminExportMasterRollReport(Request $request)
    {
        $districtModel = new District();
        $upazilaModel = new Upazila();
        $reliefModel = new Relief();

        $districtList = $districtModel->where('id', '=', 51)->pluck('bn_name', 'id')->all();
        $upazilaList = $upazilaModel->where('district_id', '=', 51)->pluck('bn_name', 'id')->all();

        $queryString = $reliefModel::query();

        $queryString->select("districts.bn_name AS dis_name", "upazilas.bn_name AS upa_name", "upazilas.up_code AS upa_code", "unions.bn_name AS uni_name", 'reliefs.id', 'reliefs.card_no', 'reliefs.card_type', 'reliefs.trible', 'reliefs.name', 'reliefs.g_name', 'reliefs.poss_help', 'reliefs.total_member', 'reliefs.hw_name', 'reliefs.age', 'reliefs.gender', 'reliefs.house_pre', 'reliefs.road_pre', 'reliefs.word_pre', 'reliefs.upo_pre', 'reliefs.uni_pre', 'reliefs.holding_pre', 'reliefs.dist_pre', 'reliefs.profession', 'reliefs.phone', 'reliefs.nid', 'reliefs.ssnp', 'reliefs.tot_child', 'reliefs.tot_male', 'reliefs.tot_female', 'reliefs.tot_transgen', 'reliefs.tot_handicap', 'reliefs.f_nid', 'reliefs.f_ssnp', 'ssnps.ssnp_name', 'relief_mappings.reg_id', 'relief_mappings.packeges', 'relief_mappings.delv_date', 'relief_mappings.last_give_pac')
            ->where('reliefs.is_deleted', '!=', config('constants.is_deleted.Deleted'))
            ->leftJoin('users', 'reliefs.created_by', 'users.id')
            ->leftJoin('upazilas', 'upazilas.id', '=', 'reliefs.upo_pre')
            ->leftJoin('unions', 'unions.id', '=', 'reliefs.uni_pre')
            ->leftJoin('districts', 'districts.id', '=', 'reliefs.dist_pre')
            ->leftJoin('relief_mappings', 'relief_mappings.reg_id', '=', 'reliefs.id')
            ->leftJoin('ssnps', 'ssnps.id', '=', 'reliefs.ssnp');

        //        dd($_POST);

        if (isset($_POST['dist']) && !empty($_POST['dist'])) {
            $dist = $_POST['dist'];
            $queryString->where('reliefs.dist_pre', '=', $_POST['dist']);
        }
        if (isset($_POST['upo']) && !empty($_POST['upo'])) {
            $upo = $_POST['upo'];
            $queryString->where('reliefs.upo_pre', '=', $_POST['upo']);
        }
        if (isset($_POST['uni_per'])) {
            $uni_per = $_POST['uni_per'];
            if (!empty($_POST['uni_per']))
                $queryString->where('reliefs.uni_pre', '=', $_POST['uni_per']);
            if ($_POST['uni_per'] === '0') {
                $queryString->where('reliefs.uni_pre', '=', $_POST['uni_per']);
            }
        }
        if (isset($_POST['word_no']) && !empty($_POST['word_no'])) {
            $word_no = $_POST['word_no'];
            $queryString->where('reliefs.word_pre', '=', $_POST['word_no']);
        }
        if (isset($_POST['name']) && !empty($_POST['name'])) {
            $name = $_POST['name'];
            $queryString->where('reliefs.name', 'like', "%" . $_POST['name'] . "%");
        }
        if (isset($_POST['phone']) && !empty($_POST['phone'])) {
            $phone = $_POST['phone'];
            $queryString->where('reliefs.phone', 'like', "%" . $_POST['phone'] . "%");
        }
        if (isset($_POST['nid']) && !empty($_POST['nid'])) {
            $nid = $_POST['nid'];
            $queryString->where('reliefs.nid', 'like', "%" . $_POST['nid'] . "%");
        }
        if (isset($_POST['card_type']) && !empty($_POST['card_type'])) {
            $card_type = $_POST['card_type'];
            $queryString->where('reliefs.card_type', '=', $_POST['card_type']);
        }
        if (isset($_POST['trible']) && !empty($_POST['trible'])) {
            $trible = $_POST['trible'];
            if ($trible == -1) {
                $queryString->where('reliefs.trible', '=', 0);
            } else {
                $queryString->Where(function ($query) use ($trible) {
                    $query->where('reliefs.trible', '=', $trible);
                });
            }
        }
        if (isset($_POST['card_no']) && !empty($_POST['card_no'])) {
            $card_no = $_POST['card_no'];
            $queryString->where('reliefs.card_no', '=', $_POST['card_no']);
        }
        if (isset($_POST['poss_help']) && !empty($_POST['poss_help'])) {
            $poss_help = $_POST['poss_help'];
            if ($poss_help == -1) {
                $queryString->where('reliefs.poss_help', '!=', 1);
            } else {
                $queryString->where('reliefs.poss_help', '=', $_POST['poss_help']);
            }
        }
        if (isset($_POST['ssnp']) && !empty($_POST['ssnp'])) {
            $ssnp = $_POST['ssnp'];
            if ($ssnp == -1)
                $queryString->where('reliefs.ssnp', '=', 0);
            else
                $queryString->where('reliefs.ssnp', '=', $_POST['ssnp']);
        }
        if (isset($_POST['packages']) && !empty($_POST['packages'])) {
            $packages = $_POST['packages'];
            $queryString->whereRaw("find_in_set({$_POST['packages']},relief_mappings.packeges)");
        }

        //        if (isset($_POST['training_id']) && !empty($_POST['training_id'])) {
//            $training_id = $_POST['training_id'];
//            $queryString->where('registrations.training_id', '=', $_POST['training_id']);
//        }
//        if (isset($_POST['reg_status']) && !empty($_POST['reg_status'])) {
//            $reg_status = $_POST['reg_status'];
//            $queryString->where('registrations.status', '=', $_POST['reg_status']);
//        }
//        if (isset($_POST['trade_name']) && !empty($_POST['trade_name'])) {
//            $trade_name = $_POST['trade_name'];
//            $queryString->where('trainings.trade_name', '=', $_POST['trade_name']);
//        }
        $reliefData = $queryString->get();
        $this->__exportMasterRollReport($reliefData);
    }

    public function adminExportBulkRegistrationReport_main(Request $request)
    {

        $reliefModel = new Relief();

        $queryString = $reliefModel::query();
        //        , 'relief_family_searches.tot_male', 'relief_family_searches.tot_female', 'relief_family_searches.tot_transgen', 'relief_family_searches.tot_child'
        $queryString->select("districts.bn_name AS dis_name", "upazilas.bn_name AS upa_name", "upazilas.up_code AS upa_code", "unions.bn_name AS uni_name", 'reliefs.id', 'reliefs.dob', 'reliefs.card_no', 'reliefs.card_type', 'reliefs.trible', 'reliefs.name', 'reliefs.g_name', 'reliefs.poss_help', 'reliefs.total_member', 'reliefs.hw_name', 'reliefs.age', 'reliefs.gender', 'reliefs.house_pre', 'reliefs.road_pre', 'reliefs.word_pre', 'reliefs.upo_pre', 'reliefs.uni_pre', 'reliefs.holding_pre', 'reliefs.dist_pre', 'reliefs.profession', 'reliefs.phone', 'reliefs.nid', 'reliefs.ssnp', 'reliefs.tot_child', 'reliefs.tot_male', 'reliefs.tot_female', 'reliefs.tot_transgen', 'reliefs.tot_handicap', 'reliefs.f_nid', 'reliefs.firm_land', 'reliefs.f_ssnp', 'ssnps.ssnp_name', 'relief_mappings.reg_id', 'relief_mappings.packeges', 'relief_mappings.delv_date', 'relief_mappings.last_give_pac', 'reliefs.tot_child')
            ->where('reliefs.is_deleted', '!=', config('constants.is_deleted.Deleted'))
            ->leftJoin('users', 'reliefs.created_by', 'users.id')
            ->leftJoin('upazilas', 'upazilas.id', '=', 'reliefs.upo_pre')
            ->leftJoin('unions', 'unions.id', '=', 'reliefs.uni_pre')
            ->leftJoin('districts', 'districts.id', '=', 'reliefs.dist_pre')
            ->leftJoin('relief_mappings', 'relief_mappings.reg_id', '=', 'reliefs.id')
            ->leftJoin('ssnps', 'ssnps.id', '=', 'reliefs.ssnp');

        //        $queryString->select("ssnps.ssnp_name AS ssnp_name", "districts.bn_name AS dis_name", "upazilas.bn_name AS upa_name", "upazilas.up_code AS upa_code", "unions.bn_name AS uni_name", "users.first_name", 'reliefs.id', 'reliefs.card_no', 'reliefs.card_type', 'reliefs.name', 'reliefs.g_name', 'reliefs.total_member', 'reliefs.hw_name', 'reliefs.age', 'reliefs.gender', 'reliefs.house_pre', 'reliefs.road_pre', 'reliefs.word_pre', 'reliefs.upo_pre', 'reliefs.uni_pre', 'reliefs.holding_pre', 'reliefs.dist_pre', 'reliefs.profession', 'reliefs.phone', 'reliefs.nid', 'reliefs.ssnp', 'reliefs.total_income', 'reliefs.total_spend', 'reliefs.dist_pre', 'relief_mappings.reg_id', 'relief_mappings.packeges', 'relief_mappings.delv_date', 'relief_mappings.last_give_pac', 'reliefs.hw_name')
//            ->where('reliefs.is_deleted', '!=', config('constants.is_deleted.Deleted'))
//            ->leftJoin('users', 'reliefs.created_by', 'users.id')
//            ->leftJoin('upazilas', 'upazilas.id', '=', 'reliefs.upo_pre')
//            ->leftJoin('unions', 'unions.id', '=', 'reliefs.uni_pre')
//            ->leftJoin('districts', 'districts.id', '=', 'reliefs.dist_pre')
//            ->leftJoin('relief_mappings', 'relief_mappings.reg_id', '=', 'reliefs.id')
//            ->leftJoin('ssnps', 'ssnps.id', '=', 'reliefs.ssnp')
//            ->leftJoin('relief_family_searches', 'relief_family_searches.relf_id', '=', 'reliefs.id');

        //        dd($_POST);

        if (isset($_POST['dist']) && !empty($_POST['dist'])) {
            $dist = $_POST['dist'];
            $queryString->where('reliefs.dist_pre', '=', $_POST['dist']);
        }
        if (isset($_POST['upo']) && !empty($_POST['upo'])) {
            $upo = $_POST['upo'];
            $queryString->where('reliefs.upo_pre', '=', $_POST['upo']);
        }
        if (isset($_POST['uni_per'])) {
            $uni_per = $_POST['uni_per'];
            if (!empty($_POST['uni_per']))
                $queryString->where('reliefs.uni_pre', '=', $_POST['uni_per']);
            if ($_POST['uni_per'] === '0') {
                $queryString->where('reliefs.uni_pre', '=', $_POST['uni_per']);
            }
        }
        if (isset($_POST['word_no']) && !empty($_POST['word_no'])) {
            $word_no = $_POST['word_no'];
            $queryString->where('reliefs.word_pre', '=', $_POST['word_no']);
        }
        if (isset($_POST['name']) && !empty($_POST['name'])) {
            $name = $_POST['name'];
            $queryString->where('reliefs.name', 'like', "%" . $_POST['name'] . "%");
        }
        if (isset($_POST['phone']) && !empty($_POST['phone'])) {
            $phone = $_POST['phone'];
            $queryString->where('reliefs.phone', 'like', "%" . $_POST['phone'] . "%");
        }
        if (isset($_POST['nid']) && !empty($_POST['nid'])) {
            $nid = $_POST['nid'];
            $queryString->where('reliefs.nid', 'like', "%" . $_POST['nid'] . "%");
        }
        if (isset($_POST['card_type']) && !empty($_POST['card_type'])) {
            $card_type = $_POST['card_type'];
            $queryString->where('reliefs.card_type', '=', $_POST['card_type']);
        }
        if (isset($_POST['trible']) && !empty($_POST['trible'])) {
            $trible = $_POST['trible'];
            if ($trible == -1) {
                $queryString->where('reliefs.trible', '=', 0);
            } else {
                $queryString->Where(function ($query) use ($trible) {
                    $query->where('reliefs.trible', '=', $trible);
                });
            }
        }
        if (isset($_POST['card_no']) && !empty($_POST['card_no'])) {
            $card_no = $_POST['card_no'];
            $queryString->where('reliefs.card_no', '=', $_POST['card_no']);
        }
        if (isset($_POST['poss_help']) && !empty($_POST['poss_help'])) {
            $poss_help = $_POST['poss_help'];
            if ($poss_help == -1) {
                $queryString->where('reliefs.poss_help', '!=', 1);
            } else {
                $queryString->where('reliefs.poss_help', '=', $_POST['poss_help']);
            }
        }
        if (isset($_POST['ssnp']) && !empty($_POST['ssnp'])) {
            $ssnp = $_POST['ssnp'];
            if ($ssnp == -1) {
                $queryString->where('reliefs.ssnp', '=', 0);
            } else {
                $queryString->Where(function ($query) use ($ssnp) {
                    $query->where('reliefs.ssnp', '=', $ssnp);
                    $query->orWhereRaw("find_in_set($ssnp,reliefs.f_ssnp)");
                });
            }

        }
        if (isset($_POST['packages']) && !empty($_POST['packages'])) {
            $packages = $_POST['packages'];
            if ($packages == -1) {
                $queryString->whereNull('relief_mappings.packeges');
            } else {
                $queryString->whereRaw("find_in_set({$_POST['packages']},relief_mappings.packeges)");
            }

        }
        if (isset($_POST['search_by_id']) && !empty($_POST['search_by_id'])) {
            $search_by_id = $_POST['search_by_id'];
            $datas = explode(",", $search_by_id);
            $queryString->whereIn('reliefs.id', $datas);
        }
        if (isset($_POST['firm_land']) && !empty($_POST['firm_land'])) {
            $firm_land = $_POST['firm_land'];
            $queryString->where('reliefs.firm_land', '=', $_POST['firm_land']);
        }
        if (isset($_POST['baby_food']) && !empty($_POST['baby_food'])) {
            $baby_food = $_POST['baby_food'];
            if ($baby_food == 1)
                $queryString->where('reliefs.tot_child', '>=', 1);
            else
                $queryString->where('reliefs.tot_child', '=', 0);

        }
        if (isset($_POST['proffs']) && !empty($_POST['proffs'])) {
            $baby_food = $_POST['proffs'];
            if ($baby_food == -99)
                $queryString->Where(function ($query) {
                    $query->where('reliefs.profession', 'LIKE', '%ক্ষুদ্র%');
                    $query->orWhere('reliefs.profession', 'LIKE', '%ক্ষৃদ%');
                    $query->orWhere('reliefs.profession', 'LIKE', '%খুদ্%');
                    $query->orWhere('reliefs.profession', 'LIKE', '%খুদ%');
                    $query->orWhere('reliefs.profession', 'LIKE', '%ক্ষদ%');
                });
            //                $queryString->where('reliefs.profession', 'LIKE', '%ক্ষুদ্র%')
//                    ->orWhere('reliefs.profession', 'LIKE', '%ক্ষৃদ%')
//                    ->orWhere('reliefs.profession', 'LIKE', '%খুদ্%')
//                    ->orWhere('reliefs.profession', 'LIKE', '%খুদ%')
//                    ->orWhere('reliefs.profession', 'LIKE', '%ক্ষদ%');
            else
                $queryString->where('reliefs.profession', '=', 0);

        }

        //        if (isset($_POST['training_id']) && !empty($_POST['training_id'])) {
//            $training_id = $_POST['training_id'];
//            $queryString->where('registrations.training_id', '=', $_POST['training_id']);
//        }
//        if (isset($_POST['reg_status']) && !empty($_POST['reg_status'])) {
//            $reg_status = $_POST['reg_status'];
//            $queryString->where('registrations.status', '=', $_POST['reg_status']);
//        }
//        if (isset($_POST['trade_name']) && !empty($_POST['trade_name'])) {
//            $trade_name = $_POST['trade_name'];
//            $queryString->where('trainings.trade_name', '=', $_POST['trade_name']);
//        }
        $reliefData = $queryString->get();
        //        dd($reliefData);
        $this->__exportBulkRegList($reliefData);
    }

    public function adminExportBulkRegistrationReport(Request $request)
    {
        //        pr($_POST);
//        dd();

        $reliefModel = '';
        $reliefModelVw = '';
        if (isset($_POST['upo']) && !empty($_POST['upo'])) {
            $upo = $_POST['upo'];
            switch ($upo) {
                case 385:
                    $reliefModel = new VwSadar();
                    $reliefModelVw = 'sadar_vw';
                    break;
                case 386:
                    $reliefModel = new VwKashiyani();
                    $reliefModelVw = 'kasiyani_vw';
                    break;
                case 387:
                    $reliefModel = new VwTungipara();
                    $reliefModelVw = 'tungipara_vw';
                    break;
                case 388:
                    $reliefModel = new VwKotalipara();
                    $reliefModelVw = 'kotalipara_vw';
                    break;
                case 389:
                    $reliefModel = new VwMuksudpur();
                    $reliefModelVw = 'muksudpur_vw';
                    break;
                default:
                    $reliefModel = new Relief();
                    $reliefModelVw = 'reliefs';
                    break;
            }
        }


        $queryString = $reliefModel::query();

        $queryString->select("ssnps.ssnp_name AS ssnp_name", "districts.bn_name AS dis_name", "upazilas.bn_name AS upa_name", "unions.bn_name AS uni_name", $reliefModelVw . '.id', $reliefModelVw . '.card_no', $reliefModelVw . '.card_type', $reliefModelVw . '.trible', $reliefModelVw . '.name', $reliefModelVw . '.g_name', $reliefModelVw . '.total_member', $reliefModelVw . '.hw_name', $reliefModelVw . '.age', $reliefModelVw . '.dob', $reliefModelVw . '.gender', $reliefModelVw . '.house_pre', $reliefModelVw . '.road_pre', $reliefModelVw . '.word_pre', $reliefModelVw . '.upo_pre', $reliefModelVw . '.uni_pre', $reliefModelVw . '.dist_pre', $reliefModelVw . '.holding_pre', $reliefModelVw . '.profession', $reliefModelVw . '.phone', $reliefModelVw . '.nid', $reliefModelVw . '.ssnp', $reliefModelVw . '.poss_help')
            ->where($reliefModelVw . '.is_deleted', '!=', config('constants.is_deleted.Deleted'))
            //                ->leftJoin('users', 'reliefs.created_by', 'users.id')
            ->leftJoin('upazilas', 'upazilas.id', '=', $reliefModelVw . '.upo_pre')
            ->leftJoin('unions', 'unions.id', '=', $reliefModelVw . '.uni_pre')
            ->leftJoin('districts', 'districts.id', '=', $reliefModelVw . '.dist_pre')
            //            ->leftJoin('relief_mappings', 'relief_mappings.reg_id', '=', $reliefModelVw . '.id')
            ->leftJoin('ssnps', 'ssnps.id', '=', $reliefModelVw . '.ssnp');

        //        , 'relief_family_searches.tot_male', 'relief_family_searches.tot_female', 'relief_family_searches.tot_transgen', 'relief_family_searches.tot_child'
//        $queryString->select("districts.bn_name AS dis_name", "upazilas.bn_name AS upa_name", "upazilas.up_code AS upa_code", "unions.bn_name AS uni_name", 'reliefs.id', 'reliefs.dob', 'reliefs.card_no', 'reliefs.card_type', 'reliefs.name', 'reliefs.g_name', 'reliefs.poss_help', 'reliefs.total_member', 'reliefs.hw_name', 'reliefs.age', 'reliefs.gender', 'reliefs.house_pre', 'reliefs.road_pre', 'reliefs.word_pre', 'reliefs.upo_pre', 'reliefs.uni_pre', 'reliefs.holding_pre', 'reliefs.dist_pre', 'reliefs.profession', 'reliefs.phone', 'reliefs.nid', 'reliefs.ssnp', 'reliefs.tot_child', 'ssnps.ssnp_name', 'relief_mappings.reg_id', 'relief_mappings.packeges', 'relief_mappings.delv_date', 'relief_mappings.last_give_pac')
//            ->where($reliefModelVw . '.is_deleted', '!=', config('constants.is_deleted.Deleted'))
////            ->leftJoin('users', 'reliefs.created_by', 'users.id')
//            ->leftJoin('upazilas', 'upazilas.id', '=', $reliefModelVw . '.upo_pre')
//            ->leftJoin('unions', 'unions.id', '=', $reliefModelVw . '.uni_pre')
//            ->leftJoin('districts', 'districts.id', '=', $reliefModelVw . '.dist_pre')
//            ->leftJoin('relief_mappings', 'relief_mappings.reg_id', '=', $reliefModelVw . '.id')
//            ->leftJoin('ssnps', 'ssnps.id', '=', $reliefModelVw . '.ssnp');

        $search_by_id = $_POST['search_by_id'];

        if (isset($_POST['dist']) && !empty($_POST['dist'])) {
            $dist = $_POST['dist'];
            $queryString->where($reliefModelVw . '.dist_pre', '=', $_POST['dist']);
        }
        if (isset($_POST['upo']) && !empty($_POST['upo'])) {
            $upo = $_POST['upo'];
            $queryString->where($reliefModelVw . '.upo_pre', '=', $_POST['upo']);
        }
        if (isset($_POST['uni_per'])) {
            $uni_per = $_POST['uni_per'];
            if (!empty($_POST['uni_per']))
                $queryString->where($reliefModelVw . '.uni_pre', '=', $_POST['uni_per']);
            if ($_POST['uni_per'] === '0') {
                $queryString->where($reliefModelVw . '.uni_pre', '=', $_POST['uni_per']);
            }
        }
        if (isset($_POST['word_no']) && !empty($_POST['word_no'])) {
            $word_no = $_POST['word_no'];
            //            $queryString->where($reliefModelVw . '.word_pre', '=', $_POST['word_no']);
            $queryString->whereIn($reliefModelVw . '.word_pre', $word_no);
        }
        if (isset($_POST['name']) && !empty($_POST['name'])) {
            $name = $_POST['name'];
            $queryString->where($reliefModelVw . '.name', 'like', "%" . $_POST['name'] . "%");
        }
        if (isset($_POST['phone']) && !empty($_POST['phone'])) {
            $phone = $_POST['phone'];
            $queryString->where($reliefModelVw . '.phone', 'like', "%" . $_POST['phone'] . "%");
        }
        if (isset($_POST['nid']) && !empty($_POST['nid'])) {
            $nid = $_POST['nid'];
            $queryString->where($reliefModelVw . '.nid', 'like', "%" . $_POST['nid'] . "%");
        }
        if (isset($_POST['card_type']) && !empty($_POST['card_type'])) {
            $card_type = $_POST['card_type'];
            //            $queryString->where($reliefModelVw . '.card_type', '=', $_POST['card_type']);
            $queryString->whereIn($reliefModelVw . '.card_type', $card_type);
        }
        if (isset($_POST['trible']) && !empty($_POST['trible'])) {
            $trible = $_POST['trible'];
            if ($trible == -1) {
                $queryString->where($reliefModelVw . '.trible', '=', 0);
            } else {
                $queryString->Where(function ($query) use ($trible, $reliefModelVw) {
                    $query->where($reliefModelVw . '.trible', '=', $trible);
                });
            }
        }
        if (isset($_POST['poss_help']) && !empty($_POST['poss_help'])) {
            $poss_help = $_POST['poss_help'];
            if ($poss_help == -1) {
                $queryString->where($reliefModelVw . '.poss_help', '!=', 1);
            } else {
                $queryString->where($reliefModelVw . '.poss_help', '=', $_POST['poss_help']);
            }
        }
        if (isset($_POST['card_no']) && !empty($_POST['card_no'])) {
            $card_no = $_POST['card_no'];
            $queryString->where($reliefModelVw . '.card_no', 'like', "%" . $_POST['card_no'] . "%");
        }
        if (isset($_POST['search_by_id']) && !empty($_POST['search_by_id'])) {
            $search_by_id = $_POST['search_by_id'];
            $datas = explode(",", $search_by_id);
            $queryString->whereIn($reliefModelVw . '.id', $datas);
        }
        if (isset($_POST['ssnp']) && !empty($_POST['ssnp'])) {
            $ssnp = $_POST['ssnp'];
            if ($ssnp == -1) {
                $queryString->where($reliefModelVw . '.ssnp', '=', 0);
            } else {
                $queryString->Where(function ($query) use ($ssnp, $reliefModelVw) {
                    $query->where($reliefModelVw . '.ssnp', '=', $ssnp);
                    $query->orWhereRaw("find_in_set($ssnp,'f_ssnp')");
                });
            }
        }
        if (isset($_POST['packages']) && !empty($_POST['packages'])) {
            $packages = $_POST['packages'];
            if ($packages == -1) {
                //                        $queryString->whereNull('relief_mappings.packeges');
                $queryString->whereNotIn($reliefModelVw . '.id', function ($query) {
                    $query->select("relief_mappings.reg_id")
                        ->from('relief_mappings');
                    //                            $query->select(DB::raw(1))
//                            $query->whereRaw("find_in_set({$_GET['packages']},relief_mappings.packeges)");
                });
            } else {
                $queryString->whereRaw("find_in_set({$_POST['packages']},relief_mappings.packeges)");
            }

        }
        //                if (isset($_POST['firm_land']) && !empty($_POST['firm_land'])) {
//                    $firm_land = $_POST['firm_land'];
//                    $queryString->where('reliefs.firm_land', '=', $_POST['firm_land']);
//                }
        if (isset($_POST['baby_food']) && !empty($_POST['baby_food'])) {
            $baby_food = $_POST['baby_food'];
            if ($baby_food == 1)
                $queryString->where($reliefModelVw . '.tot_child', '>=', 1);
            else
                $queryString->where($reliefModelVw . '.tot_child', '=', 0);

        }
        if (isset($_POST['proffs']) && !empty($_POST['proffs'])) {
            $baby_food = $_POST['proffs'];
            if ($baby_food == -99)
                $queryString->Where(function ($query) {
                    $query->where($reliefModelVw . '.profession', 'LIKE', '%ক্ষুদ্র%');
                    $query->orWhere($reliefModelVw . '.profession', 'LIKE', '%ক্ষৃদ%');
                    $query->orWhere($reliefModelVw . '.profession', 'LIKE', '%খুদ্%');
                    $query->orWhere($reliefModelVw . '.profession', 'LIKE', '%খুদ%');
                    $query->orWhere($reliefModelVw . '.profession', 'LIKE', '%ক্ষদ%');
                });
            else
                $queryString->where($reliefModelVw . '.profession', '=', 0);

        }

        if (isset($_POST['packages']) && !empty($_POST['packages'])) {
            if ($packages == -1) {
            } else {
                $queryString->addSelect('relief_mappings.reg_id', 'relief_mappings.packeges', 'relief_mappings.delv_date', 'relief_mappings.last_give_pac')
                    ->leftJoin('relief_mappings', 'relief_mappings.reg_id', '=', $reliefModelVw . '.id');
            }
        }

        $reliefData = $queryString->get();
        //            $reliefData = $queryString->toSql();
//            dd($reliefData);
//        pr($reliefData);
//        dd();
//        dd($reliefData);
        $this->__exportBulkRegList($reliefData);
    }

    public function adminTrainingReport()
    {
        if (isset($_GET['displayValue'])) {
            $displayValue = $_GET['displayValue'] != '' ? $_GET['displayValue'] : "";
        } else {
            $displayValue = '';
        }

        if (isset($_GET['searchData'])) {
            $searchData = $_GET['searchData'] != '' ? $_GET['searchData'] : "";
        } else {
            $searchData = '';
        }

        $trainingModel = new Training();
        $departmentModel = new Department();
        $tradeModel = new Trade();
        $departmentList = $departmentModel->where('status', '=', config('constants.status.Active'))
            ->pluck('name', 'id')->all();
        $tradeList = $tradeModel->where('status', '=', config('constants.status.Active'))
            ->pluck('trade_name', 'id')->all();

        $queryString = $trainingModel::query();
        $queryString = $trainingModel::query();
        $queryString->select("users.first_name")->where('is_deleted', '!=', config('constants.is_deleted.Deleted'))
            //            ->where('trainings.name', 'like', '%' . $searchData . '%')
            ->leftJoin('users', 'trainings.created_by', 'users.id')
            ->select('trainings.*', 'users.first_name');
        //            ->tosql();
//            ->get();
        if (request()->ajax()) {
            //            dd($_GET);
            if (isset($_GET['dept_id']) && !empty($_GET['dept_id'])) {
                $queryString->where('trainings.dept_id', '=', $_GET['dept_id']);
            }
            if (isset($_GET['start_date']) && !empty($_GET['start_date'])) {
                $strDate = date('Y-m-d', strtotime($_GET['start_date']));
                $strDate = $strDate . " 00:00:00";
                $queryString->where('trainings.start_date', '>=', $strDate);

                $endDate = date("Y-m-d", strtotime("now")) . " 00:00:00";
                if (isset($_GET['end_date']) && !empty($_GET['end_date'])) {
                    $endDate = date('Y-m-d', strtotime($_GET['end_date']));
                    $endDate = $endDate . " 00:00:00";
                    $queryString->where('trainings.end_date', '<=', $endDate);
                }
                //                $queryString->whereBetween('created_at',[$strDate,$endDate]);
            }
            if (isset($_GET['status']) && !empty($_GET['status'])) {
                $queryString->where('trainings.status', '=', $_GET['status']);
            }
            if (isset($_GET['trade_name']) && !empty($_GET['trade_name'])) {
                $trade_name = $_GET['trade_name'];
                $queryString->where('trainings.trade_name', '=', $_GET['trade_name']);
            }

            $trainingList = $queryString->get();
            return view('report.ajax_training_report', compact('trainingList', 'departmentList', 'tradeList'));
        } else {
            $trainingList = $queryString->get();
            return view('report.adminTrainingReport', compact('trainingList', 'departmentList', 'tradeList'));
        }
    }

    public function adminTrainingReportExport()
    {
        $trainingModel = new Training();
        $departmentModel = new Department();
        $departmentList = $departmentModel->where('status', '=', config('constants.status.Active'))
            ->pluck('name', 'id')->all();

        $queryString = $trainingModel::query();
        $queryString = $trainingModel::query();
        $queryString->select("users.first_name")->where('is_deleted', '!=', config('constants.is_deleted.Deleted'))
            //            ->where('trainings.name', 'like', '%' . $searchData . '%')
            ->leftJoin('users', 'trainings.created_by', 'users.id')
            ->select('trainings.*', 'users.first_name');
        //            ->tosql();
//            ->get();
//        if (request()->ajax()) {
//            dd($_POST);
        if (isset($_POST['dept_id']) && !empty($_POST['dept_id'])) {
            $queryString->where('trainings.dept_id', '=', $_POST['dept_id']);
        }
        if (isset($_POST['start_date']) && !empty($_POST['start_date'])) {
            $strDate = date('Y-m-d', strtotime($_POST['start_date']));
            $strDate = $strDate . " 00:00:00";
            $queryString->where('trainings.start_date', '>=', $strDate);

            $endDate = date("Y-m-d", strtotime("now")) . " 00:00:00";
            if (isset($_POST['end_date']) && !empty($_POST['end_date'])) {
                $endDate = date('Y-m-d', strtotime($_POST['end_date']));
                $endDate = $endDate . " 00:00:00";
                $queryString->where('trainings.end_date', '<=', $endDate);
            }
        }
        if (isset($_POST['status']) && !empty($_POST['status'])) {
            $queryString->where('trainings.status', '=', $_POST['status']);
        }

        if (isset($_POST['trade_name']) && !empty($_POST['trade_name'])) {
            $trade_name = $_POST['trade_name'];
            $queryString->where('trainings.trade_name', '=', $_POST['trade_name']);
        }
        //        }

        $trainingList = $queryString->get();
        $this->__exportTrainingList($trainingList, $departmentList);

    }

    public function adminParticipantReport()
    {
        $trainingModel = new Training();
        $departmentModel = new Department();
        $registrationModel = new Registration();
        $tradeModel = new Trade();

        $trainingList = $trainingModel->where('status', '=', config('constants.training_status.Pending'))
            ->where('is_deleted', '=', config('constants.is_deleted.Active'))
            ->pluck('name', 'id')->all();

        $departmentList = $departmentModel->where('status', '=', config('constants.status.Active'))
            ->pluck('name', 'id')->all();

        $tradeList = $tradeModel->where('status', '=', config('constants.status.Active'))
            ->pluck('trade_name', 'id')->all();

        $queryString = $registrationModel::query();
        $queryString->select("users.first_name", 'trainings.trade_name', 'registrations.*')
            ->where('registrations.status', '!=', config('constants.registration_status.Waiting'))
            ->leftJoin('users', 'registrations.created_by', 'users.id')
            ->leftJoin('trainings', 'registrations.training_id', 'trainings.id');
        //            ->tosql();
//            ->paginate($displayValue);

        $training_id = "";
        $trade_name = "";
        $reg_status = "";
        $registrationStatusArr = [
            2 => 'Approved',
            3 => 'Confirmed',
        ];
        $titleName = "Participant Report";
        if (request()->ajax()) {
            if (isset($_GET['training_id']) && !empty($_GET['training_id'])) {
                $training_id = $_GET['training_id'];
                $queryString->where('registrations.training_id', '=', $_GET['training_id']);
            }
            if (isset($_GET['reg_status']) && !empty($_GET['reg_status'])) {
                $trade_name = $_GET['reg_status'];
                $queryString->where('registrations.status', '=', $_GET['reg_status']);
            }
            if (isset($_GET['trade_name']) && !empty($_GET['trade_name'])) {
                $reg_status = $_GET['trade_name'];
                $queryString->where('trainings.trade_name', '=', $_GET['trade_name']);
            }
            $registrationList = $queryString->get();
            return view('report.ajax_participant_report', compact('registrationList', 'departmentList', 'trainingList', 'training_id', 'trade_name', 'reg_status', 'registrationStatusArr', 'titleName', 'tradeList'));
        } else {
            $registrationList = $queryString->get();
            return view('report.adminParticipantReport', compact('registrationList', 'departmentList', 'trainingList', 'training_id', 'trade_name', 'reg_status', 'registrationStatusArr', 'titleName', 'tradeList'));
        }
    }

    public function adminParticipantReportExport(Request $request)
    {
        $trainingModel = new Training();
        $departmentModel = new Department();
        $registrationModel = new Registration();

        $trainingList = $trainingModel->where('is_deleted', '=', config('constants.is_deleted.Active'))
            ->pluck('name', 'id')->all();

        $departmentList = $departmentModel->where('status', '=', config('constants.status.Active'))
            ->pluck('name', 'id')->all();

        $queryString = $registrationModel::query();
        $queryString->select("users.first_name", 'registrations.*')
            ->where('registrations.status', '!=', config('constants.registration_status.Waiting'))
            //            ->where('registrations.status','!=',config('constants.registration_status.Waiting'))
            ->leftJoin('users', 'registrations.created_by', 'users.id')
            ->leftJoin('trainings', 'registrations.training_id', 'trainings.id');
        //            ->tosql();
//            ->paginate($displayValue);

        $training_id = "";
        $training_status = "";
        $reg_status = "";

        if (isset($_POST['training_id']) && !empty($_POST['training_id'])) {
            $training_id = $_POST['training_id'];
            $queryString->where('registrations.training_id', '=', $_POST['training_id']);
        }
        if (isset($_POST['reg_status']) && !empty($_POST['reg_status'])) {
            $training_status = $_POST['reg_status'];
            $queryString->where('registrations.status', '=', $_POST['reg_status']);
        }
        if (isset($_POST['trade_name']) && !empty($_POST['trade_name'])) {
            $reg_status = $_POST['trade_name'];
            $queryString->where('trainings.trade_name', '=', $_POST['trade_name']);
        }
        $registrationList = $queryString->get();
        $this->__exportRegList($registrationList, $trainingList, $departmentList);
    }

    public function adminMemberSearchData()
    {
        $reliefModel = new Relief();
        $reliefDetailsModel = new ReliefDetails();
        $reliefFamilySearchModel = new ReliefFamilySearch();

        //        relief_family_searches

        DB::beginTransaction();

        try {
            $queryString = $reliefDetailsModel::query();
            $queryString->select('relief_details.rlf_id', 'relief_details.age', 'relief_details.f_gender', 'relief_details.nid', 'relief_details.ssnp');
            $queryString->groupBy('rlf_id');
            $reliefDetailsData = $queryString->get()->toArray();

            $reliefFamilySearchModel;

            $tblData = [];
            $count = 0;
            //            dd("end");

            //            $tblData['rlf_id'] = json_decode($reliefDetailsData->rlf_id, true);
//            $tblData['age'] = json_decode($reliefDetailsData->age, true);
//            $tblData['nid'] = json_decode($reliefDetailsData->nid, true);
//            $tblData['ssnp'] = json_decode($reliefDetailsData->ssnp, true);
//            $tblData['f_gender'] = json_decode($reliefDetailsData->f_gender, true);

            foreach ($reliefDetailsData as $k_name => $k_values) {
                if ($count > 20) {
                    //                    break;
                }

                $male = $female = $childern = $transgen = 0;
                $f_nid = $f_ssnp = "";
                $familySearchArr = [];
                $member_age = $member_gender = 1;
                //                pr($k_values);
                if (array_key_exists('age', $k_values)) {
                    $ageDatas = json_decode($k_values['age'], true);
                    if (!empty($k_values['age'])) {
                        foreach ($ageDatas as $ageData) {
                            if (!empty($ageData)) {
                                $member_age++;
                                if ($ageData < 6) {
                                    $childern++;
                                }
                            }
                        }
                    }
                    $familySearchArr[$k_name]['tot_child'] = $childern;
                }
                if (array_key_exists('f_gender', $k_values)) {
                    $genDatas = json_decode($k_values['f_gender'], true);
                    if (!empty($k_values['f_gender'])) {
                        foreach ($genDatas as $genData) {
                            $member_gender++;
                            if (!empty($genData) && ($genData == 1)) {
                                $male++;
                            }
                            if (!empty($genData) && ($genData == 2)) {
                                $female++;
                            }
                            if (!empty($genData) && ($genData == 3)) {
                                $transgen++;
                            }
                        }
                    }
                    $familySearchArr[$k_name]['tot_male'] = $male;
                    $familySearchArr[$k_name]['tot_female'] = $female;
                    $familySearchArr[$k_name]['tot_transgen'] = $transgen;
                    //                    $tot_mem = 1;
//                    if($member_age >= $member_gender ){
//                        $tot_mem = $tot_mem + $member_age;
//                    }else{
//                        $tot_mem = $tot_mem + $member_gender;
//                    }
                    $familySearchArr[$k_name]['tot_f_member'] = $member_age;
                }
                if (array_key_exists('nid', $k_values)) {
                    $nidDatas = json_decode($k_values['nid'], true);
                    if (!empty($k_values['nid'])) {
                        foreach ($nidDatas as $nidData) {
                            if (!empty($nidData)) {
                                $f_nid .= $nidData . ',';
                            }
                        }
                    }
                    $f_nid .= "";
                    $familySearchArr[$k_name]['f_nid'] = $f_nid;
                }
                if (array_key_exists('ssnp', $k_values)) {
                    $ssnpDatas = json_decode($k_values['ssnp'], true);
                    if (!empty($k_values['ssnp'])) {
                        foreach ($ssnpDatas as $ssnpData) {
                            if (!empty($ssnpData)) {
                                if (($ssnpData === 'তথ্য পাওয়া যায়নি।') || ($ssnpData === 'তথ্য নাই') || ($ssnpData === 'তথ্য নেই')) {
                                    $ssnpData = "";
                                }
                                $f_ssnp .= $ssnpData . ',';
                            }
                        }
                    }
                    $f_ssnp .= "";
                    $familySearchArr[$k_name]['f_ssnp'] = $f_ssnp;
                }

                $familySearchArr[$k_name]['relf_id'] = $k_values['rlf_id'];

                ReliefFamilySearch::insert($familySearchArr);

                //                $request->request->add(['familySearchData' => $familySearchArr]);
//                $res = $reliefFamilySearchModel->saveData($request);
                $count++;
            }
            pr("Done");
            DB::commit();

        } catch (\Exception $serchDataSaveException) {
            dd($serchDataSaveException);
            DB::rollback();
        }
    }

    public function adminReliefUpdate()
    {
        $reliefModel = new Relief();
        $reliefDetailsModel = new ReliefDetails();
        $reliefFamilySearchModel = new ReliefFamilySearch();

        DB::beginTransaction();

        try {
            $queryString = $reliefFamilySearchModel::query();
            $queryString->select('relief_family_searches.tot_child', 'relief_family_searches.tot_male', 'relief_family_searches.tot_female', 'relief_family_searches.tot_transgen', 'relief_family_searches.f_nid', 'relief_family_searches.f_ssnp', 'relief_family_searches.relf_id', 'relief_family_searches.tot_f_member');
            $queryString->groupBy('relf_id');
            $reliefDetailsData = $queryString->get()->toArray();

            $reliefFamilySearchModel;

            $tblData = [];
            $count = 0;
            //            dd($reliefDetailsData);

            //            $tblData['rlf_id'] = json_decode($reliefDetailsData->rlf_id, true);
//            $tblData['age'] = json_decode($reliefDetailsData->age, true);
//            $tblData['nid'] = json_decode($reliefDetailsData->nid, true);
//            $tblData['ssnp'] = json_decode($reliefDetailsData->ssnp, true);
//            $tblData['f_gender'] = json_decode($reliefDetailsData->f_gender, true);

            foreach ($reliefDetailsData as $k_name => $k_values) {
                //                if ($count > 20) {
//                    break;
//                }
//                pr($k_values);
//                pr($k_values);
                $reliefId = $k_values['relf_id'];
                unset($k_values['relf_id']);
                //                dd($k_values);
                Relief::where('id', $reliefId)->update($k_values);
                $count++;
            }
            pr("Done");
            DB::commit();

        } catch (\Exception $serchDataSaveException) {
            dd($serchDataSaveException);
            DB::rollback();
        }
    }

    public function adminImportPackageAssign()
    {
        $packageModel = new Package();
        $ssnpModel = new Ssnp();

        $packageList = $packageModel->where('status', '=', config('constants.package_status.Running'))->pluck('title', 'id')->all();
        $ssnpList = $ssnpModel->where('status', '=', config('constants.status.Active'))->pluck('ssnp_name', 'id')->all();
        $landList = config('constants.land_info');

        return view('report.adminImportPackageAssign', compact('packageList', 'landList', 'ssnpList'));

    }

    public function adminSaveImportPackageAssign(Request $request)
    {
        set_time_limit(0);

        $reliefModel = new Relief();
        $reliefDetailsModel = new ReliefDetails();
        $reliefMappingModel = new ReliefMapping();
        $packageModel = new Package();

        $packageList = $packageModel->where('status', '=', config('constants.package_status.Running'))->pluck('title', 'id')->all();

        $packag = $request->post('package');
        $ssnp = $request->post('ssnp');
        $ssnp_for = $request->post('ssnp_for');
        $firm_land = $request->post('firm_land');
        $dob = $request->post('dob');

        $dob = date('Y-m-d', strtotime($dob));
        if (empty($dob)) {
            $dob = date('Y-m-d', time());
        }

        $this->validate($request, [
            'select_file' => 'required|mimes:xls,xlsx'
        ]);

        $path = $request->file('select_file')->getRealPath();

        $data = Excel::load($path)->get();
        $insert_data = [];
        $error_msg = [];
        $success_msg = [];
        if ($data->count() > 0) {
            if (!empty($ssnp_for) && ($ssnp_for == 2)) {
                foreach ($data->toArray() as $key => $value) {
                    //                    $resData = $reliefModel->where('f_nid', 'like', "%" . $value['nid'] . "%")
//                        ->where('is_deleted', '!=', config('constants.is_deleted.Deleted'))
//                        ->select('id', 'f_ssnp')
//                        ->first();
//                    ->toSql();

                    //                    ->where('phone','like', "%" .$value['mobile'] . "%")
//                    ->where('card_no','like', "%" .$value['card_no'] . "%")

                    //                pr($resData->toArray());
//                pr($resData);
                    $nidLen = strlen(trim($value['nid']));
                    if ($nidLen < 10) {
                        $emsg = 'Invalid NID, minimum 10 digit = ' . $value['nid'];
                    } else {
                        $rlfDetailsData = $reliefDetailsModel->where(('nid'), 'like', "%" . trim($value['nid']) . "%")
                            ->where('is_deleted', '!=', config('constants.is_deleted.Deleted'))
                            ->select('id', 'rlf_id', 'nid', 'ssnp')
                            ->get()
                            ->toArray();
                        //                        ->toSql();

                        $emsg = '';
                        $smsg = '';
                        $reg_id = '';
                        $oldSsnp = '';
                        //        pr($rlfDetailsData);

                        $arraySize = count($rlfDetailsData);
                        if (!empty($rlfDetailsData)) {
                            if ($arraySize > 1) {
                                $emsg = 'Duplicate Member data found for NID = ' . trim($value['nid']);
                            } else {
                                foreach ($rlfDetailsData as $key => $members) {
                                    //                $decodeDataArr = json_decode($members);
                                    $reg_id = $members['rlf_id'];
                                    $det_id = $members['id'];
                                    $dcodeNid = json_decode($members['nid'], true);
                                    $dcodesnnp = json_decode($members['ssnp'], true);

                                    //                pr($dcodeNid);
//                pr($dcodesnnp);

                                    $srKey = array_search(trim($value['nid']), $dcodeNid);
                                    //                pr($dcodesnnp[$srKey]);

                                    if (!empty($dcodesnnp[$srKey])) {
                                        if ($dcodesnnp[$srKey] >= 1) {
                                            $emsg = 'Already Using SSNP ID = ' . $dcodesnnp[$srKey] . ' for NID = ' . trim($value['nid']);
                                        } else {
                                            $dcodesnnp[$srKey] = $ssnp;
                                            $newSsnp = json_encode($dcodesnnp);

                                            //                        try{
                                            $reliefDetailsModel->where('id', '=', $det_id)
                                                ->update(['ssnp' => $newSsnp]);

                                            $reliefData = $reliefModel->where('id', '=', $reg_id)
                                                ->select('id', 'f_ssnp', 'f_nid')
                                                ->get()
                                                ->toArray();

                                            if (!empty($reliefData)) {
                                                $oldfssnp = $reliefData[0]['f_ssnp'];
                                                $newfssnp = $oldfssnp . ',' . $ssnp;
                                                $reliefModel->where('id', '=', $reg_id)
                                                    ->update(['f_ssnp' => $newfssnp]);
                                            }
                                            //                        }catch (\Exception $exception){
//                        }
                                            $smsg = 'Data Updated for NID = ' . trim($value['nid']) . ', SSNP = ' . $ssnp;
                                        }
                                    } else {

                                        $dcodesnnp[$srKey] = $ssnp;
                                        $newSsnp = json_encode($dcodesnnp);

                                        //                        try{
                                        $reliefDetailsModel->where('id', '=', $det_id)
                                            ->update(['ssnp' => $newSsnp]);

                                        $reliefData = $reliefModel->where('id', '=', $reg_id)
                                            ->select('id', 'f_ssnp', 'f_nid')
                                            ->get()
                                            ->toArray();
                                        //                                pr($reliefData);
//                    dd();


                                        if (!empty($reliefData)) {
                                            $oldfssnp = $reliefData[0]['f_ssnp'];
                                            $newfssnp = $oldfssnp . ',' . $ssnp;
                                            $reliefModel->where('id', '=', $reg_id)
                                                ->update(['f_ssnp' => $newfssnp]);
                                        }
                                        //                        }catch (\Exception $exception){
//                        }
                                        $smsg = 'Data Updated for NID = ' . trim($value['nid']) . ', SSNP = ' . $ssnp;
                                    }
                                }
                            }
                        } else {
                            $emsg = 'No Family Member data found for NID = ' . trim($value['nid']) . ', NID = ' . trim($value['nid']) . ', Phome = ' . $value['mobile'] . ',Card No. = ' . $value['card_no'];
                        }
                    }


                    if (!empty($emsg)) {
                        $error_msg[] = $emsg;
                    }
                    if (!empty($smsg)) {
                        $success_msg[] = $smsg;
                    }
                }
                //                dd();
            } else {
                foreach ($data->toArray() as $key => $value) {
                    //                    $resData = $reliefModel->where('nid', 'like', "%" . trim($value['nid']) . "%")
                    $resData = $reliefModel->where('card_no', 'like', "%" . trim($value['card_no']) . "%")
                        ->where('is_deleted', '!=', config('constants.is_deleted.Deleted'))
                        ->select('id', 'ssnp')
                        ->first();
                    //                    ->toSql();

                    //                    ->where('phone','like', "%" .$value['mobile'] . "%")
//                    ->where('card_no','like', "%" .$value['card_no'] . "%")

                    //                pr($resData->toArray());
//                pr($resData);

                    $emsg = '';
                    $smsg = '';
                    $reg_id = '';
                    $oldSsnp = '';
                    if (!empty($resData)) {
                        $resData = $resData->toArray();
                        $reg_id = $resData['id'];
                        $oldSsnp = $resData['ssnp'];
                    }

                    if (!empty($resData)) {
                        if (!empty($packag)) {
                            //                            echo 'pac \n';
                            $reliefMappingData = $reliefMappingModel->where('reg_id', $reg_id)
                                ->select('id', 'reg_id', 'packeges', 'last_give_pac', 'delv_date')
                                ->first();
                            $packeges = $delv_date = $last_give_pac = '';
                            if (!empty($reliefMappingData)) {
                                /*  1. Get the data and update the information */
                                /*  2. Calculate the given date and set the latest date to the 'last_give_pac' field... */
                                $reliefMappingData = $reliefMappingData->toArray();
                                $curDate = date("Y-m-d", time());
                                if (empty($dob)) {
                                    $dob = $curDate;
                                }

                                $packageData = explode(',', $reliefMappingData['packeges']);
                                /*  Previous data has, have to update   */
                                if (!in_array($packag, $packageData)) {
                                    $packeges = $reliefMappingData['packeges'] . $packag . ',';
                                    $delv_date = $reliefMappingData['delv_date'] . $dob . ',';
                                    $last_given_date = $reliefMappingData['last_give_pac'];
                                    $last_given_max_date = date('Y-m-d', strtotime($last_given_date));
                                    $reliefId = $reg_id;

                                    if ($dob > $last_given_max_date) {
                                        $last_given_date = date("Y-m-d h:i:s", strtotime($dob));
                                    }

                                    $updateData = [
                                        'packeges' => $packeges,
                                        'last_give_pac' => $last_given_date,
                                        'delv_date' => $delv_date,
                                        'updated_at' => date("Y-m-d h:i:s", time()),
                                    ];

                                    $responseData = $reliefMappingModel->where('reg_id', $reliefId)
                                        ->update($updateData);

                                    if ($responseData > 1) {
                                        $emsg = 'Cannot update ID = ' . $reliefId . ', Card No = ' . trim($value['card_no']) . ', Phone No = ' . $value['mobile'] . ', NID = ' . $value['nid'];
                                    } else {
                                        $smsg = 'Data Updated for ID = ' . $reliefId . ', Packages = ' . $packeges . ', Card No = ' . trim($value['card_no']);
                                    }
                                } else {
                                    $emsg = 'Already assign to  ' . $value['card_no'] . ' Card Number';
                                }
                            } else {
                                $curDate = date("Y-m-d", time());
                                if (empty($dob)) {
                                    $dob = $curDate;
                                }

                                $packeges = $packag . ',';
                                $delv_date = $dob . ',';
                                $reliefId = $reg_id;
                                $last_given_date = date("Y-m-d h:i:s", strtotime($dob));

                                $updateData = [
                                    'reg_id' => $reliefId,
                                    'packeges' => $packeges,
                                    'last_give_pac' => $last_given_date,
                                    'delv_date' => $delv_date,
                                    'created_at' => date("Y-m-d h:i:s", time()),
                                    'status' => 1,
                                    'updated_by' => auth()->user()->id,
                                ];

                                $responseData = $reliefMappingModel->insert($updateData);

                                if ($responseData > 1) {
                                    $emsg = 'Cannot update ID = ' . $reliefId . ', Card No = ' . $value['card_no'] . ', Phone No = ' . $value['mobile'] . ', NID = ' . trim($value['nid']);
                                } else {
                                    $smsg = 'Data Updated for ID = ' . $reliefId . ', Packages = ' . $packeges . ', Card No = ' . trim($value['card_no']);
                                }
                                //                        pr($responseData);
                                pr($reliefId . ' Data has been Created for 1st Entry...');
                            }
                        } elseif (!empty($firm_land)) {
                            $landupdateData = [
                                'firm_land' => $firm_land,
                                'updated_by' => auth()->user()->id,
                                'updated_at' => date("Y-m-d h:i:s", time()),
                            ];
                            $responseData = $reliefModel->where('id', $reg_id)
                                ->update($landupdateData);

                            if ($responseData > 1) {
                                $emsg = 'Cannot update ID = ' . $reg_id . ', Card No = ' . $value['card_no'] . ', Phone No = ' . $value['mobile'] . ', NID = ' . trim($value['nid']);
                            } else {
                                $smsg = 'Data Updated for ID = ' . $reg_id . ', Firm Land = ' . $firm_land;
                            }
                        }

                        if (!empty($ssnp)) {
                            if ($oldSsnp < 1) {
                                $updateData = [
                                    'ssnp' => $ssnp,
                                ];
                                $responseData = $reliefModel->where('id', $reg_id)
                                    ->update($updateData);

                                if ($responseData > 1) {
                                    $emsg = 'Cannot update ID = ' . $reg_id . ', Card No = ' . $value['card_no'] . ', Phone No = ' . $value['mobile'] . ', NID = ' . trim($value['nid']);
                                } else {
                                    $smsg = 'Data Updated for ID = ' . $reg_id . ', SSNP ID  = ' . $ssnp . ' and NID = ' . trim($value['nid']);
                                }
                            } else {
                                $emsg = 'Already Using SSNP ID = ' . $oldSsnp . ' for NID = ' . trim($value['nid']);
                            }
                        }
                    } else {
                        $emsg = 'No data found in ID = ' . $value['id'] . ', NID = ' . trim($value['nid']) . ', Phome = ' . $value['mobile'] . ',Card No. = ' . $value['card_no'];
                    }
                    if (!empty($emsg)) {
                        $error_msg[] = $emsg;
                    }
                    if (!empty($smsg)) {
                        $success_msg[] = $smsg;
                    }
                }
            }
        }
        if (!empty($error_msg)) {
            pr('Error Data List.');
            pr($error_msg);
        }
        if (!empty($success_msg)) {
            pr('Success Data List.');
            pr($success_msg);
        }
        //        if(!empty($error_msg)){
//            return back()->with('errors', 'Excel Data Imported Failed.')->with('msg',$error_msg);
//        }else{
//            return back()->with('success', 'Excel Data Imported successfully.')->with('msg',$success_msg);
//        }
    }

    public function adminImportPackageUnAssign()
    {
        $packageModel = new Package();
        $ssnpModel = new Ssnp();

        $packageList = $packageModel->where('status', '=', config('constants.package_status.Running'))->pluck('title', 'id')->all();
        //        $ssnpList = $ssnpModel->where('status', '=', config('constants.status.Active'))->pluck('ssnp_name', 'id')->all();
//        $landList = config('constants.land_info');

        return view('report.adminImportPackageUnAssign', compact('packageList'));

    }

    public function adminSaveImportPackageUnAssign(Request $request)
    {
        set_time_limit(0);

        $reliefModel = new Relief();
        $reliefDetailsModel = new ReliefDetails();
        $reliefMappingModel = new ReliefMapping();
        $packageModel = new Package();

        $packageList = $packageModel->where('status', '=', config('constants.package_status.Running'))->pluck('title', 'id')->all();

        $packag = $request->post('package');
        //        $ssnp = $request->post('ssnp');
//        $ssnp_for = $request->post('ssnp_for');
//        $firm_land = $request->post('firm_land');
//        $dob = $request->post('dob');

        //        $dob = date('Y-m-d', strtotime($dob));
//        if (empty($dob)) {
//            $dob = date('Y-m-d', time());
//        }

        $this->validate($request, [
            'select_file' => 'required|mimes:xls,xlsx'
        ]);

        $path = $request->file('select_file')->getRealPath();

        $data = Excel::load($path)->get();
        $insert_data = [];
        $error_msg = [];
        $success_msg = [];
        if ($data->count() > 0) {
            if (!empty($ssnp_for) && ($ssnp_for == 2)) {
                foreach ($data->toArray() as $key => $value) {
                    //                    $resData = $reliefModel->where('f_nid', 'like', "%" . $value['nid'] . "%")
//                        ->where('is_deleted', '!=', config('constants.is_deleted.Deleted'))
//                        ->select('id', 'f_ssnp')
//                        ->first();
//                    ->toSql();

                    //                    ->where('phone','like', "%" .$value['mobile'] . "%")
//                    ->where('card_no','like', "%" .$value['card_no'] . "%")

                    //                pr($resData->toArray());
//                pr($resData);
                    $nidLen = strlen(trim($value['nid']));
                    if ($nidLen < 10) {
                        $emsg = 'Invalid NID, minimum 10 digit = ' . $value['nid'];
                    } else {
                        $rlfDetailsData = $reliefDetailsModel->where(('nid'), 'like', "%" . trim($value['nid']) . "%")
                            ->where('is_deleted', '!=', config('constants.is_deleted.Deleted'))
                            ->select('id', 'rlf_id', 'nid', 'ssnp')
                            ->get()
                            ->toArray();
                        //                        ->toSql();

                        $emsg = '';
                        $smsg = '';
                        $reg_id = '';
                        $oldSsnp = '';
                        //        pr($rlfDetailsData);

                        $arraySize = count($rlfDetailsData);
                        if (!empty($rlfDetailsData)) {
                            if ($arraySize > 1) {
                                $emsg = 'Duplicate Member data found for NID = ' . trim($value['nid']);
                            } else {
                                foreach ($rlfDetailsData as $key => $members) {
                                    //                $decodeDataArr = json_decode($members);
                                    $reg_id = $members['rlf_id'];
                                    $det_id = $members['id'];
                                    $dcodeNid = json_decode($members['nid'], true);
                                    $dcodesnnp = json_decode($members['ssnp'], true);

                                    //                pr($dcodeNid);
//                pr($dcodesnnp);

                                    $srKey = array_search(trim($value['nid']), $dcodeNid);
                                    //                pr($dcodesnnp[$srKey]);

                                    if (!empty($dcodesnnp[$srKey])) {
                                        if ($dcodesnnp[$srKey] >= 1) {
                                            $emsg = 'Already Using SSNP ID = ' . $dcodesnnp[$srKey] . ' for NID = ' . trim($value['nid']);
                                        } else {
                                            $dcodesnnp[$srKey] = $ssnp;
                                            $newSsnp = json_encode($dcodesnnp);

                                            //                        try{
                                            $reliefDetailsModel->where('id', '=', $det_id)
                                                ->update(['ssnp' => $newSsnp]);

                                            $reliefData = $reliefModel->where('id', '=', $reg_id)
                                                ->select('id', 'f_ssnp', 'f_nid')
                                                ->get()
                                                ->toArray();

                                            if (!empty($reliefData)) {
                                                $oldfssnp = $reliefData[0]['f_ssnp'];
                                                $newfssnp = $oldfssnp . ',' . $ssnp;
                                                $reliefModel->where('id', '=', $reg_id)
                                                    ->update(['f_ssnp' => $newfssnp]);
                                            }
                                            //                        }catch (\Exception $exception){
//                        }
                                            $smsg = 'Data Updated for NID = ' . trim($value['nid']) . ', SSNP = ' . $ssnp;
                                        }
                                    } else {

                                        $dcodesnnp[$srKey] = $ssnp;
                                        $newSsnp = json_encode($dcodesnnp);

                                        //                        try{
                                        $reliefDetailsModel->where('id', '=', $det_id)
                                            ->update(['ssnp' => $newSsnp]);

                                        $reliefData = $reliefModel->where('id', '=', $reg_id)
                                            ->select('id', 'f_ssnp', 'f_nid')
                                            ->get()
                                            ->toArray();
                                        //                                pr($reliefData);
//                    dd();


                                        if (!empty($reliefData)) {
                                            $oldfssnp = $reliefData[0]['f_ssnp'];
                                            $newfssnp = $oldfssnp . ',' . $ssnp;
                                            $reliefModel->where('id', '=', $reg_id)
                                                ->update(['f_ssnp' => $newfssnp]);
                                        }
                                        //                        }catch (\Exception $exception){
//                        }
                                        $smsg = 'Data Updated for NID = ' . trim($value['nid']) . ', SSNP = ' . $ssnp;
                                    }
                                }
                            }
                        } else {
                            $emsg = 'No Family Member data found for NID = ' . trim($value['nid']) . ', NID = ' . trim($value['nid']) . ', Phome = ' . $value['mobile'] . ',Card No. = ' . $value['card_no'];
                        }
                    }


                    if (!empty($emsg)) {
                        $error_msg[] = $emsg;
                    }
                    if (!empty($smsg)) {
                        $success_msg[] = $smsg;
                    }
                }
                //                dd();
            } else {
                foreach ($data->toArray() as $key => $value) {
                    //                    $resData = $reliefModel->where('nid', 'like', "%" . trim($value['nid']) . "%")
                    $resData = $reliefModel->where('card_no', 'like', "%" . trim($value['card_no']) . "%")
                        ->where('is_deleted', '!=', config('constants.is_deleted.Deleted'))
                        ->select('id', 'ssnp')
                        ->first();
                    //                    ->toSql();

                    //                    ->where('phone','like', "%" .$value['mobile'] . "%")
//                    ->where('card_no','like', "%" .$value['card_no'] . "%")

                    //                pr($resData->toArray());
//                pr($resData);

                    $emsg = '';
                    $smsg = '';
                    $reg_id = '';
                    $oldSsnp = '';
                    if (!empty($resData)) {
                        $resData = $resData->toArray();
                        $reg_id = $resData['id'];
                        $oldSsnp = $resData['ssnp'];
                    }

                    if (!empty($resData)) {
                        if (!empty($packag)) {
                            //                            echo 'pac \n';
                            $reliefMappingData = $reliefMappingModel->where('reg_id', $reg_id)
                                ->select('id', 'reg_id', 'packeges', 'last_give_pac', 'delv_date')
                                ->first();
                            $packeges = $delv_date = $last_give_pac = '';
                            if (!empty($reliefMappingData)) {
                                /*  1. Get the data and delete the information */
                                /*  2. Remove the corresponding Date and update the 'last_give_pac' field with the latest date... */
                                $reliefMappingData = $reliefMappingData->toArray();
                                $curDate = date("Y-m-d", time());
                                if (empty($dob)) {
                                    $dob = $curDate;
                                }

                                $packageData = explode(',', $reliefMappingData['packeges']);
                                $packageDelvData = explode(',', $reliefMappingData['delv_date']);
                                /*  Previous data has, have to update   */
                                if (in_array($packag, $packageData)) {
                                    $indexValue = array_search($packag, $packageData);
                                    unset($packageData[$indexValue]);
                                    unset($packageDelvData[$indexValue]);
                                    $packeges = implode(" ", $packageData);
                                    $delv_date = implode(" ", $packageDelvData);
                                    //                                    $packeges = $reliefMappingData['packeges'] . $packag . ',';
//                                    $delv_date = $reliefMappingData['delv_date'] . $dob . ',';

                                    //                                    $last_given_max_date = date('Y-m-d', strtotime($last_given_date));

                                    $last_given_date = $reliefMappingData['last_give_pac'];
                                    //                                    $last_given_max_date = date('Y-m-d', strtotime($last_given_date));

                                    $reliefId = $reg_id;

                                    foreach ($packageDelvData as $val) {
                                        if ($val > $last_given_date) {
                                            $last_given_date = date("Y-m-d h:i:s", strtotime($val));
                                        }
                                    }

                                    $updateData = [
                                        'packeges' => $packeges,
                                        'last_give_pac' => $last_given_date,
                                        'delv_date' => $delv_date,
                                        'updated_at' => date("Y-m-d h:i:s", time()),
                                    ];

                                    $responseData = $reliefMappingModel->where('reg_id', $reliefId)
                                        ->update($updateData);

                                    if ($responseData > 1) {
                                        $emsg = 'Cannot Removed ID = ' . $reliefId . ', Card No = ' . trim($value['card_no']) . ', Phone No = ' . $value['mobile'] . ', NID = ' . $value['nid'];
                                    } else {
                                        $smsg = 'Package Removed for ID = ' . $reliefId . ', Packages = ' . $packeges . ', Card No = ' . trim($value['card_no']);
                                    }
                                } else {
                                    $emsg = 'Package no.' . $packag . ' is not assign to  ' . $value['card_no'] . ' Card Number';
                                }
                            } else {
                                $emsg = 'No Package has found in ' . $value['card_no'] . ' Card Number';
                            }
                        }
                    } else {
                        $emsg = 'No data found in ID = ' . $value['id'] . ', NID = ' . trim($value['nid']) . ', Phome = ' . $value['mobile'] . ',Card No. = ' . $value['card_no'];
                    }
                    if (!empty($emsg)) {
                        $error_msg[] = $emsg;
                    }
                    if (!empty($smsg)) {
                        $success_msg[] = $smsg;
                    }
                }
            }
        }
        if (!empty($error_msg)) {
            pr('Error Data List.');
            pr($error_msg);
        }
        if (!empty($success_msg)) {
            pr('Success Data List.');
            pr($success_msg);
        }
        //        if(!empty($error_msg)){
//            return back()->with('errors', 'Excel Data Imported Failed.')->with('msg',$error_msg);
//        }else{
//            return back()->with('success', 'Excel Data Imported successfully.')->with('msg',$success_msg);
//        }
    }

    public function adminImportTribalAssign()
    {
        $tribalList = config('constants.trible.arr');
//        dd($tribalList);

        return view('report.adminImportTribalAssign', compact('tribalList'));

    }

    public function adminSaveImportTribalAssign(Request $request)
    {
        set_time_limit(0);

//        pr($tribalList[$tribal]);
        $reliefModel = new Relief();
        $reliefDetailsModel = new ReliefDetails();
        $reliefMappingModel = new ReliefMapping();
        $packageModel = new Package();
        $tribalList = config('constants.trible.arr');

        $packageList = $packageModel->where('status', '=', config('constants.package_status.Running'))->pluck('title', 'id')->all();

        $tribal = $request->post('tribal');

        $this->validate($request, [
            'select_file' => 'required|mimes:xls,xlsx',
            'tribal' => 'required'
        ]);

        $path = $request->file('select_file')->getRealPath();
//        $path = public_path($path);

        $data = Excel::load($path)->get();
//        dd($data);
        $insert_data = [];
        $error_msg = [];
        $success_msg = [];

        if ($data->count() > 0) {
            foreach ($data->toArray() as $key => $value) {
                //                    $resData = $reliefModel->where('nid', 'like', "%" . trim($value['nid']) . "%")


                $cardLen = strlen(trim($value['family_card_no']));
                if ($cardLen != 13) {
                    $emsg = 'Invalid Family Card No, it should be 13 digit = ' . trim($value['family_card_no']);
                } else {
                    $resData = $reliefModel->where('card_no', 'like', "%" . trim($value['family_card_no']) . "%")
                        ->where('is_deleted', '!=', config('constants.is_deleted.Deleted'))
                        ->select('id', 'ssnp', 'trible','card_no')
                        ->first();

                    $emsg = '';
                    $smsg = '';
                    $reg_id = '';
                    $oldSsnp = '';
                    $tribalId = '';
                    $mainCardNo = '';
//                    dd($resData);

                    if (!empty($resData)) {
                        $resData = $resData->toArray();
                        $reg_id = $resData['id'];
                        $mainCardNo = $resData['card_no'];
                        $oldSsnp = $resData['ssnp'];
                        $tribalId = $resData['trible'];

//                        if (($tribalId >= 1) && ($tribalId === $tribal)) {
                        if ($tribalId >= 1) {
                            $emsg = $value['family_card_no'] . ' family has already assigned with ' . $tribalList[$tribalId];
                        } else {
                            $updateData = [
                                'trible' => $tribal,
                            ];

                            $responseData = $reliefModel->where('id', $reg_id)
                                ->where('card_no', $mainCardNo)
                                ->update($updateData);

                            $smsg = 'Tribal '. $tribalList[$tribal]. ' successfully assigned to '.$value['family_card_no'].' family';
                        }
                    } else {
                        $emsg = 'Cannot find any data for Family Card No = ' . trim($value['family_card_no']) . ', Phone No = ' . $value['mobile_no'] . '.';
                    }
                }

                if (!empty($emsg)) {
                    $error_msg[] = $emsg;
                }
                if (!empty($smsg)) {
                    $success_msg[] = $smsg;
                }
            }
        }
        $allMessage = [
            'errors_arr' => $error_msg,
            'success_arr' => $success_msg,
        ];
        return back()->with('all_msg', $allMessage);
    }

    public function adminDownloadSample()
    {
        // Check if file exists in app/storage/file folder
//        $filename = 'sample_data.xlsx';
//        dd(storage_path());
//        dd();
//        $file_path = storage_path() .'/'. $filename;
//        if (file_exists($file_path))
//        {
//            // Send Download
//            return Response::download($file_path, $filename, [
//                'Content-Length: '. filesize($file_path)
//            ]);
//        }
//        else
//        {
//            // Error
//            exit('Requested file does not exist on our server!');
//        }
    }

    public function adminMyCustReportsss(Request $request)
    {
        $reliefModel = new Relief();

        $queryString = $reliefModel::query();
        //        , 'relief_family_searches.tot_male', 'relief_family_searches.tot_female', 'relief_family_searches.tot_transgen', 'relief_family_searches.tot_child'
        $queryString->select("districts.bn_name AS dis_name", "upazilas.bn_name AS upa_name", "upazilas.up_code AS upa_code", "unions.bn_name AS uni_name", 'reliefs.id', 'reliefs.dob', 'reliefs.card_no', 'reliefs.card_type', 'reliefs.name', 'reliefs.g_name', 'reliefs.poss_help', 'reliefs.total_member', 'reliefs.hw_name', 'reliefs.age', 'reliefs.gender', 'reliefs.house_pre', 'reliefs.road_pre', 'reliefs.word_pre', 'reliefs.upo_pre', 'reliefs.uni_pre', 'reliefs.holding_pre', 'reliefs.dist_pre', 'reliefs.profession', 'reliefs.phone', 'reliefs.nid', 'reliefs.ssnp', 'reliefs.tot_child', 'reliefs.tot_male', 'reliefs.tot_female', 'reliefs.tot_transgen', 'reliefs.tot_handicap', 'reliefs.f_nid', 'reliefs.firm_land', 'reliefs.f_ssnp', 'ssnps.ssnp_name', 'relief_mappings.reg_id', 'relief_mappings.packeges', 'relief_mappings.delv_date', 'relief_mappings.last_give_pac', 'reliefs.tot_child')
            ->where('reliefs.upo_pre', '=', 388)
            ->where('reliefs.uni_pre', '=', 3542)
            ->where('reliefs.is_deleted', '!=', config('constants.is_deleted.Deleted'))
            ->leftJoin('users', 'reliefs.created_by', 'users.id')
            ->leftJoin('upazilas', 'upazilas.id', '=', 'reliefs.upo_pre')
            ->leftJoin('unions', 'unions.id', '=', 'reliefs.uni_pre')
            ->leftJoin('districts', 'districts.id', '=', 'reliefs.dist_pre')
            ->leftJoin('relief_mappings', 'relief_mappings.reg_id', '=', 'reliefs.id')
            ->leftJoin('ssnps', 'ssnps.id', '=', 'reliefs.ssnp');

        //        SELECT * FROM `reliefs` WHERE `upo_pre` = 388 and `is_deleted` != 1 AND `uni_pre` = 3542

        //        $queryString->select("ssnps.ssnp_name AS ssnp_name", "districts.bn_name AS dis_name", "upazilas.bn_name AS upa_name", "upazilas.up_code AS upa_code", "unions.bn_name AS uni_name", "users.first_name", 'reliefs.id', 'reliefs.card_no', 'reliefs.card_type', 'reliefs.name', 'reliefs.g_name', 'reliefs.total_member', 'reliefs.hw_name', 'reliefs.age', 'reliefs.gender', 'reliefs.house_pre', 'reliefs.road_pre', 'reliefs.word_pre', 'reliefs.upo_pre', 'reliefs.uni_pre', 'reliefs.holding_pre', 'reliefs.dist_pre', 'reliefs.profession', 'reliefs.phone', 'reliefs.nid', 'reliefs.ssnp', 'reliefs.total_income', 'reliefs.total_spend', 'reliefs.dist_pre', 'relief_mappings.reg_id', 'relief_mappings.packeges', 'relief_mappings.delv_date', 'relief_mappings.last_give_pac', 'reliefs.hw_name')
//            ->where('reliefs.is_deleted', '!=', config('constants.is_deleted.Deleted'))
//            ->leftJoin('users', 'reliefs.created_by', 'users.id')
//            ->leftJoin('upazilas', 'upazilas.id', '=', 'reliefs.upo_pre')
//            ->leftJoin('unions', 'unions.id', '=', 'reliefs.uni_pre')
//            ->leftJoin('districts', 'districts.id', '=', 'reliefs.dist_pre')
//            ->leftJoin('relief_mappings', 'relief_mappings.reg_id', '=', 'reliefs.id')
//            ->leftJoin('ssnps', 'ssnps.id', '=', 'reliefs.ssnp')
//            ->leftJoin('relief_family_searches', 'relief_family_searches.relf_id', '=', 'reliefs.id');

        //        dd($_POST);

        //        if (isset($_POST['dist']) && !empty($_POST['dist'])) {
//            $dist = $_POST['dist'];
//            $queryString->where('reliefs.dist_pre', '=', $_POST['dist']);
//        }
//        if (isset($_POST['upo']) && !empty($_POST['upo'])) {
//            $upo = $_POST['upo'];
//            $queryString->where('reliefs.upo_pre', '=', $_POST['upo']);
//        }
//        if (isset($_POST['uni_per'])) {
//            $uni_per = $_POST['uni_per'];
//            if (!empty($_POST['uni_per']))
//                $queryString->where('reliefs.uni_pre', '=', $_POST['uni_per']);
//            if ($_POST['uni_per'] === '0') {
//                $queryString->where('reliefs.uni_pre', '=', $_POST['uni_per']);
//            }
//        }
//        if (isset($_POST['word_no']) && !empty($_POST['word_no'])) {
//            $word_no = $_POST['word_no'];
//            $queryString->where('reliefs.word_pre', '=', $_POST['word_no']);
//        }
//        if (isset($_POST['name']) && !empty($_POST['name'])) {
//            $name = $_POST['name'];
//            $queryString->where('reliefs.name', 'like', "%" . $_POST['name'] . "%");
//        }
//        if (isset($_POST['phone']) && !empty($_POST['phone'])) {
//            $phone = $_POST['phone'];
//            $queryString->where('reliefs.phone', 'like', "%" . $_POST['phone'] . "%");
//        }
//        if (isset($_POST['nid']) && !empty($_POST['nid'])) {
//            $nid = $_POST['nid'];
//            $queryString->where('reliefs.nid', 'like', "%" . $_POST['nid'] . "%");
//        }
//        if (isset($_POST['card_type']) && !empty($_POST['card_type'])) {
//            $card_type = $_POST['card_type'];
//            $queryString->where('reliefs.card_type', '=', $_POST['card_type']);
//        }
//        if (isset($_POST['card_no']) && !empty($_POST['card_no'])) {
//            $card_no = $_POST['card_no'];
//            $queryString->where('reliefs.card_no', '=', $_POST['card_no']);
//        }
//        if (isset($_POST['poss_help']) && !empty($_POST['poss_help'])) {
//            $poss_help = $_POST['poss_help'];
//            if ($poss_help == -1) {
//                $queryString->where('reliefs.poss_help', '!=', 1);
//            } else {
//                $queryString->where('reliefs.poss_help', '=', $_POST['poss_help']);
//            }
//        }
//        if (isset($_POST['ssnp']) && !empty($_POST['ssnp'])) {
//            $ssnp = $_POST['ssnp'];
//            if ($ssnp == -1) {
//                $queryString->where('reliefs.ssnp', '=', 0);
//            } else {
//                $queryString->Where(function ($query) use ($ssnp) {
//                    $query->where('reliefs.ssnp', '=', $ssnp);
//                    $query->orWhereRaw("find_in_set($ssnp,reliefs.f_ssnp)");
//                });
//            }
//
//        }
//        if (isset($_POST['packages']) && !empty($_POST['packages'])) {
//            $packages = $_POST['packages'];
//            if ($packages == -1) {
//                $queryString->whereNull('relief_mappings.packeges');
//            } else {
//                $queryString->whereRaw("find_in_set({$_POST['packages']},relief_mappings.packeges)");
//            }
//
//        }
//        if (isset($_POST['search_by_id']) && !empty($_POST['search_by_id'])) {
//            $search_by_id = $_POST['search_by_id'];
//            $datas = explode(",", $search_by_id);
//            $queryString->whereIn('reliefs.id', $datas);
//        }
//        if (isset($_POST['firm_land']) && !empty($_POST['firm_land'])) {
//            $firm_land = $_POST['firm_land'];
//            $queryString->where('reliefs.firm_land', '=', $_POST['firm_land']);
//        }
//        if (isset($_POST['baby_food']) && !empty($_POST['baby_food'])) {
//            $baby_food = $_POST['baby_food'];
//            if ($baby_food == 1)
//                $queryString->where('reliefs.tot_child', '>=', 1);
//            else
//                $queryString->where('reliefs.tot_child', '=', 0);
//
//        }
//        if (isset($_POST['proffs']) && !empty($_POST['proffs'])) {
//            $baby_food = $_POST['proffs'];
//            if ($baby_food == -99)
//                $queryString->Where(function ($query){
//                    $query->where('reliefs.profession', 'LIKE', '%ক্ষুদ্র%');
//                    $query->orWhere('reliefs.profession', 'LIKE', '%ক্ষৃদ%');
//                    $query->orWhere('reliefs.profession', 'LIKE', '%খুদ্%');
//                    $query->orWhere('reliefs.profession', 'LIKE', '%খুদ%');
//                    $query->orWhere('reliefs.profession', 'LIKE', '%ক্ষদ%');
//                });
////                $queryString->where('reliefs.profession', 'LIKE', '%ক্ষুদ্র%')
////                    ->orWhere('reliefs.profession', 'LIKE', '%ক্ষৃদ%')
////                    ->orWhere('reliefs.profession', 'LIKE', '%খুদ্%')
////                    ->orWhere('reliefs.profession', 'LIKE', '%খুদ%')
////                    ->orWhere('reliefs.profession', 'LIKE', '%ক্ষদ%');
//            else
//                $queryString->where('reliefs.profession', '=', 0);
//
//        }

        //        if (isset($_POST['training_id']) && !empty($_POST['training_id'])) {
//            $training_id = $_POST['training_id'];
//            $queryString->where('registrations.training_id', '=', $_POST['training_id']);
//        }
//        if (isset($_POST['reg_status']) && !empty($_POST['reg_status'])) {
//            $reg_status = $_POST['reg_status'];
//            $queryString->where('registrations.status', '=', $_POST['reg_status']);
//        }
//        if (isset($_POST['trade_name']) && !empty($_POST['trade_name'])) {
//            $trade_name = $_POST['trade_name'];
//            $queryString->where('trainings.trade_name', '=', $_POST['trade_name']);
//        }
        $reliefData = $queryString->get();
        //        dd($reliefData);
        $this->__exportBulkRegList_laureal($reliefData);
    }


    /*  Client Section  */
    public function clientTrainingWiseRegistration()
    {
        if (isset($_GET['displayValue'])) {
            $displayValue = $_GET['displayValue'] != '' ? $_GET['displayValue'] : "";
        } else {
            $displayValue = '';
        }

        if (isset($_GET['searchData'])) {
            $searchData = $_GET['searchData'] != '' ? $_GET['searchData'] : "";
        } else {
            $searchData = '';
        }

        $trainingModel = new Training();
        $departmentModel = new Department();
        $registrationModel = new Registration();
        $tradeModel = new Trade();

        $trainingList = $trainingModel->where('status', '=', config('constants.training_status.Pending'))
            ->where('is_deleted', '=', config('constants.is_deleted.Active'))
            ->where('trainings.dept_id', '=', Auth::user()->dept_id)
            ->pluck('name', 'id')->all();

        //        $departmentList = $departmentModel->where('status', '=', config('constants.status.Active'))
//        ->pluck('name', 'id')->all();
        $tradeList = $tradeModel->where('status', '=', config('constants.status.Active'))
            ->pluck('trade_name', 'id')->all();

        $queryString = $registrationModel::query();
        $queryString->select("users.first_name", 'trainings.trade_name', 'registrations.*')
            ->where('registrations.dept_id', '=', Auth::user()->dept_id)
            //            ->where('registrations.status','!=',config('constants.registration_status.Waiting'))
            ->leftJoin('users', 'registrations.created_by', 'users.id')
            ->leftJoin('trainings', 'registrations.training_id', 'trainings.id');
        //            ->tosql();
//            ->paginate($displayValue);

        $training_id = "";
        $trade_name = "";
        $reg_status = "";
        $registrationStatusArr = [
            1 => 'Waiting',
            2 => 'Approved',
            3 => 'Confirmed',
        ];
        if (request()->ajax()) {
            if (isset($_GET['training_id']) && !empty($_GET['training_id'])) {
                $training_id = $_GET['training_id'];
                $queryString->where('registrations.training_id', '=', $_GET['training_id']);
            }
            if (isset($_GET['reg_status']) && !empty($_GET['reg_status'])) {
                $training_status = $_GET['reg_status'];
                $queryString->where('registrations.status', '=', $_GET['reg_status']);
            }
            if (isset($_GET['trade_name']) && !empty($_GET['trade_name'])) {
                $reg_status = $_GET['trade_name'];
                $queryString->where('trainings.trade_name', '=', $_GET['trade_name']);
            }
            $registrationList = $queryString->get();
            return view('report.ajax_training_wise_registration', compact('registrationList', 'trainingList', 'training_id', 'trade_name', 'reg_status', 'registrationStatusArr', 'tradeList'));
        } else {
            $registrationList = $queryString->get();
            return view('report.clientTrainingWiseRegistration', compact('registrationList', 'trainingList', 'training_id', 'trade_name', 'reg_status', 'registrationStatusArr', 'tradeList'));
        }
    }

    public function clientExportTrainingTrainingWiseRegistration(Request $request)
    {
        $trainingModel = new Training();
        $departmentModel = new Department();
        $registrationModel = new Registration();
        $tradeModel = new Trade();

        $trainingList = $trainingModel->where('is_deleted', '=', config('constants.is_deleted.Active'))
            ->where('trainings.dept_id', '=', Auth::user()->dept_id)
            ->pluck('name', 'id')->all();

        $departmentList = $departmentModel->where('status', '=', config('constants.status.Active'))->where('id', '=', Auth::user()->dept_id)
            ->pluck('name', 'id')->all();

        $tradeList = $tradeModel->where('status', '=', config('constants.status.Active'))
            ->pluck('trade_name', 'id')->all();

        $queryString = $registrationModel::query();
        $queryString->select("users.first_name", 'trainings.trade_name', 'registrations.*')
            ->where('registrations.dept_id', '=', Auth::user()->dept_id)
            //            ->where('registrations.status','!=',config('constants.registration_status.Waiting'))
            ->leftJoin('users', 'registrations.created_by', 'users.id')
            ->leftJoin('trainings', 'registrations.training_id', 'trainings.id');
        //            ->tosql();
//            ->paginate($displayValue);

        $training_id = "";
        $training_status = "";
        $reg_status = "";

        if (isset($_POST['training_id']) && !empty($_POST['training_id'])) {
            $training_id = $_POST['training_id'];
            $queryString->where('registrations.training_id', '=', $_POST['training_id']);
        }
        if (isset($_POST['reg_status']) && !empty($_POST['reg_status'])) {
            $training_status = $_POST['reg_status'];
            $queryString->where('registrations.status', '=', $_POST['reg_status']);
        }
        if (isset($_POST['trade_name']) && !empty($_POST['trade_name'])) {
            $reg_status = $_POST['trade_name'];
            $queryString->where('trainings.trade_name', '=', $_POST['trade_name']);
        }
        $registrationList = $queryString->toSql();
        $this->__exportRegList($registrationList, $trainingList, $departmentList);
    }

    public function clientTrainingReport()
    {
        if (isset($_GET['displayValue'])) {
            $displayValue = $_GET['displayValue'] != '' ? $_GET['displayValue'] : "";
        } else {
            $displayValue = '';
        }

        if (isset($_GET['searchData'])) {
            $searchData = $_GET['searchData'] != '' ? $_GET['searchData'] : "";
        } else {
            $searchData = '';
        }

        $trainingModel = new Training();
        $departmentModel = new Department();
        $tradeModel = new Trade();

        $departmentList = $departmentModel->where('status', '=', config('constants.status.Active'))
            ->where('departments.id', '=', Auth::user()->dept_id)
            ->pluck('name', 'id')->all();

        $tradeList = $tradeModel->where('status', '=', config('constants.status.Active'))
            ->pluck('trade_name', 'id')->all();

        $queryString = $trainingModel::query();
        $queryString->select("users.first_name")->where('is_deleted', '!=', config('constants.is_deleted.Deleted'))
            //            ->where('trainings.name', 'like', '%' . $searchData . '%')
            ->where('trainings.dept_id', '=', Auth::user()->dept_id)
            ->leftJoin('users', 'trainings.created_by', 'users.id')
            ->select('trainings.*', 'users.first_name');
        //            ->tosql();
//            ->get();
//        dd($tradeList);
        if (request()->ajax()) {
            //            dd($_GET);
            if (isset($_GET['dept_id']) && !empty($_GET['dept_id'])) {
                $queryString->where('trainings.dept_id', '=', $_GET['dept_id']);
            }
            if (isset($_GET['start_date']) && !empty($_GET['start_date'])) {
                $strDate = date('Y-m-d', strtotime($_GET['start_date']));
                $strDate = $strDate . " 00:00:00";
                $queryString->where('trainings.start_date', '>=', $strDate);

                $endDate = date("Y-m-d", strtotime("now")) . " 00:00:00";
                if (isset($_GET['end_date']) && !empty($_GET['end_date'])) {
                    $endDate = date('Y-m-d', strtotime($_GET['end_date']));
                    $endDate = $endDate . " 00:00:00";
                    $queryString->where('trainings.end_date', '<=', $endDate);
                }
                //                $queryString->whereBetween('created_at',[$strDate,$endDate]);
            }
            if (isset($_GET['status']) && !empty($_GET['status'])) {
                $queryString->where('trainings.status', '=', $_GET['status']);
            }
            if (isset($_GET['trade_name']) && !empty($_GET['trade_name'])) {
                $queryString->where('trainings.trade_name', '=', $_GET['trade_name']);
            }

            $trainingList = $queryString->get();
            return view('report.ajax_training_report', compact('trainingList', 'departmentList', 'tradeList'));
        } else {
            $trainingList = $queryString->get();
            return view('report.clientTrainingReport', compact('trainingList', 'departmentList', 'tradeList'));
        }
    }

    public function clientTrainingReportExport()
    {
        $trainingModel = new Training();
        $departmentModel = new Department();
        $tradeModel = new Trade();

        $departmentList = $departmentModel->where('status', '=', config('constants.status.Active'))
            ->where('departments.id', '=', Auth::user()->dept_id)
            ->pluck('name', 'id')->all();

        $tradeList = $tradeModel->where('status', '=', config('constants.status.Active'))
            ->pluck('trade_name', 'id')->all();
        $queryString = $trainingModel::query();
        $queryString->select("users.first_name")->where('is_deleted', '!=', config('constants.is_deleted.Deleted'))
            //            ->where('trainings.name', 'like', '%' . $searchData . '%')
            ->where('trainings.dept_id', '=', Auth::user()->dept_id)
            ->leftJoin('users', 'trainings.created_by', 'users.id')
            ->select('trainings.*', 'users.first_name');
        //            ->tosql();
//            ->get();
//        if (request()->ajax()) {
//            dd($_POST);
        if (isset($_POST['dept_id']) && !empty($_POST['dept_id'])) {
            $queryString->where('trainings.dept_id', '=', $_POST['dept_id']);
        }
        if (isset($_POST['start_date']) && !empty($_POST['start_date'])) {
            $strDate = date('Y-m-d', strtotime($_POST['start_date']));
            $strDate = $strDate . " 00:00:00";
            $queryString->where('trainings.start_date', '>=', $strDate);

            $endDate = date("Y-m-d", strtotime("now")) . " 00:00:00";
            if (isset($_POST['end_date']) && !empty($_POST['end_date'])) {
                $endDate = date('Y-m-d', strtotime($_POST['end_date']));
                $endDate = $endDate . " 00:00:00";
                $queryString->where('trainings.end_date', '<=', $endDate);
            }
        }
        if (isset($_POST['status']) && !empty($_POST['status'])) {
            $queryString->where('trainings.status', '=', $_POST['status']);
        }
        if (isset($_POST['trade_name']) && !empty($_POST['trade_name'])) {
            $queryString->where('trainings.trade_name', '=', $_POST['trade_name']);
        }
        //        }

        $trainingList = $queryString->get();
        $this->__exportTrainingList($trainingList, $departmentList);

    }

    public function clientParticipantReport()
    {
        $trainingModel = new Training();
        $departmentModel = new Department();
        $registrationModel = new Registration();
        $tradeModel = new Trade();

        $trainingList = $trainingModel->where('status', '=', config('constants.training_status.Pending'))
            ->where('is_deleted', '=', config('constants.is_deleted.Active'))
            ->where('trainings.dept_id', '=', Auth::user()->dept_id)
            ->pluck('name', 'id')->all();

        //        $departmentList = $departmentModel->where('status', '=', config('constants.status.Active'))
//        ->pluck('name', 'id')->all();

        $tradeList = $tradeModel->where('status', '=', config('constants.status.Active'))
            ->pluck('trade_name', 'id')->all();

        $queryString = $registrationModel::query();
        $queryString->select("users.first_name", 'trainings.trade_name', 'registrations.*')
            ->where('registrations.dept_id', '=', Auth::user()->dept_id)
            ->where('registrations.status', '!=', config('constants.registration_status.Waiting'))
            //            ->where('registrations.status','!=',config('constants.registration_status.Waiting'))
            ->leftJoin('users', 'registrations.created_by', 'users.id')
            ->leftJoin('trainings', 'registrations.training_id', 'trainings.id');
        //            ->tosql();
//            ->paginate($displayValue);

        $training_id = "";
        $trade_name = "";
        $reg_status = "";

        $registrationStatusArr = [
            2 => 'Approved',
            3 => 'Confirmed',
        ];
        $titleName = "Participant Report";
        $registrationList = $queryString->get();
        if (request()->ajax()) {
            if (isset($_GET['training_id']) && !empty($_GET['training_id'])) {
                $training_id = $_GET['training_id'];
                $queryString->where('registrations.training_id', '=', $_GET['training_id']);
            }
            if (isset($_GET['reg_status']) && !empty($_GET['reg_status'])) {
                $training_status = $_GET['reg_status'];
                $queryString->where('registrations.status', '=', $_GET['reg_status']);
            }
            if (isset($_GET['trade_name']) && !empty($_GET['trade_name'])) {
                $trade_name = $_GET['trade_name'];
                $queryString->where('trainings.trade_name', '=', $_GET['trade_name']);
            }

            $registrationList = $queryString->get();
            return view('report.ajax_participant_report', compact('registrationList', 'trainingList', 'training_id', 'trade_name', 'reg_status', 'registrationStatusArr', 'titleName', 'tradeList'));
        } else {
            $registrationList = $queryString->get();
            return view('report.clientParticipantReport', compact('registrationList', 'trainingList', 'training_id', 'trade_name', 'reg_status', 'registrationStatusArr', 'titleName', 'tradeList'));
        }
    }

    public function clientParticipantReportExport(Request $request)
    {
        $trainingModel = new Training();
        $departmentModel = new Department();
        $registrationModel = new Registration();
        $tradeModel = new Trade();

        $trainingList = $trainingModel->where('is_deleted', '=', config('constants.is_deleted.Active'))
            ->where('trainings.dept_id', '=', Auth::user()->dept_id)
            ->pluck('name', 'id')->all();

        $departmentList = $departmentModel->where('status', '=', config('constants.status.Active'))->where('id', '=', Auth::user()->dept_id)
            ->pluck('name', 'id')->all();

        $tradeList = $tradeModel->where('status', '=', config('constants.status.Active'))
            ->pluck('trade_name', 'id')->all();

        $queryString = $registrationModel::query();
        $queryString->select("users.first_name", 'trainings.trade_name', 'registrations.*')
            ->where('registrations.dept_id', '=', Auth::user()->dept_id)
            ->where('registrations.status', '!=', config('constants.registration_status.Waiting'))
            //            ->where('registrations.status','!=',config('constants.registration_status.Waiting'))
            ->leftJoin('users', 'registrations.created_by', 'users.id')
            ->leftJoin('trainings', 'registrations.training_id', 'trainings.id');
        //            ->tosql();
//            ->paginate($displayValue);

        $training_id = "";
        $training_status = "";
        $reg_status = "";

        if (isset($_POST['training_id']) && !empty($_POST['training_id'])) {
            $training_id = $_POST['training_id'];
            $queryString->where('registrations.training_id', '=', $_POST['training_id']);
        }
        if (isset($_POST['reg_status']) && !empty($_POST['reg_status'])) {
            $training_status = $_POST['reg_status'];
            $queryString->where('registrations.status', '=', $_POST['reg_status']);
        }
        if (isset($_POST['trade_name']) && !empty($_POST['trade_name'])) {
            $reg_status = $_POST['trade_name'];
            $queryString->where('trainings.trade_name', '=', $_POST['trade_name']);
        }
        //        echo $queryString->toSql();
//        die;
        $registrationList = $queryString->get();
        $this->__exportRegList($registrationList, $trainingList, $departmentList);
    }


    /*  Common Function */
    private function __exportTrainingList($trainingList, $departmentList)
    {
        $training_array[] = array('Sr. No.', 'প্রশিক্ষণের নাম', 'প্রশিক্ষণের স্থান', 'প্রশিক্ষণের সময়কাল (দিন)', 'প্রশিক্ষণ প্রদানকারী সংস্থা', 'প্রতিষ্ঠানের কোড নং', 'প্রশিক্ষণ অনুমোদনকারী সংস্থা', 'প্রশিক্ষণ অর্থায়নকারী সংস্থা', 'অর্থায়ন মূল্য', 'ট্রেনিং এর শুরুর দিন', 'পরীক্ষার দিন', 'Status');

        foreach ($trainingList as $srNo => $training) {
            //            dd($registration->name_bn);
            $training_array[] = array(
                'Sr. No.' => $srNo + 1,
                'প্রশিক্ষণের নাম' => $training->name,
                'প্রশিক্ষণের স্থান' => $training->training_place,
                'প্রশিক্ষণের সময়কাল (দিন)' => $training->total_day,
                'প্রশিক্ষণ প্রদানকারী সংস্থা' => $departmentList[$training->dept_id],
                'প্রতিষ্ঠানের কোড নং' => $training->code,
                'প্রশিক্ষণ অনুমোদনকারী সংস্থা' => $training->proposal_org,
                'প্রশিক্ষণ অর্থায়নকারী সংস্থা' => $training->financial_org,
                'অর্থায়ন মূল্য' => $training->budget,
                'ট্রেনিং এর শুরুর দিন' => date('Y-m-d', strtotime($training->start_date)),
                'পরীক্ষার দিন' => date('Y-m-d', strtotime($training->exam_date)),
                'Status' => ($training->status == config('constants.training_status.Pending')) ? config('constants.training_status.1') : (($training->status == config('constants.training_status.Complete')) ? config('constants.training_status.2') : config('constants.training_status.0')),
            );
        }

        Excel::create("Training List", function ($excel) use ($training_array) {
            $excel->setTitle('Training List');
            $excel->sheet(
                'Training List',
                function ($sheet) use ($training_array) {
                    $sheet->fromArray($training_array, null, 'A1', false, false);
                }
            );
        })->download('xlsx');
    }

    private function __exportRegList($reliefData)
    {
        $packagesModel = new Package();
        $packageList = $packagesModel->pluck('title', 'id')->all();

        $registration_array[] = array('Sr. No.', 'কার্ড নং', 'নাম', 'পিতা/স্বামীর নাম', 'ঠিকানা', 'বয়স', 'পেশা', 'পরিবারের সদস্য সংখ্যা', 'মোবাইল নাম্বার', 'জন্ম নিবন্ধন / ভোটার আইডি নাম্বার', 'ভাতার নাম', 'ত্রাণ সহায়তা', 'সম্ভাব্য সহায়তা', 'শিশু সংখ্যা', 'স্বাক্ষর/টিপসই', 'মন্তব্য');
        //        dd($reliefData);
        foreach ($reliefData as $srNo => $relief) {
            $cardNo = '353200';
            $cardNo .= $relief['word_pre'];
            $length = strlen($relief['id']);
            for ($count = 6; $count > $length; $count--) {
                $cardNo .= '0';
            }
            $cardNo = $relief['card_no'];

            $uni_name = "";
            $parents_name = "";
            if ($relief->uni_pre < 1) {
                $uni_name = 'পৌরসভা';
            } else {
                $uni_name = $relief->uni_name;
            }

            if (!empty($relief->g_name)) {
                $parents_name = $relief->g_name;
            } else {
                $parents_name = $relief->hw_name;
            }


            $packageGivenList = '';

            if (!empty($relief->packeges)) {
                $packageArr = array_filter(explode(',', $relief->packeges));
                $dateArr = array_filter(explode(',', $relief->delv_date));
                //                    pr($packageArr);
//                    pr($dateArr);
                foreach ($packageArr as $key => $value) {
                    $packageGivenList .= $packageList[$key + 1] . '(' . $dateArr[$key] . '),';
                }
            }

            $registration_array[] = array(
                'Sr. No.' => $srNo + 1,
                'কার্ড নং' => $cardNo,
                'নাম' => $relief->name,
                'পিতা/স্বামীর নাম' => $parents_name,
                'ঠিকানা' => 'বাড়ি নংঃ ' . $relief['house_pre'] . ', হোল্ডিং নংঃ ' . $relief['road_pre'] . ', রোড নংঃ ' . $relief['road_pre'] . ', ওয়ার্ড নংঃ ' . $relief['word_pre'] . ', ইউনিয়নঃ ' . $uni_name . ', উপজেলাঃ ' . $relief['upa_name'] . ', জেলাঃ ' . $relief['dis_name'],
                'বয়স' => $relief->age,
                'পেশা' => $relief->profession,
                'পরিবারের সদস্য সংখ্যা' => $relief->total_member,
                'মোবাইল নাম্বার' => $relief->phone,
                'জন্ম নিবন্ধন / ভোটার আইডি নাম্বার' => $relief->nid,
                'ভাতার নাম' => !empty($relief->ssnp_name) ? $relief->ssnp_name : "তথ্য নেই",
                'ত্রাণ সহায়তা' => $packageGivenList,
                'সম্ভাব্য সহায়তা' => config('constants.possible_help.' . $relief->poss_help),
                'শিশু সংখ্যা' => $relief->tot_child,
                'স্বাক্ষর/টিপসই' => "",
                'মন্তব্য' => ''
            );
        }

        //        dd("sfsfaddhiu");

        Excel::create("Registration Report", function ($excel) use ($registration_array) {
            $excel->setTitle('Registration List');
            $excel->sheet(
                'Registration List',
                function ($sheet) use ($registration_array) {
                    $sheet->fromArray($registration_array, null, 'A1', false, false);
                }
            );
        })->download('xlsx');
    }

    private function __exportMasterRollReport($reliefData)
    {
        $registration_array[] = array('Sr. No.', 'কার্ড নং', 'তথ্য', 'বিবরণ', 'স্বাক্ষর/টিপসই', 'মন্তব্য');
        //        dd($reliefData);
        foreach ($reliefData as $srNo => $relief) {
            //            $cardNo = config('constants.location_code.dist.'.$relief['dist_pre']);
//            $cardNo .= config('constants.location_code.upo.'.$relief['upo_pre']);
//            $cardNo .= '00';
//            $cardNo .= $relief['word_pre'];
//            $length = strlen($relief['id']);
//            for($count = 6; $count > $length; $count--){
//                $cardNo .= '0';
//            }
            $cardNo = $relief['card_no'];
            $uni_name = "";
            if ($relief->uni_pre < 1) {
                $uni_name = 'পৌরসভা';
            } else {
                $uni_name = $relief->uni_name;
            }

            if (!empty($relief->g_name)) {
                $parents_name = $relief->g_name;
            } else {
                $parents_name = $relief->hw_name;
            }

            $registration_array[] = array(
                'Sr. No.' => $srNo + 1,
                'কার্ড নং' => $cardNo,
                'পিতা/স্বামীর নাম' => $parents_name,
                'তথ্য' => 'নাম: ' . $relief->name . '। পিতা/স্বামীর নাম: ' . $parents_name . '  । ঠিকানা:' . $relief['house_pre'] . ', ' . $relief['road_pre'] . ', ' . $relief['road_pre'] . ', ' . $relief['word_pre'] . ', ' . $uni_name . ', ' . $relief['upa_name'] . '। মোবাইল নং :' . $relief->phone . '। জন্ম নিবন্ধন / ভোটার আইডি নাম্বার:  ' . $relief->nid,
                'বিবরণ' => "",
                'স্বাক্ষর/টিপসই' => "",
                'মন্তব্য' => ''
            );
        }

        //        dd("sfsfaddhiu");

        Excel::create("Master Roll", function ($excel) use ($registration_array) {
            $excel->setTitle('Master Roll List');
            $excel->sheet(
                'Master Roll List',
                function ($sheet) use ($registration_array) {
                    $sheet->fromArray($registration_array, null, 'A1', false, false);
                }
            );
        })->download('xlsx');
    }

    private function __exportBulkRegList($reliefData)
    {
        try {

            $packagesModel = new Package();
            $packageList = $packagesModel->pluck('title', 'id')->all();

            $registration_array[] = array(
                'ক্রমিক নং.',
                'জেলা কোড',
                'উপজেলা কোড',
                'পরিবার প্রধানের নাম',
                'পিতা/স্বামীর নাম',
                'জন্ম তারিখ',
                'বয়স',
                'লিঙ্গ',
                'পেশা',
                'জাতীয় পরিচয়পত্র/জন্ম নিবন্ধন',
                'জাতীয় পরিচয়পত্র/জন্ম নিবন্ধন নম্বর',
                'মোবাইল নম্বর',
                'সামাজিক নিরাপত্তায়  থাকলে তার নাম',
                'জেলা',
                'উপজেলা/থানা',
                'সিটিকর্পোরেশন/পৌরসভা/ইউনিয়ন',
                'ওয়ার্ড',
                'গ্রাম /পাড়া /মহল্লা',
                'বাসা ও সড়ক /মহল্লা (নাম /নম্বর )',
                'সম্ভাব্য সহায়তা',
                'ত্রাণ সহায়তা',
                'কার্ডের ধরণ',
                'কার্ড নং',
                'পরিবারের কৃষি জমি সংক্রান্ত তথ্যঃ'
            );

            foreach ($reliefData as $srNo => $relief) {
                $cardNo = '353200';
                $cardNo .= $relief['word_pre'];
                $length = strlen($relief['id']);
                for ($count = 6; $count > $length; $count--) {
                    $cardNo .= '0';
                }
                $cardNo = $relief['card_no'];

                $uni_name = "";
                if ($relief->uni_pre < 1) {
                    $uni_name = 'পৌরসভা';
                } else {
                    $uni_name = $relief->uni_name;
                }


                $packageGivenList = '';
                //            dd($relief->packeges);

                if (isset($relief->packeges)) {
                    $packageArr = array_filter(explode(',', $relief->packeges));
                    $dateArr = array_filter(explode(',', $relief->delv_date));
                    foreach ($packageArr as $key => $value) {
                        $packageGivenList = $packageList[$value];
                    }

                    //                $packageArr = array_filter(explode(',', $relief->packeges));
//                $dateArr = array_filter(explode(',', $relief->delv_date));
////                    pr($packageArr);
////                    pr($dateArr);
//                foreach ($packageArr as $key => $value) {
//                    $packageGivenList .= $packageList[$key + 1] . '(' . $dateArr[$key] . '),';
//                }
                }

                $g_name = '';
                $gender = 'অন্যান্য';
                //            dd($reliefData);
//                if (!empty($relief->g_name)) {
//                    $g_name = $relief->g_name;
//                }
//                else {
                $g_name = $relief['hw_name'];
                //                }

                if ($relief->gender == 1) {
                    $gender = "পুরুষ";
                } elseif ($relief->gender == 2) {
                    $gender = "মহিলা";
                }

                $registration_array[] = array(
                    'ক্রমিক নং' => $srNo + 1,
                    'জেলা কোড' => 35,
                    'উপজেলা কোড' => $relief->upa_pre,
                    'পরিবার প্রধানের নাম' => $relief->name,
                    'পিতা/স্বামীর নাম' => $g_name,
                    'জন্ম তারিখ' => date("Y/m/d", strtotime($relief->dob)),
                    'বয়স' => $relief->age,
                    'লিঙ্গ' => $gender,
                    'পেশা' => $relief->profession,
                    'জাতীয় পরিচয়পত্র/জন্ম নিবন্ধন' => '',
                    'জাতীয় পরিচয়পত্র/জন্ম নিবন্ধন নম্বর' => $this->bn2en($relief->nid),
                    'মোবাইল নম্বর' => $this->bn2en($relief->phone),
                    'সামাজিক নিরাপত্তায় থাকলে তার নাম' => $relief->ssnp_name,
                    'জেলা' => $relief['dis_name'],
                    'উপজেলা/থানা' => $relief['upa_name'],
                    'সিটিকর্পোরেশন/পৌরসভা/ইউনিয়ন' => $uni_name,
                    'ওয়ার্ড' => $relief['word_pre'],
                    'গ্রাম /পাড়া /মহল্লা' => $relief['house_pre'],
                    'বাসা ও সড়ক /মহল্লা (নাম /নম্বর )' => $relief['road_pre'],
                    'সম্ভাব্য সহায়তা' => config('constants.possible_help.' . $relief->poss_help),
                    'ত্রাণ সহায়তা' => $packageGivenList,
                    'কার্ডের ধরণ' => config('constants.card_type.arr.' . $relief->card_type),
                    'কার্ড নং' => $relief['card_no'],
                    'পরিবারের কৃষি জমি সংক্রান্ত তথ্যঃ' => config('constants.land_info.' . $relief->firm_land),
                );

                //                pr($registration_array);
            }

            // php code for logging error into a given file

            // error message to be logged
//            $error_message = "This is an error message!";

            // path of the log file where errors need to be logged
//            $log_file = "./my-errors.log";

            // logging error message to given log file
//            error_log(pr($registration_array), 3, $log_file);
//        dd();
//        dd("sfsfaddhiu");

            Excel::create("Export List", function ($excel) use ($registration_array) {
                $excel->setTitle('Export List');
                $excel->sheet(
                    'Export List',
                    function ($sheet) use ($registration_array) {
                        $sheet->fromArray($registration_array, null, 'A1', false, false);
                    }
                );
            })->download('xlsx');
        } catch (\Exception $exception) {
            dd($exception->getMessage());
        }
    }

    private function __exportBulkRegList_laureal($reliefData)
    {
        $packagesModel = new Package();
        $packageList = $packagesModel->pluck('title', 'id')->all();

        $registration_array[] = array('পরিবার প্রধানের নাম', 'পিতা/স্বামীর নাম', 'জন্ম তারিখ', 'বয়স', 'লিঙ্গ', 'পেশা', 'জাতীয় পরিচয়পত্র/জন্ম নিবন্ধন', 'জাতীয় পরিচয়পত্র/জন্ম নিবন্ধন নম্বর', 'মোবাইল নম্বর', 'সামাজিক নিরাপত্তায়  থাকলে তার নাম', 'উপজেলা/থানা', 'সিটিকর্পোরেশন/পৌরসভা/ইউনিয়ন', 'ওয়ার্ড', 'গ্রাম /পাড়া /মহল্লা', 'বাসা ও সড়ক /মহল্লা (নাম /নম্বর )', 'সম্ভাব্য সহায়তা', 'ত্রাণ সহায়তা', 'কার্ডের ধরণ', 'কার্ড নং', 'পরিবারের কৃষি জমি সংক্রান্ত তথ্যঃ');
        foreach ($reliefData as $srNo => $relief) {
            $cardNo = '353200';
            $cardNo .= $relief['word_pre'];
            $length = strlen($relief['id']);
            for ($count = 6; $count > $length; $count--) {
                $cardNo .= '0';
            }
            $cardNo = $relief['card_no'];

            $uni_name = "";
            if ($relief->uni_pre < 1) {
                $uni_name = 'পৌরসভা';
            } else {
                $uni_name = $relief->uni_name;
            }


            $packageGivenList = '';
            //            dd($relief->packeges);

            if (!empty($relief->packeges)) {
                $packageArr = array_filter(explode(',', $relief->packeges));
                $dateArr = array_filter(explode(',', $relief->delv_date));
                foreach ($packageArr as $key => $value) {
                    $packageGivenList = $packageList[$value];
                }

                //                $packageArr = array_filter(explode(',', $relief->packeges));
//                $dateArr = array_filter(explode(',', $relief->delv_date));
////                    pr($packageArr);
////                    pr($dateArr);
//                foreach ($packageArr as $key => $value) {
//                    $packageGivenList .= $packageList[$key + 1] . '(' . $dateArr[$key] . '),';
//                }
            }

            $g_name = '';
            $gender = 'অন্যান্য';
            //            dd($reliefData);
            if (!empty($relief['g_name'])) {
                $g_name = $relief['g_name'];
            } else {
                $g_name = $relief['hw_name'];
            }

            if ($relief->gender == 1) {
                $gender = "পুরুষ";
            } elseif ($relief->gender == 2) {
                $gender = "মহিলা";
            }

            $registration_array[] = array(
                'পরিবার প্রধানের নাম' => $relief->name,
                'পিতা/স্বামীর নাম' => $g_name,
                'জন্ম তারিখ' => date("Y/m/d", strtotime($relief->dob)),
                'বয়স' => $relief->age,
                'লিঙ্গ' => $gender,
                'পেশা' => $relief->profession,
                'জাতীয় পরিচয়পত্র/জন্ম নিবন্ধন' => '',
                'জাতীয় পরিচয়পত্র/জন্ম নিবন্ধন নম্বর' => $this->bn2en($relief->nid),
                'মোবাইল নম্বর' => $this->bn2en($relief->phone),
                'সামাজিক নিরাপত্তায় থাকলে তার নাম' => $relief->ssnp_name,
                'উপজেলা/থানা' => $relief['upa_name'],
                'সিটিকর্পোরেশন/পৌরসভা/ইউনিয়ন' => $uni_name,
                'ওয়ার্ড' => $relief['word_pre'],
                'গ্রাম /পাড়া /মহল্লা' => $relief['house_pre'],
                'বাসা ও সড়ক /মহল্লা (নাম /নম্বর )' => $relief['road_pre'],
                'সম্ভাব্য সহায়তা' => config('constants.possible_help.' . $relief->poss_help),
                'ত্রাণ সহায়তা' => $packageGivenList,
                'কার্ডের ধরণ' => config('constants.card_type.arr.' . $relief->card_type),
                'কার্ড নং' => $relief['card_no'],
                'পরিবারের কৃষি জমি সংক্রান্ত তথ্যঃ' => config('constants.land_info.' . $relief->firm_land),
            );
        }

        //        dd("sfsfaddhiu");

        Excel::create("Export List", function ($excel) use ($registration_array) {
            $excel->setTitle('Export List');
            $excel->sheet(
                'Export List',
                function ($sheet) use ($registration_array) {
                    $sheet->fromArray($registration_array, null, 'A1', false, false);
                }
            );
        })->download('xlsx');
    }

    public static $bn = array("১", "২", "৩", "৪", "৫", "৬", "৭", "৮", "৯", "০");
    public static $en = array("1", "2", "3", "4", "5", "6", "7", "8", "9", "0");

    public static function bn2en($number)
    {
        return str_replace(self::$bn, self::$en, $number);
    }

}