<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Model\Event;
use App\Model\Category;
use App\Model\Eligibility;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Spatie\Permission\Traits\HasRoles; // For ACL Functionality

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;
    use HasRoles; // For ACL Functionality

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name','last_name', 'email', 'password', 'phone', 'image', 'gender', 'dob', 'address', 'user_type','dept_id', 'created_by',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function getDataById($id){
        $userData = User::find($id);
        return $userData;
    }

    public static function getUserNameById($id){
        $userData = User::find($id);
        return $userData->first_name . ' ' . $userData->last_name;
        //return $userData;
    }

    public function updateData($data, $id)
    {
        $user = $this->find($id);

        foreach($data as $key => $value){
            if ($key != "_token"){
                $user->$key = $value;
            }
        }
        $user->dob = date('Y-m-d',strtotime($data['dob']));
//        $ticket->status= $data->status;
        $user->save();
        return 1;
    }

    public function getJWTIdentifier(){
        return $this->getKey();
    }

    public function getJWTCustomClaims(){
        return [];
    }

    public function getFullNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function events()
    {
        return $this->hasMany('App\Model\Event')->orderBy('created_at', 'DESC');
    }

    public function categories()
    {
        return $this->hasMany('App\Model\Category')->orderBy('created_at', 'DESC');
    }

    public function eligibilities()
    {
        return $this->hasMany('App\Model\Eligibility')->orderBy('created_at', 'DESC');
    }

    public function organizations()
    {
        return $this->hasMany('App\Model\Organization')->orderBy('created_at', 'DESC');
    }

    public function properties()
    {
        return $this->hasMany('App\Model\Property')->orderBy('created_at', 'DESC');
    }

    public function apartments()
    {
        return $this->hasMany('App\Model\Apartment')->orderBy('created_at', 'DESC');
    }

    public function pendingledgers()
    {
        return $this->hasMany('App\Model\PendingLedger')->orderBy('created_at', 'DESC');
    }

    public function ledgers()
    {
        return $this->hasMany('App\Model\Ledger')->orderBy('created_at', 'DESC');
    }

    public function bills()
    {
        return $this->hasMany('App\Model\Bill')->orderBy('created_at', 'DESC');
    }

    public function departments()
    {
        return $this->hasMany('App\Model\Departments')->orderBy('created_at', 'DESC');
    }

    public function trainings()
    {
        return $this->hasMany('App\Model\Training')->orderBy('created_at', 'DESC');
    }

    public function registrations()
    {
        return $this->hasMany('App\Model\Registration')->orderBy('created_at', 'DESC');
    }

    public function reliefs()
    {
        return $this->hasMany('App\Model\Relief')->orderBy('created_at', 'DESC');
    }

    public function trades()
    {
        return $this->hasMany('App\Model\Trade')->orderBy('created_at', 'DESC');
    }

    public function packages()
    {
        return $this->hasMany('App\Model\Package')->orderBy('created_at', 'DESC');
    }

    public function snnps()
    {
        return $this->hasMany('App\Model\Ssnp')->orderBy('created_at', 'DESC');
    }
}

class Status {

    const Active = 1;
    const Deleted = 2;
    const Draft = 3;
    const Issued = 4;
    const Returned = 5;

    private static $status = array(
        Status::Active => 'Active',
        Status::Deleted => 'Deleted',
        Status::Draft => 'Draft',
        Status::Issued => 'Issued',
        Status::Returned => 'Returned',
    );

    public static function getAllStatus() {
        return Status::$status;
    }

    static function getStatus($type = null) {
        if (!empty($type) OR $type === '0') {
            return !empty(Status::$status[$type]) ? Status::$status[$type] : null;
        }
        return 'NULL';
    }

//    static function getUserNameById($id){
//        $userData = User->find($id);
//    }

}
