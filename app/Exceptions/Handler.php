<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenBlacklistedException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param \Exception $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Exception $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
//        die("dasd");
//        if ($exception instanceof UnauthorizedHttpExceptionn) {
//            $preException = $exception->getPrevious();
//            if ($preException instanceof
//                TokenExpiredException) {
//                return response()->json(['data' => null,
//                        'status' => false,
//                        'err_' => [
//                            'message' => 'Token Expired',
//                            'code' =>1
//                        ]
//                    ]
//                );
//            }
//            else if ($preException instanceof
//                TokenInvalidException) {
//                return response()->json([
//                        'data' => null,
//                        'status' => false,
//                        'err_' => [
//                            'message' => 'Token Invalid',
//                            'code' => 1
//                        ]
//                    ]
//                );
//            } else if ($preException instanceof
//                TokenBlacklistedException) {
//                return response()->json([
//                        'data' => null,
//                        'status' => false,
//                        'err_' => [
//                            'message' => 'Token Blacklisted',
//                            'code' => 1
//                        ]
//                    ]
//                );
//            }
//            if ($exception->getMessage() === 'Token not provided') {
//                return response()->json([
//                        'data' => null,
//                        'status' => false,
//                        'err_' => [
//                            'message' => 'Token not provided',
//                            'code' => 1
//                        ]
//                    ]
//                );
//            }else if( $exception->getMessage() === 'User not found'){
//                return response()->json([
//                        'data' => null,
//                        'status' => false,
//                        'err_' => [
//                            'message' => 'User Not Found',
//                            'code' => 1
//                        ]
//                    ]
//                );
//            }
//        }


        if ($request->is('api/*') || $request->expectsJson() || $request->is('webhook/*')) {
//            die($exception->getMessage());
//            dd();
            if ($exception instanceof UnauthorizedHttpException) {
                if ($exception->getMessage() === 'The token has been blacklisted') {
                    return response()->json([
                            'data' => null,
                            'status' => false,
                            'err_' => [
                                'message' => 'Token has been Blacklisted.',
                                'code' => 1
                            ]
                        ]
                    );
                } else if ($exception->getMessage() === 'Token Signature could not be verified.') {
                    return response()->json([
                            'data' => null,
                            'status' => false,
                            'err_' => [
                                'message' => 'Token Signature could not be verified.',
                                'code' => 1
                            ]
                        ]
                    );
                } else if ($exception->getMessage() === 'Token has been Expired') {
                    return response()->json([
                            'data' => null,
                            'status' => false,
                            'err_' => [
                                'message' => 'Token has been Expired',
                                'code' => 1
                            ]
                        ]
                    );
                }
                if ($exception instanceof Tymon\JWTAuth\Exceptions\TokenExpiredException) {
                    return response()->json(['errors' => 'Token has been Expired']);

                }
                if ($exception instanceof Tymon\JWTAuth\Exceptions\TokenInvalidException) {
                    return response()->json(['errors' => 'Invalid Token']);

                }
                if ($exception instanceof Tymon\JWTAuth\Exceptions\TokenBlacklistedException) {
                    return response()->json(['errors' => 'Token has been Blacklisted']);

                }
            }

            if ($exception instanceof JWTException) {
                if ($exception->getMessage() === 'A token is required') {
                    return response()->json([
                            'data' => null,
                            'status' => false,
                            'err_' => [
                                'message' => 'A token is required',
                                'code' => 1
                            ]
                        ]
                    );
                }
            }
//            JWTException


        }
        return parent::render($request, $exception);
    }

    protected function unauthenticated($request, AuthenticationException $exception)
    {

        $guard = array_get($exception->guards(), 0);
        switch ($guard) {
            case 'admin':
                $login = 'admin.login';
                break;
            case 'client':
                $login = 'client.login';
                break;
            case 'user':
                $login = 'user.login';
                break;
            default:
                $login = 'login';
                break;
        }
//        return redirect()->guest(route($login));

        return $request->expectsJson() ? response()->json(['error' => 'Unauthenticated.'], 401) : redirect()->guest(route($login));

//        if ($request->expectsJson()) {
//            return response()->json(['error' => 'Unauthenticated.'], 401);
//        }
    }
}
