<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ApartmentRent extends Model
{
    protected $fillable = [
        'apt_id','rent_amount','service_charge', 'adv_month','adv_rent','electricity', 'gas','water','status',
    ];

    public function saveData($data)
    {
        //dd($data->property_name);
        $this->apt_id = $data->apt_id;
        $this->rent_amount = $data->rent_amount;
        $this->service_charge = $data->service_charge;
        $this->adv_month = $data->adv_month;
        $this->adv_rent = $data->adv_rent;
        $this->electricity = ($data->electricity != 1) ? 0 : 1;
        $this->gas = ($data->gas != 1) ? 0 : 1;
        $this->water = ($data->water != 1) ? 0 : 1;
        $this->status= $data->status;
        $this->save();
        return 1;
    }

    public function updateData($data, $id)
    {
        $ticket = $this->where('apt_id', '=', $id)->first();

        foreach($data as $key => $value){
            if (isset($ticket->$key)){
                $ticket->$key = $value;
            }
        }

//        $ticket->rent_amount = $data->rent_amount;
//        $ticket->service_charge = $data->service_charge;
//        $ticket->adv_month = $data->adv_month;
//        $ticket->adv_rent = $data->adv_rent;
//        $ticket->electricity = ($data->electricity != 1) ? 0 : 1;
//        $ticket->gas = ($data->gas != 1) ? 0 : 1;
//        $ticket->water = ($data->water != 1) ? 0 : 1;
//        $ticket->status= $data->status;
        $ticket->save();
        return 1;
    }

    public function apartment()
    {
        return $this->hasOne('App\Model\Apartment',"apt_id", "id");
    }
}
