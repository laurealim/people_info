<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Competition extends Model
{
    protected $fillable = [
        'name','created_by', 'status',
    ];
}
