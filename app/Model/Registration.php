<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Registration extends Model
{
    protected $fillable = [
        'created_by','updated_by'
    ];

    public function user()
    {
        return $this->hasOne('App\User', 'id', "created_by");
    }

    public function saveData($data)
    {
        foreach($data->request as $key => $value){
            if ($key != "_token"){
                $this->$key = $value;
            }
        }
        $this->dept_id = auth()->user()->dept_id;
        $this->created_by = auth()->user()->id;
        $this->status = config('constants.registration_status.Waiting');
        $this->dob = date('Y-m-d', strtotime($data->dob));
        $this->save();
        return $this->id;
    }

    public function updateData($data)
    {
        $ticket = $this->find($data['id']);
        foreach($data->request as $key => $value){
            if ($key != "_token"){
                $ticket->$key = $value;
            }
        }
        $this->updated_by = auth()->user()->id;
        $ticket->dob = date('Y-m-d', strtotime($ticket->dob));
        $ticket->save();
        return 1;
    }

    public function trainings()
    {
        return $this->hasOne('App\Model\User', 'training_id', "id");
    }
}
