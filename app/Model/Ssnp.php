<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Ssnp extends Model
{
    protected $fillable = [
        'snnp_name','created_by','status',
    ];

    public function saveData($data)
    {
//        dd($data);
        $this->ssnp_name = $data->ssnp_name;
        $this->created_by = auth()->user()->id;
        $this->status = config('constants.status.Active');
        $this->save();
        return 1;
    }

    public function updateData($data)
    {
        $ticket = $this->find($data['id']);
        $ticket->ssnp_name = $data->ssnp_name;
        $ticket->status = $data->status;
        $ticket->updated_by = auth()->user()->id;
        $ticket->save();
        return 1;
    }

    public function user()
    {
        return $this->hasMany('App\User')->orderBy('name', 'ASC');
    }
}
