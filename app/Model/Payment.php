<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = [
        'apt_id', 'client_id', 'pending_ledger_id', 'ledger_id', 'total_bill_payment', 'payment_type', 'comments', 'status', 'payment_month', 'payment_year',
    ];

    public function saveData($data)
    {
    }

    public function updateData($data, $id)
    {
        $ticket = $this->find($id);

        foreach($data as $key => $value){
            if (isset($ticket->$key)){
                $ticket->$key = $value;
            }
        }
        $ticket->updated_by = auth()->user()->id;
//        $ticket->owner = auth()->user()->id;
//        $ticket->property_id = $data->property_id;
//        $ticket->apt_number = $data->apt_number;
//        $ticket->apt_size = $data->apt_size;
//        $ticket->apt_level = $data->apt_level;
//        $ticket->bed_room = $data->bed_room;
//        $ticket->toilet = $data->toilet;
//        $ticket->veranda = $data->veranda;
//        $ticket->intercom = $data->intercom;
//        $ticket->geyser = $data->geyser;
//        $ticket->car_parking = $data->car_parking;
//        $ticket->status= $data->status;
        $ticket->save();
        return 1;
    }
}
