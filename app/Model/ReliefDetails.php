<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ReliefDetails extends Model
{
    protected $fillable = [
        'f_gender','created_by','updated_by'
    ];

    public function user()
    {
        return $this->hasOne('App\User', 'id', "created_by");
    }

    public function saveData($data)
    {
//        $id = $data->request['lastInsertedId'];
//        dd($data->request);
        foreach($data->request as $keys => $values){
            if ($keys == "tblData"){
                foreach($values as $key => $value) {
                    $this->$key = json_encode($value);
//                    pr($key);
                }
            }
            if ($keys == "lastInsertedId"){
                $this->rlf_id = $values;
            }
        }
//        dd();
        $this->created_by = auth()->user()->id;
//        $this->rlf_id = 3;
        $this->save();
        return 1;
    }

    public function updateData($data)
    {
        $ticket = $this->where('rlf_id', $data['id'])->first();
//        pr($data);
        foreach($data->request as $keys => $values){
            if ($keys == "tblData"){
                foreach($values as $key => $value) {
                    $ticket->$key = json_encode($value);
                }
            }
        }
//        dd($ticket);
        $ticket->updated_by = auth()->user()->id;
        $ticket->save();
        return 1;
    }
}
