<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Model\Country;

class City extends Model
{
    protected $fillable = [
        'name', 'created_by', 'country_id'
    ];

    public function saveData($data)
    {
        $this->created_by = auth()->user()->id;
        $this->name = $data->name;
        $this->country_id = $data->country_id;
        $this->status = $data->status;
        $this->save();
        return 1;
    }

    public function updateData($data)
    {
        $ticket = $this->find($data['id']);
        $ticket->name = $data->name;
        $ticket->country_id = $data->country_id;
        $ticket->status = $data->status;
        $ticket->save();
        return 1;
    }

    public function user()
    {
        return $this->hasOne('App\User', "id", "created_by");
    }

    public function country()
    {
        return $this->hasOne('App\Model\Country',"id", "country_id");
    }
}
