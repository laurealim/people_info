<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ReliefFamilySearch extends Model
{
    protected $fillable = [
        'created_by','updated_by'
    ];

    public function reliefs()
    {
        return $this->hasOne('App\Model\Reliefs', 'id', "relf_id");
    }

    public function saveData($data)
    {
        foreach($data->request as $keys => $values){
            if ($keys == "familySearchData"){
                foreach($values as $key => $value) {
                    $this->$key = $value;
                }
            }
            if ($keys == "lastInsertedId"){
                $this->relf_id = $values;
            }
        }
        $this->created_by = auth()->user()->id;
        $this->save();
        return 1;
    }

    public function updateData($data)
    {
        $ticket = $this->where('relf_id', $data['id'])->first();
        foreach($data->request as $keys => $values){
            if ($keys == "familySearchData"){
                foreach($values as $key => $value) {
                    $ticket->$key = $value;
                }
            }
        }
        $ticket->updated_by = auth()->user()->id;
        $ticket->save();
        return 1;
    }

}
