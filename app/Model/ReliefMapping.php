<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ReliefMapping extends Model
{
    //

    public function reliefs()
    {
        return $this->hasOne('App\Model\Relief', "id", "reg_id");
    }
}
