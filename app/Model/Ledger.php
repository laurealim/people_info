<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Ledger extends Model
{
    protected $fillable = [
        'apt_id', 'client_id', 'pending_ledger_id', 'amount', 'secure_amount', 'previous_amount', 'current_amount', 'ledger_for', 'ledger_type', 'status',
    ];

    public function saveData($data)
    {
        //dd($data->property_name);
//        $this->property_id = $data->property_id;
//        $this->apt_number = $data->apt_number;
        $this->created_by = auth()->user()->id;
        $this->owner = auth()->user()->id;
//        $this->apt_size = $data->apt_size;
//        $this->apt_level = $data->apt_level;
        $this->bed_room = $data->bed_room;
        $this->toilet = $data->toilet;
        $this->veranda = $data->veranda;
        $this->intercom = $data->intercom;
        $this->geyser = $data->geyser;
        $this->car_parking = $data->car_parking;
        $this->status = $data->status;
        $this->is_assigned = $data->is_assigned;
        $this->client_id = $data->client_id;
        $this->save();
        return 1;
    }

    public function updateData($data, $id)
    {
        $ticket = $this->find($id);

        foreach($data as $key => $value){
            if (isset($ticket->$key)){
                $ticket->$key = $value;
            }
        }
        $ticket->updated_by = auth()->user()->id;
//        $ticket->owner = auth()->user()->id;
//        $ticket->property_id = $data->property_id;
//        $ticket->apt_number = $data->apt_number;
//        $ticket->apt_size = $data->apt_size;
//        $ticket->apt_level = $data->apt_level;
//        $ticket->bed_room = $data->bed_room;
//        $ticket->toilet = $data->toilet;
//        $ticket->veranda = $data->veranda;
//        $ticket->intercom = $data->intercom;
//        $ticket->geyser = $data->geyser;
//        $ticket->car_parking = $data->car_parking;
//        $ticket->status= $data->status;
        $ticket->save();
        return 1;
    }

    public function user()
    {
        return $this->hasOne('App\User', "id", "created_by");
    }
}