<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    protected $fillable = [
        'p_code','created_by','status',
    ];

    public function saveData($data)
    {
//        dd($data);
        $this->title = $data->title;
        $this->p_code = $data->p_code;
        $this->desc = $data->desc;
        $this->created_by = auth()->user()->id;
        $this->status = config('constants.package_status.Upcoming');
        $this->dob = date('Y-m-d', strtotime($data->dob));
        $this->save();
        return 1;
    }

    public function updateData($data)
    {
        $ticket = $this->find($data['id']);
        $ticket->title = $data->title;
        $ticket->p_code = $data->p_code;
        $ticket->desc = $data->desc;
        $ticket->status = $data->status;
        $ticket->updated_by = auth()->user()->id;
        $ticket->dob = date('Y-m-d', strtotime($data->dob));
        $ticket->save();
        return 1;
    }

    public function user()
    {
        return $this->hasMany('App\User')->orderBy('name', 'ASC');
    }
}
