<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Apartment extends Model
{
    protected $fillable = [
        'property_id','apt_number', 'apt_images','created_by','owner','apt_size','apt_level', 'bed_room','toilet','veranda', 'intercom','geyser','car_parking','status','is_assigned','client_id',
    ];

    public function saveData($data)
    {
        //dd($data->property_name);
//        $this->property_id = $data->property_id;
//        $this->apt_number = $data->apt_number;
        $this->created_by = auth()->user()->id;
        $this->owner = auth()->user()->id;
//        $this->apt_size = $data->apt_size;
//        $this->apt_level = $data->apt_level;
        $this->bed_room = $data->bed_room;
        $this->toilet = $data->toilet;
        $this->veranda = $data->veranda;
        $this->intercom = $data->intercom;
        $this->geyser = $data->geyser;
        $this->car_parking = $data->car_parking;
        $this->status= $data->status;
        $this->is_assigned= $data->is_assigned;
        $this->client_id= $data->client_id;
        $this->save();
        return 1;
    }

    public function updateData($data, $id)
    {
        $ticket = $this->find($id);

        foreach($data as $key => $value){
            if (isset($ticket->$key)){
                $ticket->$key = $value;
            }
        }
        $ticket->updated_by = auth()->user()->id;
        $ticket->save();
        return 1;
    }

    public function user()
    {
        return $this->hasOne('App\User', "id", "created_by");
    }

    public function owner()
    {
        return $this->hasOne('App\User', "id", "owner");
    }

    public function apartmentRent()
    {
        return $this->hasOne('App\Model\ApartmentRent',"id", "apt_id");
    }
}
