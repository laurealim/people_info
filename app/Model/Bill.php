<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Bill extends Model
{
    protected $fillable = [
        'apt_id', 'client_id', 'pending_ledger_id', 'ledger_id', 'rent_amount', 'service_charge', 'security_bill', 'security_bill_month', 'total_bill', 'current_amount', 'bill_type', 'comments', 'status', 'billing_month', 'billing_year',
    ];

    public function saveData($data)
    {
        //dd($data->property_name);
//        $this->property_id = $data->property_id;
//        $this->apt_number = $data->apt_number;
        $this->created_by = auth()->user()->id;
        $this->owner = auth()->user()->id;
//        $this->apt_size = $data->apt_size;
//        $this->apt_level = $data->apt_level;
        $this->bed_room = $data->bed_room;
        $this->toilet = $data->toilet;
        $this->veranda = $data->veranda;
        $this->intercom = $data->intercom;
        $this->geyser = $data->geyser;
        $this->car_parking = $data->car_parking;
        $this->status = $data->status;
        $this->billing_month = $data->billing_month;
        $this->billing_year = $data->billing_year;
        $this->is_assigned = $data->is_assigned;
        $this->client_id = $data->client_id;
        $this->save();
        return 1;
    }

    public function updateData($data, $id)
    {
        $ticket = $this->find($id);

        foreach($data as $key => $value){
            if (isset($ticket->$key)){
                $ticket->$key = $value;
            }
        }
        $ticket->updated_by = auth()->user()->id;
//        $ticket->owner = auth()->user()->id;
//        $ticket->property_id = $data->property_id;
//        $ticket->apt_number = $data->apt_number;
//        $ticket->apt_size = $data->apt_size;
//        $ticket->apt_level = $data->apt_level;
//        $ticket->bed_room = $data->bed_room;
//        $ticket->toilet = $data->toilet;
//        $ticket->veranda = $data->veranda;
//        $ticket->intercom = $data->intercom;
//        $ticket->geyser = $data->geyser;
//        $ticket->car_parking = $data->car_parking;
//        $ticket->status= $data->status;
        $ticket->save();
        return 1;
    }

    public function getPendingLedgerIdByBillId($billId){
        $resData = $this->where('status','!=',config('constants.state.Rejected'))->select('pending_ledger_id')->find($billId);
        return $resData->pending_ledger_id;
    }

    public function user()
    {
        return $this->hasOne('App\User', "id", "created_by");
    }


}
