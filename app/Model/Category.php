<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'name','created_by',
    ];

    public function saveData($data)
    {
        $this->created_by = auth()->user()->id;
        $this->name = $data->name;
        $this->status= $data->status;
        $this->save();
        return 1;
    }

    public function updateData($data)
    {
        $ticket = $this->find($data['id']);
        $ticket->name = $data->name;
        $ticket->status= $data->status;
        $ticket->save();
        return 1;
    }

    public function user()
    {
        return $this->hasOne('App\User', "id", "created_by");
    }
}
