<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Organization extends Model
{
    protected $fillable = [
        'name','type','created_by','address','phone'
    ];

    public static $organizationType = [
        1 => 'Business School',
        2 => 'Engineering College',
        3 => 'Company',
        4 => 'University',
        5 => 'Others',
    ];

    public function saveData($data)
    {
        $this->created_by = auth()->user()->id;
        $this->name = $data->name;
        $this->type = $data->type;
        $this->address = $data->address;
        $this->phone = $data->phone;
        $this->status= $data->status;
        $this->save();
        return 1;
    }

    public function updateData($data)
    {
        $ticket = $this->find($data['id']);
        $ticket->name = $data->name;
        $this->type = $data->type;
        $this->address = $data->address;
        $this->phone = $data->phone;
        $ticket->status= $data->status;
        $ticket->save();
        return 1;
    }

    public function user()
    {
        return $this->hasOne('App\User', "id", "created_by");
    }
}
