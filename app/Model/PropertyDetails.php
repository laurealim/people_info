<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PropertyDetails extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'property_id','builders','total_unit','total_floor','unit_floor',
    ];

    public function saveData($data)
    {
        $this->property_id = $data->property_id;
        $this->builders = $data->builders;
        $this->total_unit = $data->total_unit;
        $this->total_floor = $data->total_floor;
        $this->unit_floor= $data->unit_floor;
        $this->has_gas= ($data->has_gas != 1) ? 0 : 1;
        $this->has_lift= ($data->has_lift != 1) ? 0 : 1;
        $this->has_generator= ($data->has_generator != 1) ? 0 : 1;
        $this->p_description= $data->p_description;
        $this->p_images= $data->p_images;

        $this->save();
        return 1;
    }

    public function updateData($data)
    {
//        $ticket = $this->find($data['property_id']);
        $ticket = $this->where('property_id', $data['property_id'])->first();
        $ticket->builders = $data->builders;
        $ticket->total_unit = $data->total_unit;
        $ticket->total_floor = $data->total_floor;
        $ticket->unit_floor= $data->unit_floor;
        $ticket->has_gas= ($data->has_gas != 1) ? 0 : 1;
        $ticket->has_lift= ($data->has_lift != 1) ? 0 : 1;
        $ticket->has_generator= ($data->has_generator != 1) ? 0 : 1;
        $ticket->p_description= $data->p_description;
        $ticket->p_images= ($data->p_images != "" ? $data->p_images : $ticket->p_images);
        $ticket->save();
        return 1;
    }

    public function user()
    {
        return $this->hasOne('App\User', "id", "created_by");
    }
}
