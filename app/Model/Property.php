<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    protected $fillable = [
        'property_name','created_by','address','country_id','division_id','district_id',
    ];

    public function saveData($data)
    {
//        dd($data);
        $this->created_by = auth()->user()->id;
        $this->owner = $data->owner;
        $this->property_name = $data->property_name;
        $this->country_id = $data->country_id;
        $this->division_id = $data->division_id;
        $this->district_id = $data->district_id;
        $this->address = $data->address;
        $this->status= $data->status;
        $this->save();
        return 1;
    }

    public function updateData($data)
    {
        $ticket = $this->find($data['id']);
        $ticket->property_name = $data->property_name;
        $ticket->country_id = $data->country_id;
        $ticket->division_id = $data->division_id;
        $ticket->district_id = $data->district_id;
        $ticket->address = $data->address;
        $ticket->updated_by = auth()->user()->id;
        $ticket->status= $data->status;
        $ticket->save();
        return 1;
    }

    public function getOwnerIdByPropertyId($id)
    {
        $res =  $this->select('owner')->find($id);
        return $res['owner'];
    }

    public function user()
    {
        return $this->hasOne('App\User', "id", "created_by");
    }
}
