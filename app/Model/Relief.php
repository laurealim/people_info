<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Relief extends Model
{
    protected $fillable = [
        'created_by',
        'updated_by',
        'trible'
    ];

    public function user()
    {
        return $this->hasOne('App\User', 'id', "created_by");
    }

    public function saveData($data)
    {
        foreach ($data->request as $key => $value) {
            if ($key != "_token" && $key != "tblData") {
                $this->$key = $value;
            }
        }
        $this->created_by = auth()->user()->id;
        $this->dob = date('Y-m-d', strtotime($data->dob));
        $this->save();
        return $this->id;
    }

    public function updateData($data)
    {
        $ticket = $this->find($data['id']);
        foreach ($data->request as $key => $value) {
            if ($key != "_token" && $key != "tblData") {
                $ticket->$key = $value;
            }
        }
        $ticket->updated_by = auth()->user()->id;
        $ticket->dob = date('Y-m-d', strtotime($ticket->dob));
        $ticket->save();
        return 1;
    }

    public function trainings()
    {
        return $this->hasOne('App\Model\User', 'training_id', "id");
    }

    public function relief_details()
    {
        return $this->hasMany('App\Model\ReliefDetails')->orderBy('created_at', 'DESC');
    }

    public function upazilas()
    {
        return $this->hasOne('App\Model\Upazila', "id", "upo_pre");
    }

    public function districts()
    {
        return $this->hasOne('App\Model\District', "id", "dist_pre");
    }

    public function relief_mappings()
    {
        return $this->hasOne('App\Model\ReliefMapping', "reg_id", "id");
    }

    public function relief_family_searchs()
    {
        return $this->hasOne('App\Model\ReliefFamilySearch', "relf_id", "id");
    }
}