<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $fillable = [
    'name','created_by','status','code',
];

    public function saveData($data)
    {
        $this->created_by = auth()->user()->id;
        $this->name = $data->name;
        $this->code = time();
        $this->status = config('constants.status.Active');
        $this->save();
        return 1;
    }

    public function updateData($data)
    {
        $ticket = $this->find($data['id']);
        $ticket->name = $data->name;
        $ticket->status = $data->status;
        $ticket->save();
        return 1;
    }

    public function user()
    {
        return $this->hasMany('App\User')->orderBy('name', 'ASC');;
    }

    public function trainings()
    {
        return $this->hasMany('App\Model\Training')->orderBy('dept_id', 'ASC');;
    }
}
