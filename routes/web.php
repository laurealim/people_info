<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
    //    return view('welcome');
//    return view('auth.mlogin');
});

Route::get('/clear-cache', function () {
    Artisan::call('cache:clear');
    Artisan::call('view:clear');
    Artisan::call('config:cache');
    return "Cache cleared";
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

/*  API Portion Routes Status   */
/*================================*/
Route::prefix('api')->group(function () {
    Route::get('password/reset/{token}', 'Api\Auth\ResetPasswordController@showResetForm')->name('password.request');
    Route::post('password/reset', 'Api\Auth\ResetPasswordController@postReset')->name('password.reset');
});

/*  User Portion Routes Starts   */
/*================================*/
Route::prefix('user')->group(function () {

    Route::get('login', 'Auth\LoginController@showLoginForm')->name('user.form');
    Route::post('login', 'Auth\LoginController@login')->name('user.login');
    Route::post('logout', 'Auth\LoginController@logout')->name('user.logout');
    Route::get('/', 'DashboardController@index')->name('user.dashboard');

    // Registration Routes...
    $this->get('register', 'Auth\RegisterController@showRegistrationForm')->name('user.register');
    $this->post('register', 'Auth\RegisterController@register')->name('user.submit.register');

    // Password Reset Routes...
    Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('user.password.request');
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('user.password.email');
    Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('user.password.reset');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');

});

Route::middleware('auth:user')->prefix('user')->group(function () {

    /*  User Dashboard Route Section    */
    Route::get('dashboard/registration', 'DashboardController@userGetRegistration')->name('dashboard.userGetRegistration'); // index

    //*  District Route Section */
    Route::post('district/districtSelectAjaxList', 'DistrictController@userDistrictSelectAjaxList')->name('district.userDistrictSelectAjaxList'); // Ajax District List

    //*  Upazila-- Route Section */
    Route::post('upazila/upazilaSelectAjaxList', 'UpazilaController@userUpazilaSelectAjaxList')->name('upazila.userUpazilaSelectAjaxList'); // Ajax Upazila List

    //*  Union Route Section */
    Route::post('union/unionSelectAjaxList', 'UnionController@userUnionSelectAjaxList')->name('union.userUnionSelectAjaxList'); // Ajax District List

    //*  Client Route Section */
    Route::get('client/list', 'ClientController@userList')->name('client.userList'); // index
    Route::get('client/add', 'ClientController@userForm')->name('client.userForm'); // create
    Route::post('client/store', 'ClientController@userStore')->name('client.userStore'); // store
    Route::get('client/change_password', 'ClientController@userChangePasswordForm')->name('client.userChangePasswordForm'); // Change Password Form
    Route::get('client/{id}', 'ClientController@userEdit')->name('client.userEdit'); // edit
    Route::post('client/change_password', 'ClientController@userChangePassword')->name('client.userChangePassword'); // Change Password
    Route::post('client/{id}', 'ClientController@userUpdate')->name('client.userUpdate'); // update
    Route::delete('client/{id}', 'ClientController@userDestroy')->name('client.userDelete'); // update
//    Route::delete('client/ajaxDelete', 'ClientController@adminAjaxDelete')->name('client.adminAjaxDelete');    // update


    //*  Registration Route Section */
    Route::get('registration/list/', 'RegistrationController@userList')->name('registration.userList'); // index
    Route::get('registration/list/{id}', 'RegistrationController@userListId')->name('registration.userListId'); // index
    Route::get('registration/add', 'RegistrationController@userForm')->name('registration.userForm'); // create
    Route::post('registration/store', 'RegistrationController@userStore')->name('registration.userStore'); // store
    Route::post('registration/nid_duplicate_check', 'RegistrationController@userNidDuplicateCheck')->name('registration.userNidDuplicateCheck'); // Ajax NID Duplicate Check
    Route::post('registration/phone_duplicate_check', 'RegistrationController@userPhoneDuplicateCheck')->name('registration.userPhoneDuplicateCheck'); // Ajax Phone Duplicate Check
    Route::get('registration/print', 'RegistrationController@userPrintCard')->name('registration.userPrintCard'); // Print Data
    Route::post('registration/count_print_hit', 'RegistrationController@userCountPrintHit')->name('registration.userCountPrintHit'); // Participant Export
    Route::get('registration/{id}', 'RegistrationController@userEdit')->name('registration.userEdit'); // edit
    Route::post('registration/{id}', 'RegistrationController@userUpdate')->name('registration.userUpdate'); // update
    Route::delete('registration/{id}', 'RegistrationController@userDestroy')->name('registration.userDestroy'); // update
    Route::delete('registration/status/{id}/{status}', 'RegistrationController@userRegStatusChange')->name('registration.userRegStatusChange'); // update
});

/*  User Portion Routes Ends   */
/*=============================*/

/*  Client Portion Routes Starts   */
/*================================*/
Route::prefix('client')->group(function () {

    Route::get('login', 'Auth\ClientLoginController@showLoginForm')->name('client.form');
    Route::post('login', 'Auth\ClientLoginController@login')->name('client.login');
    Route::post('logout', 'Auth\ClientLoginController@logout')->name('client.logout');
    Route::get('/', 'ClientDashboardController@index')->name('client.dashboard');

    // Registration Routes...
    $this->get('register', 'Auth\ClientRegisterController@showRegistrationForm')->name('client.register');
    $this->post('register', 'Auth\ClientRegisterController@register')->name('client.submit.register');

    // Password Reset Routes...
    Route::get('password/reset', 'Auth\ClientForgotPasswordController@showLinkRequestForm')->name('client.password.request');
    Route::post('password/email', 'Auth\ClientForgotPasswordController@sendResetLinkEmail')->name('client.password.email');
    Route::get('password/reset/{token}', 'Auth\ClientResetPasswordController@showResetForm')->name('client.password.reset');
    Route::post('password/reset', 'Auth\ClientResetPasswordController@reset');

});

Route::middleware('auth:client')->prefix('client')->group(function () {

    /*  Client Dashboard Route Section  */
    //    Route::get('dashboard/status', 'ClientDashboardController@clientGetTrainingStatus')->name('dashboard.adminGetTrainingStatus');   // index
    Route::get('dashboard/registration', 'ClientDashboardController@clientGetRegistration')->name('dashboard.clientGetRegistration'); // index
    Route::get('dashboard/latest_training', 'ClientDashboardController@clientGetLastTraining')->name('dashboard.clientGetLastTraining'); // index
//    Route::get('dashboard/training_pi', 'ClientDashboardController@adminTrainingPiChart')->name('dashboard.adminTrainingPiChart');   // index

    //*  District Route Section */
    Route::post('district/districtSelectAjaxList', 'DistrictController@clientDistrictSelectAjaxList')->name('district.clientDistrictSelectAjaxList'); // Ajax District List

    //*  Upazila-- Route Section */
    Route::post('upazila/upazilaSelectAjaxList', 'UpazilaController@clientUpazilaSelectAjaxList')->name('upazila.clientUpazilaSelectAjaxList'); // Ajax Upazila List

    //*  Union Route Section */
    Route::post('union/unionSelectAjaxList', 'UnionController@clientUnionSelectAjaxList')->name('union.clientUnionSelectAjaxList'); // Ajax District List

    //*  Client Route Section */
    Route::get('client/list', 'ClientController@clientList')->name('client.clientList'); // index
    Route::get('client/add', 'ClientController@clientForm')->name('client.clientForm'); // create
    Route::post('client/store', 'ClientController@clientStore')->name('client.clientStore'); // store
    Route::get('client/change_password', 'ClientController@clientChangePasswordForm')->name('client.clientChangePasswordForm'); // Change Password Form
    Route::get('client/{id}', 'ClientController@clientEdit')->name('client.clientEdit'); // edit
    Route::post('client/change_password', 'ClientController@clientChangePassword')->name('client.clientChangePassword'); // Change Password
    Route::post('client/{id}', 'ClientController@clientUpdate')->name('client.clientUpdate'); // update
    Route::delete('client/{id}', 'ClientController@clientDestroy')->name('client.clientDelete'); // update
//    Route::delete('client/ajaxDelete', 'ClientController@adminAjaxDelete')->name('client.adminAjaxDelete');    // update

    //*  Trades Route Section */
    Route::get('trade/list', 'TradeController@clientList')->name('trade.clientList'); // index
    Route::get('trade/add', 'TradeController@clientForm')->name('trade.clientForm'); // create
    Route::post('trade/store', 'TradeController@clientStore')->name('trade.clientStore'); // store
    Route::get('trade/change_password', 'TradeController@clientChangePasswordForm')->name('trade.clientChangePasswordForm'); // Change Password Form
    Route::get('trade/{id}', 'TradeController@clientEdit')->name('trade.clientEdit'); // edit
    Route::post('trade/change_password', 'TradeController@clientChangePassword')->name('trade.clientChangePassword'); // Change Password
    Route::post('trade/reset_password/{id}', 'TradeController@clientResetPassword')->name('trade.clientResetPassword'); // Reset Password
    Route::post('trade/{id}', 'TradeController@clientUpdate')->name('trade.clientUpdate'); // update
    Route::delete('trade/{id}', 'TradeController@clientDestroy')->name('trade.clientDestroy'); // update
//    Route::delete('client/ajaxDelete', 'ClientController@adminAjaxDelete')->name('client.adminAjaxDelete');    // update

    //*  Training Route Section */
    Route::get('training/list', 'TrainingController@clientList')->name('training.clientList'); // index
    Route::get('training/add', 'TrainingController@clientForm')->name('training.clientForm'); // create
    Route::post('training/store', 'TrainingController@clientStore')->name('training.clientStore'); // store
    Route::get('training/export', 'TrainingController@clientExportTraining')->name('training.clientExportTraining'); // Export Excel (xls) Data
    Route::get('training/{id}', 'TrainingController@clientEdit')->name('training.clientEdit'); // edit
    Route::post('training/{id}', 'TrainingController@clientUpdate')->name('training.clientUpdate'); // update
    Route::delete('training/{id}', 'TrainingController@clientDestroy')->name('training.clientDestroy'); // update

    //*  Participant Route Section */
    Route::get('participant/list/', 'ParticipantController@clientList')->name('participant.clientList'); // index
    Route::get('participant/list/{id}', 'ParticipantController@clientListId')->name('participant.clientListId'); // index
    Route::get('participant/add', 'ParticipantController@clientForm')->name('participant.clientForm'); // create
    Route::post('participant/store', 'ParticipantController@clientStore')->name('participant.clientStore'); // store
    Route::get('participant/{id}', 'ParticipantController@clientEdit')->name('participant.clientEdit'); // edit
    Route::post('participant/{id}', 'ParticipantController@clientUpdate')->name('participant.clientUpdate'); // update
    Route::delete('participant/{id}', 'ParticipantController@clientDestroy')->name('participant.clientDestroy'); // update
    Route::delete('participant/status/{id}/{status}', 'ParticipantController@clientRegStatusChange')->name('participant.clientRegStatusChange'); // update
    Route::post('participant/confirm/{id}', 'ParticipantController@clientRegConfirm')->name('participant.clientRegConfirm'); // Confirm Registration

    //*  Registration Route Section */
    Route::get('registration/list/', 'RegistrationController@clientList')->name('registration.clientList'); // index
    Route::get('registration/list/{id}', 'RegistrationController@clientListId')->name('registration.clientListId'); // index
    Route::get('registration/add', 'RegistrationController@clientForm')->name('registration.clientForm'); // create
    Route::post('registration/store', 'RegistrationController@clientStore')->name('registration.clientStore'); // store
    Route::get('registration/export', 'RegistrationController@clientExportRegister')->name('registration.clientExportRegister'); // Export Excel (xls) Data
    Route::get('registration/{id}', 'RegistrationController@clientEdit')->name('registration.clientEdit'); // edit
    Route::post('registration/{id}', 'RegistrationController@clientUpdate')->name('registration.clientUpdate'); // update
    Route::delete('registration/{id}', 'RegistrationController@clientDestroy')->name('registration.clientDestroy'); // update
    Route::delete('registration/status/{id}/{status}', 'RegistrationController@clientRegStatusChange')->name('registration.clientRegStatusChange'); // update

    //*  Report Route Section */
    Route::get('report/training_wise_registration/', 'ReportController@clientTrainingWiseRegistration')->name('report.clientTrainingWiseRegistration'); // List
    Route::post('report/export_training_wise_registration', 'ReportController@clientExportTrainingTrainingWiseRegistration')->name('report.clientExportTrainingTrainingWiseRegistration'); // List
    Route::get('report/training_report/', 'ReportController@clientTrainingReport')->name('report.clientTrainingReport'); // Training Report
    Route::post('report/training_report_export', 'ReportController@clientTrainingReportExport')->name('report.clientTrainingReportExport'); // Training Export
    Route::get('report/participant_report/', 'ReportController@clientParticipantReport')->name('report.clientParticipantReport'); // Participant Report
    Route::post('report/participant_report_export', 'ReportController@clientParticipantReportExport')->name('report.clientParticipantReportExport'); // Participant Export
});

/*  Client Portion Routes Ends   */
/*=============================*/




/*  Admin Portion Routes Starts   */
/*================================*/
Route::prefix('admin')->group(function () {

    Route::get('login', 'Auth\AdminLoginController@showLoginForm')->name('admin.form');
    Route::post('login', 'Auth\AdminLoginController@login')->name('admin.login');
    Route::post('logout', 'Auth\AdminLoginController@logout')->name('admin.logout');
    Route::get('/', 'AdminController@index')->name('admin.dashboard');

    // Registration Routes...
    $this->get('register', 'Auth\AdminRegisterController@showRegistrationForm')->name('admin.register');
    $this->post('register', 'Auth\AdminRegisterController@register')->name('admin.submit.register');

    // Password Reset Routes...
    Route::get('password/reset', 'Auth\AdminForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
    Route::post('password/email', 'Auth\AdminForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
    Route::get('password/reset/{token}', 'Auth\AdminResetPasswordController@showResetForm')->name('admin.password.reset');
    Route::post('password/reset', 'Auth\AdminResetPasswordController@reset');

});

Route::middleware('auth:admin')->prefix('admin')->group(function () {
    /*  Dashboard Route Section */
    Route::get('dashboard/status', 'AdminController@adminGetTrainingStatus')->name('dashboard.adminGetTrainingStatus'); // index
    Route::get('dashboard/registration', 'AdminController@adminGetRegistration')->name('dashboard.adminGetRegistration'); // index
    Route::get('dashboard/latest_training', 'AdminController@adminGetLastTraining')->name('dashboard.adminGetLastTraining'); // index
    Route::get('dashboard/training_pi', 'AdminController@adminTrainingPiChart')->name('dashboard.adminTrainingPiChart'); // index
    Route::get('dashboard/trible_list', 'AdminController@adminTribleList')->name('dashboard.adminTribleList'); // index

    //*  Country Route Section */
    Route::get('country/list', 'CountryController@adminList')->name('country.adminList'); // index
    Route::get('country/add', 'CountryController@adminForm')->name('country.adminForm'); // create
    Route::post('country/store', 'CountryController@adminStore')->name('country.adminStore'); // store
    Route::get('country/{id}', 'CountryController@adminEdit')->name('country.adminEdit'); // edit
    Route::post('country/{id}', 'CountryController@adminUpdate')->name('country.adminUpdate'); // update
    Route::delete('country/{id}', 'CountryController@adminDestroy')->name('country.adminDelete'); // update

    //*  Division Route Section */
    Route::get('division/list', 'DivisionController@adminList')->name('division.adminList'); // index
    Route::get('division/add', 'DivisionController@adminForm')->name('division.adminForm'); // create
    Route::post('division/store', 'DivisionController@adminStore')->name('division.adminStore'); // store
    Route::post('division/divisionSelectAjaxList', 'DivisionController@adminDivisionSelectAjaxList')->name('division.adminDivisionSelectAjaxList'); // Ajax Division List
    Route::get('division/{id}', 'DivisionController@adminEdit')->name('division.adminEdit'); // edit
    Route::post('division/{id}', 'DivisionController@adminUpdate')->name('division.adminUpdate'); // update
    Route::delete('division/{id}', 'DivisionController@adminDestroy')->name('division.adminDelete'); // delete

    //*  District Route Section */
    Route::get('district/list', 'DistrictController@adminList')->name('district.adminList'); // index
    Route::get('district/add', 'DistrictController@adminForm')->name('district.adminForm'); // create
    Route::post('district/store', 'DistrictController@adminStore')->name('district.adminStore'); // store
    Route::post('district/districtSelectAjaxList', 'DistrictController@adminDistrictSelectAjaxList')->name('district.adminDistrictSelectAjaxList'); // Ajax District List
    Route::get('district/{id}', 'DistrictController@adminEdit')->name('district.adminEdit'); // edit
    Route::post('district/{id}', 'DistrictController@adminUpdate')->name('district.adminUpdate'); // update
    Route::delete('district/{id}', 'DistrictController@adminDestroy')->name('district.adminDelete'); // delete

    //*  Upazila-- Route Section */
    Route::get('upazila/list', 'UpazilaController@adminList')->name('upazila.adminList'); // index
    Route::get('upazila/add', 'UpazilaController@adminForm')->name('upazila.adminForm'); // create
    Route::post('upazila/store', 'UpazilaController@adminStore')->name('upazila.adminStore'); // store
    Route::post('upazila/upazilaSelectAjaxList', 'UpazilaController@adminUpazilaSelectAjaxList')->name('upazila.adminUpazilaSelectAjaxList'); // Ajax Upazila List
    Route::get('upazila/{id}', 'UpazilaController@adminEdit')->name('upazila.adminEdit'); // edit
    Route::post('upazila/{id}', 'UpazilaController@adminUpdate')->name('upazila.adminUpdate'); // update
    Route::delete('upazila/{id}', 'UpazilaController@adminDestroy')->name('upazila.adminDelete'); // delete

    //*  Union Route Section */
    Route::get('union/list', 'UnionController@adminList')->name('union.adminList'); // index
    Route::get('union/add', 'UnionController@adminForm')->name('union.adminForm'); // create
    Route::post('union/store', 'UnionController@adminStore')->name('union.adminStore'); // store
    Route::post('union/unionSelectAjaxList', 'UnionController@adminUnionSelectAjaxList')->name('union.adminUnionSelectAjaxList'); // Ajax District List
    Route::get('union/{id}', 'UnionController@adminEdit')->name('union.adminEdit'); // edit
    Route::post('union/{id}', 'UnionController@adminUpdate')->name('union.adminUpdate'); // update
    Route::delete('union/{id}', 'UnionController@adminDestroy')->name('union.adminDelete'); // delete

    //*  City Route Section */
    Route::get('city/list', 'CityController@adminList')->name('city.adminList'); // index
    Route::get('city/add', 'CityController@adminForm')->name('city.adminForm'); // create
    Route::post('city/store', 'CityController@adminStore')->name('city.adminStore'); // store
    Route::get('city/{id}', 'CityController@adminEdit')->name('city.adminEdit'); // edit
    Route::post('city/{id}', 'CityController@adminUpdate')->name('city.adminUpdate'); // update
    Route::delete('city/{id}', 'CityController@adminDestroy')->name('city.adminDelete'); // update

    //*  Profile Route Section */
    Route::get('profile', 'Admin\AdminController@showProfile')->name('admin.profile.view');
    Route::post('profile', 'Admin\AdminController@updateProfile')->name('admin.profile.update');

    //*  Property Route Section */
    Route::get('property/list', 'PropertyController@adminList')->name('property.adminList'); // index
    Route::get('property/add', 'PropertyController@adminForm')->name('property.adminForm'); // create
    Route::post('property/store', 'PropertyController@adminStore')->name('property.adminStore'); // store
    Route::get('property/{id}', 'PropertyController@adminEdit')->name('property.adminEdit'); // edit
    Route::post('property/{id}', 'PropertyController@adminUpdate')->name('property.adminUpdate'); // update
    Route::delete('property/{id}', 'PropertyController@adminDestroy')->name('property.adminDelete'); // update

    //*  Apartment Route Section */
    Route::get('apt/list', 'ApartmentController@adminList')->name('apt.adminList'); // index
    Route::get('apt/add', 'ApartmentController@adminForm')->name('apt.adminForm'); // create
    Route::post('apt/store', 'ApartmentController@adminStore')->name('apt.adminStore'); // store
    Route::get('apt/confirm_assign_client', 'ApartmentController@adminConfirmAssignClient')->name('apt.adminConfirmAssignClient'); // confirm list
    Route::get('apt/approve_client/{apt_ids}/{client_ids}', 'ApartmentController@adminApproveClient')->name('apt.adminApproveClient'); // approve client
    Route::get('apt/{id}', 'ApartmentController@adminEdit')->name('apt.adminEdit'); // edit
    Route::post('apt/assign_client', 'ApartmentController@adminAssignClient')->name('apt.adminAssignClient'); // Assign Client
    Route::post('apt/{id}', 'ApartmentController@adminUpdate')->name('apt.adminUpdate'); // update
    Route::delete('apt/ajaxDelete', 'ApartmentController@adminAjaxDelete')->name('apt.adminAjaxDelete'); // ajax delete
    Route::delete('apt/reject-client/{apt_id}/{client_id}', 'ApartmentController@adminRejectClient')->name('apt.adminRejectClient'); // reject client
    Route::delete('apt/{id}', 'ApartmentController@adminDestroy')->name('apt.adminDelete'); // delete

    //*  Client Route Section */
    Route::get('client/list', 'ClientController@adminList')->name('client.adminList'); // index
    Route::get('client/add', 'ClientController@adminForm')->name('client.adminForm'); // create
    Route::post('client/store', 'ClientController@adminStore')->name('client.adminStore'); // store
    Route::get('client/change_password', 'ClientController@adminChangePasswordForm')->name('client.adminChangePasswordForm'); // Change Password Form
    Route::get('client/export_list', 'ClientController@adminExportClientList')->name('client.adminExportClientList'); // Export List
    Route::get('client/{id}', 'ClientController@adminEdit')->name('client.adminEdit'); // edit
    Route::post('client/change_password', 'ClientController@adminChangePassword')->name('client.adminChangePassword'); // Change Password
    Route::post('client/reset_password/{id}', 'ClientController@adminResetPassword')->name('client.adminResetPassword'); // Reset Password
    Route::post('client/{id}', 'ClientController@adminUpdate')->name('client.adminUpdate'); // update
    Route::delete('client/{id}', 'ClientController@adminDestroy')->name('client.adminDelete'); // update
//    Route::delete('client/ajaxDelete', 'ClientController@adminAjaxDelete')->name('client.adminAjaxDelete');    // update

    //*  Trades Route Section */
    Route::get('trade/list', 'TradeController@adminList')->name('trade.adminList'); // index
    Route::get('trade/add', 'TradeController@adminForm')->name('trade.adminForm'); // create
    Route::post('trade/store', 'TradeController@adminStore')->name('trade.adminStore'); // store
    Route::get('trade/change_password', 'TradeController@adminChangePasswordForm')->name('trade.adminChangePasswordForm'); // Change Password Form
    Route::get('trade/{id}', 'TradeController@adminEdit')->name('trade.adminEdit'); // edit
    Route::post('trade/change_password', 'TradeController@adminChangePassword')->name('trade.adminChangePassword'); // Change Password
    Route::post('trade/reset_password/{id}', 'TradeController@adminResetPassword')->name('trade.adminResetPassword'); // Reset Password
    Route::post('trade/{id}', 'TradeController@adminUpdate')->name('trade.adminUpdate'); // update
    Route::delete('trade/{id}', 'TradeController@adminDestroy')->name('trade.adminDestroy'); // update
//    Route::delete('client/ajaxDelete', 'ClientController@adminAjaxDelete')->name('client.adminAjaxDelete');    // update

    //*  Department Route Section */
    Route::get('department/list', 'DepartmentController@adminList')->name('department.adminList'); // index
    Route::get('department/add', 'DepartmentController@adminForm')->name('department.adminForm'); // create
    Route::post('department/store', 'DepartmentController@adminStore')->name('department.adminStore'); // store
    Route::get('department/{id}', 'DepartmentController@adminEdit')->name('department.adminEdit'); // edit
    Route::post('department/{id}', 'DepartmentController@adminUpdate')->name('department.adminUpdate'); // update
    Route::delete('department/{id}', 'DepartmentController@adminDestroy')->name('department.adminDestroy'); // update

    //*  Training Route Section */
    Route::get('training/list', 'TrainingController@adminList')->name('training.adminList'); // index
    Route::get('training/add', 'TrainingController@adminForm')->name('training.adminForm'); // create
    Route::post('training/store', 'TrainingController@adminStore')->name('training.adminStore'); // store
    Route::get('training/export', 'TrainingController@adminExportTraining')->name('training.adminExportTraining'); // Export Excel (xls) Data
    Route::get('training/{id}', 'TrainingController@adminEdit')->name('training.adminEdit'); // edit
    Route::post('training/{id}', 'TrainingController@adminUpdate')->name('training.adminUpdate'); // update
    Route::delete('training/{id}', 'TrainingController@adminDestroy')->name('training.adminDestroy'); // update

    //*  Registration Route Section */
    Route::get('registration/list/', 'RegistrationController@adminList')->name('registration.adminList'); // index
    Route::get('registration/list/{id}', 'RegistrationController@adminListId')->name('registration.adminListId'); // index
    Route::get('registration/add', 'RegistrationController@adminForm')->name('registration.adminForm'); // create
    Route::get('registration/card_gen', 'RegistrationController@adminCardGen')->name('registration.adminCardGen'); // Card Generation
    Route::get('registration/eng_convert', 'RegistrationController@adminEngConvert')->name('registration.adminEngConvert'); // Card Generation
    Route::get('registration/set_dob', 'RegistrationController@adminSetDob')->name('registration.adminSetDob'); // Card Generation
    Route::get('registration/registration_upazilla_pi_chart', 'RegistrationController@adminTotalRegistrationUpazillaPiChart')->name('registration.adminTotalRegistrationUpazillaPiChart'); // Pie Chart Dashboard
    Route::get('registration/family_status', 'RegistrationController@adminGetFamilyStatus')->name('registration.adminGetFamilyStatus'); // Pie Chart Dashboard
    Route::get('registration/population_status', 'RegistrationController@adminGetPopulationStatus')->name('registration.adminGetPopulationStatus'); // Pie Chart Dashboard
    Route::get('registration/trible_list', 'RegistrationController@adminGetTribleDataList')->name('registration.adminGetTribleDataList'); // Trible Data List in Dashboard
    Route::post('registration/store', 'RegistrationController@adminStore')->name('registration.adminStore'); // store
    Route::post('registration/assign_package', 'RegistrationController@adminAssignPackage')->name('registration.adminAssignPackage'); // Assign Package
    Route::post('registration/manage_package', 'RegistrationController@adminManagePackage')->name('registration.adminManagePackageN'); // Manage Package
    Route::post('registration/nid_duplicate_check', 'RegistrationController@adminNidDuplicateCheck')->name('registration.adminNidDuplicateCheck'); // Ajax NID Duplicate Check
    Route::post('registration/phone_duplicate_check', 'RegistrationController@adminPhoneDuplicateCheck')->name('registration.adminPhoneDuplicateCheck'); // Ajax Phone Duplicate Check
    Route::get('registration/export', 'RegistrationController@adminExportRegister')->name('registration.adminExportRegister'); // Export Excel (xls) Data
    Route::get('registration/print', 'RegistrationController@adminPrintCard')->name('registration.adminPrintCard'); // Print Data
    Route::get('registration/duplicate', 'RegistrationController@adminDuplicateCheck')->name('registration.adminDuplicateCheck'); // Print Data
    Route::get('registration/duplicate/list/{id}/{type}', 'RegistrationController@adminDuplicateList')->name('registration.adminDuplicateList'); // Print Data
    Route::get('registration/member_count', 'RegistrationController@adminMemberCount')->name('registration.adminMemberCount'); // Family Member COunt
    Route::get('registration/import_export', 'RegistrationController@adminImportExportExcelORCSV')->name('registration.adminImportExportExcelORCSV'); // Participant Export
    Route::post('registration/manobik_sohayota_excel_import', 'RegistrationController@adminManobikSohayotaExcelImport')->name('registration.adminManobikSohayotaExcelImport'); // Participant Export
    Route::post('registration/count_print_hit', 'RegistrationController@adminCountPrintHit')->name('registration.adminCountPrintHit'); // Participant Export
    Route::get('registration/{id}', 'RegistrationController@adminEdit')->name('registration.adminEdit'); // edit
    Route::post('registration/{id}', 'RegistrationController@adminUpdate')->name('registration.adminUpdate'); // update
    Route::delete('registration/duplicate/{id}', 'RegistrationController@adminDestroyDuplicate')->name('registration.adminDestroyDuplicate'); // Delete
    Route::delete('registration/{id}', 'RegistrationController@adminDestroy')->name('registration.adminDestroy'); // Delete
    Route::delete('registration/status/{id}/{status}', 'RegistrationController@adminRegStatusChange')->name('registration.adminRegStatusChange'); // Status Change

    //*  Participant Route Section */
    Route::get('participant/list/', 'ParticipantController@adminList')->name('participant.adminList'); // index
    Route::get('participant/list/{id}', 'ParticipantController@adminListId')->name('participant.adminListId'); // index
    Route::get('participant/add', 'ParticipantController@adminForm')->name('participant.adminForm'); // create
    Route::post('participant/store', 'ParticipantController@adminStore')->name('participant.adminStore'); // store
    Route::get('participant/{id}', 'ParticipantController@adminEdit')->name('participant.adminEdit'); // edit
    Route::post('participant/{id}', 'ParticipantController@adminUpdate')->name('participant.adminUpdate'); // update
    Route::delete('participant/{id}', 'ParticipantController@adminDestroy')->name('participant.adminDestroy'); // update
    Route::delete('participant/status/{id}/{status}', 'ParticipantController@adminRegStatusChange')->name('participant.adminRegStatusChange'); // update
    Route::post('participant/confirm/{id}', 'ParticipantController@adminRegConfirm')->name('participant.adminRegConfirm'); // Confirm Registration

    //*  Report Route Section */
    Route::get('report/registration_report/', 'ReportController@adminRegistrationReport')->name('report.adminRegistrationReport'); // Registration Report
    Route::get('report/masterroll_report/', 'ReportController@adminMasterRollReport')->name('report.adminMasterRollReport'); // Master ROll Report
    Route::get('report/bulk_registration_report/', 'ReportController@adminBulkRegistrationReport')->name('report.adminBulkRegistrationReport'); // Registration Report
    Route::post('report/export_registration_report', 'ReportController@adminExportRegistrationReport')->name('report.adminExportRegistrationReport'); // Registration Export
    Route::post('report/export_masterroll_report', 'ReportController@adminExportMasterRollReport')->name('report.adminExportMasterRollReport'); // Master Roll Report Export
    Route::post('report/export_bulk_registration_report', 'ReportController@adminExportBulkRegistrationReport')->name('report.adminExportBulkRegistrationReport'); // Registration Export
    Route::get('report/training_report/', 'ReportController@adminTrainingReport')->name('report.adminTrainingReport'); // Training Report
    Route::post('report/training_report_export', 'ReportController@adminTrainingReportExport')->name('report.adminTrainingReportExport'); // Training Export
    Route::get('report/participant_report/', 'ReportController@adminParticipantReport')->name('report.adminParticipantReport'); // Participant Report
    Route::get('report/member_search_data/', 'ReportController@adminMemberSearchData')->name('report.adminMemberSearchData'); // Member Search Data Report
    Route::get('report/relief_update/', 'ReportController@adminReliefUpdate')->name('report.adminReliefUpdate'); // Member Search Data Report
    Route::post('report/participant_report_export', 'ReportController@adminParticipantReportExport')->name('report.adminParticipantReportExport'); // Participant Export
    Route::get('report/import_package_assign', 'ReportController@adminImportPackageAssign')->name('report.adminImportPackageAssign'); // Import package assign
    Route::post('report/save_import_package_assign', 'ReportController@adminSaveImportPackageAssign')->name('report.adminSaveImportPackageAssign'); // Import package assign
    Route::get('report/import_tribal_assign', 'ReportController@adminImportTribalAssign')->name('report.adminImportTribalAssign'); // Import tribal assign
    Route::post('report/save_import_tribal_assign', 'ReportController@adminSaveImportTribalAssign')->name('report.adminSaveImportTribalAssign'); // Import Tribal assign
    Route::get('report/import_package_un_assign', 'ReportController@adminImportPackageUnAssign')->name('report.adminImportPackageUnAssign'); // Import package un-assign
    Route::post('report/save_import_package_un_assign', 'ReportController@adminSaveImportPackageUnAssign')->name('report.adminSaveImportPackageUnAssign'); // Import package un-assign
    Route::get('report/download_sample', 'ReportController@adminDownloadSample')->name('report.adminDownloadSample'); // Import package assign
    Route::get('report/my_cust_report', 'ReportController@adminMyCustReport')->name('report.adminMyCustReport'); // Custom Report

    //* Relief Router Section */
    Route::get('relief/list/', 'ParticipantController@adminList')->name('participant.adminList'); // index
    Route::get('relief/list/{id}', 'ParticipantController@adminListId')->name('participant.adminListId'); // index
    Route::get('relief/add', 'ParticipantController@adminForm')->name('participant.adminForm'); // create
    Route::post('relief/store', 'ParticipantController@adminStore')->name('participant.adminStore'); // store
    Route::get('relief/{id}', 'ParticipantController@adminEdit')->name('participant.adminEdit'); // edit
    Route::post('relief/{id}', 'ParticipantController@adminUpdate')->name('participant.adminUpdate'); // update
    Route::delete('relief/{id}', 'ParticipantController@adminDestroy')->name('participant.adminDestroy'); // update
    Route::delete('relief/status/{id}/{status}', 'ParticipantController@adminRegStatusChange')->name('participant.adminRegStatusChange'); // update
    Route::post('relief/confirm/{id}', 'ParticipantController@adminRegConfirm')->name('participant.adminRegConfirm'); // Confirm Registration

    //* Package Router Section */
    Route::get('package/list/', 'PackageController@adminList')->name('package.adminList'); // index
    Route::get('package/list/{id}', 'PackageController@adminListId')->name('package.adminListId'); // index
    Route::get('package/add', 'PackageController@adminForm')->name('package.adminForm'); // create
    Route::get('package/family_wise_list/{id}', 'PackageController@adminFamilyWiseList')->name('package.adminFamilyWiseList'); // create
    Route::post('package/save_family_wise_list', 'PackageController@adminSaveFamilyWiseList')->name('package.adminSaveFamilyWiseList'); // store
    Route::post('package/store', 'PackageController@adminStore')->name('package.adminStore'); // store
    Route::post('package/package_list_by_id', 'PackageController@adminPackageListByRegId')->name('package.adminPackageListByRegId'); // Ajax District List
    Route::get('package/{id}', 'PackageController@adminEdit')->name('package.adminEdit'); // edit
    Route::post('package/assign_package', 'PackageController@adminAssignPackage')->name('package.adminAssignPackage'); // Assign Package To a Family
    Route::post('package/{id}', 'PackageController@adminUpdate')->name('package.adminUpdate'); // update
    Route::delete('package/{id}', 'PackageController@adminDestroy')->name('package.adminDestroy'); // update
    Route::delete('package/status/{id}/{status}', 'PackageController@adminRegStatusChange')->name('package.adminRegStatusChange'); // update


    //* SNNP Router Section */
    Route::get('ssnp/list/', 'SsnpController@adminList')->name('ssnp.adminList'); // index
    Route::get('ssnp/add', 'SsnpController@adminForm')->name('ssnp.adminForm'); // create
    Route::post('ssnp/store', 'SsnpController@adminStore')->name('ssnp.adminStore'); // store
    Route::get('ssnp/{id}', 'SsnpController@adminEdit')->name('ssnp.adminEdit'); // edit
    Route::post('ssnp/{id}', 'SsnpController@adminUpdate')->name('ssnp.adminUpdate'); // update
    Route::delete('ssnp/{id}', 'SsnpController@adminDestroy')->name('ssnp.adminDestroy'); // update

    //* Role Router Section */
    Route::get('role/list/', 'RoleController@adminList')->name('role.adminList'); // index
    Route::get('role/add', 'RoleController@adminForm')->name('role.adminForm'); // create
    Route::post('role/store', 'RoleController@adminStore')->name('role.adminStore'); // store
    Route::get('role/{id}', 'RoleController@adminEdit')->name('role.adminEdit'); // edit
    Route::post('role/{id}', 'RoleController@adminUpdate')->name('role.adminUpdate'); // update
    Route::delete('role/{id}', 'RoleController@adminDestroy')->name('role.adminDestroy'); // update

    //* ACL Router Section */
    Route::get('acl/list/', 'AclController@adminList')->name('acl.adminList'); // index
//    Route::get('role/add', 'RoleController@adminForm')->name('role.adminForm');    // create
//    Route::post('role/store', 'RoleController@adminStore')->name('role.adminStore');   // store
//    Route::get('role/{id}', 'RoleController@adminEdit')->name('role.adminEdit');   // edit
//    Route::post('role/{id}', 'RoleController@adminUpdate')->name('role.adminUpdate');    // update
//    Route::delete('role/{id}', 'RoleController@adminDestroy')->name('role.adminDestroy');    // update

    //* Permission Router Section */
    Route::get('permission/list/', 'PermissionController@adminList')->name('permission.adminList'); // index
    Route::get('permission/add', 'PermissionController@adminForm')->name('permission.adminForm'); // create
    Route::post('permission/store', 'PermissionController@adminStore')->name('permission.adminStore'); // store
    Route::get('permission/{id}', 'PermissionController@adminEdit')->name('permission.adminEdit'); // edit
    Route::post('permission/{id}', 'PermissionController@adminUpdate')->name('permission.adminUpdate'); // update
    Route::delete('permission/{id}', 'PermissionController@adminDestroy')->name('permission.adminDestroy'); // update

    /*Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
    Artisan::call('view:clear');
    Artisan::call('config:cache');
    return "Cache cleared";
    }); */

});

/*  Admin Portion Routes Ends   */
/*==============================*/