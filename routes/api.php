<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

//Route::prefix('api')->group(function () {
//    Route::post('register', 'Api\AuthController@register')->name('api.register');
//    Route::post('login', 'Api\AuthController@login')->name('api.login');
//});
//
//
//Route::middleware('auth:api')->prefix('api')->group(function () {
//    Route::get('logout', 'Api\Auth\AuthController@logout')->name('api.logout');
////    Route::get('registration/list', 'DashboardController@userGetRegistration')->name('dashboard.userGetRegistration');   // index
//});
//
//Route::prefix('admin')->group(function () {
//});

//Route::middleware('auth:api')->group(function () {
//    Route::post('login', 'ApiController@login');
//});

Route::group(['middleware' => ['api']], function () {
    Route::post('login', 'Api\ApiController@login');
    Route::group(['middleware' => 'jwt.auth'], function () {
        Route::get('user', 'Api\ApiController@getAuthUser');
        Route::post('logout', 'Api\ApiController@logout');

        /*  Family Route    */
        Route::get('get_family_info', 'Api\ApiController@getFamilyInfoByAnyId')->name('family.getFamilyInfoByAnyId');   // edit
        Route::post('save_family_info', 'Api\ApiController@saveFamilyInfo')->name('family.saveFamilyInfo');   // store
    });
});